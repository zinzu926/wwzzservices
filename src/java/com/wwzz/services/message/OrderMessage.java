/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.message;

import com.wwzz.services.utils.Constants;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;
import org.springframework.jms.core.MessageCreator;

/**
 *
 * @author JBOSS
 */
public class OrderMessage implements MessageCreator,Constants{
    
    private Object messageObject=null;
    @Override
    public Message createMessage(Session session) throws JMSException {
        MapMessage msg=session.createMapMessage();
        msg.setInt("type", type);
        msg.setString("notice", this.notice);
        if(type==MESSAGE_TYPE_JRS)msg.setString("bbid",bbid);
        System.out.println("Created message:"+msg);
        if(messageObject!=null)msg.setObject("objetcs", messageObject);
        return msg;
    }
    
    private int type;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
    
    private String notice;

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }
    
    private String bbid;

    public String getBbid() {
        return bbid;
    }

    public void setBbid(String bbid) {
        this.bbid = bbid;
    }

    public Object getMessageObject() {
        return messageObject;
    }

    public void setMessageObject(Object messageObject) {
        this.messageObject = messageObject;
    }

}
