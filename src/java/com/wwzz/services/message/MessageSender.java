/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

/**
 *
 * @author JBOSS
 */
@Service
public class MessageSender {
    
    @Autowired
    @Qualifier("jmsTemplate")
    private JmsTemplate jmsTemplate;
    
    private int type;
    private String notice;
    private String bbid;
    private Object messageObject;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getNotice() {
        return notice;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }
    
    public void send(){
        OrderMessage messgae=new OrderMessage();
        messgae.setNotice(notice);
        messgae.setType(type);
        messgae.setBbid(bbid);
        messgae.setMessageObject(messageObject);
        jmsTemplate.send(messgae);
        System.out.println("Send jms message successfuly..");
    }

    public String getBbid() {
        return bbid;
    }

    public void setBbid(String bbid) {
        this.bbid = bbid;
    }

    public Object getMessageObject() {
        return messageObject;
    }

    public void setMessageObject(Object messageObject) {
        this.messageObject = messageObject;
    }
    
    
    
}
