/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.client;

import com.wwzz.services.api.apps.AppServices;
import com.wwzz.services.api.pingtai.UserServices;
import com.wwzz.services.domain.admin.TAuthority;
import com.wwzz.services.domain.admin.TRole;
import com.wwzz.services.domain.admin.TSystemProperty;
import com.wwzz.services.domain.apps.TAppsRegister;
import com.wwzz.services.model.SystemProperty;
import com.wwzz.services.utils.Blowfish;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author JBoss
 */
@Service
public class ApplicationClient {
    @Autowired
    private AppServices services;
//    @Autowired
//    private RedisServices redisService;
    @Autowired
    private UserServices userService;
    
    public void load(){
        List data=new ArrayList();
        List lst=services.view_app_register();
        for (int i = 0; i < lst.size(); i++) {
            TAppsRegister ap = (TAppsRegister) lst.get(i);
            Map m=new HashMap();
            m.put("appKey", ap.getCertKeys());
            m.put("status", ap.getStatus());
            m.put("appId", ap.getAppsId());
            m.put("server", ap.getServer());
            m.put("type", ap.getType());
            data.add(m);
        }
        //redisService.add_appinfo(data);
        this.appInfos=data;
        load_role();
        load_system();
    }
    
    private List appInfos;

    public List getAppInfos() {
        return appInfos;
    }
    private List<SystemProperty> systemInfo;
    
    public String getAppKey(String app){
        if(this.appInfos==null)return null;
        for (int i = 0; i < appInfos.size(); i++) {
            Map mp = (Map) appInfos.get(i);
            String appid=(String) mp.get("appId");
            if(app.equals(appid)){
                return (String) mp.get("appKey");
            }
        }
        return null;
    }
    
    public String set_uid(String client,String id){
        Blowfish bw=new Blowfish();
        bw.setKey(getAppKey(client));
        return bw.encryptString(id);
    }
    
    public String get_uid(String client,String id){
        Blowfish bw=new Blowfish();
        bw.setKey(getAppKey(client));
        return bw.decryptString(id);
    }
    
    public boolean isWeixin(String app){
        if(this.appInfos!=null){
            for (Object appInfo : appInfos) {
                Map mp = (Map) appInfo;
                String appid=(String) mp.get("appId");
                String type=(String) mp.get("type");
                if(app.equals(appid) && type.equals("weixin")){
                    return true;
                }
            }
        }
        return false;
    }
    
    public String getType(String app){
        if(this.appInfos!=null){
            for (Object appInfo : appInfos) {
                Map mp = (Map) appInfo;
                String appid=(String) mp.get("appId");
                String type=(String) mp.get("type");
                if(app.equals(appid)){
                    return type;
                }
            }
        }
        return null;
    }
    
    public void load_role(){
        List<TRole> list=userService.find_role_all();
        roles=new ArrayList();
        for(TRole role:list){
            List<TAuthority> auths=role.getAuthorities();
            for(TAuthority auth:auths){
                Map m=new HashMap();
                m.put("role", role.getName());
                if(auth.getLevel()==0 && auth.getSort()==1)m.put("url", "/manager");
                else m.put("url", "/manager/"+auth.getMark());
                m.put("method", auth.getChain());
                roles.add(m);
            }
        }
    }
    
    public void load_system(){
        List lst=services.load_system_property();
        systemInfo=new ArrayList();
        for (int i = 0; i < lst.size(); i++) {
            TSystemProperty sys = (TSystemProperty) lst.get(i);
            SystemProperty sp=new SystemProperty(sys.getKey(), sys.getValue());
            systemInfo.add(sp);
        }
    }
    private List roles=new ArrayList();

    public List getRoles() {
        return roles;
    }
    
    public String getSystemProperty(String key){
        if(systemInfo==null)return null;
        for (SystemProperty sp : systemInfo) {
            if(sp.getKey().equals(key))return sp.getValue();
        }
        return null;
    }
    
    public void addSystemProperty(String key,String value){
        if(systemInfo==null)systemInfo=new ArrayList();
        SystemProperty sp=new SystemProperty(key, value);
        if(!systemInfo.contains(sp))systemInfo.add(sp);
    }
    
    public String get_topic_info(String client,String topic){
        Blowfish bw=new Blowfish();
        bw.setKey(getAppKey(client));
        StringBuilder sb=new StringBuilder();
        sb.append(getSystemProperty("notification.server")).append("/").append(getSystemProperty("notification.server.user")).append("/").
                append(getSystemProperty("notification.server.key")).append("/").append(topic);
        return bw.encryptString(sb.toString());
    }
    
    public String getUrl(String urlProperty){
        String api=getSystemProperty("api.server");
        if(api==null)return null;
        String ul=getSystemProperty(urlProperty);
        if(ul==null)return null;
        String url=StringUtils.replace(ul, "{api.server}", api);
        return url;
    }
    
    public String getUrlByFile(String urlProperty){
        String api=getSystemProperty("file.server");
        if(api==null)return null;
        String ul=getSystemProperty(urlProperty);
        if(ul==null)return null;
        String url=StringUtils.replace(ul, "{file.server}", api);
        return url;
    }
    
    public Map getFileServerConf(){
        Map m=new HashMap();
        m.put("connect_timeout", getSystemProperty("file.server.connect_timeout"));
        m.put("network_timeout", getSystemProperty("file.server.network_timeout"));
        m.put("charset", getSystemProperty("file.server.charset"));
        m.put("http.tracker_http_port", getSystemProperty("file.server.http.tracker_http_port"));
        m.put("http.anti_steal_token", getSystemProperty("file.server.http.anti_steal_token"));
        m.put("tracker_server", getUrlByFile("file.server.tracker_server"));
        return m;
    }
}
