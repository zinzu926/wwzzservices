/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.listener;

import com.wwzz.services.client.ApplicationClient;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Web application lifecycle listener.
 *
 * @author JBoss
 */
public class ContextEventListener implements ServletContextAttributeListener {
    private static final Logger logger=LoggerFactory.getLogger(ContextEventListener.class);
    @Override
    public void attributeAdded(ServletContextAttributeEvent event) {
        if(event.getName().equals(WWZZSERVICES_SERVLET_CONTEXT_ATTR_NAME)){
           WebApplicationContext ctx=getDispatcherServletWebApplicationContext(event.getServletContext());
           ApplicationClient apps=ctx.getBean(ApplicationClient.class);
           apps.load();
        }
    }

    @Override
    public void attributeRemoved(ServletContextAttributeEvent event) {
        
    }

    @Override
    public void attributeReplaced(ServletContextAttributeEvent arg0) {
        
    }
    
    private WebApplicationContext getDispatcherServletWebApplicationContext(ServletContext stx){
        return WebApplicationContextUtils.getWebApplicationContext(stx,WWZZSERVICES_SERVLET_CONTEXT_ATTR_NAME);
    }
    
    private static final String WWZZSERVICES_SERVLET_CONTEXT_ATTR_NAME =
            "org.springframework.web.servlet.FrameworkServlet.CONTEXT.wwzzservices";
}
