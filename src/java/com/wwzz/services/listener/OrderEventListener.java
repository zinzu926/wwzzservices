/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.listener;

import com.wwzz.services.api.UserNoticeServices;
import com.wwzz.services.utils.Constants;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Service;

/**
 *
 * @author JBOSS
 */
@Service
public class OrderEventListener implements ApplicationListener<OrderEvent>,Constants{
   
    private static final Logger logger = LoggerFactory.getLogger(OrderEventListener.class);
    @Override
    public void onApplicationEvent(OrderEvent e) {
        int type=e.getEventType();
        if(type==SYSTEM_NOTICE_CATEGORY_1){
            UserNoticeServices services=(UserNoticeServices) e.getSource();
            services.cancel_booking(e.getTarget());
        }
        if(type==BOOKING_NOTICE){
            UserNoticeServices services=(UserNoticeServices) e.getSource();
            services.booking_notice(e.getTarget());
        }
        if(type==BID_NOTICE){
            UserNoticeServices services=(UserNoticeServices) e.getSource();
            services.bid_notice(e.getTarget());
        }
        if(type==MESSAGE_TYPE_BOOKING_PAY_STATE){
            UserNoticeServices services=(UserNoticeServices) e.getSource();
            Map m=(Map) e.getTarget();
            Integer st=Integer.parseInt(m.get("state").toString());
            services.notice_booking_ready_pay_state(m.get("booking"),st);
        }
    }
    
}
