/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.listener;

import com.wwzz.services.utils.Constants;
import org.springframework.context.ApplicationEvent;

/**
 *
 * @author JBOSS
 */
public class OrderEvent extends ApplicationEvent implements Constants{
    private final Object target;
    private final int eventType;

    public OrderEvent(Object source,int eventType,Object target) {
        super(source);
        this.target=target;
        this.eventType=eventType;
    }

    public Object getTarget() {
        return target;
    }

    public int getEventType() {
        return eventType;
    }

    
    
}
