/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.model;

/**
 *
 * @author JBOSS
 */
public class ChatItem implements Comparable<ChatItem>{
    private String uname;
    private String jid;
    private String curl;
    private String message;
    private long date;
    private String count;

    public ChatItem() {
    }
    
    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getJid() {
        return jid;
    }

    public void setJid(String jid) {
        this.jid = jid;
    }

    public String getCurl() {
        return curl;
    }

    public void setCurl(String curl) {
        this.curl = curl;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof ChatItem))return false;
        ChatItem other=(ChatItem) obj;
        return this.date==other.getDate();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + (int) (this.date ^ (this.date >>> 32));
        return hash;
    }

    @Override
    public int compareTo(ChatItem o) {
        if(this.date==o.getDate())return 0;
        else{
            if(this.date>o.getDate())return 1;
            else return -1;
        }
    }
    
}
