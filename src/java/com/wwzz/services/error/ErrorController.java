/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.error;

import com.wwzz.services.utils.Constants;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

/**
 *
 * @author JBoss
 */
@Service
public class ErrorController implements Constants{
    @Autowired
    private MessageSource resource;
    
    private final Locale locale;
    
    public ErrorController() {
        locale=new Locale("zh");
    }
    
    public Map createResultMap(int rcode,String rmsg,int error,String errmsg,String... values){
        Map m=new HashMap();
        m.put(RETURN_CODE, rcode==RETURN_CODE_OK?RETURN_SUCCESS_CODE:RETURN_ERROR_CODE);
        m.put(RETURN_MESSAGE, rcode==RETURN_CODE_OK?"OK":resource.getMessage(rmsg, values, locale));
        m.put(ERROR_CODE, error);
        m.put(ERROR_CODE_DESC, error==ERROR_CODE_OK?"--":resource.getMessage(errmsg, values, locale));
        return m;
    }
}
