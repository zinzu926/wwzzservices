/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.impl;

import com.wwzz.services.api.ChatManager;
import com.wwzz.services.domain.users.TUser;
import com.wwzz.services.domain.users.TUserChatMessage;
import com.wwzz.services.model.ChatItem;
import com.wwzz.services.repository.TUserChatMessageRepository;
import com.wwzz.services.repository.TUserRepository;
import com.wwzz.services.utils.Constants;
import com.wwzz.services.utils.Utils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.StringUtils;
import org.igniterealtime.restclient.RestApiClient;
import org.igniterealtime.restclient.entity.AuthenticationToken;
import org.igniterealtime.restclient.entity.RosterEntities;
import org.igniterealtime.restclient.entity.RosterItemEntity;
import org.igniterealtime.restclient.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author JBOSS
 */
@Service
@Transactional
public class ChatServices extends ResultDataServices implements ChatManager,Constants{
    
    @Autowired
    private TUserRepository userRepo;
    @Autowired
    private TUserChatMessageRepository chatRepo;
    
    private RestApiClient restApiClient;
    
    @Override
    public String add_chat_user(String user, String pass,String name) {
        init_rest_client();
        UserEntity ue=get_chat_user(user);
        if(ue!=null){
            return ue.getUsername()+"@"+clientApp.getSystemProperty("xmpp.server");
        }else ue=new UserEntity();
        ue.setPassword(pass);
        ue.setUsername(user);
        ue.setName(name);
        boolean b=restApiClient.createUser(ue);
        if(b){
            String s=ue.getUsername()+"@"+clientApp.getSystemProperty("xmpp.server");
            return s;
        }else{
            return null;
        }
    }
    
    private void init_rest_client() {
        AuthenticationToken authenticationToken = new AuthenticationToken(clientApp.getSystemProperty("xmpp.rest.shared.key"));
        restApiClient = new RestApiClient(clientApp.getSystemProperty("xmpp.server"), Integer.parseInt(clientApp.getSystemProperty("xmpp.server.port")), authenticationToken);
    }

    @Override
    public Map add_user_roster(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String jid=(String) params.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(jid))return raiseError(1032);
        TUser target=userRepo.findByChatid(jid);
        if(target==null)return raiseError(1032);
        boolean b=add_user_roster(user,target);
        if(b)rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        else return raiseError(1139);
        return rm;
    }

    @Override
    public Map get_user_rost(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        init_rest_client();
        List data=new ArrayList();
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        if(hasText(user.getChatid())){
            String chat=StringUtils.remove(user.getMobile(), "-");
            rm.put("chatid", user.getChatid());
            RosterEntities re=restApiClient.getRoster(chat);
            if(re!=null){
                List<RosterItemEntity> lst=re.getRoster();
                if(lst!=null){
                    if(!lst.isEmpty()){
                        String[] jids=new String[lst.size()];
                        for (int i=0;i<lst.size();i++) {
                            RosterItemEntity ree=lst.get(i);
                            jids[i]=ree.getJid();
                        }
                        List<TUser> users=userRepo.findByChatidIsIn(jids);
                        List<ChatItem> datas=new ArrayList<>();
                        for (TUser usr : users) {
                            ChatItem ct=new ChatItem();
                            if(usr.getName()!=null && !usr.getName().equals("--"))ct.setUname(usr.getName());
                            else ct.setUname(StringUtils.remove(usr.getMobile(),"-"));
                            ct.setJid(usr.getChatid());
                            ct.setCurl(Utils.valueOf(usr.getHeadUrl()));
                            Map mm=get_last_message(user.getChatid(),usr.getChatid());
                            if(mm!=null){
                                if(mm.containsKey("msg"))ct.setMessage(mm.get("msg").toString());
                                else ct.setMessage(" ");
                                ct.setDate((Long)mm.get("date"));
                            }else{
                                ct.setMessage("");ct.setDate(0);
                            } 
                            Long cnt=count_chat_unread_message(usr.getChatid(),user.getChatid());
                            ct.setCount(Long.toString(cnt));
                            datas.add(ct);
                        }
                        Collections.sort(datas);
                        Collections.reverse(datas);
                        for(ChatItem ch:datas){
                            Map m=new HashMap();
                            m.put("uname", ch.getUname());
                            m.put("curl", ch.getCurl());
                            m.put("jid", ch.getJid());
                            m.put("intro", ch.getMessage());
                            m.put("count", ch.getCount());
                            data.add(m);
                        }
                    }
                }
            }
        }else rm.put("chatid", "--");
        rm.put("sssss", user.getDecryptPassword());
        rm.put(DATA_KEY, data);
        return rm;
    }
    private boolean rosterExist(String user,String target){
        init_rest_client();
        RosterEntities re=restApiClient.getRoster(user);
        if(re!=null){
            List<RosterItemEntity> lst=re.getRoster();
            if(lst==null)return false;
            for (RosterItemEntity ree : lst) {
                if(ree.getJid().equals(target))return true;
            }
        }
        return false;
    }
    private boolean add_user_roster(TUser user, TUser target) {
        init_rest_client();
        boolean b=true;
        String chatid;
        if(!hasText(user.getChatid())){
            chatid=StringUtils.remove(user.getMobile(),"-");
            String cc=add_chat_user(chatid, user.getDecryptPassword(), Utils.valueOf(user.getName()));
            if(cc==null)return false;
            user.setChatid(cc);
            userRepo.save(user);
        }
        if(!hasText(target.getChatid())){
            chatid=StringUtils.remove(target.getMobile(),"-");
            String cc=add_chat_user(chatid, target.getDecryptPassword(), Utils.valueOf(target.getName()));
            if(cc==null)return false;
            target.setChatid(cc);
            userRepo.save(target);
        }
        RosterItemEntity roster=new RosterItemEntity();
        chatid=StringUtils.substringBeforeLast(user.getChatid(), "@");
        System.out.println("chatid:"+chatid);
        if(!rosterExist(chatid, target.getChatid())){
            roster.setJid(target.getChatid());
            roster.setSubscriptionType(3);
            roster.setNickname(target.getName());
            b=restApiClient.addRosterEntry(chatid, roster);
            if(!b)return b;
        }
        chatid=StringUtils.substringBeforeLast(target.getChatid(), "@");
        if(!rosterExist(chatid, user.getChatid())){
            roster=new RosterItemEntity();
            roster.setJid(user.getChatid());
            roster.setSubscriptionType(3);
            roster.setNickname(user.getName());
            b=restApiClient.addRosterEntry(chatid, roster);
            if(!b)return b;
        }
        return b;
    }
     
    private UserEntity get_chat_user(String user){
        UserEntity ue=restApiClient.getUser(user);
        return ue;
    }

    @Override
    public Map add_chat_message(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String pt=(String) params.get("partner");
        if(!hasText(pt))return raiseError(1032);
        String type=clientApp.getType(pt);
        String from=(String) params.get("from");
        String to=(String) params.get("to");
        String content=(String) params.get("content");
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        if(hasText(from) && hasText(to)){
            TUser from_user=userRepo.findByChatid(from);
            TUser to_user=userRepo.findByChatid(to);
            if(from_user!=null && to_user!=null){
                TUserChatMessage msg=new TUserChatMessage();
                Date time=new Date();
                msg.setCreated(time);
                msg.setFromUser(from_user);
                msg.setToUser(to_user);
                msg.setFromType(type);
                msg.setContents(content==null?"":content);
                msg.setStatus(0);
                msg.setChatId(UUID.randomUUID().toString());
                chatRepo.save(msg);
                rm.put("chatid", msg.getChatId());
            }
        }
        return rm;
    }

    @Override
    public Map view_chat_history(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String target=(String) params.get("target");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(target))return raiseError(1032);
        TUser to=userRepo.findByChatid(target);
        if(to==null)return raiseError(1032);
        Integer start=(Integer) params.get("start");
        Integer count=(Integer) params.get("count");
        if(start==null)start=1;
        if(count==null)count=10;
        else if(count>10)count=10;
        Pageable pg=new PageRequest(start, count);
        Page<TUserChatMessage> msgPg=chatRepo.search_chat_message(user.getChatid(), to.getChatid(), pg);
        List<TUserChatMessage> msgList=msgPg.getContent();
        long total=msgPg.getTotalElements();
        List data=new ArrayList();
        String cc=new String(new char[]{0x266B});
        for (TUserChatMessage msg : msgList) {
            Map mm=new HashMap();
            mm.put("time", Utils.dateToString(msg.getCreated()));
            mm.put("msg", msg.getContents());
            mm.put("from", msg.getFromUser().getChatid());
            mm.put("to", msg.getToUser().getChatid());
            
            data.add(mm);
            if(msg.getStatus()==0 && !msg.getContents().startsWith(cc)){
                msg.setStatus(1);
                chatRepo.save(msg);
            }
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        rm.put("total", Long.toString(total));
        return rm;
    }
    
    private Map get_last_message(String from,String to){
        String sql="select p from TUserChatMessage p where (p.fromUser.chatid=:from and p.toUser.chatid=:to) or" +
            " (p.fromUser.chatid=:to and p.toUser.chatid=:from) order by p.created desc";
        Map m=new HashMap();
        try{
           TUserChatMessage msg=(TUserChatMessage) em.createQuery(sql).setParameter("from", from).setParameter("to", to).
                setMaxResults(1).setFirstResult(0).getSingleResult();
           String intro=msg.getContents();
            if(hasText(intro)){
                if(intro.length()<=20)m.put("msg", intro);
                else m.put("msg", intro.substring(0, 20));
            }
            m.put("date", msg.getCreated().getTime());
        }catch(NoResultException ex){
            return null;
        }
        return m;
    }
    
    private Long count_chat_unread_message(String from,String to){
        String sql="select count(p) from TUserChatMessage p where (p.fromUser.chatid=:from and p.toUser.chatid=:to) " +
            " and p.status=0";
        try{
           Long count=(Long) em.createQuery(sql).setParameter("from", from).setParameter("to", to).
                getSingleResult();
           return count;
        }catch(NoResultException ex){
            return 0L;
        }
    }
    
    @PersistenceContext
    private EntityManager em;

    @Override
    public void update_chat_state(Map param) {
        String cid=(String) param.get("chatid");
        Integer st=(Integer) param.get("state");
        if(hasText(cid) || st!=null){
            TUserChatMessage msg=chatRepo.findByChatId(cid);
            if(msg!=null){
                msg.setStatus(st);
                chatRepo.save(msg);
            }
        }
    }
}
