/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.impl;

import com.wwzz.services.api.DataViewServices;
import com.wwzz.services.api.MasterDataServices;
import com.wwzz.services.domain.base.TArea;
import com.wwzz.services.domain.base.TCityRestaurantProperty;
import com.wwzz.services.domain.base.TCityScenicSpotsProperty;
import com.wwzz.services.domain.base.TTourismLineCategory;
import com.wwzz.services.domain.booking.TUserBookingOrder;
import com.wwzz.services.domain.booking.TUserBookingTempOrder;
import com.wwzz.services.domain.booking.TUserOrderDocents;
import com.wwzz.services.domain.booking.TUserOrderDriver;
import com.wwzz.services.domain.booking.TUserOrderLocalGuide;
import com.wwzz.services.domain.booking.TUserOrderLongGuide;
import com.wwzz.services.domain.booking.TUserOrderOGuide;
import com.wwzz.services.domain.booking.TUserOrderProcessCharge;
import com.wwzz.services.domain.booking.TUserOrderRestaurant;
import com.wwzz.services.domain.booking.TUserOrderShop;
import com.wwzz.services.domain.booking.TUserOrderShopFiles;
import com.wwzz.services.domain.booking.TUserOrderTicket;
import com.wwzz.services.domain.journey.TUserJourney;
import com.wwzz.services.domain.journey.TUserJourneyCarInfo;
import com.wwzz.services.domain.journey.TUserJourneyDaily;
import com.wwzz.services.domain.journey.TUserJourneyHotelInfo;
import com.wwzz.services.domain.users.TUser;
import com.wwzz.services.domain.users.TUserBill;
import com.wwzz.services.domain.users.TUserDriverExpandedTemp;
import com.wwzz.services.domain.users.TUserDriverTemp;
import com.wwzz.services.domain.users.TUserIdentity;
import com.wwzz.services.domain.users.TUserIdentityDocents;
import com.wwzz.services.domain.users.TUserIdentityDocentsExpanded;
import com.wwzz.services.domain.users.TUserIdentityDriver;
import com.wwzz.services.domain.users.TUserIdentityDriverExpanded;
import com.wwzz.services.domain.users.TUserIdentityLCGuide;
import com.wwzz.services.domain.users.TUserIdentityLCGuideExpanded;
import com.wwzz.services.domain.users.TUserIdentityLongGuide;
import com.wwzz.services.domain.users.TUserIdentityLongGuideExpanded;
import com.wwzz.services.domain.users.TUserIdentityOGuide;
import com.wwzz.services.domain.users.TUserIdentityOGuideExpanded;
import com.wwzz.services.domain.users.TUserPhotos;
import com.wwzz.services.model.BookingItem;
import com.wwzz.services.repository.TUserBillRepository;
import com.wwzz.services.repository.TUserBookingOrderRepository;
import com.wwzz.services.repository.TUserBookingTempOrderRepository;
import com.wwzz.services.repository.TUserDriverTempRepository;
import com.wwzz.services.repository.TUserIdentityDocentsRepository;
import com.wwzz.services.repository.TUserIdentityDriverRepository;
import com.wwzz.services.repository.TUserIdentityLocalGuideRepository;
import com.wwzz.services.repository.TUserIdentityLongGuideRepository;
import com.wwzz.services.repository.TUserIdentityOGuideRepository;
import com.wwzz.services.repository.TUserIdentityRepository;
import com.wwzz.services.repository.TUserJourneyCarInfoRepository;
import com.wwzz.services.repository.TUserJourneyDailyRepository;
import com.wwzz.services.repository.TUserJourneyHotelInfoRepository;
import com.wwzz.services.repository.TUserJourneyRepository;
import com.wwzz.services.repository.TUserOrderDocentsRepository;
import com.wwzz.services.repository.TUserOrderDriverRepository;
import com.wwzz.services.repository.TUserOrderLocalGuideRepository;
import com.wwzz.services.repository.TUserOrderLongGuideRepository;
import com.wwzz.services.repository.TUserOrderOGuideRepository;
import com.wwzz.services.repository.TUserOrderProcessRepository;
import com.wwzz.services.repository.TUserOrderRestaurantRepository;
import com.wwzz.services.repository.TUserOrderShopFilesRepository;
import com.wwzz.services.repository.TUserOrderShopRepository;
import com.wwzz.services.repository.TUserOrderTicketRepository;
import com.wwzz.services.repository.TUserPhotosRepository;
import com.wwzz.services.repository.TUserRepository;
import static com.wwzz.services.utils.Constants.BOOKING_CATEGORY_DOCENTS;
import static com.wwzz.services.utils.Constants.BOOKING_CATEGORY_DRV_1;
import static com.wwzz.services.utils.Constants.BOOKING_CATEGORY_DRV_2;
import static com.wwzz.services.utils.Constants.BOOKING_CATEGORY_LC;
import static com.wwzz.services.utils.Constants.BOOKING_CATEGORY_LG;
import static com.wwzz.services.utils.Constants.BOOKING_CATEGORY_OG;
import static com.wwzz.services.utils.Constants.ERROR_CODE_OK;
import static com.wwzz.services.utils.Constants.RETURN_ERROR_CODE;
import static com.wwzz.services.utils.Constants.RETURN_SUCCESS_CODE;
import com.wwzz.services.utils.Utils;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.oxm.castor.CastorMappingException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author JBOSS
 */
@Service
@Transactional
public class DataViewServicesImpl extends ResultDataServices implements DataViewServices{
    @Autowired
    private TUserBookingOrderRepository bookingRepo;
    @Autowired
    private TUserRepository userRepo;
    @Autowired
    private TUserOrderDocentsRepository orderDcRepo;
    @Autowired
    private VelocityEngine velocityEngine;
    @Autowired
    private TUserOrderShopRepository orderShRepo;
    @Autowired
    private TUserOrderRestaurantRepository orderResRepo;
    @Autowired
    private TUserOrderTicketRepository orderTkRepo;
    @Autowired
    private TUserBookingTempOrderRepository orderTempRepo;
    @Autowired
    private TUserIdentityDocentsRepository userDcRepo;
    @Autowired
    private TUserIdentityDriverRepository userDrvRepo;
    @Autowired
    private TUserIdentityLocalGuideRepository userLcRepo;
    @Autowired
    private TUserIdentityLongGuideRepository userLgRepo;
    @Autowired
    protected TUserPhotosRepository userPhRepo;
    @Autowired
    private TUserJourneyRepository userJRepo;
    @Autowired
    private TUserJourneyCarInfoRepository userJCar;
    @Autowired
    private TUserJourneyDailyRepository userJD;
    @Autowired
    private TUserOrderShopFilesRepository orderSfRepo;
    @Autowired
    private TUserOrderDriverRepository orderDrRepo;
    @Autowired
    private TUserOrderLongGuideRepository orderLgRepo;
    @Autowired
    private TUserOrderOGuideRepository orderOgRepo;
    @Autowired
    private TUserOrderLocalGuideRepository orderLcRepo;
    @Autowired
    private TUserOrderProcessRepository pcRepo;
    @Autowired
    private TUserBookingTempOrderRepository tempRepo;
    @Autowired
    private TUserJourneyHotelInfoRepository jorHRepo;
    @Autowired
    private TUserIdentityRepository  userIdRepo;
    @Autowired
    private MasterDataServices services;
    @Autowired
    private TUserDriverTempRepository  userDrvTenp;
    @Autowired
    private TUserBillRepository billRepo;
    @Autowired
    private TUserIdentityOGuideRepository userOgRepo;
    @Override
    public Map view_my_booking_list(Map params) {
        Map m=auth_apps(params, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        Integer cat=(Integer) params.get("cat");
        Integer st=(Integer) params.get("status");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(cat==null)cat=0;
        if(st==null)st=0;
        Integer start=(Integer) params.get("start");
        Integer count=(Integer) params.get("count");
        if(start==null)start=1;
        if(count==null)count=20;
        else if(count>20)count=20;
        Pageable pg=new PageRequest(start, count);
        Page<TUserBookingOrder> pglst;
        if(st==0 && cat==0)pglst=bookingRepo.findByApplicantAndStatusIsLessThanEqualOrderByApplicationTimeDesc(user, 6, pg);
        else if(cat==0){
            pglst=bookingRepo.findByApplicantAndStatusOrderByApplicationTimeDesc(user, st,pg);
        }else if(st==0){
            pglst=bookingRepo.findByApplicantAndCategoryOrderByApplicationTimeDesc(user, cat,pg);
        }else pglst=bookingRepo.findByApplicantAndStatusAndCategoryOrderByApplicationTimeDesc(user, st,cat,pg);
        List<TUserBookingOrder> lst=pglst.getContent();
        List data=new ArrayList();
        if(lst!=null){
            for (TUserBookingOrder tb : lst) {
                Map m1=new HashMap();
                m1.put("cattitle", BOOKING_CATEGORIES[tb.getCategory()-1]);
                m1.put("id", tb.getId().toString());
                m1.put("title", tb.getTitle());
                m1.put("time",Utils.dateToString1(tb.getApplicationTime()));
                m1.put("stn", BOOKING_STATES[tb.getStatus()-1]);
                m1.put("cancel", tb.getStatus()<=BOOKING_STATE_2?1:0);
                m1.put("curl", getResource("booking.img.prefix", Integer.toString(tb.getCategory())));
                switch(tb.getCategory()){
                    case BOOKING_CATEGORY_SHOP:
                    case BOOKING_CATEGORY_RESTURANT:
                    case BOOKING_CATEGORY_SS_PIAO:
                        m1.put("cat", 100);break;
                    case BOOKING_CATEGORY_DRV_1:    
                    case BOOKING_CATEGORY_DRV_2:
                    case BOOKING_CATEGORY_LG:
                    case BOOKING_CATEGORY_LC:
                    case BOOKING_CATEGORY_OG:
                        switch(tb.getStatus()){
                            case BOOKING_STATE_6:
                                m1.put("cat", 400);break;
                            case BOOKING_STATE_7:
                            case BOOKING_STATE_8:
                                m1.put("cat", 200);break;
                            default:
                                if(tb.getStatus()==BOOKING_STATE_1)m1.put("cat", 200);
                                else m1.put("cat", 400);break;
                        }
                        break;
                    case BOOKING_CATEGORY_DOCENTS:
                        TUserOrderDocents to= orderDcRepo.findByBooking(tb);
                        if(to.getType()==1){
                            switch(tb.getStatus()){
                                case BOOKING_STATE_6:
                                    m1.put("cat", 400);break;
                                case BOOKING_STATE_7:
                                case BOOKING_STATE_8:
                                    m1.put("cat", 200);break;
                                default:
                                    if(tb.getStatus()==BOOKING_STATE_1)m1.put("cat", 200);
                                    else m1.put("cat", 400);break;
                            }
                        }else{
                            m1.put("cat", 300);
                        } 
                        break;
                    case BOOKING_CATEGORY_PP:
                        m1.put("cat", 300);
                    default:
                        m1.put("cat", 300);break;
                }
                data.add(m1);
            }
        }
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        rm.put("count", pglst.getTotalElements());
        return rm;
    }

    @Override
    public Map view_my_booking_data_cat1(Map params) {
        Map m=auth_apps(params, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String id=(String) params.get("bbid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(!hasText(id)){
            return raiseError(1032);
        }
        long idd;
        try{
            idd=Long.parseLong(id);
        }catch(NumberFormatException ex){
            return raiseError(1032);
        }
        TUserBookingOrder bo=bookingRepo.findOne(Long.parseLong(id));
        if(bo==null)return raiseError(1032);
        int cat=bo.getCategory();
        Map data=new HashMap();Map m1=new HashMap();
        data.put("bbid", bo.getId().toString());
        switch(cat){
            case BOOKING_CATEGORY_SHOP:
                TUserOrderShop ts=orderShRepo.findByBooking(bo);
                data.put("image", ts.getShop().getImageUrl());
                data.put("name", valueOf(ts.getShop().getNameZh()));
                if(ts.getShop().getProperties()!=null)
                data.put("intro",valueOf(ts.getShop().getProperties().getIntroductionZh()));
                else data.put("intro","--");
                m1.put("phone", valueOf(ts.getShop().getPhone()));
                data.put("phone", valueOf(ts.getShop().getPhone()));
                if(ts.getShop().getProperties()!=null){
                    m1.put("addr", valueOf(ts.getShop().getProperties().getAddressZh()));
                    m1.put("site", valueOf(ts.getShop().getProperties().getWebsite()));
                }else {
                    m1.put("addr", "--");m1.put("site", "--");
                }
                data.put("detail", getDetailInfo("shop", m1));
                data.put("pay", 0);
                break;
            case BOOKING_CATEGORY_RESTURANT:
                TUserOrderRestaurant rs=orderResRepo.findByBooking(bo);
                TCityRestaurantProperty pr=rs.getRestaurant().getProperties();
                data.put("image", rs.getRestaurant().getImageUrl());
                data.put("name", valueOf(rs.getRestaurant().getNameZh()));
                data.put("phone", valueOf(rs.getRestaurant().getPhone()));
                m1.put("phone", valueOf(rs.getRestaurant().getPhone()));
                if(pr!=null){
                    data.put("intro",valueOf(pr.getIntroductionZh()));
                    m1.put("addr", valueOf(pr.getAddressZh()));
                    m1.put("site", valueOf(pr.getWebsite()));
                    m1.put("aa", valueOf(pr.getConsumptionPerPerson()));
                }else {
                    m1.put("addr", "--");m1.put("site", "--");
                    m1.put("aa", "--");
                    data.put("intro","--");
                }
                m1.put("price", bo.getOrderAmount());
                m1.put("state", BOOKING_STATES[bo.getStatus()-1]);
                data.put("detail", getDetailInfo("rest", m1));
                if(bo.getStatus()>=BOOKING_STATE_6){
                    data.put("pay", 0);
                }else{
                    data.put("pay", bo.getStatus()==BOOKING_STATE_2?1:0);
                }
                break;
            case BOOKING_CATEGORY_SS_PIAO:
                TUserOrderTicket tt=orderTkRepo.findByBooking(bo);
                TCityScenicSpotsProperty sr=tt.getScenic().getProperties();
                data.put("image", tt.getScenic().getImageUrl());
                data.put("name", valueOf(tt.getScenic().getNameZh()));
                if(sr!=null){
                    data.put("intro",valueOf(sr.getIntroductionZh()));
                    data.put("phone", valueOf(sr.getPhone()));
                    m1.put("phone", valueOf(sr.getPhone()));
                    m1.put("addr", valueOf(sr.getLocationZh()));
                    m1.put("site", valueOf(sr.getWebsite()));
                    m1.put("time", valueOf(sr.getOpenTime()));
                }else {
                    m1.put("addr", "--");m1.put("site", "--");
                    m1.put("phone", "--");m1.put("time", "--");
                    data.put("intro","--");
                    data.put("phone", "--");
                }
                m1.put("price", bo.getOrderAmount());
                m1.put("state", BOOKING_STATES[bo.getStatus()-1]);
                data.put("detail", getDetailInfo("menpiao", m1));
                if(bo.getStatus()>=BOOKING_STATE_6){
                    data.put("pay", 0);
                }else{
                    data.put("pay", bo.getStatus()==BOOKING_STATE_2?1:0);
                }
                break;
        }
        
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        return rm;
    }
    private String valueOf(String s){
        if(!hasText(s))return "--";
        else return s;
    }
    private String getDetailInfo(String vm, Map data){
        String ss="resources/"+vm+".vm";
        String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, ss,"UTF-8", data);
        return text;
    }

    @Override
    public Map view_my_bids_list(Map params) {
        Map m=auth_apps(params, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String id=(String) params.get("bbid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(!hasText(id) || !NumberUtils.isNumber(id)){
            return raiseError(1032);
        }
        TUserBookingOrder bo=bookingRepo.findOne(Long.parseLong(id));
        if(bo==null)return raiseError(1032);
        List<TUserBookingTempOrder> lst=orderTempRepo.findByBooking(bo);
        List data=new ArrayList();
        if(lst!=null){
            int work = 0;
            for (TUserBookingTempOrder to : lst) {
                data.add(to_bidders_info(to));
            }
        }
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        rm.put("money", Utils.priceToString(bo.getOrderAmount()));
        rm.put("cnt", "--");
        rm.put("status", BOOKING_STATES[bo.getStatus()-1]);
        rm.put("cat", bo.getCategory());
        rm.put("bid", bo.getId().toString());
        return rm;
    }
    
    private Map getWorkYear(Object oo){
        TUser user=null;int st=0;
        if(oo instanceof TUserBookingOrder){
            user=((TUserBookingOrder)oo).getExecutor();
            st=((TUserBookingOrder)oo).getCategory();
        }
        if(oo instanceof TUserBookingTempOrder){
            user=((TUserBookingTempOrder)oo).getBidder();
            st=((TUserBookingTempOrder)oo).getBooking().getCategory();
        }
        Map m=get_user_work_year(user, st);
        return m;
    }
    /**
     * bidder
     * @param to
     * @return 
     */
    private Map to_bidders_info(TUserBookingTempOrder to){
        Map dd=new HashMap();
        int work=0;
        int st=to.getBooking().getCategory();
        StringBuilder sb=new StringBuilder();
        sb.append(getResource("user.cert"));
        Map mm=get_user_work_year(to.getBidder(), st);
        dd.put("work", mm.get("work"));
        dd.put("workV",mm.get("workV"));
        dd.put("cert", mm.get("cert"));
        dd.put("uname", to.getBidder().getName());
        dd.put("bid", to.getId().toString());
        dd.put("url", to.getBidder().getHeadUrl());
        dd.put("bdate", to.getSubmissionTime().getTime());
        return dd;
    }
    //
    private Map to_excutor_info(TUserBookingOrder to){
        Map dd=new HashMap();
        int work=0;
        int st=to.getCategory();
        if(to.getExecutor().getNameEn().equals(ADMIN)){
           dd.put("work", "--"); dd.put("cert","--");dd.put("admin",1);
        }else{
            StringBuilder sb=new StringBuilder();
            sb.append(getResource("user.cert"));
            Map mm=get_user_work_year(to.getExecutor(),st);
            dd.put("work", mm.get("work"));
            dd.put("cert", mm.get("cert"));
            dd.put("admin",0);
        }
        dd.put("uname", to.getExecutor().getName());
        dd.put("bid", to.getId().toString());
        dd.put("url", to.getExecutor().getHeadUrl());
        return dd;
    }
    @Override
    public Map view_my_booking_target(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String id=(String) params.get("bbid");
        String flag=(String) params.get("flag");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(!hasText(id) || !NumberUtils.isDigits(id) || !hasText(flag)){
            return raiseError(1032);
        }
        Map m=new HashMap();
        TUser bidder;
        if(!flag.equals("bid")){
            TUserBookingTempOrder bo=orderTempRepo.findOne(Long.parseLong(id));
            if(bo==null)return raiseError(1032);
            bidder=bo.getBidder();
            if(bo.getBooking().getCategory()==BOOKING_CATEGORY_LG || 
               bo.getBooking().getCategory()==BOOKING_CATEGORY_OG){
               m.put("pay", 0);m.put("jr", 1);
            }else{
                m.put("pay", bo.getBooking().getStatus()<=BOOKING_STATE_2?1:0);
                m.put("jr", 0);
            }
            m.put("price", Utils.priceToString(bo.getBidAmount()));
            Map mm=getWorkYear(bo);
            int work=0;
            if(mm!=null)work=(int) mm.get("workV");
            if(work==0){
                m.put("wy",getResource("user.work.year2"));
            }else m.put("wy",getResource("user.work.year1", Integer.toString(work)));
            if(bo.getBooking().getExecutor()==null)m.put("status", 0);
            else{
                TUser uu=bo.getBooking().getExecutor();
                if(uu.getMobile().equals(bidder.getMobile()))m.put("status", 1);
                else m.put("status", 0);
            }
            if(bo.getBooking().getStatus()>=BOOKING_STATE_6){
                m.put("pay", 0);m.put("jr", 0);
                m.put("status", 1);
            } 
        }else{
            TUserBookingOrder cc=bookingRepo.findOne(Long.parseLong(id));
            if(cc==null)return raiseError(1032);
            if(cc.getCategory()==BOOKING_CATEGORY_LG || 
               cc.getCategory()==BOOKING_CATEGORY_OG){
                if(cc.getExecutor()==null)return raiseError(1032);
                if(cc.getJourney()==null){
                    m.put("jr", 1);m.put("pay", 1);
                }
                else {
                    m.put("jr", 0);
                    m.put("pay", 1);
                }
            }else{
                m.put("pay", cc.getStatus()<=BOOKING_STATE_2?1:0);
                m.put("jr", 0);
            }
            bidder=cc.getExecutor();
            m.put("price", Utils.priceToString(cc.getOrderAmount()));
            Map mm=getWorkYear(cc);
            int work=0;
            if(mm!=null){
                work=(int) mm.get("workV");
            }
            if(work==0){
                m.put("wy",getResource("user.work.year2"));
            }else  m.put("wy",getResource("user.work.year1", Integer.toString(work)));
            m.put("title", BOOKING_CATEGORIES[cc.getCategory()-1]);
            m.put("status", 1);
            if(cc.getStatus()>=BOOKING_STATE_6){
                m.put("pay", 0);m.put("jr", 0);
                m.put("status", 1);
            } 
        }
        m.put("uname", bidder.getName());
        m.put("unameEn", bidder.getNameEn());
        m.put("phone",StringUtils.remove(bidder.getMobile(), "-"));
        m.put("chatid",bidder.getChatid());
        m.put("url", bidder.getHeadUrl());
        TArea aa=bidder.getResidence();
        String ss = "";
        if(aa!=null){
            if(aa.getParent()!=null)ss=aa.getParent().getName1()+" ";
            ss=aa.getName1();
        }
        m.put("resi",getResource("user.resi", ss));
        
        if(bidder.getProperties()!=null){
            m.put("intro", bidder.getProperties().getIntroduce());
        }else m.put("intro", "");
        List<TUserPhotos> photos=userPhRepo.findByUser(bidder);
        List data=new ArrayList();
        if(photos!=null){
            for (TUserPhotos ph : photos) {
                data.add(ph.getPhotoUrl());
            }
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        m.put("photo", data);
        rm.put("info", m);
        return rm;
    }

    @Override
    public Map view_my_bid_info(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String id=(String) params.get("bbid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(!hasText(id)){
            return raiseError(1032);
        }
        if(!NumberUtils.isNumber(id))return raiseError(1032);
        TUserBookingOrder bo=bookingRepo.findOne(Long.parseLong(id));
        if(bo==null)return raiseError(1032);
        if(bo.getExecutor()==null){
            return raiseError(1032);
        }
        Map dd=to_excutor_info(bo);
        dd.put("money", Utils.priceToString(bo.getOrderAmount()));
        dd.put("cnt", "--");
        dd.put("chatid", bo.getExecutor().getChatid());
        dd.put("status", BOOKING_STATES[bo.getStatus()-1]);
        dd.put("title", BOOKING_CATEGORIES[bo.getCategory()-1]);
        if(bo.getStatus()>=BOOKING_STATE_6){
            dd.put("pay", 1);
        }else  dd.put("pay", (bo.getStatus()==BOOKING_STATE_2)?0:1);
        dd.put("phone", StringUtils.remove(bo.getExecutor().getMobile(), "-"));
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("info", dd);
        return rm;
    }

    @Override
    public Map view_my_journey_list(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        Integer aa=(Integer) params.get("st");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        List<TUserJourney> lst;
        if(aa==null)lst=userJRepo.find_by_user_target(user);
        else{
            switch(aa){
                case 1001:
                    lst=userJRepo.find_by_user_less_status(user,JOURNEY_STATE_2);
                    break;
                case 1002:
                    lst=userJRepo.find_by_user_status(user,JOURNEY_STATE_3);
                    break;
                case 1003:
                    lst=userJRepo.find_by_user_status(user,JOURNEY_STATE_4);
                    break;
                case 1004:
                    lst=userJRepo.find_by_user_status(user,JOURNEY_STATE_5);
                    break;
                default:
                    lst=userJRepo.find_by_user_target(user);
            }
        }
        List data=new ArrayList();
        for (TUserJourney ju : lst) {
            Map m=new HashMap();
            m.put("jid", ju.getId().toString());
            m.put("title", valueOf(ju.getNameZh()));
            m.put("date", getResource("journey.date", Utils.dateToString(ju.getBeginTime(), "-"),
                    Utils.dateToString(ju.getEndTime(), "-")));
            m.put("status", JOURNEY_STATES[ju.getStatus()-1]);
            m.put("turl", valueOf(ju.getTravelAgency().getImageUrl()));
            m.put("jr", ju.getStatus()==JOURNEY_STATE_3?1:0);
            if(ju.getStatus()==JOURNEY_STATE_1)m.put("cat", 100);
            else if(ju.getStatus()==JOURNEY_STATE_2)m.put("cat", 200);
            else m.put("cat", 300);
            data.add(m);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        return rm;
    }
    
    @Override
    public Map view_journey_option(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        
        if(!hasText(jid) || !NumberUtils.isDigits(jid)){
            return raiseError(1114);
        }
        TUserJourney journey=userJRepo.findOne(Long.parseLong(jid));
        if(journey==null){
            return raiseError(1114);
        }
        List<TUserJourneyDaily> jds=userJD.findByJourney(journey);
        List data=new ArrayList();
        for (TUserJourneyDaily jd : jds) {
            Map m=new HashMap();
            m.put("did", jd.getId().toString());
            m.put("dtitle", jd.getTitle());
            String dds=Utils.dateToString(jd.getJourneyDate(), "-");
            m.put("date", StringUtils.substringAfter(dds, "-"));
            data.add(m);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("daily", data);
        rm.put("o1", journey.getOther1());
        rm.put("o2", journey.getOther2());
        rm.put("o3", journey.getOther3());
        List<TUserJourneyCarInfo> tcs=userJCar.findByJourney(journey);
        if(!tcs.isEmpty()){
            TUserJourneyCarInfo tc=tcs.get(0);
            Map m1=new HashMap();
            m1.put("cm", valueOf(tc.getCarModel()));
            m1.put("name", valueOf(tc.getDriverName()));
            m1.put("cn", valueOf(tc.getCarNumber()));
            m1.put("cm", valueOf(tc.getPhone()));
            String ss=getDetailInfo("car", m1);
            rm.put("carinfo", ss);
        }else{
            rm.put("carinfo", "--");
        }
        return rm;
    }

    @Override
    public Map view_booking_detail(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String bid=(String) param.get("bbid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        
        if(!hasText(bid) || !NumberUtils.isDigits(bid)){
            return raiseError(1032);
        }
        TUserBookingOrder booking=bookingRepo.findOne(Long.parseLong(bid));
        if(booking==null){
            return raiseError(1032);
        }
        Map m=new HashMap();
        m.put("title", BOOKING_CATEGORIES[booking.getCategory()-1]);
        List data=new ArrayList();
        String prefix,prefix1="booking.items.";
        int cat=booking.getCategory();
        switch(cat){
            case BOOKING_CATEGORY_SHOP:
                TUserOrderShop shop=orderShRepo.findByBooking(booking);
                prefix="booking.items.shop.";
                data.add(createBookinItem(prefix+"time",Utils.dateToString1(shop.getBeginTime())));
                if(shop.getCard()!=null){
                    data.add(createBookinItem(prefix+"card",shop.getCard().getCardNumber()));
                }
                if(shop.getCid()!=null){
                  List<TUserOrderShopFiles> fs=orderSfRepo.findByCid(shop.getCid());
                  List lst=new ArrayList();
                    for (TUserOrderShopFiles f : fs) {
                        Map m1=new HashMap();
                        m1.put("icon", isImageFile(f.getViewUrl())?f.getViewUrl():getFileType(f.getViewUrl()));
                        m1.put("url", f.getViewUrl());
                        lst.add(m1);
                    }
                    m.put("fslist", lst);m.put("files", "OK");
                }else m.put("files", "--");
                data.add(createBookinItem(prefix1+"man", Integer.toString(shop.getBookingNumber())));
                data.add(createBookinItem(prefix1+"note", valueOf(shop.getRequirements())));
                break;
            case BOOKING_CATEGORY_RESTURANT:
                TUserOrderRestaurant rt=orderResRepo.findByBooking(booking);
                prefix="booking.items.rest.";
                if(rt!=null){
                    String style=rt.getStyle()==1?"午餐":"晚餐";
                    data.add(createBookinItem(prefix+"time",Utils.dateToString(rt.getBookingTime(),"-")+"  "+style));
                    data.add(createBookinItem(prefix+"pkg",valueOf(rt.getPackSet().getNameZh())));
                    data.add(createBookinItem(prefix1+"man",Integer.toString(rt.getAdultsNumber())));
                    data.add(createBookinItem(prefix+"ha",Integer.toString(rt.getHalalNumber())));
                    data.add(createBookinItem(prefix+"ve",Integer.toString(rt.getVegetarianNumber())));
                    data.add(createBookinItem(prefix1+"price",Utils.priceToString(booking.getExpectedAmount())));
                    data.add(createBookinItem(prefix1+"note", valueOf(rt.getRequirement())));
                }
                m.put("files", "--");
                break;
            case BOOKING_CATEGORY_SS_PIAO:
                TUserOrderTicket tk=orderTkRepo.findByBooking(booking);
                prefix="booking.items.piao.";
                if(tk!=null){
                    data.add(createBookinItem(prefix+"etime",Utils.dateToString1(tk.getEnterTime())));
                    data.add(createBookinItem(prefix+"gtime",Utils.dateToString1(tk.getGetTime())));
                    data.add(createBookinItem(prefix1+"man1",Integer.toString(tk.getAdultsNumber())));
                    data.add(createBookinItem(prefix1+"child",Integer.toString(tk.getChildrenNumber())));
                    data.add(createBookinItem(prefix1+"price",Utils.priceToString(booking.getExpectedAmount())));
                    data.add(createBookinItem(prefix1+"note", valueOf(tk.getRequirement())));
                }
                m.put("files", "--");
                break;
            case BOOKING_CATEGORY_DRV_2:
                TUserOrderDriver dr=orderDrRepo.findByBooking(booking);
                prefix="booking.items.drv.";
                if(dr!=null){
                    data.add(createBookinItem(prefix+"type", DRV_ORDER_TYPE[dr.getType()-1]));
                    if(dr.getType()==1){
                        data.add(createBookinItem(prefix+"1.area", dr.getStartingCity().getParent().getName1()));
                        data.add(createBookinItem(prefix+"1.city", dr.getStartingCity().getName1()));
                        data.add(createBookinItem(prefix+"1.saddr", valueOf(dr.getStartingAddress())));
                        data.add(createBookinItem(prefix+"1.eaddr", valueOf(dr.getEndAddress())));
                    }
                   if(dr.getType()==2)data.add(createBookinItem(prefix+"2.area", dr.getArea().getName1()));
                   if(dr.getType()==3)data.add(createBookinItem(prefix+"2.area", valueOf(dr.getVia())));
                   if(dr.getType()==2 || dr.getType()==3){
                       data.add(createBookinItem(prefix+"2.saddr", dr.getStartingCity().getName1()));
                       data.add(createBookinItem(prefix+"2.eaddr", dr.getEndCity().getName1()));
                   }
                   data.add(createBookinItem(prefix+"stime", Utils.dateToString1(dr.getBeginTime())));
                   data.add(createBookinItem(prefix+"etime", Utils.dateToString1(dr.getEndTime())));
                   data.add(createBookinItem(prefix+"model", dr.getCarModel().getModel()));
                   data.add(createBookinItem(prefix1+"man",Integer.toString(dr.getBookingNumber()))); 
                   data.add(createBookinItem(prefix+"guide", dr.getNeedGuide()==1?"有":"不限"));
                   data.add(createBookinItem(prefix+"lic", dr.getNeedLicenses()==1?"有":"不限"));
                   data.add(createBookinItem(prefix1+"price",Utils.priceToString(booking.getExpectedAmount())));
                   data.add(createBookinItem(prefix1+"note", valueOf(dr.getRequirement())));
                   m.put("files", "--");
                  
                }
                break;
            case BOOKING_CATEGORY_DRV_1:
                TUserOrderDriver dr1=orderDrRepo.findByBooking(booking);
                prefix="booking.items.drvv.";
                if(dr1!=null){
                    data.add(createBookinItem(prefix+"type", DRV_ORDER_TYPE[dr1.getType()-1]));
                    if(dr1.getType()==4){
                        data.add(createBookinItem(prefix+"area", dr1.getStartingCity().getParent().getName1()));
                        data.add(createBookinItem(prefix+"city", dr1.getStartingCity().getName1()));
                        data.add(createBookinItem(prefix+"airport", dr1.getAirport().getNameZh()));
                        data.add(createBookinItem(prefix+"flight", valueOf(dr1.getFlight())));
                        data.add(createBookinItem(prefix+"time", Utils.dateToString1(dr1.getBeginTime())));
                        data.add(createBookinItem(prefix+"start", valueOf(dr1.getStartingAddress())));
                    }
                    if(dr1.getType()==5){
                        data.add(createBookinItem(prefix+"model", dr1.getCarModel().getModel()));
                        data.add(createBookinItem(prefix+"stime", Utils.dateToString1(dr1.getBeginTime())));
                        data.add(createBookinItem(prefix+"etime", Utils.dateToString1(dr1.getEndTime())));
                        data.add(createBookinItem(prefix+"area", dr1.getStartingCity().getParent().getName1()));
                        data.add(createBookinItem(prefix+"city", dr1.getStartingCity().getName1()));
                        data.add(createBookinItem(prefix+"spos", valueOf(dr1.getStartingAddress())));
                        data.add(createBookinItem(prefix+"epos", valueOf(dr1.getEndAddress())));
                    }
                    data.add(createBookinItem(prefix1+"man",Integer.toString(dr1.getBookingNumber()))); 
                    data.add(createBookinItem(prefix1+"note",valueOf(dr1.getRequirement()))); 
                }
                m.put("files", "--");
                break;
            case BOOKING_CATEGORY_DOCENTS:
                TUserOrderDocents dc=orderDcRepo.findByBooking(booking);
                prefix="booking.items.dc.";
                if(dc!=null){
                    m.put("type", dc.getType());
                    data.add(createBookinItem(prefix+"type",dc.getType()==1?"独立":"拼团"));
                    data.add(createBookinItem(prefix+"vtime",Utils.dateToString(dc.getVisitDate(), "-")));
                    data.add(createBookinItem(prefix1+"man1",Integer.toString(dc.getAdultsNumber()))); 
                    data.add(createBookinItem(prefix1+"child",Integer.toString(dc.getChildrenNumber()))); 
                    data.add(createBookinItem(prefix+"time",dc.getExplainTime()));
                    data.add(createBookinItem(prefix+"tk",dc.isNeedTicket()==1?"需要":"不需要"));
                    data.add(createBookinItem(prefix1+"price",Utils.priceToString(booking.getExpectedAmount())));
                    if(dc.getType()==1){
                        data.add(createBookinItem(prefix+"gender",GENDER_NAME[dc.getGender()-1]));
                        data.add(createBookinItem(prefix+"nt",NATIONALITY[dc.getNationality()-1]));
                    }
                    data.add(createBookinItem(prefix1+"note", valueOf(dc.getRequirements())));
                }
                m.put("files", "--");
                break;
            case BOOKING_CATEGORY_LG:
                TUserOrderLongGuide lg=orderLgRepo.findByBooking(booking);
                prefix="booking.items.lg.";
                if(lg!=null){
                   List<TTourismLineCategory> lst=lg.getLineCategories();
                   StringBuilder sb=new StringBuilder();
                   if(!lst.isEmpty()){
                       for (TTourismLineCategory cc : lst) {
                           sb.append(cc.getNameZh()).append("  ");
                       }
                   }
                   data.add(createBookinItem(prefix+"line",sb.toString()));
                   data.add(createBookinItem(prefix+"vip",NATURE[lg.getNature()-1]));
                   sb=new StringBuilder();
                   sb.append(lg.getCountryNumber()).append("国");
                   sb.append(lg.getDayNumber()).append("日");
                   data.add(createBookinItem(prefix+"guo",sb.toString()));
                   data.add(createBookinItem(prefix+"ocity",lg.getDepartureExitCity().getName1()));
                   data.add(createBookinItem(prefix+"icity",lg.getDepartureEntryCity().getName1()));
                   data.add(createBookinItem(prefix+"stime",Utils.dateToString(lg.getBeginTime(),"-")));
                   data.add(createBookinItem(prefix+"etime",Utils.dateToString(lg.getEndTime(),"-")));
                   data.add(createBookinItem(prefix1+"man",Integer.toString(lg.getBookingNumber()))); 
                   data.add(createBookinItem(prefix+"leader",lg.getNeedLeaderCertificate()==1?"有":"不限"));
                   data.add(createBookinItem(prefix+"guide",lg.getNeedEmploymentCertificate()==1?"有":"不限"));
                   data.add(createBookinItem(prefix1+"note",valueOf(lg.getRequirement())));
                   m.put("files", "--");
                }
                break;
            case BOOKING_CATEGORY_OG:
                TUserOrderOGuide og=orderOgRepo.findByBooking(booking);
                prefix="booking.items.lg.";
                if(og!=null){
                    List<TTourismLineCategory> lst=og.getLineCategories();
                   StringBuilder sb=new StringBuilder();
                   if(!lst.isEmpty()){
                       for (TTourismLineCategory cc : lst) {
                           sb.append(cc.getNameZh()).append("  ");
                       }
                   }
                   data.add(createBookinItem(prefix+"line",sb.toString()));
                   data.add(createBookinItem(prefix+"vip",NATURE[og.getNature()-1]));
                   sb=new StringBuilder();
                   sb.append(og.getCountryNumber()).append("国");
                   sb.append(og.getDayNumber()).append("日");
                   data.add(createBookinItem(prefix+"guo",sb.toString()));
                   if(og.getDepartureExitCity()!=null)
                   data.add(createBookinItem(prefix+"ocity",og.getDepartureExitCity().getName1()));
                   else data.add(createBookinItem(prefix+"ocity","--"));
                   data.add(createBookinItem(prefix+"icity",og.getDepartureEntryCity().getName1()));
                   data.add(createBookinItem(prefix+"oocity",og.getReturnExitCity().getName1()));
                   data.add(createBookinItem(prefix+"stime",Utils.dateToString(og.getBeginTime(),"-")));
                   data.add(createBookinItem(prefix+"etime",Utils.dateToString(og.getEndTime(),"-")));
                   data.add(createBookinItem(prefix+"itime",Utils.dateToString(og.getDepartureEntryTime(),"-")));
                   data.add(createBookinItem(prefix+"otime",Utils.dateToString(og.getReturnExitTime(),"-")));
                   data.add(createBookinItem(prefix1+"man",Integer.toString(og.getBookingNumber()))); 
                   data.add(createBookinItem(prefix+"guide",og.getNeedEmploymentCertificate()==1?"有":"不限"));
                   data.add(createBookinItem(prefix1+"note",valueOf(og.getRequirement())));
                }
                m.put("files", "--");
                break;
            case BOOKING_CATEGORY_LC:
                TUserOrderLocalGuide lc=orderLcRepo.findByBooking(booking);
                prefix="booking.items.lc.";
                if(lc!=null){
                   data.add(createBookinItem(prefix+"area",lc.getArea().getName1())); 
                   data.add(createBookinItem(prefix+"stime",Utils.dateToString1(lc.getBeginTime()))); 
                   data.add(createBookinItem(prefix+"etime",Utils.dateToString1(lc.getEndTime()))); 
                   data.add(createBookinItem(prefix+"guide",lc.getNeedGuideCertificate()==1?"有":"不限"));
                   data.add(createBookinItem(prefix+"man",Integer.toString(lc.getBookingNumber())));
                   data.add(createBookinItem(prefix1+"note",valueOf(lc.getRequirement())));
                }
                break;
            case BOOKING_CATEGORY_PP:
                TUserOrderProcessCharge pc=pcRepo.findByBooking(booking);
                prefix="booking.items.pc.";
                if(pc!=null){
                   data.add(createBookinItem(prefix+"area",pc.getArea().getParent().getName1())); 
                   data.add(createBookinItem(prefix+"city",pc.getArea().getName1())); 
                   data.add(createBookinItem(prefix+"date",Utils.dateToString(pc.getBookingDate(),"-"))); 
                   data.add(createBookinItem(prefix+"vc",valueOf(pc.getVehicleCompanyName())));
                   data.add(createBookinItem(prefix+"tnum",valueOf(pc.getVehicleLicence())));
                   data.add(createBookinItem(prefix+"cpaddr",valueOf(pc.getCompanyAddress())));
                   data.add(createBookinItem(prefix+"taf",valueOf(pc.getCompanyTax())));
                   data.add(createBookinItem(prefix+"cphone",valueOf(pc.getCompanyPhone())));
                   data.add(createBookinItem(prefix+"email",valueOf(pc.getReceiveEmail())));
                   data.add(createBookinItem(prefix+"fax",valueOf(pc.getReceiveFax())));
                   data.add(createBookinItem(prefix+"price",Utils.priceToString(booking.getExpectedAmount())));
                }
                m.put("files", "--");
                break;
                
        }
        m.put(DATA_KEY, data);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, m);
        return rm;
    }
    
    private boolean isImageFile(String file){
        if(file!=null){
            String ext=StringUtils.substringAfterLast(file, ".");
            return ext.equalsIgnoreCase("jpg") || ext.equalsIgnoreCase("bmp") || ext.equalsIgnoreCase("png")
                    || ext.equalsIgnoreCase("gif") || ext.equalsIgnoreCase("jpeg") || ext.equalsIgnoreCase("tiff");
        }
        return false;
    }
    private String getFileType(String file){
        String prefix=clientApp.getUrl("file.type.prefix");
        if(file!=null){
            String ext=StringUtils.substringAfterLast(file, ".");
            return prefix+ext+".png";
        }
        return null;
    }
    
    private BookingItem createBookinItem(String name,String value){
        String n=getResource(name);
        BookingItem bk=new BookingItem(n, value);
        return bk;
    }
    /**
     * get booking search items
     * api url: /dataview/sitems
     * @param param
     * @return 
     */
    @Override
    public Map view_booking_items(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        List ss=new ArrayList();
        for (int i = 0; i < BOOKING_CATEGORIES.length; i++) {
            if((i>=0 && i<=5) || i==9){
                Map m=new HashMap();
                m.put("id", i+1);
                m.put("title", BOOKING_CATEGORIES[i]);
                ss.add(m);
            }
        }
        rm.put("cat", ss);
        rm.put("status", BOOKING_STATES);
        return rm;
    }

    @Override
    public Map view_bider_skill(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String bid=(String) param.get("bbid");
        String flag=(String) param.get("flag");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        
        if(!hasText(bid) || !NumberUtils.isDigits(bid) || !hasText(flag)){
            return raiseError(1032);
        }
        TUser bidder;int cat;
        if(!flag.equals("bid")){
            TUserBookingTempOrder booking=orderTempRepo.findOne(Long.parseLong(bid));
            if(booking==null){
                return raiseError(1032);
            }
            bidder=booking.getBidder();
            cat=booking.getBooking().getCategory();
        }else{
            TUserBookingOrder order=bookingRepo.findOne(Long.parseLong(bid));
            if(order==null){
                return raiseError(1032);
            }
            bidder=order.getExecutor();
            cat=order.getCategory();
        }
        
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("uid", clientApp.set_uid(pt, bidder.getMobile()));
        switch(cat){
            case BOOKING_CATEGORY_LG:rm.put("cat","LG");break;
            case BOOKING_CATEGORY_OG:rm.put("cat","OG");break;
            case BOOKING_CATEGORY_LC:rm.put("cat","LC");break;
            case BOOKING_CATEGORY_DRV_1:
            case BOOKING_CATEGORY_DRV_2:
                rm.put("cat","DRV");break;
            case BOOKING_CATEGORY_DOCENTS:
                rm.put("cat","DC");break;
        }
        return rm;
    }

    @Override
    public Map view_weather(Map param) {
        String lat=(String)param.get("lat");
        String lon=(String)param.get("lon");
        if(!hasText(lat) || !NumberUtils.isNumber(lat)){
            return raiseError(1032);
        }
        if(!hasText(lon) || !NumberUtils.isNumber(lon)){
            return raiseError(1032);
        }
        RestTemplate req=new RestTemplate();
        List converters=new ArrayList<>();
        converters.add(new MappingJackson2HttpMessageConverter());
        converters.add(new StringHttpMessageConverter(Charset.forName("UTF-8")));
        req.setMessageConverters(converters);
        StringBuilder sb=new StringBuilder();
        sb.append("http://api.openweathermap.org/data/2.5/weather?");
        sb.append("lat=").append(lat).append("&lon=").append(lon).append("&lang=zh_cn");
        String ds = null,icon = null;
        try{
            Map mm=req.getForObject(sb.toString(), Map.class);
            
            List ws=(List) mm.get("weather");
            Map m1=new HashMap();
            for (int i = 0; i < ws.size(); i++) {
                Map gg = (Map) ws.get(i);
                if(gg.containsKey("description")){
                    ds=(String) gg.get("description");
                }
                if(gg.containsKey("icon")){
                    icon=(String) gg.get("icon");
                }
            }
            Map m2=(Map) mm.get("main");
            Double temp=(Double) m2.get("temp");
            temp=temp-273;
            Integer hd=(Integer) m2.get("humidity");
            Double pr= Double.parseDouble(m2.get("pressure")!=null?m2.get("pressure").toString():"0.0");
            m2=(Map) mm.get("wind");
            Double sp=Double.parseDouble(m2.get("speed")!=null?m2.get("speed").toString():"0.0");
            Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
            rm.put("icon", "http://openweathermap.org/img/w/"+icon+".png");
            rm.put("desc", ds);
            rm.put("temp", String.format("%.1f",temp)+" °C");
            m2=new HashMap();
            m2.put("wd", sp.toString());
            m2.put("hd", hd.toString());
            m2.put("pr", pr.toString());
            String desc=getDetailInfo("weather", m2);
            rm.put("detail", desc);
            rm.put("date", Utils.dateToString_byChina(new Date())+" "+Utils.timeToString(new Date()));
            return rm;
        }catch(RestClientException | CastorMappingException ex){
            return raiseError(1032);
        }
        
    }

    @Override
    public Map view_weixin_bid_state(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String bid=(String) param.get("bbid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(bid) || !NumberUtils.isNumber(bid))return raiseError(1032);
        TUserBookingTempOrder bbk=tempRepo.findOne(Long.parseLong(bid));
        if(bbk==null)return raiseError(1032);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        TUserBookingOrder bk=bbk.getBooking();
        if(bk==null)return raiseError(1032);
        if(bk.getExecutor()==null){
            rm.put("status", "nobid");
        }else{
            rm.put("status", "bid");
        }
        rm.put("bbid", bk.getId().toString());
        return rm;
    }

    @Override
    public Map view_journey_hotel(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String hid=(String) param.get("hid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(hid) || !NumberUtils.isNumber(hid)){
            return raiseError(1032);
        }
        TUserJourneyHotelInfo hinfo=jorHRepo.findOne(Long.parseLong(hid));
        if(hinfo==null)return raiseError(1032);
        Map m=new HashMap();
        m.put("date", Utils.dateToString(hinfo.getJourneyDate(),"-"));
        m.put("hname", hinfo.getHotelName());
        m.put("phone", hinfo.getPhone()==null?" ":hinfo.getPhone());
        m.put("fax", hinfo.getFax()==null?" ":hinfo.getFax());
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, m);
        return rm;
    }
    /**
     * 입찰후보목록얻기
     * @param param
     * @return 
     */
    @Override
    public Map view_bid_will(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String sn=(String) param.get("sn");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(sn)){
            return raiseError(1032);
        }
        TUserBookingOrder order=bookingRepo.findBySerialNumber(sn);
        if(order==null)return raiseError(1032);
        int cat=order.getCategory();
        String sql = null;
        StringBuilder sb=new StringBuilder();
        switch (cat) {
            case BOOKING_CATEGORY_LG:
                TUserOrderLongGuide lg = orderLgRepo.findByBooking(order);
                if (lg != null) {
                    sql = "select p.user from TUserIdentityLongGuide p";
                    sb.append(" where p.user.id<>:user and p.status=1 and p.user.identity.statusLg=2 and p.user.status<3 and "
                            + "p.user.identity.hasIdentityLongGuide=1 and ");
                    switch (lg.getNature()) {
                        case 1:
                            sb.append("p.supportTourAssemble=1 and ");
                            break;
                        case 2:
                            sb.append("p.supportTourIndependent=1 and ");
                            break;
                        case 3:
                            sb.append("p.supportTourVip=1 and ");
                            break;
                    }
                    if (lg.getNeedLeaderCertificate() == 1) {
                        sb.append("p.hasLeaderCertificate=1 and ");
                    }
                    if (lg.getNeedEmploymentCertificate() == 1) {
                        sb.append("p.hasGuideCertificate=1 and ");
                    }
                    String s = StringUtils.substringBeforeLast(sb.toString(), " and ");
                    sql = sql + s;
                }
                break;
            case BOOKING_CATEGORY_OG:
                TUserOrderOGuide og = orderOgRepo.findByBooking(order);
                if (og != null) {
                    sql = "select p.user from TUserIdentityOGuide p";
                    sb.append(" where p.user.id<>:user and p.status=1 and p.user.identity.statusOg=2 and  p.user.status<3 and "
                            + "p.user.identity.hasIdentityOverseasLocalGuide=1 and ");
                    switch (og.getNature()) {
                        case 1:
                            sb.append("p.supportTourAssemble=1 and ");
                            break;
                        case 2:
                            sb.append("p.supportTourIndependent=1 and ");
                            break;
                        case 3:
                            sb.append("p.supportTourVip=1 and ");
                            break;
                    }
                    if (og.getNeedEmploymentCertificate() == 1) {
                        sb.append("p.hasEmploymentCertificate=1 and ");
                    }
                    String s = StringUtils.substringBeforeLast(sb.toString(), " and ");
                    sql = sql + s;
                }
                break;
            case BOOKING_CATEGORY_LC:
                TUserOrderLocalGuide lc = orderLcRepo.findByBooking(order);
                if (lc != null) {
                    sql = "select p.user from TUserIdentityLCGuide p";
                    sb.append(" where p.user.id<>:user and p.status=1 and p.user.identity.statusLc=2 and  p.user.status<3 and "
                            + "p.user.identity.hasIdentityLocalGuide=1 and");
                    sb.append(" p.serviceCity.id='").append(lc.getArea().getId()).append("'");
                    if (lc.getNeedGuideCertificate() == 1) {
                        sb.append(" and p.hasGuideCertificate=1");
                    }
                    sql = sql + sb.toString();
                }
                break;
            case BOOKING_CATEGORY_DOCENTS:
                TUserOrderDocents dc = orderDcRepo.findByBooking(order);
                if (dc != null) {
                    if (dc.getType()== 1) {
                        sql = "select p.user from TUserIdentityDocents p";
                        sb.append(" where p.user.id<>:user and p.status=1 and p.user.identity.statusDc=2 and  p.user.status<3 and "
                                + "p.user.identity.hasIdentityDocents=1 and ");
                        sb.append("p.supportMuseum=1 and EXISTS (select pp from p.spots pp where pp.id=").append(dc.getScenic().getId()).append(") and ");
                        if(dc.getGender()!=3)sb.append("p.user.gender=").append(dc.getGender()).append(" and ");
                        if(dc.getNationality()!=3)sb.append("p.nationality=").append(dc.getNationality()).append(" and ");
                        String s = StringUtils.substringBeforeLast(sb.toString(), " and ");
                        sql = sql + s;
                    }
                }
                break;
            case BOOKING_CATEGORY_DRV_1:
            case BOOKING_CATEGORY_DRV_2:
                TUserOrderDriver dr = orderDrRepo.findByBooking(order);
                if (dr != null) {
                    sql = "select p.user from TUserIdentityDriver p";
                    sb.append(" where p.user.id<>:user and p.user.identity.statusDrv=2 and p.user.identity.hasIdentityDriver=1 and  p.user.status<3 and ");
                    if(dr.getStartingCity()!=null){
                        sb.append("p.area.id='").append(dr.getStartingCity().getParent().getId()).append("' and ");
                    }
                    if (dr.getCategory() == 1) {
                        if (dr.getNeedLicenses() == 1) {
                            sb.append("p.hasLicenses=1 and ");
                        }
                        if (dr.getNeedGuide() == 1) {
                            sb.append("p.supportGuide=1 and ");
                        }
                        if (dr.getType() == 1) {
                            sb.append("p.supportSightseeing=1 and ");
                        }
                        if (dr.getType() == 2) {
                            sb.append("p.supportSingleCountry=1 and ");
                        }
                        if (dr.getType() == 3) {
                            sb.append("p.supportMultipleCountry=1 and ");
                        }
                    }
                    if(dr.getType()==4) {
                    } else {
                      if(dr.getCarModel()!=null)sb.append("p.models.id=").append(dr.getCarModel().getId()).append(" and ");
                    }
                    String s = StringUtils.substringBeforeLast(sb.toString(), " and ");
                    sql = sql + s;
                }
                break;
        }
        List<TUser> users=em.createQuery(sql).setParameter("user", user.getId()).getResultList();
        List data=new ArrayList();
        for (TUser usr : users) {
            Map m=get_user_work_year(usr, cat);
            Map dd=new HashMap();
            dd.put("work", m.get("work"));
            dd.put("cert", m.get("cert"));
            dd.put("uname", usr.getName());
            dd.put("url", usr.getHeadUrl());
            data.add(dd);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        return rm;
    }
    @PersistenceContext
    private EntityManager em;
    
    private Map get_user_work_year(TUser user,int st){
        int work=0;
        StringBuilder sb=new StringBuilder();
        String prefix=getResource("user.cert");
        Map m=new HashMap();
        TUserIdentity tid=userIdRepo.findByUser(user);

        switch(st){
            case BOOKING_CATEGORY_LG:
                TUserIdentityLongGuide gg=userLgRepo.findByUserAndStatus(user,tid.getStatusLg()==USER_STATE_READY?1:0);
                if(gg!=null){
                    TUserIdentityLongGuideExpanded pr=gg.getProperties();
                    if(pr!=null){
                        Date dt=pr.getEmploymentDate();
                        if(dt!=null){
                            Long t=System.currentTimeMillis()-dt.getTime();
                            t=t/31536000000L;
                            work=t.intValue();
                        }
                    }else work=0;
                    if(gg.getHasGuideCertificate()==1)sb.append(getResource("user.work.cert2")).append(", ");
                    if(gg.getHasLeaderCertificate()==1)sb.append(getResource("user.work.cert1")).append(", ");
                }
                break;
            case BOOKING_CATEGORY_OG:
                TUserIdentityOGuide og=userOgRepo.findByUserAndStatus(user,tid.getStatusLg()==USER_STATE_READY?1:0);
                if(og!=null){
                    TUserIdentityOGuideExpanded pr1=og.getProperties();
                    if(pr1!=null){
                        Date dt=pr1.getEmploymentDate();
                        if(dt!=null){
                            Long t=System.currentTimeMillis()-dt.getTime();
                            t=t/31536000000L;
                            work=t.intValue();
                        }
                    }else work=0;
                    if(og.getHasEmploymentCertificate()==1)sb.append(getResource("user.work.cert2")).append(", ");
                }
                break;
            case BOOKING_CATEGORY_LC:
                TUserIdentityLCGuide cc=userLcRepo.findByUserAndStatus(user,tid.getStatusLc()==USER_STATE_READY?1:0);
                if(cc!=null){
                    TUserIdentityLCGuideExpanded pr=cc.getProperties();
                    if(pr!=null){
                        Date dt=pr.getEmploymentDate();
                        if(dt!=null){
                            Long t=System.currentTimeMillis()-dt.getTime();
                            t=t/31536000000L;
                            work=t.intValue();
                        }
                    }else work=0;
                    if(cc.getHasGuideCertificate()==1)sb.append(getResource("user.work.cert2")).append(", ");
                }
                break;
            case BOOKING_CATEGORY_DOCENTS:
                TUserIdentityDocents ss=userDcRepo.findByUserAndStatus(user,tid.getStatusDc()==USER_STATE_READY?1:0);
                if(ss!=null){
                    TUserIdentityDocentsExpanded pr=ss.getProperties();
                    if(pr!=null){
                        Date dt=pr.getEmploymentDate();
                        if(dt!=null){
                            Long t=System.currentTimeMillis()-dt.getTime();
                            t=t/31536000000L;
                            work=t.intValue();
                        }
                    }else work=0;
                }
                break;
            case BOOKING_CATEGORY_DRV_1:
            case BOOKING_CATEGORY_DRV_2:
                if(tid.getStatusDrv()==USER_STATE_READY){
                    TUserIdentityDriver dr=userDrvRepo.findByUser(user);
                    if(dr!=null){
                        TUserIdentityDriverExpanded pr=dr.getProperties();
                        if(pr!=null){
                           Date dt=pr.getEmploymentDate();
                            if(dt!=null){
                                Long t=System.currentTimeMillis()-dt.getTime();
                                t=t/31536000000L;
                                work=t.intValue();break;
                            }
                        }else work=0;    
                    } else work=0;
                }else{
                    TUserDriverTemp dt=userDrvTenp.findByUser(user);
                    if(dt!=null){
                        TUserDriverExpandedTemp pt=dt.getProperties();
                        if(pt!=null){
                           Date dtt=pt.getEmploymentDate();
                            if(dtt!=null){
                                Long t=System.currentTimeMillis()-dtt.getTime();
                                t=t/31536000000L;
                                work=t.intValue();break;
                            }
                        }else work=0;  
                    }else work=0;
                }
                break;
        }
        if(work==0){
            m.put("work", getResource("user.work.year.no"));
        }else{
            m.put("work", getResource("user.work.year",Integer.toString(work)));
        }
        m.put("workV", work);
        if(hasText(sb.toString())){
            prefix=prefix+StringUtils.substringBeforeLast(sb.toString(), ", ");
        }else{
            prefix=prefix+" 无";
        }
        m.put("cert", prefix);
        return m;
    }

    @Override
    public Map view_alipay_info(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String bid=(String) param.get("bid");
        String type=(String) param.get("type");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(bid)){
            return raiseError(1032);
        }
        if(!hasText(type)){
            return raiseError(1032);
        }
        TUserBookingOrder booking;
        if(type.equals("sn"))booking=bookingRepo.findBySerialNumber(bid);
        else{
            if(!NumberUtils.isNumber(bid))return raiseError(1032);
            booking=bookingRepo.findOne(Long.parseLong(bid));
        }
        if(booking==null){
            return raiseError(1032);
        }
        if(booking.getStatus()!=BOOKING_STATE_2){
            return raiseError(1032);
        }
        if(booking.getExecutor()==null){
            return raiseError(1032);
        }
        if(booking.getOrderAmount()==null || booking.getOrderAmount()<=0f){
            return raiseError(1032);
        }
        String exrate=services.view_exchange_rate_byCode(MONEY_EUR, MONEY_CNY, "1");
        System.out.println("Exchange rate:"+exrate);
        if(exrate.equals("0")){
            return raiseError(1032);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        Map m=new HashMap();
        m.put("pt", clientApp.getSystemProperty("alipay.partner"));
        m.put("pub", StringUtils.remove(clientApp.getSystemProperty("alipay.public.key"),"\r\n"));
        m.put("priv", StringUtils.remove(clientApp.getSystemProperty("alipay.private.key"),"\r\n"));
        m.put("subject", BOOKING_CATEGORIES[booking.getCategory()-1]);
        m.put("body", booking.getTitle());
        Float price=Float.parseFloat(exrate)*booking.getOrderAmount();
        price=Float.parseFloat(String.format("%.2f", price));
        //m.put("price", price.toString());
        m.put("price", "1");
        m.put("sign_type", clientApp.getSystemProperty("alipay.sign.type"));
        String notify=clientApp.getSystemProperty("alipay.notify.url");
        notify=notify+"/"+uid+"/"+pt+"/"+booking.getSerialNumber();
        m.put("order_info", getOrderInfo(BOOKING_CATEGORIES[booking.getCategory()-1],booking.getTitle(),price.toString(),notify,booking.getSerialNumber()));
        rm.put("pay_info", m);
        return rm;
    }
    
    public String getOutTradeNo() {
        SimpleDateFormat format = new SimpleDateFormat("MMddHHmmss",Locale.getDefault());
        Date date = new Date();
        String key = format.format(date);
        Random r = new Random();
        key = key + r.nextInt();
        key = key.substring(0, 15);
        return key;
    }
    
    public String getOrderInfo(String subject, String body, String price,String notify,String tradeno) {
        // 签约合作者身份ID
        String orderInfo = "partner=" + "\"" + clientApp.getSystemProperty("alipay.partner") + "\"";
        // 签约卖家支付宝账号
        orderInfo += "&seller_id=" + "\"" + clientApp.getSystemProperty("alipay.seller.id") + "\"";
        // 商户网站唯一订单号
        orderInfo += "&out_trade_no=" + "\"" + tradeno + "\"";
        // 商品名称
        orderInfo += "&subject=" + "\"" + subject + "\"";
        // 商品详情
        orderInfo += "&body=" + "\"" + body + "\"";
        // 商品金额
        orderInfo += "&total_fee=" + "\"" + price + "\"";
        // 服务器异步通知页面路径
        
        orderInfo += "&notify_url=" + "\"" + notify + "\"";
        // 服务接口名称， 固定值
        orderInfo += "&service=\"mobile.securitypay.pay\"";
        // 支付类型， 固定值
        orderInfo += "&payment_type=\"1\"";
        // 参数编码， 固定值
        orderInfo += "&_input_charset=\"utf-8\"";
        // 取值范围：1m～15d。
        // m-分钟，h-小时，d-天，1c-当天（无论交易何时创建，都在0点关闭）。
        // 该参数数值不接受小数点，如1.5h，可转换为90m。
        orderInfo += "&it_b_pay=\"30m\"";
        // 支付宝处理完请求后，当前页面跳转到商户指定页面的路径，可空
        orderInfo += "&return_url=\"m.alipay.com\"";
        return orderInfo;
    }

    @Override
    public Map view_user_bill_list(Map params) {
        Map m=auth_apps(params, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        Integer cat=(Integer) params.get("cat");
        Integer st=(Integer) params.get("status");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(cat==null)cat=0;
        if(st==null)st=0;
        Integer start=(Integer) params.get("start");
        Integer count=(Integer) params.get("count");
        if(start==null)start=1;
        if(count==null)count=20;
        else if(count>20)count=20;
        Pageable pg=new PageRequest(start, count);
        Page<TUserBill> pglist;
        if(st==0){
            pglist=billRepo.findByUserOrderByPaymentTimeDesc(user, pg);
        }else{
            pglist=billRepo.findByUserAndStatusOrderByPaymentTimeDesc(user, st,pg);
        }
        List data=new ArrayList();
        List<TUserBill> lst=pglist.getContent();
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        for (TUserBill bill : lst) {
            Map m1=new HashMap();
            m1.put("id", bill.getId().toString());
            m1.put("title", bill.getOrderName());
            m1.put("date", df.format(bill.getPaymentTime()));
            m1.put("status", PAY_STATEMENTS[bill.getStatus()-1]);
            if(bill.getPaymentType()==PAYMENT_TYPE_EXPORT)
            m1.put("turl", getResource("booking.img.prefix", "minus"));
            else m1.put("turl", getResource("booking.img.prefix", "plus"));
            m1.put("amount", Utils.priceToString(bill.getTotalAmount()));
            data.add(m1);
        }
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        rm.put("count", pglist.getTotalElements());
        return rm;
    }

    @Override
    public Map view_user_bill_info(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String bid=(String) params.get("bill");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(!hasText(bid) || !NumberUtils.isNumber(bid)){
            return raiseError(1032);
        }
        TUserBill bill=billRepo.findOne(Long.parseLong(bid));
        if(bill==null)return raiseError(1032);
        if(bill.getBooking().getExecutor()==null)return raiseError(1032);
        Map m=new HashMap();
        if(bill.getPaymentType()==PAYMENT_TYPE_EXPORT){
            m.put("hurl", Utils.valueOf(bill.getBooking().getExecutor().getHeadUrl()));
            m.put("uname", bill.getBooking().getExecutor().getName());
        }else {
            m.put("uname", bill.getBooking().getApplicant().getName());
            m.put("hurl", Utils.valueOf(bill.getBooking().getApplicant().getHeadUrl()));
        }
        m.put("bcat", BOOKING_CATEGORIES[bill.getBooking().getCategory()-1]);
        m.put("price", Utils.priceToString(bill.getTotalAmount()));
        m.put("pay_type", PAY_SYSTEMS[bill.getPaymentMethod()-1]);
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        m.put("pdate", df.format(bill.getPaymentTime()));
        m.put("no", bill.getPaymentSerialNumber());
        m.put("status", PAY_STATEMENTS[bill.getStatus()-1]);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, m);
        return rm;
    }
}
