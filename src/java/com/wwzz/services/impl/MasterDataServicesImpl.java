/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.show.api.ShowApiRequest;
import com.wwzz.services.api.MasterDataServices;
import com.wwzz.services.domain.base.TAirport;
import com.wwzz.services.domain.base.TArea;
import com.wwzz.services.domain.base.TCarModels;
import com.wwzz.services.domain.base.TCityIntroduction;
import com.wwzz.services.domain.base.TCityRestaurant;
import com.wwzz.services.domain.base.TCityScenicSpots;
import com.wwzz.services.domain.base.TCityShop;
import com.wwzz.services.domain.base.TLang;
import com.wwzz.services.domain.base.TProcessChargeCategory;
import com.wwzz.services.domain.base.TTourismLineCategory;
import com.wwzz.services.domain.base.TTravelAgency;
import com.wwzz.services.domain.users.TUserOrganization;
import com.wwzz.services.repository.TAirportRepository;
import com.wwzz.services.repository.TAreaRepository;
import com.wwzz.services.repository.TCarModelsRepository;
import com.wwzz.services.repository.TCityIntroductionRepository;
import com.wwzz.services.repository.TCityRestaurantRepository;
import com.wwzz.services.repository.TCityScenicSpotsRepository;
import com.wwzz.services.repository.TCityShopRepository;
import com.wwzz.services.repository.TLangRepository;
import com.wwzz.services.repository.TProcessChargeCategoryRepository;
import com.wwzz.services.repository.TTourismLineCategoryRepository;
import com.wwzz.services.repository.TTravelAgencyRepository;
import com.wwzz.services.repository.TUserOrganizationRepository;
import com.wwzz.services.utils.Constants;
import static com.wwzz.services.utils.Constants.ERROR_CODE_OK;
import static com.wwzz.services.utils.Constants.RETURN_SUCCESS_CODE;
import com.wwzz.services.utils.Utils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author JBoss
 */
@Service
@Transactional
public class MasterDataServicesImpl extends ResultDataServices  implements MasterDataServices,Constants{
    
    @Autowired
    private TAreaRepository areaRepo;
    @Autowired
    private TLangRepository langRepo;
    @Autowired
    private TTourismLineCategoryRepository lineCat;
    @Autowired
    private TTravelAgencyRepository travelRepo;
    @Autowired
    private TUserOrganizationRepository userOrgepo;
    @Autowired
    private TCarModelsRepository carRepo;
    @Autowired
    private TCityScenicSpotsRepository citySpRepo;
    @Autowired
    private TAirportRepository airRepo;
    @Autowired
    private TCityIntroductionRepository cityRepo;
    @Autowired
    private TCityRestaurantRepository restRepo;
    @Autowired
    private TCityShopRepository shopRepo;
    @Autowired
    private TCityScenicSpotsRepository ssRepo;
    @Autowired
    private TProcessChargeCategoryRepository jinRepo;

    public MasterDataServicesImpl() {
        load_properties();
    }
    
    @Override
    public Map view_country_codes(String level) {
        Sort sort=new Sort(Sort.Direction.ASC, "name1");
        TArea ar1;
        if(level.equals("--"))ar1=null;
        else ar1=areaRepo.findOne(level);
        List<TArea> lst=areaRepo.findByParent(ar1, sort);
        List data=new ArrayList();
        for (TArea ar : lst) {
            Map m=new HashMap();
            m.put("id", ar.getId());
            m.put("name", ar.getName1());
            m.put("code", ar.getCode());
            data.add(m);
        }
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("data", data);
        return rm;
    }


    @Override
    public Map view_lanaguage() {
        List<TLang> lst=langRepo.findAll(new Sort(Sort.Direction.ASC, "nameZh"));
        List data=new ArrayList();
        for (TLang lang : lst) {
            Map m=new HashMap();
            m.put("iid", lang.getId());
            m.put("lname", lang.getNameZh());
            data.add(m);
        }
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("data", data);
        return rm;
    }

    @Override
    public Map view_line_category() {
        List<TTourismLineCategory> cats=lineCat.findAll();
        List data=new ArrayList();
        for (TTourismLineCategory dt : cats) {
            Map m=new HashMap();
            m.put("id", dt.getId());
            m.put("name", dt.getNameZh());
            data.add(m);
        }
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("data", data);
        return rm;
    }

    @Override
    public Map view_travel() {
        List<TTravelAgency> cats=travelRepo.findAll();
        List data=new ArrayList();
        for (TTravelAgency dt : cats) {
            Map m=new HashMap();
            m.put("id", dt.getId());
            m.put("name", dt.getNameZh());
            data.add(m);
        }
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("data", data);
        return rm;
    }

    @Override
    public Map view_user_orgs() {
        List<TUserOrganization> lst=userOrgepo.findAll();
        List data=new ArrayList();
        for (TUserOrganization lang : lst) {
            Map m=new HashMap();
            m.put("oid", lang.getId());
            m.put("oname", lang.getName());
            data.add(m);
        }
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("data", data);
        return rm;
    }

    @Override
    public Map view_car_models() {
        List<TCarModels> lst=carRepo.findAll();
        List data=new ArrayList();
        for (TCarModels lang : lst) {
            Map m=new HashMap();
            m.put("cid", lang.getId());
            m.put("cname", lang.getModel());
            data.add(m);
        }
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("data", data);
        return rm;
    }

    @Override
    public Map view_scenic_spots() {
        List<TCityScenicSpots> lst=citySpRepo.findByHasDocents(1);
        List data=new ArrayList();
        for (TCityScenicSpots lang : lst) {
            Map m=new HashMap();
            m.put("sid", lang.getId());
            m.put("sname", lang.getNameZh());
            data.add(m);
        }
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("data", data);
        return rm;
    }

    @Override
    public Map search_city_by_name(String country, String name) {
        List<TArea> lst;
        if(country.equals("--")){
            lst=areaRepo.find_country_by_name(name.toLowerCase());
        }else{
            if(!hasText(name))name="";
            lst=areaRepo.find_city_by_name(country,name.toLowerCase());
        }
        List data=new ArrayList();
        for (TArea ar : lst) {
            Map m1=new HashMap();
            m1.put("aid", ar.getId());
            m1.put("name", ar.getName1());
            m1.put("nameEn", ar.getName2());
            m1.put("pid", ar.getParent()==null?"--":ar.getParent().getId());
            data.add(m1);
        }
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public Map view_airports_by_city(String city) {
        TArea ar=areaRepo.findOne(city);
        List data=new ArrayList();
        if(ar!=null){
            List<TAirport> dd=airRepo.findByArea(ar);
            for (TAirport ap : dd) {
                Map m=new HashMap();
                m.put("id", ap.getId());
                m.put("name", ap.getNameZh());
                data.add(m);
            }
        }
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public Map view_europe_city_all() {
        List<TArea> lst;
        lst=areaRepo.find_europe_city_all("中国");
        List data=new ArrayList();
        for (TArea ar : lst) {
            Map m1=new HashMap();
            m1.put("aid", ar.getId());
            m1.put("name", ar.getName1());
            m1.put("nameEn", ar.getName2());
            m1.put("pid", ar.getParent()==null?"--":ar.getParent().getId());
            data.add(m1);
        }
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public Map view_exchange_rate() {
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        List data=new ArrayList();
        for(int i=0;i<MONEY_NAME.length;i++){
            Map m=new HashMap();
            String money=view_exchange_rate_byCode(MONEY_CODE[i], MONEY_CNY, "100");
            m.put("name", MONEY_NAME[i]);
            m.put("money", money);
            data.add(m);
            if(i==0){
                rm.put("main", MONEY_NAME[0]);
                rm.put("mainM", money);
            }
        }
        rm.put(DATA_KEY, data);
        rm.put("date", Utils.dateToString_byChina(new Date()));
        return rm;
    }
    
    private void load_properties(){
        prop=new Properties();
        try {
            prop.load(getClass().getResourceAsStream("/services.properties"));
        } catch (IOException ex) {
            prop=null;
        }
    }
    
    private Properties prop;

    public String getProperty(String properties){
        if(prop!=null)return prop.getProperty(properties);
        else return null;
    }

    @Override
    public Map view_logo_city() {
        List<TCityIntroduction> lst=cityRepo.findByLogo(1);
        List data=new ArrayList();
        for (TCityIntroduction ct : lst) {
            Map m=new HashMap();
            if(ct.getArea().getParent()==null)continue;
            m.put("aid", ct.getArea().getId());
            m.put("pid", ct.getArea().getParent().getId());
            m.put("url", ct.getViewUrl());
            m.put("name", ct.getArea().getName1());
            m.put("nameEn", ct.getArea().getName2());
            m.put("aname", ct.getArea().getName1());
            data.add(m);
        }
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public String view_exchange_rate_byCode(String from,String to,String price) {
        String url = clientApp.getSystemProperty(SHOW_API_URL);
        String res=new ShowApiRequest(url,clientApp.getSystemProperty(SHOW_API_APP_ID),clientApp.getSystemProperty(SHOW_API_SIGN))
		   .addTextPara("fromCode",from)
                   .addTextPara("toCode",to)
                   .addTextPara("money",price)
                   .addTextPara("code","")
		   .post();
        String money="0";
        ObjectMapper mapper = new ObjectMapper();
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        try {
            Map rm1=mapper.readValue(res, Map.class);
            Integer error=(Integer) rm1.get("showapi_res_code");
            if(error==0){
                Map m1=(Map) rm1.get("showapi_res_body");
                money=(String) m1.get("money");
            }
        } catch (IOException ex) {money="0";}
        return money;
    }

    @Override
    public Map view_provice_of_china() {
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        List<TArea> lst=areaRepo.find_provice_of_china("中国");
        List data=new ArrayList();
        if(!lst.isEmpty()){
            for (TArea ar : lst) {
                Map m=new HashMap();
                m.put("id", ar.getId());
                m.put("name", ar.getName1());
                data.add(m);
            }
        }
        rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public Map view_travel_list(Map params) {
        String area=(String) params.get("area");
        String key=(String) params.get("key");
        if(!hasText(area)){
            return raiseError(1032);
        }
        TArea ar=areaRepo.findOne(area);
        if(ar==null)return raiseError(1032);
        if(!hasText(key))key="";
        List<TTravelAgency> lst=travelRepo.findByNameZhContainingAndAreaOrderByNameZhAsc(key, ar);
        List data=new ArrayList();
        if(!lst.isEmpty()){
            for(TTravelAgency tr:lst){
                Map m=new HashMap();
                m.put("id", tr.getId());
                m.put("name", tr.getNameZh());
                data.add(m);
            }
        }
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public Map view_china_city(Map params) {
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        List<TArea> lst=areaRepo.find_city_of_china();
        List data=new ArrayList();
        if(!lst.isEmpty()){
            for (TArea ar : lst) {
                Map m=new HashMap();
                m.put("aid", ar.getId());
                m.put("name", ar.getName1());
                m.put("nameEn", ar.getName2());
                m.put("pid", ar.getParent().getId());
                data.add(m);
            }
        }
        rm.put(DATA_KEY, data);
        return rm;
    }

    /**
     * 첫페지 추천도시 정보얻기
     * @param param
     * @return 
     */
    @Override
    public Map view_home_city(Map param) {
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        List<TCityIntroduction> cc=cityRepo.findByHome(1);
        if(cc.isEmpty())return rm;
        TCityIntroduction ccc=cc.get(0);
        Map m=get_object_of_city(ccc);
        rm.put(DATA_KEY, m);
        return rm;
    }
    
    private Map get_object_of_city(TCityIntroduction city){
        TArea area=city.getArea();
        TCityRestaurant rest=restRepo.findByLogoAndArea(1, area);
        Map m=new HashMap();
        Map rm=new HashMap();
        if(rest!=null){
            m.put("id", rest.getId());
            m.put("name", Utils.valueOf(rest.getNameZh()));
            m.put("image", Utils.valueOf(rest.getViewUrl()));
            m.put("cat", rest.getCategories().getLevel());
        }else {
            m.put("id", -1);
        }
        rm.put("rest", m);
        m=new HashMap();
        TCityShop sh=shopRepo.findByLogoAndArea(1, area);
        if(sh!=null){
            m.put("id", sh.getId());
            m.put("name", Utils.valueOf(sh.getNameZh()));
            m.put("image", Utils.valueOf(sh.getViewUrl()));
        }else{
            m.put("id", -1);
        }
        rm.put("shop", m);
        List<TCityScenicSpots> lst=ssRepo.findByLogoAndArea(1, area);
        Map mm=new HashMap();
        if(!lst.isEmpty()){
            TCityScenicSpots ss=lst.get(0);
            mm.put("id", ss.getId());
            mm.put("name", Utils.valueOf(ss.getNameZh()));
            mm.put("image", Utils.valueOf(ss.getImageUrl()));
            mm.put("dcs", ss.getHasDocents());
        }else{
            mm.put("id", -1);
        }
        rm.put("ss", mm);
        rm.put("cid", area.getId());
        rm.put("city", area.getName1());
        rm.put("cityEn", area.getName2());
        rm.put("intro", Utils.valueOf(city.getIntroduction()));
        rm.put("url", Utils.valueOf(city.getViewUrl()));
        return rm;
    }
    @Override
    public Map view_europ_country(Map param) {
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        List<TArea> lst=areaRepo.find_europ_country();
        List data=new ArrayList();
        for(TArea a:lst){
            Map m=new HashMap();
            m.put("cid", a.getId());
            m.put("cname", Utils.valueOf(a.getName1()));
            data.add(m);
        }
        rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public Map view_city_object(Map param) {
        String cid=(String)param.get("cid");
        if(!hasText(cid)){
            return raiseError(1032);
        }
        TArea ar=areaRepo.findOne(cid);
        if(ar==null)return raiseError(1032);
        List<TCityIntroduction> cities=cityRepo.findByArea(ar);
        Map m = null;
        if(!cities.isEmpty()){
            m=get_object_of_city(cities.get(0));
        }
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, m);
        return rm;
    }

    @Override
    public Map view_jinsong_country() {
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        List data=new ArrayList();
        List<TArea> lst=areaRepo.findByCflagAndLevelOrderBySortAsc(1, 1);
        for (TArea ar : lst) {
           Map m=new HashMap();
           m.put("id", ar.getId());
           m.put("name", ar.getName1());
           data.add(m); 
        }
        rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public Map view_jinsong_city(Map param) {
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        String cat=(String) param.get("cid");
        if(cat==null)return raiseError(1032);
        TArea country=areaRepo.findOne(cat);
        if(country==null)return raiseError(1032);
        List<TArea> lst=areaRepo.findByCflagAndParentAndLevelOrderBySortAsc(1, country, 2);
        List data=new ArrayList();
        for (TArea ar : lst) {
           Map m=new HashMap();
           m.put("cid", ar.getId());
           m.put("cname", ar.getName1());
           m.put("count", ar.getPcount());
           data.add(m); 
        }
        rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public Map view_jinsong_items(Map param) {
       Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
       String cid=(String) param.get("cid");
       Integer level=(Integer) param.get("level");
       String parent=(String) param.get("pid");
       if(cid==null)return raiseError(1032);
       TArea ar=areaRepo.findOne(cid);
       if(ar==null)return raiseError(1032);
       if(level==null || level<0)return raiseError(1032);
       if(!hasText(parent) || !NumberUtils.isNumber(parent))return raiseError(1032);
       long p=Long.parseLong(parent);
       List data=new ArrayList();
       List<TProcessChargeCategory> lst;
       if(p==0)lst=jinRepo.findByAreaAndLevel(ar, level);
       else{
           TProcessChargeCategory cc=jinRepo.findOne(p);
           if(cc==null)return raiseError(1032);
           lst=jinRepo.findByAreaAndLevelAndParent(ar, level,cc);
       }
       for (TProcessChargeCategory cc : lst) {
           Map m=new HashMap();
           m.put("id", cc.getId().toString());
           if(hasText(cc.getNameEn()))m.put("name", Utils.valueOf(cc.getNameZh())+"("+cc.getNameEn()+")");
           else m.put("name", Utils.valueOf(cc.getNameZh()));
           if(cc.getPrice()==null)m.put("price", 0f);
           else m.put("price", cc.getPrice());
           m.put("level", cc.getLevel());
           if(level==0)m.put("desc", Utils.valueOf(cc.getDescriptionZh()));
           else m.put("desc", "--");
           data.add(m);
       }
       rm.put(DATA_KEY, data);
       return rm;
    }

    @Override
    public Map view_current_exchange(Map param) {
        String price=(String) param.get("price");
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        if(!hasText(price) || !NumberUtils.isNumber(price)){
            return raiseError(1032);
        }
        price=view_exchange_rate_byCode(MONEY_EUR, MONEY_CNY, price);
        rm.put("price", price);
        return rm;
    }
}
