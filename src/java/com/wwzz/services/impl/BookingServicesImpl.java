/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wwzz.pay.alipay.config.AlipayConfig;
import com.wwzz.pay.alipay.util.AlipaySubmit;
import com.wwzz.services.api.BookingServices;
import com.wwzz.services.api.MasterDataServices;
import com.wwzz.services.api.UserNoticeServices;
import com.wwzz.services.domain.base.TAirport;
import com.wwzz.services.domain.base.TArea;
import com.wwzz.services.domain.base.TCarModels;
import com.wwzz.services.domain.base.TCityRestaurant;
import com.wwzz.services.domain.base.TCityRestaurantPackage;
import com.wwzz.services.domain.base.TCityRestaurantProperty;
import com.wwzz.services.domain.base.TCityScenicSpots;
import com.wwzz.services.domain.base.TCityScenicSpotsProperty;
import com.wwzz.services.domain.base.TCityShop;
import com.wwzz.services.domain.base.TDocentsSchedule;
import com.wwzz.services.domain.base.TDocentsScheduleAssign;
import com.wwzz.services.domain.base.TDocentsScheduleItems;
import com.wwzz.services.domain.base.TProcessChargeCategory;
import com.wwzz.services.domain.base.TTourismLineCategory;
import com.wwzz.services.domain.booking.TCityRestaurantReservationRecord;
import com.wwzz.services.domain.booking.TUserBookingOrder;
import com.wwzz.services.domain.booking.TUserBookingTempOrder;
import com.wwzz.services.domain.booking.TUserOrderDocents;
import com.wwzz.services.domain.booking.TUserOrderDriver;
import com.wwzz.services.domain.booking.TUserOrderDriverFiles;
import com.wwzz.services.domain.booking.TUserOrderLocalGuide;
import com.wwzz.services.domain.booking.TUserOrderLongGuide;
import com.wwzz.services.domain.booking.TUserOrderLongGuideFiles;
import com.wwzz.services.domain.booking.TUserOrderOGuide;
import com.wwzz.services.domain.booking.TUserOrderOGuideFiles;
import com.wwzz.services.domain.booking.TUserOrderProcessCharge;
import com.wwzz.services.domain.booking.TUserOrderRestaurant;
import com.wwzz.services.domain.booking.TUserOrderShop;
import com.wwzz.services.domain.booking.TUserOrderShopFiles;
import com.wwzz.services.domain.booking.TUserOrderTicket;
import com.wwzz.services.domain.journey.TUserJourney;
import com.wwzz.services.domain.users.TUser;
import com.wwzz.services.domain.users.TUserBill;
import com.wwzz.services.domain.users.TUserCard;
import com.wwzz.services.listener.OrderEvent;
import com.wwzz.services.repository.TAirportRepository;
import com.wwzz.services.repository.TAreaRepository;
import com.wwzz.services.repository.TCarModelsRepository;
import com.wwzz.services.repository.TCityRestaurantPackageRepository;
import com.wwzz.services.repository.TCityRestaurantRecordRepository;
import com.wwzz.services.repository.TCityRestaurantRepository;
import com.wwzz.services.repository.TCityScenicSpotDocentRepository;
import com.wwzz.services.repository.TCityScenicSpotsRepository;
import com.wwzz.services.repository.TCityShopRepository;
import com.wwzz.services.repository.TDocentsScheduleAssignRepository;
import com.wwzz.services.repository.TDocentsScheduleItemsRepository;
import com.wwzz.services.repository.TDocentsScheduleRepository;
import com.wwzz.services.repository.TProcessChargeCategoryRepository;
import com.wwzz.services.repository.TTourismLineCategoryRepository;
import com.wwzz.services.repository.TUserBillRepository;
import com.wwzz.services.repository.TUserBookingOrderRepository;
import com.wwzz.services.repository.TUserBookingTempOrderRepository;
import com.wwzz.services.repository.TUserCardRepository;
import com.wwzz.services.repository.TUserJourneyRepository;
import com.wwzz.services.repository.TUserNoticeRelationRepository;
import com.wwzz.services.repository.TUserNoticeRepository;
import com.wwzz.services.repository.TUserOrderDocentsRepository;
import com.wwzz.services.repository.TUserOrderDriverFilesRepository;
import com.wwzz.services.repository.TUserOrderDriverRepository;
import com.wwzz.services.repository.TUserOrderLocalGuideRepository;
import com.wwzz.services.repository.TUserOrderLongGuideFilesRepository;
import com.wwzz.services.repository.TUserOrderLongGuideRepository;
import com.wwzz.services.repository.TUserOrderOGuideFilesRepository;
import com.wwzz.services.repository.TUserOrderOGuideRepository;
import com.wwzz.services.repository.TUserOrderProcessRepository;
import com.wwzz.services.repository.TUserOrderRestaurantRepository;
import com.wwzz.services.repository.TUserOrderShopFilesRepository;
import com.wwzz.services.repository.TUserOrderShopRepository;
import com.wwzz.services.repository.TUserOrderTicketRepository;
import com.wwzz.services.repository.TUserRepository;
import com.wwzz.services.utils.Blowfish;
import static com.wwzz.services.utils.Constants.RETURN_ERROR_CODE;
import com.wwzz.services.utils.Utils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.velocity.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * This class is implementation of BookingServices
 * @author JBOSS
 */
@Service
@Transactional
public class BookingServicesImpl extends ResultDataServices implements BookingServices, ApplicationEventPublisherAware{
    @Autowired
    private TAreaRepository areaRepo;
    @Autowired
    private TCityRestaurantRepository cityRes;
    @Autowired
    private TCityShopRepository cityShop;
    @Autowired
    private TCityScenicSpotsRepository citySS;
    @Autowired
    private TUserRepository userRepo;
    @Autowired
    private TUserCardRepository cardRepo;
    @Autowired
    private TUserBookingOrderRepository bookingRepo;
    @Autowired
    private TUserOrderShopRepository orderShop;
    @Autowired
    private TCityRestaurantPackageRepository resPkgRepo;
    @Autowired
    private TCityRestaurantRecordRepository resRcdRepo;
    @Autowired
    private TUserOrderRestaurantRepository orderRestaurant;
    @Autowired
    private TUserOrderTicketRepository ticketRepo;
    @Autowired
    private TUserOrderShopFilesRepository orderShFRepo;
    @Autowired
    private TUserOrderDocentsRepository orderDcRepo;
    @Autowired
    private TCityScenicSpotDocentRepository citySsDocRepo;
    @Autowired
    private TUserOrderLongGuideFilesRepository orderLgFRepo;
    @Autowired
    private TTourismLineCategoryRepository lineCat;
    @Autowired
    private TUserOrderLongGuideRepository orderLgRepo;
    @Autowired
    private TUserOrderOGuideFilesRepository orderOgFRepo;
    @Autowired
    private TUserOrderOGuideRepository orderOgRepo;
    @Autowired
    private TUserOrderLocalGuideRepository orderLcRepo;
    @Autowired
    private TAirportRepository airRepo;
    @Autowired
    private TUserOrderDriverRepository orderDrvRepo;
    @Autowired
    private TCarModelsRepository carRepo;
    @Autowired
    private TUserOrderDriverFilesRepository orderDrvFRepo;
    @Autowired
    private TUserBookingTempOrderRepository orderTpRepo;
    @Autowired
    private UserNoticeServices noticeServices;
    @Autowired
    private TUserJourneyRepository userJRepo;
    @Autowired
    private TProcessChargeCategoryRepository ccRepo;
    @Autowired
    private TUserOrderProcessRepository cpRepo;
    @Autowired
    private TUserNoticeRepository ntRepo;
    @Autowired
    private TUserNoticeRelationRepository nsRepo;
    @Autowired
    private TDocentsScheduleRepository dcSdlRepo;
    @Autowired
    private TDocentsScheduleItemsRepository dcSdlItRepo;
    @Autowired
    private TDocentsScheduleAssignRepository dcAsgRepo;
    @Autowired
    private TUserBillRepository billRepo;
    @Autowired
    private MasterDataServices services;
 
    /**
     * See BookingServices
     * @return 
     */
    @Override
    public Map view_country_booking() {
        List<TArea> list=areaRepo.findByFlagAndParentOrderByName1Asc(1, null);
        List data=new ArrayList();
        for (TArea ar : list) {
            Map m=new HashMap();
            m.put("aaid", ar.getId());
            m.put("acode", ar.getCode());
            m.put("aname", ar.getName1());
            data.add(m);
        }
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("data", data);
        return rm;
    }
    /**
     * See BookingServices
     * @return 
     */
    @Override
    public Map view_recommended_object_list(Map params) {
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        Integer cat=(Integer) params.get("cat");
        Integer start=(Integer) params.get("start");
        Integer count=(Integer) params.get("count");
        if(cat==null)return rm;
        if(start==null)return rm;
        if(count==null)return rm;
        if(start<0)return rm;
        if(count<0)return rm;
        count=count>20?20:count;
        Pageable page=new PageRequest(start, count);
        List data=new ArrayList();
        long total=0;
        switch(cat){
            case BOOKING_RESTURANT:
                Page<TCityRestaurant> dt=cityRes.findByHasRecommend(1, page);
                total=dt.getTotalElements();
                data=view_resturant(dt.getContent());
                break;
            case BOOKING_SCENIC_PIAO:
                Page<TCityScenicSpots> dt1=citySS.findByHasRecommend(1, page);
                total=dt1.getTotalElements();
                data=view_ss(dt1.getContent());
                break;
            case BOOKING_SCENIC_DOCENTS:
                Page<TCityScenicSpots> dt3=citySS.findByHasDocents(1, page);
                total=dt3.getTotalElements();
                data=view_ss(dt3.getContent());
                break;
            case BOOKING_SHOP:
                Page<TCityShop> dt2=cityShop.findByHasRecommend(1, page);
                total=dt2.getTotalElements();
                data=view_shop(dt2.getContent());
                break;
        }
        rm.put("data", data);
        rm.put("count", total);
        rm.put("cat", cat);
        return rm;
    }
    /**
     * See BookingServices
     * @return 
     */
    private List view_resturant(List<TCityRestaurant> data){
        List lst=new ArrayList();
        if(data!=null){
            for (int i = 0; i < data.size(); i++) {
                TCityRestaurant cc = data.get(i);
                Map m=new HashMap();
                m.put("id", cc.getId());
                m.put("area", cc.getArea().getName1());
                m.put("name", cc.getNameZh()==null?"--":cc.getNameZh());
                m.put("nameEn", cc.getNameEn()==null?"--":cc.getNameEn());
                m.put("phone", Utils.valueOf(cc.getPhone()));
                if(cc.getProperties()!=null){
                    m.put("intro", cc.getProperties().getIntroductionZh()==null?"--":cc.getProperties().getIntroductionZh());
                    m.put("addr", Utils.valueOf(cc.getProperties().getAddressZh()));
                }else{
                    m.put("intro", "--");
                    m.put("addr", "--");
                }
                m.put("url",  Utils.valueOf(cc.getImageUrl()));
                m.put("hurl", Utils.valueOf(cc.getHeaderUrl()));
                if(cc.getCategories()!=null){
                    m.put("cat", cc.getCategories().getLevel());
                    m.put("catUrl", cc.getCategories().getIconUrl());
                }else{
                    continue;
                }
                lst.add(m);
            }
        }
        return lst;
    }
    
    private List view_ss(List<TCityScenicSpots> data){
        List lst=new ArrayList();
        if(data!=null){
            for (int i = 0; i < data.size(); i++) {
                TCityScenicSpots cc = data.get(i);
                Map m=new HashMap();
                m.put("id", cc.getId());
                m.put("area", cc.getArea().getName1());
                m.put("name", cc.getNameZh()==null?"--":cc.getNameZh());
                m.put("nameEn", cc.getNameEn()==null?"--":cc.getNameEn());
                if(cc.getProperties()!=null){
                    m.put("intro", cc.getProperties().getIntroductionZh()==null?"--":cc.getProperties().getIntroductionZh());
                    m.put("a1", cc.getProperties().getAdultTicketPrice());
                    m.put("a12", cc.getProperties().getAdultService());
                    m.put("a2", cc.getProperties().getChildrenTicketPrice());
                    m.put("a22", cc.getProperties().getChildService());
                    m.put("phone", Utils.valueOf(cc.getProperties().getPhone()));
                    m.put("addr", Utils.valueOf(cc.getProperties().getLocationZh()));
                }else{
                    m.put("intro", "--");
                    m.put("a1", 0);
                    m.put("a12", 0);
                    m.put("a2", 0);
                    m.put("a22", 0);
                    m.put("phone", "--");
                    m.put("addr", "--");
                }
                m.put("url", Utils.valueOf(cc.getImageUrl()));
                m.put("hurl", Utils.valueOf(cc.getHeaderUrl()));
                m.put("dcs", cc.getHasDocents());
                lst.add(m);
            }
        }
        return lst;
    }
    
    private List view_shop(List<TCityShop> data){
        List lst=new ArrayList();
        if(data!=null){
            for (int i = 0; i < data.size(); i++) {
                TCityShop cc = data.get(i);
                Map m=new HashMap();
                m.put("id", cc.getId());
                m.put("area", cc.getArea().getName1());
                m.put("name", cc.getNameZh()==null?"--":cc.getNameZh());
                m.put("nameEn", cc.getNameEn()==null?"--":cc.getNameEn());
                if(cc.getProperties()!=null){
                    m.put("intro", cc.getProperties().getIntroductionZh()==null?"--":cc.getProperties().getIntroductionZh());
                }else{
                    m.put("intro", "--");
                }
                m.put("url", Utils.valueOf(cc.getImageUrl()));
                m.put("hurl", Utils.valueOf(cc.getHeaderUrl()));
                m.put("phone",cc.getPhone()!=null?cc.getPhone():"--");
                if(cc.getProperties()!=null){
                    m.put("addr",cc.getProperties().getAddressZh()!=null?cc.getProperties().getAddressZh():"--");
                }else m.put("addr", "--");
                m.put("t1", "--");m.put("t2", "--");
                lst.add(m);
            }
        }
        return lst;
    }
    /**
     * See BookingServices
     * @return 
     */
    @Override
    public Map view_all_city(Map params) {
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        List<TArea> lst=areaRepo.find_city();
        List data=new ArrayList();
        for (TArea oo : lst) {
            Map m=new HashMap();
            m.put("cid", oo.getId());
            m.put("cname", oo.getName1());
            m.put("sid", oo.getParent().getId());
            m.put("sname", oo.getParent().getName1());
            data.add(m);
        }
        rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public Map view_country_recommended(Map params) {
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        List<TArea> lst=areaRepo.find_city1();
        List data=new ArrayList();
        for (TArea oo : lst) {
            Map m=new HashMap();
            m.put("cid", oo.getId());
            m.put("cname", oo.getName1());
            data.add(m);
        }
        rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public Map search_object_by_city(Map params) {
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        Integer cat=(Integer) params.get("cat");
        Integer start=(Integer) params.get("start");
        Integer count=(Integer) params.get("count");
        Integer sid=(Integer) params.get("sid");
        String key=(String) params.get("key");
        String cid=(String) params.get("cid");
        if(cat==null)return rm;
        if(start==null)return rm;
        if(count==null)return rm;
        if(start<0)return rm;
        if(count<0)return rm;
        count=count>20?20:count;
        if(hasText(key)){
            key="%"+key.toLowerCase()+"%";
        }else{
            key="%%";
        }
        if(sid==null)sid=1;
        Pageable page=new PageRequest(start, count);
        List data=new ArrayList();
        long total=0;
        switch(cat){
            case BOOKING_RESTURANT:
                Page<TCityRestaurant> dt=null;
                if(sid==1)dt=cityRes.find_resturant_by_country(cid, page);
                if(sid==2)dt=cityRes.find_resturant_by_city(cid, page);
                if(sid==3)dt=cityRes.find_resturant_by_name(key, page);
                if(sid==4)dt=cityRes.find_resturant_by_name_country(key,cid,page);
                if(sid==5)dt=cityRes.find_resturant_by_name_city(key,cid,page);
                if(dt!=null){
                    total=dt.getTotalElements();
                    data=view_resturant(dt.getContent());
                }
                break;
            case BOOKING_SCENIC_PIAO:
                Page<TCityScenicSpots> dt1=null;
                if(sid==1)dt1=citySS.find_ss_by_country(cid, page);
                if(sid==2)dt1=citySS.find_ss_by_city(cid, page);
                if(sid==3)dt1=citySS.find_ss_by_name(key, page);
                if(sid==4)dt1=citySS.find_ss_by_name_country(key,cid,page);
                if(sid==5)dt1=citySS.find_ss_by_name_city(key,cid,page);
                if(dt1!=null){
                    total=dt1.getTotalElements();
                    data=view_ss(dt1.getContent());
                }
                break;
            case BOOKING_SCENIC_DOCENTS:
                Page<TCityScenicSpots> dt3=null;
                if(sid==1)dt3=citySS.find_ss_docents_by_country(cid, page);
                if(sid==2)dt3=citySS.find_ss_docents_by_city(cid, page);
                if(sid==3)dt3=citySS.find_ss_docents_by_name(key, page);
                if(sid==4)dt3=citySS.find_ss_docents_by_name_country(key,cid,page);
                if(sid==5)dt3=citySS.find_ss_docents_by_name_city(key,cid,page);
                if(dt3!=null){
                    total=dt3.getTotalElements();
                    data=view_ss(dt3.getContent());
                }
                break;
            case BOOKING_SHOP:
                Page<TCityShop> dt2=null;
                if(sid==1)dt2=cityShop.find_shop_by_country(cid, page);
                if(sid==2)dt2=cityShop.find_shop_by_city(cid, page);
                if(sid==3)dt2=cityShop.find_shop_by_name(key, page);
                if(sid==4)dt2=cityShop.find_shop_by_name_country(key,cid,page);
                if(sid==5)dt2=cityShop.find_shop_by_name_city(key,cid,page);
                if(dt2!=null){
                    total=dt2.getTotalElements();
                    data=view_shop(dt2.getContent());
                }
                break;
        }
        rm.put("data", data);
        rm.put("count", total);
        rm.put("cat", cat);
        return rm;
    }

    @Override
    public Map add_shop_card(Map params) {
        Map m=auth_apps(params, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String card=(String) params.get("cn");
        Integer sid=(Integer) params.get("sid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(!hasText(card)){
            return raiseError(1023);
        }
        if(sid==null){
            return raiseError(1022);
        }
        TCityShop shop=cityShop.findOne(Long.parseLong(sid.toString()));
        if(shop==null){
            return raiseError(1022);
        }
        TUserCard tc=cardRepo.findByUserAndShopAndCardNumber(user, shop, card);
        if(tc!=null){
            return raiseError(1024);
        }
        tc=new TUserCard();
        tc.setCardNumber(card);
        tc.setCreated(new Date());
        tc.setModified(new Date());
        tc.setShop(shop);
        tc.setUser(user);
        cardRepo.save(tc);
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map view_shop_cards(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        Integer sid=(Integer) params.get("sid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        } 
        if(sid==null){
            return raiseError(1022);
        }
        TCityShop shop=null;
        if(sid>0){
            shop=cityShop.findOne(Long.parseLong(sid.toString()));
            if(shop==null){
                return raiseError(1022);
            }
        }
        List<TUserCard> cards;
        if(shop!=null)cards=cardRepo.findByUserAndShop(user, shop);
        else cards=cardRepo.findByUser(user);
        List data=new ArrayList();
        for (TUserCard card : cards) {
            Map m1=new HashMap();
            m1.put("cid", card.getId());
            m1.put("card", card.getCardNumber());
            m1.put("shop", card.getShop().getNameZh());
            data.add(m1);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public Map remove_shop_card(Map params) {
        Map m=auth_apps(params, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String card=(String) params.get("cn");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(!hasText(card)){
            return raiseError(1023);
        }
        TUserCard tc=cardRepo.findOne(Long.parseLong(card));
        if(tc!=null){
            long cc=orderShop.countByCard(tc);
            if(cc>0){
                return raiseError(1028);
            }else cardRepo.delete(tc);
        }
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map booking_order_shop(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String sid=(String) params.get("sid");
        String cid=(String) params.get("cid");
        Integer cn=(Integer) params.get("cn");
        String bt=(String) params.get("btime");
        String desc=(String) params.get("desc");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(user.getStatus()==USER_STATE_DISABLED){
            return raiseError(1144);
        }
        if(sid==null){
            return raiseError(1022);
        }
        if(!hasText(bt) || !NumberUtils.isNumber(bt)){
            return raiseError(1026);
        }
        String jid=null;
        if(params.containsKey("jid"))jid=(String) params.get("jid");
        TUserJourney journey = null;
        if(hasText(jid) && NumberUtils.isNumber(jid)){
            journey=userJRepo.findOne(Long.parseLong(jid));
            if(journey==null)return raiseError(1032);
        }
        TCityShop shop;
        shop=cityShop.findOne(Long.parseLong(sid));
        if(shop==null){
            return raiseError(1022);
        }
        TUserCard card=null;
        if(hasText(cid)){
            card=cardRepo.findOne(Long.parseLong(cid));
            if(card==null){
                return raiseError(1023);
            }
        }
        if(cn==null){
            return raiseError(1025);
        }
        if(cn<1)return raiseError(1025);
        TUserBookingOrder booking=new TUserBookingOrder();
        booking.setApplicant(user);
        booking.setSerialNumber("SD-"+Long.toString(System.currentTimeMillis()));
        booking.setApplicationTime(new Date());
        booking.setCategory(BOOKING_CATEGORY_SHOP);
        booking.setType(BOOKING_TYPE_2);
        booking.setStatus(BOOKING_STATE_5);
        booking.setExpectedAmount(0f);
        booking.setCreated(new Date());
        booking.setModified(new Date());
        booking.setClient(clientApp.getType(pt));
        TUserOrderShop osh=new TUserOrderShop();
        osh.setBeginTime(new Date(Long.parseLong(bt)));
        osh.setBookingNumber(cn);
        if(card!=null)osh.setCard(card);
        osh.setBooking(booking);
        osh.setShop(shop);
        StringBuilder sb=new StringBuilder();
        sb.append("[").append(shop.getNameZh()).append("] ");
        sb.append(osh.getShop().getOpenTime());
        booking.setTitle(sb.toString());
        booking.setServiceTime(osh.getBeginTime());
        booking.setArea(osh.getShop().getArea());
        if(journey!=null)booking.setJourney(journey);
        if(hasText(desc))osh.setRequirements(desc);
        orderShop.save(osh);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map booking_order_restaurant(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        Integer cat=(Integer) params.get("cat");
        String rid=(String) params.get("rid");
        String pkg=(String) params.get("pkg");
        Integer cn=(Integer) params.get("total");
        Integer c1=(Integer) params.get("c1");
        Integer c2=(Integer) params.get("c2");
        Integer st=(Integer) params.get("style");
        String dt=(String) params.get("dt");
        String desc=(String) params.get("desc");
        
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(user.getStatus()==USER_STATE_DISABLED){
            return raiseError(1144);
        }
        if(!hasText(rid)){
            return raiseError(1029);
        }
        if(cat==null){
            return raiseError(1032);
        }
        if(cat<1 || st>cat){
            return raiseError(1032);
        }
        String jid=null;
        if(params.containsKey("jid"))jid=(String) params.get("jid");
        TUserJourney journey = null;
        if(hasText(jid) && NumberUtils.isNumber(jid)){
            journey=userJRepo.findOne(Long.parseLong(jid));
            if(journey==null)return raiseError(1032);
        }
        TCityRestaurant rt=cityRes.findOne(Long.parseLong(rid));
        if(rt==null){
            return raiseError(1029);
        }
        if(st==null){
            return raiseError(1032);
        }
        if(st<1 || st>2){
            return raiseError(1032);
        }
        long time;
        try{
            time=Long.parseLong(dt);
        }catch(NumberFormatException ex){
            return raiseError(1033);
        }
        Date dtt=new Date(time);
        if(!hasText(pkg)){
            return raiseError(1030);
        }
        TCityRestaurantPackage pkg1=resPkgRepo.findOne(Long.parseLong(pkg));
        if(pkg1==null){
            return raiseError(1030);
        }
        if(cn==null || cn<1){
            return raiseError(1031);
        }
        TUserBookingOrder booking=new TUserBookingOrder();
        booking.setApplicant(user);
        booking.setSerialNumber("CT-"+Long.toString(System.currentTimeMillis()));
        booking.setApplicationTime(new Date());
        booking.setCategory(BOOKING_CATEGORY_RESTURANT);
        booking.setType(BOOKING_TYPE_2);
        //booking.setStatus(BOOKING_STATE_2);
        booking.setStatus(BOOKING_STATE_5);
        booking.setOrderAmount(cn*pkg1.getAdultPrice());
        booking.setCreated(new Date());
        booking.setModified(new Date());
        booking.setClient(clientApp.getType(pt));
        if(journey!=null)booking.setJourney(journey);
        TUserOrderRestaurant oor=new TUserOrderRestaurant();
        oor.setBooking(booking);
        oor.setBookingTime(dtt);
        StringBuilder sb=new StringBuilder();
        sb.append("[").append(rt.getNameZh()).append("] ");
        sb.append(Utils.dateToString(oor.getBookingTime(), "-")).append(")");
        booking.setTitle(sb.toString());
        booking.setServiceTime(dtt);booking.setArea(rt.getArea());
        if(hasText(desc))oor.setRequirement(desc);
        oor.setRestaurant(rt);
        oor.setStyle(st);
        if(cat==1){
            oor.setVegetarianNumber(c2==null?0:c2);
            oor.setHalalNumber(c1==null?0:12);
        }else{
            oor.setVegetarianNumber(0);
            oor.setHalalNumber(0);
        }
        oor.setPackSet(pkg1);
        orderRestaurant.save(oor);
        TCityRestaurantReservationRecord ccr=resRcdRepo.findByRestaurantAndReservationTime(rt, dtt);
        if(ccr==null){
            ccr=new TCityRestaurantReservationRecord();
            ccr.setReservationTime(dtt);
            ccr.setRestaurant(rt);
            ccr.setTotal(cn);
        }else{
            ccr.setTotal(ccr.getTotal()+cn);
        }
        resRcdRepo.save(ccr);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map view_city_recommended(Map params) {
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        List<TArea> lst=areaRepo.find_city2();
        List data=new ArrayList();
        for (TArea oo : lst) {
            Map m=new HashMap();
            m.put("cid", oo.getId());
            m.put("cname", oo.getName1());
            m.put("sid", oo.getParent().getId());
            m.put("sname", oo.getParent().getName1());
            data.add(m);
        }
        rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public Map view_restaurant_set(Map params) {
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        String rid=(String) params.get("rid");
        if(!hasText(rid)){
            return raiseError(1029);
        }
        TCityRestaurant rt=cityRes.findOne(Long.parseLong(rid));
        if(rt==null){
            return raiseError(1029);
        }
        List<TCityRestaurantPackage> lst=resPkgRepo.findByRestaurant(rt);
        List data=new ArrayList();
        for (TCityRestaurantPackage pkg : lst) {
            Map m=new HashMap();
            m.put("id", pkg.getId());
            m.put("name", pkg.getNameZh());
            m.put("price", pkg.getAdultPrice());
            m.put("total", pkg.getReservationNumber());
            data.add(m);
        }
        rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public Map booking_order_ticket(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String sid=(String) params.get("sid");
        Integer cn=(Integer) params.get("total");
        Integer cn1=(Integer) params.get("total1");
        String dt1=(String) params.get("dt1");
        String dt2=(String) params.get("dt2");
        String desc=(String) params.get("desc");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(user.getStatus()==USER_STATE_DISABLED){
            return raiseError(1144);
        }
        if(!hasText(sid)){
            return raiseError(1034);
        }
        String jid=null;
        if(params.containsKey("jid"))jid=(String) params.get("jid");
        TUserJourney journey = null;
        if(hasText(jid) && NumberUtils.isNumber(jid)){
            journey=userJRepo.findOne(Long.parseLong(jid));
            if(journey==null)return raiseError(1032);
        }
        TCityScenicSpots rt=citySS.findOne(Long.parseLong(sid));
        if(rt==null){
            return raiseError(1034);
        }
        long time,time1;
        try{
            time=Long.parseLong(dt1);
            time1=Long.parseLong(dt2);
        }catch(NumberFormatException ex){
            return raiseError(1033);
        }
        Date dtt=new Date(time);
        Date dtt1=new Date(time1);
        if(cn==null || cn<1){
            return raiseError(1031);
        }
        TUserBookingOrder booking=new TUserBookingOrder();
        booking.setApplicant(user);
        booking.setSerialNumber("MP-"+Long.toString(System.currentTimeMillis()));
        booking.setApplicationTime(new Date());
        booking.setCategory(BOOKING_CATEGORY_SS_PIAO);
        booking.setType(BOOKING_TYPE_2);
        //booking.setStatus(BOOKING_STATE_2);
        booking.setStatus(BOOKING_STATE_5);
        booking.setClient(clientApp.getType(pt));
        if(journey!=null)booking.setJourney(journey);
        float price=0f;
        if(rt.getProperties()!=null){
            price=cn*rt.getProperties().getAdultTicketPrice();
            price=price+cn*rt.getProperties().getAdultService();
        }
        if(cn1!=null){
            if(rt.getProperties()!=null){
                price=price+cn1*rt.getProperties().getChildService();
                price=price+cn1*rt.getProperties().getChildrenTicketPrice();
            }
        }
        booking.setExpectedAmount(price);
        booking.setCreated(new Date());
        booking.setModified(new Date());
        TUserOrderTicket oor=new TUserOrderTicket();
        oor.setScenic(rt);
        oor.setBooking(booking);
        oor.setEnterTime(dtt);
        oor.setGetTime(dtt1);
        oor.setAdultsNumber(cn);
        oor.setChildrenNumber(cn1!=null?cn1:0);
        StringBuilder sb=new StringBuilder();
        sb.append(oor.getScenic().getNameZh());
        sb.append("-").append(Utils.dateToString1(oor.getEnterTime()));
        booking.setTitle(sb.toString());booking.setServiceTime(dtt1);
        booking.setArea(oor.getScenic().getArea());
        if(hasText(desc))oor.setRequirement(desc);
        ticketRepo.save(oor);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map view_restaurant_record(Map params) {
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        String rid=(String) params.get("rid");
        String dt=(String) params.get("dt");
        if(!hasText(rid)){
            return raiseError(1029);
        }
        TCityRestaurant rt=cityRes.findOne(Long.parseLong(rid));
        if(rt==null){
            return raiseError(1029);
        }
        if(!hasText(dt)){
            return raiseError(1033);
        }
        long time;
        try{
            time=Long.parseLong(dt);
        }catch(NumberFormatException ex){
            return raiseError(1033);
        }
        TCityRestaurantReservationRecord record=resRcdRepo.findByRestaurantAndReservationTime(rt, new Date(time));
        if(record==null){
            rm.put("amount", 0);
        }else rm.put("amount", record.getTotal());
        return rm;
    }

    @Override
    public Map view_shop_by_city(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String cid=(String) params.get("cid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(!hasText(cid)){
            return raiseError(1035);
        }
        TArea ar=areaRepo.findOne(cid);
        if(ar==null){
            return raiseError(1035);
        }

        List<TCityShop> shops = cityShop.findByArea(ar);
        List data=new ArrayList();
        for (TCityShop card : shops) {
            Map m=new HashMap();
            m.put("cid", card.getId());
            m.put("sname",card.getNameZh());
            m.put("phone",card.getPhone()!=null?card.getPhone():"--");
            if(card.getProperties()!=null){
                m.put("addr",card.getProperties().getAddressZh()!=null?card.getProperties().getAddressZh():"--");
            }else m.put("addr", "--");
            m.put("t1", "--");m.put("t2", "--");
            data.add(m);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public Map view_user_cards(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        List<TUserCard> cards = cardRepo.findByUser(user);
        List data=new ArrayList();
        for (TUserCard card : cards) {
            Map m=new HashMap();
            m.put("cid", card.getId());
            m.put("card", card.getCardNumber());
            m.put("shop", card.getShop().getNameZh());
            m.put("phone",card.getShop().getPhone()!=null?card.getShop().getPhone():"--");
            if(card.getShop().getProperties()!=null){
                m.put("addr",card.getShop().getProperties().getAddressZh()!=null?card.getShop().getProperties().getAddressZh():"--");
            }else m.put("addr", "--");
            m.put("t1", "--");m.put("t2", "--");
            m.put("area", card.getShop().getArea().getName1());
            data.add(m);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        return rm; 
    }

    @Override
    public Map generate_conversion_id(Map params) {
        String uid=UUID.randomUUID().toString();
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("ccid", uid);
        return rm;
    }

    @Override
    public Map add_shop_order_file(Map param) {
        Map rm=auth_apps(param,2);
        if(!isAppAuth(rm))return rm;
        String pt=(String) param.get("partner");
        String cid=(String) param.get("cid");
        String store=(String) param.get("store");
        String view=(String) param.get("view");
        if(!hasText(cid)){
            return raiseError(1004);
        }
        if(!hasText(view)){
            return raiseError(1004);
        }
        if(!hasText(store)){
            return raiseError(1004);
        }
        TUserOrderShopFiles ff=new TUserOrderShopFiles();
        ff.setId(UUID.randomUUID().toString());
        ff.setCid(cid);
        ff.setStoragePath(store);
        ff.setViewUrl(view);
        orderShFRepo.save(ff);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("view", ff.getViewUrl());
        rm.put("fid", ff.getId());
        return rm;
    }

    @Override
    public Map remove_shop_order_file(Map param) {
        Map rm=auth_apps(param,2);
        if(!isAppAuth(rm))return rm;
        String pt=(String) param.get("partner");
        String cid=(String) param.get("cid");
        if(!hasText(cid)){
            return raiseError(1032);
        }
        TUserOrderShopFiles sf=orderShFRepo.findOne(cid);
        if(sf!=null){
            file_delete_on_fastdfs(sf.getStoragePath());
            orderShFRepo.delete(sf);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map booking_order_docents(Map params) {
        Map m=auth_apps(params, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(user.getStatus()==USER_STATE_DISABLED){
            return raiseError(1144);
        }
        String sid=(String) params.get("sid");
        String vdate=(String) params.get("vd");
        String ddate=(String) params.get("dd");
        Integer flag=(Integer) params.get("flag");
        Integer cn1=(Integer) params.get("cn1");
        Integer cn2=(Integer) params.get("cn2");
        Integer tflag=(Integer) params.get("tflag");
        String ssid=(String) params.get("ssid");
        Integer gender=(Integer) params.get("gender");
        Integer nt=(Integer) params.get("nt");
        String desc=(String) params.get("desc");
        String jid=null;
        if(params.containsKey("jid"))jid=(String) params.get("jid");
        TUserJourney journey = null;
        if(hasText(jid) && NumberUtils.isNumber(jid)){
            journey=userJRepo.findOne(Long.parseLong(jid));
            if(journey==null)return raiseError(1032);
        }
        if(!hasText(sid)){
            return raiseError(1036);
        }
        TCityScenicSpots ss=citySS.findOne(Long.parseLong(sid));
        if(ss==null){
            return raiseError(1036);
        }
        if(ss.getProperties()==null){
            return raiseError(1036);
        }
        if(!hasText(vdate)){
            return raiseError(1035);
        }
        long time;
        try{
            time=Long.parseLong(vdate);
        }catch(NumberFormatException ex){
            return raiseError(1035);
        }
        if(flag==null)return raiseError(1038);
        if(flag<1 || flag>2)return raiseError(1038);
        String[] sp = null;
        if(flag==1){
            if(!hasText(ddate)){
                return raiseError(1040);
            }
            sp=ddate.split(":");
            if(sp.length!=2){
                return raiseError(1040);
            }
            if(gender==null){
                return raiseError(1042);
            }else if(gender<1 || gender>3){
                return raiseError(1042);
            }
            if(nt==null){
                return raiseError(1043);
            }else if(nt<1 || nt>3){
                return raiseError(1043);
            }
        }
        TDocentsScheduleItems ddc = null;
        List<TDocentsScheduleAssign> ddcs;
        if(flag==2){
          if(!hasText(ssid) || !NumberUtils.isNumber(ssid)){
              return raiseError(1040);
          }
          ddc=dcSdlItRepo.findOne(Long.parseLong(ssid));
          if(ddc==null){
              return raiseError(1040);
          }
          ddcs=dcAsgRepo.findByScheduleAndDocentsTimeOrderByDocents_IdAsc(ddc.getSchedule(), ddc.getDocentTime());
          if(ddcs.isEmpty()){
              return raiseError(1040);
          }
        }
        if(cn1==null || cn2==null){
            return raiseError(1038);
        }else if(cn1<1 && cn2<1){
            return raiseError(1038);
        }
        if(tflag==null)return raiseError(1041);
        if(tflag<0 || tflag>1)return raiseError(1041);
        TUserBookingOrder booking=new TUserBookingOrder();
        booking.setApplicant(user);
        booking.setSerialNumber("SSDC-"+Long.toString(System.currentTimeMillis()));
        booking.setApplicationTime(new Date());
        booking.setClient(clientApp.getType(pt));
        booking.setCategory(BOOKING_CATEGORY_DOCENTS);
        booking.setType(flag==2?BOOKING_TYPE_2:BOOKING_TYPE_1);
        //booking.setStatus(flag==1?BOOKING_STATE_1:BOOKING_STATE_2);
        booking.setStatus(BOOKING_STATE_1);
        if(flag==2){
            booking.setExecutor(userRepo.find_admin());
        }
        float price=0;if(cn2==null)cn2=0;
        if(flag==1){
            if(ss.getProperties()!=null){
                price=cn1*ss.getProperties().getAdultTicketPrice();
                price=price+cn1*ss.getProperties().getAdultService();
            }

            if(ss.getProperties()!=null){
                price=price+cn2*ss.getProperties().getChildService();
                price=price+cn2*ss.getProperties().getChildrenTicketPrice();
            }

        }else{
            price=cn1*ddc.getSchedule().getAdultPrice();
            price=price+cn2*ddc.getSchedule().getChildrenPrice();
        }
        Calendar cal=Calendar.getInstance();
        if(flag==2)booking.setOrderAmount(price);
        booking.setExpectedAmount(price);
        booking.setCreated(new Date());
        booking.setModified(new Date());
        booking.setArea(ss.getArea());
        TUserOrderDocents oor=new TUserOrderDocents();
        oor.setBooking(booking);
        if(flag==2){
            booking.setExecutor(userRepo.find_admin());
            oor.setSchedule(ddc.getSchedule());
        }
        oor.setVisitDate(new Date(time));
        oor.setNeedTicket(tflag);
        oor.setAdultsNumber(cn1);
        oor.setChildrenNumber(cn2);
        oor.setScenic(ss);
        oor.setType(flag);
        
        if(flag==1){
            oor.setGender(gender);oor.setNationality(nt);
            cal.setTime(oor.getVisitDate());
            cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sp[0]));
            cal.set(Calendar.MINUTE, Integer.parseInt(sp[1]));
            oor.setExplainTime(Utils.timeToString(cal.getTime()));
            booking.setServiceTime(cal.getTime());
            if(hasText(desc))oor.setRequirements(desc);
        }else{
            oor.setExplainTime(ddc.getDocentTime());
            cal=Calendar.getInstance();
            cal.setTime(ddc.getSchedule().getDocentDate());
            String[] sps=ddc.getDocentTime().split(":");
            cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sps[0]));
            cal.set(Calendar.MINUTE, Integer.parseInt(sps[1]));
            booking.setServiceTime(cal.getTime());
        }
        StringBuilder sb=new StringBuilder();
        sb.append(getResource("title.booking.docents",Utils.dateToString(oor.getVisitDate(), "-"),
              oor.getExplainTime(),oor.getScenic().getNameZh(),SS_ORDER_TYPE[oor.getType()-1]));
        booking.setTitle(sb.toString());
        if(journey!=null)booking.setJourney(journey);
        //booking.setServiceTime(oor.getVisitDate());
        orderDcRepo.save(oor);
        if(ddc!=null){
            int n=ddc.getReserverNumber()+cn1+cn2;
            if(n>ddc.getTotalNumber()){
                return raiseError(1081);
            }else{
                ddc.setReserverNumber(ddc.getReserverNumber()+cn1+cn2);
                ddc.setRemainNumber(ddc.getTotalNumber()-ddc.getReserverNumber());
            }
            dcSdlItRepo.save(ddc);
        }
        if(flag==2){
            TUserBookingTempOrder tp=new TUserBookingTempOrder();
            tp.setBooking(booking); tp.setCreated(new Date());
            tp.setModified(new Date());tp.setSubmissionTime(new Date());
            tp.setStatus(1);tp.setBidAmount(booking.getExpectedAmount());
            tp.setBidder(userRepo.find_admin());
            orderTpRepo.save(tp);
        }
        publisher.publishEvent(new OrderEvent(noticeServices, BOOKING_NOTICE, booking));
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        m.put("sn", booking.getSerialNumber());
        return m;
    }

    @Override
    public Map view_ss_docents_schedule(Map params) {
        Map rm=auth_apps(params, 2);
        if(!isAppAuth(rm))return rm;
        String pt=(String) params.get("partner");
        String sid=(String) params.get("sid");
        String vdate=(String) params.get("vd");
        if(!hasText(sid) && !NumberUtils.isNumber(sid)){
            return raiseError(1036);
        }
        TCityScenicSpots ss=citySS.findOne(Long.parseLong(sid));
        if(ss==null){
            return raiseError(1036);
        }
        
        if(!hasText(vdate)){
            return raiseError(1037);
        }
        long time;
        try{
            time=Long.parseLong(vdate);
        }catch(NumberFormatException ex){
            return raiseError(1037);
        }
        Sort sort=new Sort(Sort.Direction.ASC, "apm","docentItems");
        TDocentsSchedule dcsdl=dcSdlRepo.findByScenicAndDocentDate(ss, new Date(time));
        List<TDocentsScheduleItems> items=dcSdlItRepo.findByScheduleOrderByDocentTimeAsc(dcsdl);
        List data=new ArrayList();
        for (TDocentsScheduleItems cc : items) {
            Map m=new HashMap();
            m.put("id", cc.getId().toString());
            m.put("apm", cc.getApm());
            m.put("name", cc.getDocentTime()+" "+getResource("short.title", Integer.toString(cc.getRemainNumber())));
            m.put("price", Utils.priceToString(cc.getSchedule().getAdultPrice()));
            m.put("price1", Utils.priceToString(cc.getSchedule().getChildrenPrice()));
            m.put("rm", cc.getRemainNumber());
            data.add(m);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public Map view_search_items(Map params) {
        Map rm=auth_apps(params, 2);
        if(!isAppAuth(rm))return rm;
        String sid=(String) params.get("sid");
        Integer cat=(Integer) params.get("cat");
        if(cat==null)return raiseError(1032);
        if(!hasText(sid)){
            return raiseError(1032);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        switch(cat){
            case BOOKING_RESTURANT:
                TCityRestaurant dt=cityRes.findOne(Long.parseLong(sid));
                if(dt!=null){
                    Map m=new HashMap();
                    m.put("id", dt.getId());
                    m.put("area", dt.getArea().getName1());
                    m.put("name", dt.getNameZh()==null?"--":dt.getNameZh());
                    m.put("nameEn", dt.getNameEn()==null?"--":dt.getNameEn());
                    if(dt.getProperties()!=null){
                        m.put("intro", dt.getProperties().getIntroductionZh()==null?"--":dt.getProperties().getIntroductionZh());
                    }else{
                        m.put("intro", "--");
                    }
                    m.put("url", dt.getImageUrl()==null?"--":dt.getImageUrl());
                    m.put("hurl", dt.getHeaderUrl()==null?"--":dt.getHeaderUrl());
                    m.put("cat", dt.getCategories().getLevel());
                    m.put("catUrl", dt.getCategories().getIconUrl());
                    m.put("phone", Utils.valueOf(dt.getPhone()));
                    TCityRestaurantProperty rp=dt.getProperties();
                    if(rp!=null)m.put("addr", Utils.valueOf(rp.getAddressZh()));
                    else m.put("addr", " ");
                    rm.put(DATA_KEY, m);
                }
                break;
            case BOOKING_SCENIC_DOCENTS:
                TCityScenicSpots dt1=citySS.findOne(Long.parseLong(sid));
                if(dt1!=null){
                   Map m=new HashMap();
                    m.put("id", dt1.getId());
                    m.put("area", dt1.getArea().getName1());
                    m.put("name", dt1.getNameZh()==null?"--":dt1.getNameZh());
                    m.put("nameEn", dt1.getNameEn()==null?"--":dt1.getNameEn());
                    if(dt1.getProperties()!=null){
                        m.put("intro", dt1.getProperties().getIntroductionZh()==null?"--":dt1.getProperties().getIntroductionZh());
                        m.put("a1", dt1.getProperties().getAdultTicketPrice());
                        m.put("a12", dt1.getProperties().getAdultService());
                        m.put("a2", dt1.getProperties().getChildrenTicketPrice());
                        m.put("a22", dt1.getProperties().getChildService());
                    }else{
                        m.put("intro", "--");
                        m.put("a1", 0);
                        m.put("a12", 0);
                        m.put("a2", 0);
                        m.put("a22", 0);
                    }
                    m.put("url", dt1.getImageUrl()==null?"--":dt1.getImageUrl());
                    m.put("hurl", dt1.getHeaderUrl()==null?"--":dt1.getHeaderUrl());
                    m.put("dcs", dt1.getHasDocents());
                    TCityScenicSpotsProperty pps=dt1.getProperties();
                    if(pps!=null){
                        m.put("phone", Utils.valueOf(pps.getPhone()));
                        m.put("addr", Utils.valueOf(pps.getLocationZh()));
                    }else {
                        m.put("phone", "  ");
                        m.put("addr", "  ");
                    }
                    rm.put(DATA_KEY, m);
                }
                break;
            case BOOKING_SHOP:
                TCityShop cc=cityShop.findOne(Long.parseLong(sid));
                if(cc!=null){
                   Map m=new HashMap();
                    m.put("id", cc.getId());
                    m.put("area", cc.getArea().getName1());
                    m.put("name", cc.getNameZh()==null?"--":cc.getNameZh());
                    m.put("nameEn", cc.getNameEn()==null?"--":cc.getNameEn());
                    if(cc.getProperties()!=null){
                        m.put("intro", cc.getProperties().getIntroductionZh()==null?"--":cc.getProperties().getIntroductionZh());
                    }else{
                        m.put("intro", "--");
                    }
                    m.put("url", cc.getImageUrl()==null?"--":cc.getImageUrl());
                    m.put("hurl", cc.getHeaderUrl()==null?"--":cc.getHeaderUrl());
                    m.put("phone",cc.getPhone()!=null?cc.getPhone():"--");
                    if(cc.getProperties()!=null){
                        m.put("addr",cc.getProperties().getAddressZh()!=null?cc.getProperties().getAddressZh():"--");
                    }else m.put("addr", "--");
                    m.put("t1", "--");m.put("t2", "--");
                    rm.put(DATA_KEY, m);
                }
                break;
        }
        return rm;
    }

    @Override
    public Map view_shop_card_detail(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String sid=(String) params.get("sid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        } 
        if(sid==null){
            return raiseError(1044);
        }
        TUserCard card=cardRepo.findOne(Long.parseLong(sid));
        Map m1=new HashMap();
        m1.put("cid", card.getId().toString());
        m1.put("card", card.getCardNumber());
        m1.put("shop", card.getShop().getNameZh());
        m1.put("phone", card.getShop().getPhone());
        if(card.getShop().getProperties()!=null){
            m1.put("addr", card.getShop().getProperties().getAddressZh());
        }else{
            m1.put("addr", "--");
        }
        m1.put("t1", "--");m1.put("t2", "--");
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, m1);
        return rm;
    }

    @Override
    public Map add_lg_order_file(Map param) {
        Map rm=auth_apps(param,2);
        if(!isAppAuth(rm))return rm;
        String pt=(String) param.get("partner");
        String cid=(String) param.get("cid");
        String store=(String) param.get("store");
        String view=(String) param.get("view");
        if(!hasText(cid)){
            return raiseError(1004);
        }
        if(!hasText(view)){
            return raiseError(1004);
        }
        if(!hasText(store)){
            return raiseError(1004);
        }
        TUserOrderLongGuideFiles ff=new TUserOrderLongGuideFiles();
        ff.setId(UUID.randomUUID().toString());
        ff.setConversion(cid);
        ff.setStoragePath(store);
        ff.setViewUrl(view);
        orderLgFRepo.save(ff);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("view", ff.getViewUrl());
        rm.put("fid", ff.getId());
        return rm;
    }

    @Override
    public Map remove_lg_order_file(Map param) {
        Map rm=auth_apps(param,2);
        if(!isAppAuth(rm))return rm;
        String pt=(String) param.get("partner");
        String cid=(String) param.get("cid");
        if(!hasText(cid)){
            return raiseError(1032);
        }
        TUserOrderLongGuideFiles sf=orderLgFRepo.findOne(cid);
        if(sf!=null){
            file_delete_on_fastdfs(sf.getStoragePath());
            orderLgFRepo.delete(sf);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map booking_lg_order(Map params) {
        Map m=auth_apps(params, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(user.getStatus()==USER_STATE_DISABLED){
            return raiseError(1144);
        }
        String cid=(String) params.get("cid");
        String lcat=(String) params.get("lcat");
        String oa=(String) params.get("oa");
        String ia=(String) params.get("ia");
        String odate=(String) params.get("odate");
        String edate=(String) params.get("edate");
        Integer nt=(Integer) params.get("nt");
        Integer tcount=(Integer) params.get("tcount");
        Integer acount=(Integer) params.get("acount");
        Integer cn=(Integer) params.get("cn");
        Integer f1=(Integer) params.get("f1");
        Integer f2=(Integer) params.get("f2");
        String desc=(String) params.get("desc");
        if(cid==null){
            return raiseError(1032);
        }
        if(!hasText(lcat)){
            return raiseError(1045);
        }
        String[] cats=lcat.split("-");
        if(cats.length<1){
            return raiseError(1045);
        }
        if(nt==null){
            return raiseError(1046);
        }else if(nt<1 || nt>4){
            return raiseError(1046);
        }
        if(tcount==null){
            return raiseError(1047);
        }else if(tcount<1)return raiseError(1047);
        if(acount==null){
            return raiseError(1048);
        }else if(tcount<1)return raiseError(1048);
        if(!hasText(oa)){
            return raiseError(1049);
        }
        if(!hasText(ia)){
            return raiseError(1050);
        }
        if(!hasText(odate)){
            return raiseError(1051);
        }
        if(!hasText(edate)){
            return raiseError(1052);
        }
        if(cn==null){
            return raiseError(1053);
        }else if(cn<1)return raiseError(1053);
        if(f1==null){
            return raiseError(1054);
        }else if(f1<1 || f1>2)return raiseError(1054);
        if(f2==null){
            return raiseError(1055);
        }else if(f2<1 || f2>2)return raiseError(1055);
        long time,time1;
        try{
            time=Long.parseLong(odate);
        }catch(NumberFormatException ex){
            return raiseError(1056);
        }
        try{
            time1=Long.parseLong(edate);
        }catch(NumberFormatException ex){
            return raiseError(1056);
        }
        TUserOrderLongGuide oog=new TUserOrderLongGuide();
        TArea ar=areaRepo.findOne(oa);
        if(ar==null){
            return raiseError(1049);
        }else{
            oog.setDepartureExitCity(ar);
        }
        TArea ar1=areaRepo.findOne(ia);
        if(ar1==null){
            return raiseError(1050);
        }else{
            oog.setDepartureEntryCity(ar1);
        }
        TUserBookingOrder booking=new TUserBookingOrder();
        booking.setApplicant(user);
        booking.setSerialNumber("LG-"+Long.toString(System.currentTimeMillis()));
        booking.setApplicationTime(new Date());
        booking.setCategory(BOOKING_CATEGORY_LG);
        booking.setType(BOOKING_TYPE_1);
        booking.setStatus(BOOKING_STATE_1);
        booking.setCreated(new Date());
        booking.setModified(new Date());
        booking.setClient(clientApp.getType(pt));
        List<TTourismLineCategory> ccs=new ArrayList<>();
        for (String cat : cats) {
            TTourismLineCategory cc=lineCat.findOne(Long.parseLong(cat));
            if(cc!=null)ccs.add(cc);
        }
        oog.setLineCategories(ccs);
        oog.setBooking(booking);oog.setDayNumber(tcount);oog.setCountryNumber(acount);
        oog.setBeginTime(new Date(time));
        oog.setBookingNumber(cn);    oog.setEndTime(new Date(time1));   oog.setNature(nt);
        if(hasText(desc))oog.setRequirement(desc);
        oog.setNeedLeaderCertificate(f1);
        oog.setNeedEmploymentCertificate(f2);
        StringBuilder sb=new StringBuilder();
        sb.append(getResource("title.booking.lg",Utils.dateToString(oog.getBeginTime(), "-"),
                Integer.toString(oog.getCountryNumber()),Integer.toString(oog.getDayNumber())));
        booking.setTitle(sb.toString());
        booking.setServiceTime(oog.getBeginTime());
        //if(orderLgFRepo.countByConversion(cid)>0)oog.setConversion(cid);
        orderLgRepo.save(oog);
        publisher.publishEvent(new OrderEvent(noticeServices, BOOKING_NOTICE, booking));
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        m.put("sn", booking.getSerialNumber());
        return m;
    }

    @Override
    public Map add_og_order_file(Map param) {
        Map rm=auth_apps(param,2);
        if(!isAppAuth(rm))return rm;
        String pt=(String) param.get("partner");
        String cid=(String) param.get("cid");
        String store=(String) param.get("store");
        String view=(String) param.get("view");
        if(!hasText(cid)){
            return raiseError(1004);
        }
        if(!hasText(view)){
            return raiseError(1004);
        }
        if(!hasText(store)){
            return raiseError(1004);
        }
        TUserOrderOGuideFiles ff=new TUserOrderOGuideFiles();
        ff.setId(UUID.randomUUID().toString());
        ff.setConversion(cid);
        ff.setStoragePath(store);
        ff.setViewUrl(view);
        orderOgFRepo.save(ff);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("view", ff.getViewUrl());
        rm.put("fid", ff.getId());
        return rm;
    }

    @Override
    public Map remove_og_order_file(Map param) {
        Map rm=auth_apps(param,2);
        if(!isAppAuth(rm))return rm;
        String pt=(String) param.get("partner");
        String cid=(String) param.get("cid");
        if(!hasText(cid)){
            return raiseError(1032);
        }
        TUserOrderOGuideFiles sf=orderOgFRepo.findOne(cid);
        if(sf!=null){
            file_delete_on_fastdfs(sf.getStoragePath());
            orderOgFRepo.delete(sf);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map booking_og_order(Map params) {
        Map m=auth_apps(params, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(user.getStatus()==USER_STATE_DISABLED){
            return raiseError(1144);
        }
        String cid=(String) params.get("cid");
        String lcat=(String) params.get("lcat");
        String oa=(String) params.get("oa");
        String ia=(String) params.get("ia");
        String oa1=(String) params.get("oa1");
        String odate=(String) params.get("odate");
        String edate=(String) params.get("edate");
        String idate=(String) params.get("idate");
        String odate1=(String) params.get("odate1");
        Integer nt=(Integer) params.get("nt");
        Integer tcount=(Integer) params.get("tcount");
        Integer acount=(Integer) params.get("acount");
        Integer cn=(Integer) params.get("cn");
        Integer f1=(Integer) params.get("f1");
        String desc=(String) params.get("desc");
        if(cid==null){
            return raiseError(1032);
        }
        if(!hasText(lcat)){
            return raiseError(1045);
        }
        String[] cats=lcat.split("-");
        if(cats.length<1){
            return raiseError(1045);
        }
        if(nt==null){
            return raiseError(1046);
        }else if(nt<1 || nt>4){
            return raiseError(1046);
        }
        if(tcount==null){
            return raiseError(1047);
        }else if(tcount<1)return raiseError(1047);
        if(acount==null){
            return raiseError(1048);
        }else if(tcount<1)return raiseError(1048);
        if(!hasText(oa)){
            return raiseError(1049);
        }
        if(!hasText(ia)){
            return raiseError(1050);
        }
        if(!hasText(oa1)){
            return raiseError(1057);
        }
        if(!hasText(odate)){
            return raiseError(1051);
        }
        if(!hasText(edate)){
            return raiseError(1052);
        }
        if(!hasText(idate)){
            return raiseError(1058);
        }
        if(!hasText(odate1)){
            return raiseError(1059);
        }
        if(cn==null){
            return raiseError(1053);
        }else if(cn<1)return raiseError(1053);
        if(f1==null){
            return raiseError(1054);
        }else if(f1<1 || f1>2)return raiseError(1054);
        long time,time1,time2,time3;
        try{
            time=Long.parseLong(odate);
        }catch(NumberFormatException ex){
            return raiseError(1056);
        }
        try{
            time1=Long.parseLong(edate);
        }catch(NumberFormatException ex){
            return raiseError(1056);
        }
        try{
            time2=Long.parseLong(idate);
        }catch(NumberFormatException ex){
            return raiseError(1058);
        }
        try{
            time3=Long.parseLong(odate1);
        }catch(NumberFormatException ex){
            return raiseError(1059);
        }
        TUserOrderOGuide oog=new TUserOrderOGuide();
        TArea ar=areaRepo.findOne(oa);
        if(ar==null){
            return raiseError(1049);
        }else{
            oog.setDepartureExitCity(ar);
        }
        TArea ar1=areaRepo.findOne(ia);
        if(ar1==null){
            return raiseError(1050);
        }else{
            oog.setDepartureEntryCity(ar1);
        }
        TArea ar2=areaRepo.findOne(oa1);
        if(ar2==null){
            return raiseError(1057);
        }else{
            oog.setReturnExitCity(ar2);
        }
        TUserBookingOrder booking=new TUserBookingOrder();
        booking.setApplicant(user);
        booking.setSerialNumber("LG-"+Long.toString(System.currentTimeMillis()));
        booking.setApplicationTime(new Date());
        booking.setCategory(BOOKING_CATEGORY_OG);
        booking.setType(BOOKING_TYPE_1);
        booking.setStatus(BOOKING_STATE_1);
        booking.setCreated(new Date());
        booking.setModified(new Date());
        booking.setClient(clientApp.getType(pt));
        List<TTourismLineCategory> ccs=new ArrayList<>();
        for (String cat : cats) {
            TTourismLineCategory cc=lineCat.findOne(Long.parseLong(cat));
            if(cc!=null)ccs.add(cc);
        }
        oog.setLineCategories(ccs);
        oog.setBooking(booking);oog.setDayNumber(tcount);
        oog.setCountryNumber(acount);
        oog.setBeginTime(new Date(time));
        oog.setBookingNumber(cn);    oog.setEndTime(new Date(time1));   oog.setNature(nt);
        oog.setDepartureEntryTime(new Date(time2));
        oog.setReturnExitTime(new Date(time3));
        if(hasText(desc))oog.setRequirement(desc);
        oog.setNeedLeaderCertificate(f1);
        StringBuilder sb=new StringBuilder();
        sb.append(getResource("title.booking.lg",Utils.dateToString(oog.getBeginTime(), "-"),
                Integer.toString(oog.getCountryNumber()),Integer.toString(oog.getDayNumber())));
        booking.setTitle(sb.toString());
        booking.setServiceTime(oog.getBeginTime());
        if(orderOgFRepo.countByConversion(cid)>0)oog.setConversion(cid);
        orderOgRepo.save(oog);
        //noticeServices.booking_notice(booking);
        publisher.publishEvent(new OrderEvent(noticeServices, BOOKING_NOTICE, booking));
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        m.put("sn", booking.getSerialNumber());
        return m;
    }

    @Override
    public Map booking_lc_order(Map params) {
        Map m=auth_apps(params, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(user.getStatus()==USER_STATE_DISABLED){
            return raiseError(1144);
        }
        String jid=null;
        if(params.containsKey("jid"))jid=(String) params.get("jid");
        TUserJourney journey = null;
        if(hasText(jid) && NumberUtils.isNumber(jid)){
            journey=userJRepo.findOne(Long.parseLong(jid));
            if(journey==null)return raiseError(1032);
        }
        String area=(String) params.get("area");
        String start=(String) params.get("sdate");
        String end=(String) params.get("edate");
        Integer cn=(Integer) params.get("cn");
        Integer f1=(Integer) params.get("f1");
        String desc=(String) params.get("desc");
        if(!hasText(area)){
            return raiseError(1060);
        }
        TArea ar=areaRepo.findOne(area);
        if(ar==null){
            return raiseError(1060);
        }else if(ar.getLevel()!=2){
            return raiseError(1060);
        }
        if(!hasText(start)){
            return raiseError(1061);
        }
        if(!hasText(end)){
            return raiseError(1062);
        }
        if(cn==null){
            return raiseError(1063);
        }else if(cn<1) return raiseError(1063);
        if(f1==null){
            return raiseError(1064);
        }else if(f1<1 || f1>2)return raiseError(1064);
        
        long time,time1;
        try{
            time=Long.parseLong(start);
        }catch(NumberFormatException ex){
            return raiseError(1061);
        }
        try{
            time1=Long.parseLong(end);
        }catch(NumberFormatException ex){
            return raiseError(1062);
        }
        TUserOrderLocalGuide oog=new TUserOrderLocalGuide();
        TUserBookingOrder booking=new TUserBookingOrder();
        booking.setApplicant(user);
        booking.setSerialNumber("LC-"+Long.toString(System.currentTimeMillis()));
        booking.setApplicationTime(new Date());
        booking.setCategory(BOOKING_CATEGORY_LC);
        booking.setType(BOOKING_TYPE_1);
        booking.setStatus(BOOKING_STATE_1);
        booking.setCreated(new Date());
        booking.setModified(new Date());
        booking.setClient(clientApp.getType(pt));
        oog.setArea(ar);
        oog.setBooking(booking);
        oog.setBeginTime(new Date(time));
        oog.setBookingNumber(cn);    
        oog.setEndTime(new Date(time1));   
        if(hasText(desc))oog.setRequirement(desc);
        oog.setNeedGuideCertificate(f1);
        StringBuilder sb=new StringBuilder();
        sb.append(getResource("title.booking.lc",oog.getArea().getParent().getName1(),oog.getArea().getName1()));
        booking.setTitle(sb.toString());booking.setArea(oog.getArea());
        if(journey!=null)booking.setJourney(journey);
        booking.setServiceTime(oog.getBeginTime());
        orderLcRepo.save(oog);
        //noticeServices.booking_notice(booking);
        publisher.publishEvent(new OrderEvent(noticeServices, BOOKING_NOTICE, booking));
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        m.put("sn", booking.getSerialNumber());
        return m;
    }
    /**
     * 预约接送司机
     * @param params
     * @return 
     */
    @Override
    public Map booking_driver_order(Map params) {
        Integer cat=(Integer)params.get("cat");
        if(cat==null){
            return raiseError(1065);
        }
        if(cat==4){
            return booking_driver_1(params);
        }else if(cat==5){
            return booking_driver_2(params);
        }else{
            return booking_driver_3(params);
        }
    }
    
    private Map booking_driver_1(Map params){
        Map m=auth_apps(params, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(user.getStatus()==USER_STATE_DISABLED){
            return raiseError(1144);
        }
        String jid=null;
        if(params.containsKey("jid"))jid=(String) params.get("jid");
        TUserJourney journey = null;
        if(hasText(jid) && NumberUtils.isNumber(jid)){
            journey=userJRepo.findOne(Long.parseLong(jid));
            if(journey==null)return raiseError(1032);
        }
        String area=(String) params.get("area");
        String air=(String) params.get("air");
        String fn=(String) params.get("fn");
        String dt=(String) params.get("dtime");
        Integer cn=(Integer) params.get("cn");
        String here=(String) params.get("here");
        if(!hasText(area)){
            return raiseError(1066);
        }
        TArea ar=areaRepo.findOne(area);
        if(ar==null){
            return raiseError(1066);
        }
        if(!hasText(air)){
            return raiseError(1067);
        }
        TAirport ap=airRepo.findOne(Long.parseLong(air));
        if(ap==null){
            return raiseError(1067);
        }
        if(!hasText(fn)){
            return raiseError(1068);
        }
        if(!hasText(dt)){
            return raiseError(1069);
        }
        long time;
        try{
            time=Long.parseLong(dt);
        }catch(NumberFormatException ex){
            return raiseError(1069);
        }
        if(cn==null){
            return raiseError(1063);
        }else if(cn<1)return raiseError(1063);
        if(!hasText(here)){
            return raiseError(1070);
        }
        TUserOrderDriver dd=new TUserOrderDriver();
        dd.setCategory(2);dd.setType(4);
        dd.setStartingCity(ar);
        dd.setStartingAddress(here);
        dd.setAirport(ap);
        dd.setFlight(fn);
        dd.setBookingNumber(cn);
        dd.setBeginTime(new Date(time));
        TUserBookingOrder booking=new TUserBookingOrder();
        booking.setApplicant(user);
        booking.setSerialNumber("DRV-"+Long.toString(System.currentTimeMillis()));
        booking.setApplicationTime(new Date());
        booking.setCategory(BOOKING_CATEGORY_DRV_1);
        booking.setType(BOOKING_TYPE_1);
        booking.setStatus(BOOKING_STATE_1);
        booking.setCreated(new Date());
        booking.setModified(new Date());
        booking.setClient(clientApp.getType(pt));
        StringBuilder sb=new StringBuilder();
        String ss=dd.getStartingCity().getParent().getName1()+" "+dd.getStartingCity().getName1();
        sb.append(getResource("title.booking.drv.1",ss,dd.getFlight(),DRV_ORDER_TYPE[3]));
        booking.setTitle(sb.toString());
        if(journey!=null)booking.setJourney(journey);
        booking.setServiceTime(dd.getBeginTime());booking.setArea(ar);
        dd.setBooking(booking);
        orderDrvRepo.save(dd);
        //noticeServices.booking_notice(booking);
        publisher.publishEvent(new OrderEvent(noticeServices, BOOKING_NOTICE, booking));
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        m.put("sn", booking.getSerialNumber());
        return m;
    }
    
    private Map booking_driver_2(Map params){
        Map m=auth_apps(params, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(user.getStatus()==USER_STATE_DISABLED){
            return raiseError(1144);
        }
        String jid=null;
        if(params.containsKey("jid"))jid=(String) params.get("jid");
        TUserJourney journey = null;
        if(hasText(jid) && NumberUtils.isNumber(jid)){
            journey=userJRepo.findOne(Long.parseLong(jid));
            if(journey==null)return raiseError(1032);
        }
        String area=(String) params.get("area");
        String car=(String) params.get("car");
        String dt=(String) params.get("dtime");
        String et=(String) params.get("etime");
        Integer cn=(Integer) params.get("cn");
        String here=(String) params.get("here");
        String here1=(String) params.get("here1");
        String desc=(String) params.get("desc");
        if(!hasText(area)){
            return raiseError(1066);
        }
        TArea ar=areaRepo.findOne(area);
        if(ar==null){
            return raiseError(1066);
        }
        if(!hasText(car)){
            return raiseError(1071);
        }
        TCarModels cm=carRepo.findOne(Integer.parseInt(car));
        if(cm==null){
            return raiseError(1071);
        }
        if(!hasText(dt)){
            return raiseError(1069);
        }
        long time,time1;
        try{
            time=Long.parseLong(dt);
        }catch(NumberFormatException ex){
            return raiseError(1069);
        }
        if(!hasText(et)){
            return raiseError(1072);
        }
        try{
            time1=Long.parseLong(et);
        }catch(NumberFormatException ex){
            return raiseError(1072);
        }
        if(cn==null){
            return raiseError(1063);
        }else if(cn<1)return raiseError(1063);
        if(!hasText(here)){
            return raiseError(1073);
        }
        if(!hasText(here1)){
            return raiseError(1074);
        }
        TUserOrderDriver dd=new TUserOrderDriver();
        dd.setCategory(2);dd.setType(5);
        dd.setStartingCity(ar);
        dd.setStartingAddress(here);
        dd.setEndAddress(here1);
        dd.setEndTime(new Date(time1));
        dd.setBookingNumber(cn);
        dd.setBeginTime(new Date(time));
        if(hasText(desc)){
            dd.setRequirement(desc);
        }
        dd.setCarModel(cm);
        TUserBookingOrder booking=new TUserBookingOrder();
        booking.setApplicant(user);
        booking.setSerialNumber("DRV-"+Long.toString(System.currentTimeMillis()));
        booking.setApplicationTime(new Date());
        booking.setCategory(BOOKING_CATEGORY_DRV_1);
        booking.setType(BOOKING_TYPE_1);
        booking.setStatus(BOOKING_STATE_1);
        booking.setCreated(new Date());
        booking.setModified(new Date());
        booking.setClient(clientApp.getType(pt));
        StringBuilder sb=new StringBuilder();
        String ss=dd.getStartingCity().getParent().getName1()+" "+dd.getStartingCity().getName1();
        sb.append(getResource("title.booking.drv.2",ss,DRV_ORDER_TYPE[4]));
        booking.setTitle(sb.toString());
        if(journey!=null)booking.setJourney(journey);
        booking.setServiceTime(dd.getBeginTime());booking.setArea(ar);
        dd.setBooking(booking);
        orderDrvRepo.save(dd);
        //noticeServices.booking_notice(booking);
        publisher.publishEvent(new OrderEvent(noticeServices, BOOKING_NOTICE, booking));
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        m.put("sn", booking.getSerialNumber());
        return m;
    }
    
    private Map booking_driver_3(Map params){
        Map m=auth_apps(params, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(user.getStatus()==USER_STATE_DISABLED){
            return raiseError(1144);
        }
        String jid=null;
        if(params.containsKey("jid"))jid=(String) params.get("jid");
        TUserJourney journey = null;
        if(hasText(jid) && NumberUtils.isNumber(jid)){
            journey=userJRepo.findOne(Long.parseLong(jid));
            if(journey==null)return raiseError(1032);
        }
        Integer type=(Integer) params.get("type");
        if(type==null){
            return raiseError(1032);
        }
        TUserOrderDriver dd=new TUserOrderDriver();
        String area=(String) params.get("area");
        if(type!=3){
            if(!hasText(area)){
                return raiseError(1066);
            }
            TArea ar=areaRepo.findOne(area);
            if(ar==null){
                return raiseError(1066);
            }
            if(type==1)dd.setStartingCity(ar);
            if(type==2)dd.setArea(ar);
        }
        String here=(String) params.get("here");
        String here1=(String) params.get("here1");
        if(!hasText(here)){
            return raiseError(1073);
        }
        if(!hasText(here1)){
            return raiseError(1074);
        }
        if(type==1){
            dd.setStartingAddress(here);
            dd.setEndAddress(here1);
        }    
        if(type==2 || type==3){
            TArea ar2=areaRepo.findOne(here);
            TArea ar3=areaRepo.findOne(here1);
            dd.setStartingCity(ar2);
            dd.setEndCity(ar3);
        }
         if(type==3){
            String via=(String) params.get("via");
            if(!hasText(via)){
                return raiseError(1079);
            }
            dd.setVia(via);
         }
        String car=(String) params.get("car");
        String dt=(String) params.get("dtime");
        String et=(String) params.get("etime");
        Integer cn=(Integer) params.get("cn");
        Integer nl=(Integer) params.get("nl");
        Integer ng=(Integer) params.get("ng");
        String pr=(String) params.get("price");
        String desc=(String) params.get("desc");
        
        if(!hasText(car)){
            return raiseError(1071);
        }
        TCarModels cm=carRepo.findOne(Integer.parseInt(car));
        if(cm==null){
            return raiseError(1071);
        }
        if(!hasText(dt)){
            return raiseError(1069);
        }
        long time,time1;
        try{
            time=Long.parseLong(dt);
        }catch(NumberFormatException ex){
            return raiseError(1069);
        }
        if(!hasText(et)){
            return raiseError(1072);
        }
        try{
            time1=Long.parseLong(et);
        }catch(NumberFormatException ex){
            return raiseError(1072);
        }
        if(cn==null){
            return raiseError(1063);
        }else if(cn<1)return raiseError(1063);
        if(nl==null){
            return raiseError(1077);
        }else if(nl<1 || nl>2)return raiseError(1075);
        if(ng==null){
            return raiseError(1078);
        }else if(ng<1 || ng>2)return raiseError(1078);
        float price=0f;
        if(!hasText(pr) || !NumberUtils.isNumber(pr)){
            return raiseError(1075);
        }else {
            price=Float.parseFloat(pr);
            if(price<=0)return raiseError(1075);
        }
        dd.setCategory(1);dd.setType(type);
        dd.setEndTime(new Date(time1));
        dd.setBookingNumber(cn);
        dd.setBeginTime(new Date(time));
        if(hasText(desc)){
            dd.setRequirement(desc);
        }
        dd.setCarModel(cm);
        dd.setNeedGuide(ng);
        dd.setNeedLicenses(nl);
        TUserBookingOrder booking=new TUserBookingOrder();
        booking.setApplicant(user);
        booking.setSerialNumber("DRV-"+Long.toString(System.currentTimeMillis()));
        booking.setApplicationTime(new Date());
        booking.setCategory(BOOKING_CATEGORY_DRV_2);
        booking.setType(BOOKING_TYPE_1);
        booking.setStatus(BOOKING_STATE_1);
        booking.setCreated(new Date());
        booking.setModified(new Date());
        booking.setExpectedAmount(price);
        booking.setArea(dd.getStartingCity());
        booking.setClient(clientApp.getType(pt));
        StringBuilder sb=new StringBuilder();
        sb.append(getResource("title.booking.drv.3",Utils.dateToString1(dd.getBeginTime()),
                Utils.dateToString1(dd.getEndTime()),DRV_ORDER_TYPE[type-1]));
        booking.setTitle(sb.toString());
        if(journey!=null)booking.setJourney(journey);
        booking.setServiceTime(dd.getBeginTime());
        //if(orderDrvFRepo.countByConversion(cid)>0)dd.setConversion(cid);
        dd.setBooking(booking);
        orderDrvRepo.save(dd);
        //noticeServices.booking_notice(booking);
        publisher.publishEvent(new OrderEvent(noticeServices, BOOKING_NOTICE, booking));
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        m.put("sn", booking.getSerialNumber());
        return m;
    }

    @Override
    public Map add_driver_order_file(Map param) {
        Map rm=auth_apps(param,2);
        if(!isAppAuth(rm))return rm;
        String pt=(String) param.get("partner");
        String cid=(String) param.get("cid");
        String store=(String) param.get("store");
        String view=(String) param.get("view");
        if(!hasText(cid)){
            return raiseError(1004);
        }
        if(!hasText(view)){
            return raiseError(1004);
        }
        if(!hasText(store)){
            return raiseError(1004);
        }
        TUserOrderDriverFiles ff=new TUserOrderDriverFiles();
        ff.setId(UUID.randomUUID().toString());
        ff.setConversion(cid);
        ff.setStoragePath(store);
        ff.setViewUrl(view);
        orderDrvFRepo.save(ff);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("view", ff.getViewUrl());
        rm.put("fid", ff.getId());
        return rm;
    }

    @Override
    public Map remove_driver_order_file(Map param) {
        Map rm=auth_apps(param,2);
        if(!isAppAuth(rm))return rm;
        String pt=(String) param.get("partner");
        String cid=(String) param.get("cid");
        if(!hasText(cid)){
            return raiseError(1032);
        }
        TUserOrderDriverFiles sf=orderDrvFRepo.findOne(cid);
        if(sf!=null){
            file_delete_on_fastdfs(sf.getStoragePath());
            orderDrvFRepo.delete(sf);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map view_my_booking_notice(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String id=(String) params.get("bbid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(!hasText(id)){
            return raiseError(1032);
        }
        if(!hasText(id) || !NumberUtils.isNumber(id)){
            return raiseError(1032);
        }
        TUserBookingOrder booking=bookingRepo.findOne(Long.parseLong(id));
        if(booking==null)return raiseError(1032);
        Map data=new HashMap();
        if(orderTpRepo.findByBidderAndBooking(user, booking)!=null){
            data.put("bidded", 1);
        }else{
            data.put("bidded", 0);
        }
        data.put("bbid", booking.getId().toString());
        data.put("cnt","--");
        data.put("title",BOOKING_CATEGORIES[booking.getCategory()-1]);
        data.put("cat",booking.getCategory()>2?1:0);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public Map add_bidding(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String id=(String) params.get("bbid");
        String mn=(String) params.get("money");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(!hasText(id)){
            return raiseError(1032);
        }
        if(!hasText(id) || !NumberUtils.isNumber(id)){
            return raiseError(1032);
        }
        TUserBookingOrder booking=bookingRepo.findOne(Long.parseLong(id));
        if(booking==null){
            return raiseError(1032);
        }
        if(booking.getStatus()!=BOOKING_STATE_1){
            if(booking.getStatus()>=BOOKING_STATE_2 && booking.getStatus()<=BOOKING_STATE_5){
                return raiseError(1150);
            }else{
                return raiseError(1149);
            }
        }
        TUserBookingTempOrder tmp=orderTpRepo.findByBidderAndBooking(user, booking);
        if(tmp!=null){
           return raiseError(1140); 
        }
       // if(booking.getStatus()!=)
        float money=0f;
        if(booking.getCategory()>2){
            if(!hasText(mn) || !NumberUtils.isNumber(mn)){
                return raiseError(1122);
            }
            money=Float.parseFloat(mn);
            if(money<0)return raiseError(1122);
        }
       // if(booking.getStatus())
        tmp=new TUserBookingTempOrder();
        tmp.setBooking(booking);tmp.setCreated(new Date());
        tmp.setModified(new Date());tmp.setBidder(user);
        if(booking.getCategory()>2 && booking.getStatus()==BOOKING_STATE_1){
            tmp.setBidAmount(money);
        }else tmp.setBidAmount(0f);
        tmp.setSubmissionTime(new Date());
        tmp.setStatus(0);
        orderTpRepo.save(tmp);
        publisher.publishEvent(new OrderEvent(noticeServices, BID_NOTICE, tmp));
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map cancel_my_booking(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String id=(String) params.get("bbid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(!hasText(id)){
            return raiseError(1032);
        }
        if(!hasText(id) || !NumberUtils.isNumber(id)){
            return raiseError(1032);
        }
        TUserBookingOrder booking=bookingRepo.findOne(Long.parseLong(id));
        if(booking==null)return raiseError(1032);
        if(!booking.getApplicant().equals(user))return raiseError(1032);
        if(booking.getStatus()>BOOKING_STATE_2){
            return raiseError(1124);
        }else{
            booking.setStatus(BOOKING_STATE_7);
            booking.setModified(new Date());
            bookingRepo.save(booking);
            publisher.publishEvent(new OrderEvent(noticeServices, SYSTEM_NOTICE_CATEGORY_1, booking));
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map confirm_guide_bidder(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String id=(String) params.get("bbid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(!hasText(id) || !NumberUtils.isNumber(id)){
            return raiseError(1032);
        }
        TUserBookingTempOrder booking=orderTpRepo.findOne(Long.parseLong(id));
        if(booking==null)return raiseError(1032);
        if(!booking.getBooking().getApplicant().equals(user))return raiseError(1032);
        TUserBookingOrder bk=booking.getBooking();
        booking.setStatus(1);
        if(bk.getCategory()==BOOKING_CATEGORY_LG || 
           bk.getCategory()==BOOKING_CATEGORY_LC ||
           bk.getCategory()==BOOKING_CATEGORY_DOCENTS ||
           bk.getCategory()==BOOKING_CATEGORY_SS_PIAO ||
           bk.getCategory()==BOOKING_CATEGORY_DRV_1 ||
           bk.getCategory()==BOOKING_CATEGORY_DRV_2 ||
           bk.getCategory()==BOOKING_CATEGORY_OG)
         bk.setStatus(BOOKING_STATE_5);
        else bk.setStatus(BOOKING_STATE_2);
        bk.setExecutor(booking.getBidder());
        bk.setTradedTime(new Date());
        bk.setModified(new Date());
        bk.setOrderAmount(booking.getBidAmount());
        booking.setModified(new Date());
        bookingRepo.save(bk);
        orderTpRepo.save(booking);
        Map m=new HashMap();
        m.put("booking", bk);m.put("state", 1);
        publisher.publishEvent(new OrderEvent(noticeServices, MESSAGE_TYPE_BOOKING_PAY_STATE, m));
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map combine_with_journey(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String bid=(String) params.get("bid");
        String bbid=(String) params.get("bbid");
        String jid=(String) params.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        if(!hasText(bid) || !NumberUtils.isNumber(bid)){
            return raiseError(1032);
        }
        TUserBookingOrder bk=bookingRepo.findOne(Long.parseLong(bid));
        if(bk==null)return raiseError(1032);
        if(bk.getJourney()!=null){
            return raiseError(1143);
        }
        TUserJourney jr=userJRepo.findOne(Long.parseLong(jid));
        if(jr==null)return raiseError(1125);
        int n=bookingRepo.count_by_journey(jr.getId());
        if(n>0){
            return raiseError(1143);
        }
        if(jr.getTarget()!=null){
            if(jr.getTarget().equals(user)){
                return raiseError(1142);
            }
        }
        if(bk.getExecutor()==null){
            if(!hasText(bbid) || !NumberUtils.isNumber(bbid)){
                return raiseError(1032);
            }
            TUserBookingTempOrder tp=orderTpRepo.findOne(Long.parseLong(bbid));
            if(tp==null)return raiseError(1032);
            tp.setStatus(1);tp.setModified(new Date());
            if(bk.getCategory()==BOOKING_CATEGORY_LG || bk.getCategory()==BOOKING_CATEGORY_OG)
             bk.setStatus(BOOKING_STATE_5);
            else bk.setStatus(BOOKING_STATE_2);
            bk.setExecutor(tp.getBidder());
            bk.setTradedTime(new Date());
            orderTpRepo.save(tp);
            bookingRepo.save(bk);
        }
        bk.setJourney(jr);
        bk.setModified(new Date());
        bookingRepo.save(bk);
        jr.setTarget(bk.getExecutor());
        userJRepo.save(jr);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher aep) {
        this.publisher=aep;
    }
    private ApplicationEventPublisher publisher;

    @Override
    public Map booking_process_charge(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String vcname=(String) params.get("vc");
        String tnum=(String) params.get("tnum");
        String cpaddr=(String) params.get("cpaddr");
        String cptaf=(String) params.get("taf");
        String cphone=(String) params.get("cphone");
        String email=(String) params.get("email");
        String fax=(String) params.get("fax");
        String pid=(String) params.get("pid");
        String price=(String) params.get("price");
        String ar=(String) params.get("area");
        String dt=(String) params.get("date");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(user.getStatus()==USER_STATE_DISABLED){
            return raiseError(1144);
        }
        if(!hasText(ar))return raiseError(1032);
        TArea area=areaRepo.findOne(ar);
        if(area==null)return raiseError(1138);
        if(area.getLevel()!=2)return raiseError(1138);
        if(!hasText(dt) || !NumberUtils.isNumber(dt))return raiseError(1137);
        if(!hasText(pid) || !NumberUtils.isNumber(pid))return raiseError(1032);
        TProcessChargeCategory cc=ccRepo.findOne(Long.parseLong(pid));
        if(cc==null)return raiseError(1032);
        if(!hasText(vcname))return raiseError(1129);
        if(!hasText(tnum))return raiseError(1130);
        if(!hasText(cpaddr))return raiseError(1131);
        if(!hasText(cptaf))return raiseError(1132);
        if(!hasText(cphone))return raiseError(1133);
        if(!hasText(email))return raiseError(1134);
        if(!hasText(fax))return raiseError(1135);
        if(!hasText(price) || !NumberUtils.isNumber(price))return raiseError(1136);
        TUserOrderProcessCharge op=new TUserOrderProcessCharge();
        TUserBookingOrder booking=new TUserBookingOrder();
        booking.setApplicant(user);
        booking.setApplicationTime(new Date());
        booking.setArea(area);
        booking.setCategory(BOOKING_CATEGORY_PP);
        booking.setCreated(new Date());
        booking.setExecutor(userRepo.find_admin());
        booking.setExpectedAmount(Float.parseFloat(price));
        booking.setOrderAmount(Float.parseFloat(price));
        booking.setModified(new Date());
        booking.setClient(clientApp.getType(pt));
        booking.setSerialNumber("PC-"+Long.toString(System.currentTimeMillis()));
        booking.setServiceTime(new Date(Long.parseLong(dt)));
        String tit=getResource("booking.jin.title", area.getParent().getName1(),area.getName1(),
                Utils.dateToString(booking.getServiceTime(), "-"));
        booking.setTitle(tit);booking.setStatus(BOOKING_STATE_2);
        booking.setType(BOOKING_TYPE_2);
        op.setArea(area);op.setBooking(booking);
        op.setBookingDate(new Date(Long.parseLong(dt)));
        op.setCompanyAddress(cpaddr);
        op.setCompanyPhone(cphone);
        op.setCompanyTax(cptaf);
        op.setProcessCharge(cc);
        op.setReceiveEmail(email);
        op.setReceiveFax(fax);
        op.setVehicleCompanyName(vcname);
        op.setVehicleLicence(tnum);
        cpRepo.save(op);
        Map m=new HashMap();
        m.put("booking", booking);m.put("state", 1);
        publisher.publishEvent(new OrderEvent(noticeServices, MESSAGE_TYPE_BOOKING_PAY_STATE, m));
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("sn", booking.getSerialNumber());
        return rm;
    }

    @Override
    public void alipay_post_process(String uid, String pt, String bid) {
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return;
        }
        if(bid!=null){
            TUserBookingOrder booking=bookingRepo.findBySerialNumber(bid);
            if(booking!=null){
                booking.setModified(new Date());
                switch(booking.getCategory()){
                    case BOOKING_CATEGORY_PP:
                    case BOOKING_CATEGORY_RESTURANT:
                        booking.setStatus(BOOKING_STATE_5);
                        break;
                    case BOOKING_CATEGORY_DOCENTS:
                        TUserOrderDocents dc=orderDcRepo.findByBooking(booking);
                        if(dc.getType()==2)
                            booking.setStatus(BOOKING_STATE_5);
                        else booking.setStatus(BOOKING_STATE_3);
                        break;
                    default:
                        booking.setStatus(BOOKING_STATE_3);
                        break;
                }
                bookingRepo.save(booking);
            }
        }
    }

    @Override
    public Map get_weixin_payinfo_booking(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String bid=(String) params.get("bid");
        String cat=(String) params.get("cat");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(!hasText(cat)){
            return raiseError(1032);
        }
        if(!hasText(bid)){
            return raiseError(1032);
        }
        TUserBookingOrder booking;
        if(cat.equals("sn")){
            booking=bookingRepo.findBySerialNumber(bid);
            if(booking==null)return raiseError(1032);
        }else{
            if(!NumberUtils.isNumber(bid))return raiseError(1032);
            booking=bookingRepo.findOne(Long.parseLong(bid));
            if(booking==null)return raiseError(1032);
        }
        Map m=new HashMap();
        m.put("wx_appid", clientApp.getSystemProperty("weixin.native.app.id"));
        m.put("wx_secrete", clientApp.getSystemProperty("weixin.native.app.secret"));
        m.put("wx_pay_partner", clientApp.getSystemProperty("weixin.pay.partner.mp"));
        m.put("wx_pay_partner_key", clientApp.getSystemProperty("weixin.pay.partner.mp.key"));
        m.put("wx_body", BOOKING_CATEGORIES[booking.getCategory()-1]);
        String url=clientApp.getUrl("weixin.pay.js.notify.url");
        url+=uid+"/"+pt+"/"+booking.getSerialNumber();
        m.put("wx_notify_url", url);
        String sn=org.apache.commons.lang3.StringUtils.substringBefore(booking.getSerialNumber(), "-");
        sn+="-"+Long.toString(System.currentTimeMillis());
        m.put("sn", sn);
        ObjectMapper om=new ObjectMapper();
        try {
            String ss=om.writeValueAsString(m);
            String key=clientApp.getAppKey(pt);
            Blowfish bw=new Blowfish();
            bw.setKey(key);
            String cnt=bw.encryptString(ss);
            rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
            rm.put(DATA_KEY, cnt);
        } catch (JsonProcessingException ex) {
            return raiseError(1032);
        }
        return rm;
    }

    @Override
    public void add_user_bill(String uid, String pt, String bid, String email, String no, 
            String price,String time,int type,String tid) {
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return;
        }
        if(bid!=null){
            TUserBookingOrder booking=bookingRepo.findBySerialNumber(bid);
            if(booking!=null && booking.getExecutor()!=null){
                TUserBill ub=new TUserBill();
                ub.setBooking(booking);
                ub.setCreated(new Date());
                ub.setModified(new Date());
                ub.setOrderName(BOOKING_CATEGORIES[booking.getCategory()-1]);
                ub.setOrderTime(booking.getApplicationTime());
                ub.setPayer(user.getName());
                ub.setPaymentAccount(email==null?"--":email);
                ub.setPaymentMethod(type);
                ub.setPaymentSerialNumber(no==null?"--":no);
                ub.setPaymentType(PAYMENT_TYPE_EXPORT);
                ub.setRefundAmount(0.0f);
                ub.setSerialNumber(booking.getSerialNumber());
                ub.setClient(clientApp.getType(pt));
                if(type==PAYMENT_SYSTEM_WEIXIN)ub.setWxpayTransaction(tid);
                if(price==null)ub.setTotalAmount(0.0f);
                else{
                    try{
                        Float pr=Float.parseFloat(price);
                        if(type==PAYMENT_SYSTEM_WEIXIN)ub.setTotalAmount(pr/100.0f);
                        else ub.setTotalAmount(pr);
                    }catch(NumberFormatException ex){
                        ub.setTotalAmount(0.0f);
                    }
                }
                SimpleDateFormat ss=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    Date dt=ss.parse(time);
                    ub.setPaymentTime(dt);
                } catch (ParseException ex) {
                    ub.setPaymentTime(new Date());
                }
                ub.setUser(user);
                if(booking.getCategory()==BOOKING_CATEGORY_PP || booking.getCategory()==BOOKING_CATEGORY_RESTURANT)
                  ub.setStatus(PAYMENT_STATE_2);
                else if(booking.getCategory()==BOOKING_CATEGORY_DOCENTS){
                    TUserOrderDocents dc=orderDcRepo.findByBooking(booking);
                    if(dc.getType()==2)ub.setStatus(PAYMENT_STATE_2);
                    else ub.setStatus(PAYMENT_STATE_1);
                }else ub.setStatus(PAYMENT_STATE_1);
                if(billRepo.findByUserAndBooking(user, booking)==null)billRepo.save(ub);
                //
                ub=new TUserBill();
                ub.setBooking(booking);
                ub.setCreated(new Date());
                ub.setModified(new Date());
                ub.setOrderName(BOOKING_CATEGORIES[booking.getCategory()-1]);
                ub.setOrderTime(booking.getApplicationTime());
                ub.setPayer(user.getName());
                ub.setPaymentAccount(email==null?"--":email);
                ub.setPaymentMethod(type);
                ub.setPaymentSerialNumber(no==null?"--":no);
                ub.setPaymentType(PAYMENT_TYPE_IMPORT);
                ub.setRefundAmount(0.0f);
                ub.setSerialNumber(booking.getSerialNumber());
                ub.setClient(clientApp.getType(pt));
                if(type==PAYMENT_SYSTEM_WEIXIN)ub.setWxpayTransaction(tid);
                if(price==null)ub.setTotalAmount(0.0f);
                else{
                    try{
                        Float pr=Float.parseFloat(price);
                        if(type==PAYMENT_SYSTEM_WEIXIN)ub.setTotalAmount(pr/100.0f);
                        else ub.setTotalAmount(pr);
                    }catch(NumberFormatException ex){
                        ub.setTotalAmount(0.0f);
                    }
                }
                ss=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                try {
                    Date dt=ss.parse(time);
                    ub.setPaymentTime(dt);
                } catch (ParseException ex) {
                    ub.setPaymentTime(new Date());
                }
                ub.setUser(booking.getExecutor());
                if(booking.getCategory()==BOOKING_CATEGORY_PP || booking.getCategory()==BOOKING_CATEGORY_RESTURANT)
                  ub.setStatus(PAYMENT_STATE_2);
                else if(booking.getCategory()==BOOKING_CATEGORY_DOCENTS){
                    TUserOrderDocents dc=orderDcRepo.findByBooking(booking);
                    if(dc.getType()==2)ub.setStatus(PAYMENT_STATE_2);
                    else ub.setStatus(PAYMENT_STATE_1);
                }else ub.setStatus(PAYMENT_STATE_1);
                if(billRepo.findByUserAndBooking(booking.getExecutor(), booking)==null)billRepo.save(ub);
                billRepo.save(ub);
            }
        }
    }

    @Override
    public Map alipay_batch_process(Map data) {
        Map<String, String> sParaTemp = new HashMap<>();
	sParaTemp.put("service", "batch_trans_notify");
        sParaTemp.put("partner", clientApp.getSystemProperty("alipay.partner"));
        sParaTemp.put("_input_charset", AlipayConfig.input_charset);
        sParaTemp.put("notify_url", (String)data.get("notify_url"));
        sParaTemp.put("email", clientApp.getSystemProperty("alipay.seller.id"));
        sParaTemp.put("account_name", clientApp.getSystemProperty("alipay.account.name"));
        SimpleDateFormat df=new SimpleDateFormat("yyyyMMdd");
        sParaTemp.put("pay_date", df.format(new Date()));
        sParaTemp.put("batch_no", (String) data.get("batch_no"));
        sParaTemp.put("batch_fee", (String) data.get("batch_fee"));
        sParaTemp.put("batch_num", (String) data.get("batch_num"));
        sParaTemp.put("detail_data", (String) data.get("detail_data"));
        System.out.println("Batch Param:"+sParaTemp);
        //建立请求
        String sHtmlText = AlipaySubmit.buildRequest(sParaTemp,"get","确认",clientApp.getSystemProperty("alipay.batch.partner.key"));
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, sHtmlText);
        return rm;
    }

    /**
     * 
     * @param params
     * @return 
     */
    @Override
    public Map get_weixin_payinfo_for_app(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String bid=(String) params.get("bid");
        String cat=(String) params.get("cat");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(!hasText(cat)){
            return raiseError(1032);
        }
        if(!hasText(bid)){
            return raiseError(1032);
        }
        TUserBookingOrder booking;
        if(cat.equals("sn")){
            booking=bookingRepo.findBySerialNumber(bid);
            if(booking==null)return raiseError(1032);
        }else{
            if(!NumberUtils.isNumber(bid))return raiseError(1032);
            booking=bookingRepo.findOne(Long.parseLong(bid));
            if(booking==null)return raiseError(1032);
        }
        String exrate=services.view_exchange_rate_byCode(MONEY_EUR, MONEY_CNY, "1");
        System.out.println("Exchange rate:"+exrate);
        if(exrate.equals("0")){
            return raiseError(1032);
        }
        Map m=new HashMap();
        m.put("wx_appid", clientApp.getSystemProperty("weixin.app.id"));
        m.put("wx_secrete", clientApp.getSystemProperty("weixin.app.secret"));
        m.put("wx_pay_partner", clientApp.getSystemProperty("weixin.pay.partner"));
        m.put("wx_pay_partner_key", clientApp.getSystemProperty("weixin.pay.partner.key"));
        m.put("wx_body", BOOKING_CATEGORIES[booking.getCategory()-1]);
        String url=clientApp.getUrl("weixin.pay.notify.url");
        url+=uid+"/"+pt+"/"+booking.getSerialNumber();
        m.put("wx_notify_url", url);
        Float price=Float.parseFloat(exrate)*booking.getOrderAmount();
        Integer mm=Math.round(price*100.0f);
        //m.put("wx_total_fee", Integer.toString(mm));
        m.put("wx_total_fee", "100");
        String sn=org.apache.commons.lang3.StringUtils.substringBefore(booking.getSerialNumber(), "-");
        sn+="-"+Long.toString(System.currentTimeMillis());
        m.put("wx_out_trade_no", sn);
        m.put("sn", booking.getSerialNumber());
        if(clientApp.getType(pt).equals("ios")){
            rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
            rm.put(DATA_KEY, m);
        }else{
            ObjectMapper om=new ObjectMapper();
            try {
                String ss=om.writeValueAsString(m);
                String key=clientApp.getAppKey(pt);
                Blowfish bw=new Blowfish();
                bw.setKey(key);
                String cnt=bw.encryptString(ss);
                rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
                rm.put(DATA_KEY, cnt);
            } catch (JsonProcessingException ex) {
                return raiseError(1032);
            }
        }
        return rm;
    }

    @Override
    public void alipay_batch_postprocess(String partner,String bills) {
        if(hasText(bills)){
            Blowfish bw=new Blowfish();
            bw.setKey(clientApp.getAppKey(partner));
            String bls;
            try{
                bls=bw.decryptString(bills);
            }catch(Exception ex){return;}
            String[] sp=StringUtils.split(bls, "-");
            if(sp==null)return;
            if(sp.length==0)return;
            for (int i = 0; i < sp.length; i++) {
                String s = sp[i];
                if(!NumberUtils.isNumber(s))continue;
                TUserBill bill=billRepo.findOne(Long.parseLong(s));
                if(bill!=null){
                    bill.setModified(new Date());
                    bill.setRefundAmount(bill.getTotalAmount());
                    bill.setStatus(PAYMENT_STATE_2);
                    TUserBookingOrder booking=bill.getBooking();
                    booking.setStatus(BOOKING_STATE_5);
                    booking.setModified(new Date());
                    billRepo.save(bill);
                    bookingRepo.save(booking);
                }
            }
        }
    }
}

