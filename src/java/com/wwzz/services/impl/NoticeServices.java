/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.impl;

import com.wwzz.services.api.UserNoticeServices;
import com.wwzz.services.domain.base.TArea;
import com.wwzz.services.domain.base.TCityIntroduction;
import com.wwzz.services.domain.base.TCityRestaurant;
import com.wwzz.services.domain.base.TCityScenicSpots;
import com.wwzz.services.domain.base.TCityShop;
import com.wwzz.services.domain.booking.TUserBookingOrder;
import com.wwzz.services.domain.booking.TUserBookingTempOrder;
import com.wwzz.services.domain.booking.TUserOrderDocents;
import com.wwzz.services.domain.booking.TUserOrderDriver;
import com.wwzz.services.domain.booking.TUserOrderLocalGuide;
import com.wwzz.services.domain.booking.TUserOrderLongGuide;
import com.wwzz.services.domain.booking.TUserOrderOGuide;
import com.wwzz.services.domain.booking.TUserOrderProcessCharge;
import com.wwzz.services.domain.booking.TUserOrderRestaurant;
import com.wwzz.services.domain.booking.TUserOrderShop;
import com.wwzz.services.domain.journey.TUserJourney;
import com.wwzz.services.domain.journey.TUserJourneyDaily;
import com.wwzz.services.domain.journey.TUserJourneyDailyCity;
import com.wwzz.services.domain.news.TUserNotice;
import com.wwzz.services.domain.news.TUserNoticeRelation;
import com.wwzz.services.domain.users.TUser;
import com.wwzz.services.message.MessageSender;
import com.wwzz.services.repository.TAreaRepository;
import com.wwzz.services.repository.TCityIntroductionRepository;
import com.wwzz.services.repository.TCityRestaurantRepository;
import com.wwzz.services.repository.TCityScenicSpotsRepository;
import com.wwzz.services.repository.TCityShopRepository;
import com.wwzz.services.repository.TUserBookingOrderRepository;
import com.wwzz.services.repository.TUserBookingTempOrderRepository;
import com.wwzz.services.repository.TUserJourneyDailyCityRepository;
import com.wwzz.services.repository.TUserJourneyDailyRepository;
import com.wwzz.services.repository.TUserJourneyRepository;
import com.wwzz.services.repository.TUserNoticeRelationRepository;
import com.wwzz.services.repository.TUserNoticeRepository;
import com.wwzz.services.repository.TUserOrderDocentsRepository;
import com.wwzz.services.repository.TUserOrderDriverRepository;
import com.wwzz.services.repository.TUserOrderLocalGuideRepository;
import com.wwzz.services.repository.TUserOrderLongGuideRepository;
import com.wwzz.services.repository.TUserOrderOGuideRepository;
import com.wwzz.services.repository.TUserOrderProcessRepository;
import com.wwzz.services.repository.TUserOrderRestaurantRepository;
import com.wwzz.services.repository.TUserOrderShopRepository;
import com.wwzz.services.repository.TUserRepository;
import com.wwzz.services.repository.TUserWorkTimesRepository;
import static com.wwzz.services.utils.Constants.ERROR_CODE_OK;
import static com.wwzz.services.utils.Constants.RETURN_SUCCESS_CODE;
import com.wwzz.services.utils.Utils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author JBOSS
 */
@Service
@Transactional
public class NoticeServices extends ResultDataServices implements UserNoticeServices{
    @Autowired
    private TUserBookingOrderRepository orderRepo;
    @Autowired
    private TUserNoticeRepository noticeRepo;
    @Autowired
    private TUserOrderLongGuideRepository orderLgRep;
    @Autowired
    private TUserOrderOGuideRepository orderOgRep;
    @Autowired
    private TUserOrderLocalGuideRepository orderLcRep;
    @Autowired
    private TUserOrderDocentsRepository orderDcRepo;
    @Autowired
    private TUserOrderDriverRepository orderDrRepo;
    @Autowired
    private TUserRepository userRepo;
    @Autowired
    private TUserJourneyDailyRepository journeyDRepo;
    @Autowired
    private TCityIntroductionRepository cityRepo;
    @Autowired
    private TCityRestaurantRepository restRepo;
    @Autowired
    private TCityShopRepository shopRepo;
    @Autowired
    private TCityScenicSpotsRepository ssRepo;
    @Autowired
    private TAreaRepository areaRepo;
    @Autowired
    private TUserJourneyDailyCityRepository dcRepo;
    @Autowired
    private TUserOrderShopRepository orderShRepo;
    @Autowired
    private TUserOrderRestaurantRepository orderRsRepo;
    @Autowired
    private VelocityEngine velocityEngine;
    @Autowired
    private TUserNoticeRelationRepository nsRepo;
    @Autowired
    private TUserBookingTempOrderRepository orderTRepo;
    @Autowired
    private TUserJourneyRepository userJRepo;
    @Autowired
    private MessageSender jmsSender;
    @Autowired
    private TUserOrderProcessRepository pcRepo;
    @Autowired
    private TUserWorkTimesRepository userWRepo;
    
    @PersistenceContext
    private EntityManager em;
    
    @Override
    public void order_notice(String sn) {
        if(hasText(sn)){
           TUserBookingOrder order=orderRepo.findBySerialNumber(sn);
           if(order!=null){
               
           }
        }
    }
    
    private void process_order_notice(TUserBookingOrder order){
        if(order==null){
            return;
        }
        int cat=order.getCategory();
        String sql = null;
        StringBuilder sb=new StringBuilder();
        TUserNotice notice;
        List<TUser> users=new ArrayList<>();
        Date dt1=null,dt2=null;
        switch(cat){
            case BOOKING_CATEGORY_LG:
                TUserOrderLongGuide lg=orderLgRep.findByBooking(order);
                if(lg!=null){
                    sql="select p.user from TUserIdentityLongGuide p";
                    sb.append(" where p.user.id<>:user and p.status=1 and p.user.identity.statusLg=2 and p.user.identity.hasIdentityLongGuide=1 and  p.user.status<3 and ");
                    sb.append("p.user.receivingTaskNotice=1 and ");
                    sb.append("(NOT EXISTS (select b from TUserOrderLongGuide b where b.booking.status in (3,4,5) and b.booking.executor.id=p.user.id and ((b.beginTime between :dt5 and :dt6) or (b.endTime between :dt5 and :dt6)))) and ");
                    sb.append("(NOT EXISTS (select t.user from TUserWorkTimes t where t.user.id=p.user.id and (t.workDate between :dt1 and :dt2)) or ");
                    sb.append("EXISTS (select tt.user from TUserWorkTimes tt where tt.user.id=p.user.id and tt.workDate=:dt3)) and ");
                    switch(lg.getNature()){
                        case 1:
                            sb.append("p.supportTourAssemble=1 and ");
                            break;
                        case 2:sb.append("p.supportTourIndependent=1 and ");
                            break;
                        case 3:sb.append("p.supportTourVip=1 and ");
                            break;
                    }
                    if(lg.getNeedLeaderCertificate()==1)sb.append("p.hasLeaderCertificate=1 and ");
                    if(lg.getNeedEmploymentCertificate()==1)sb.append("p.hasGuideCertificate=1 and ");
                    String s=StringUtils.substringBeforeLast(sb.toString(), " and ");
                    dt1=lg.getBeginTime();dt2=lg.getEndTime();
                    sql=sql+s;
                }
                break;
            case BOOKING_CATEGORY_OG:
                TUserOrderOGuide og=orderOgRep.findByBooking(order);
                if(og!=null){
                    sql="select p.user from TUserIdentityOGuide p";
                    sb.append(" where p.user.id<>:user and p.status=1 and p.user.identity.statusOg=2 and p.user.identity.hasIdentityOverseasLocalGuide=1 and ");
                    sb.append("p.user.status<3 and p.user.receivingTaskNotice=1 and ");
                    sb.append("(NOT EXISTS (select b from TUserOrderOGuide b where b.booking.status in (3,4,5) and b.booking.executor.id=p.user.id and ((b.beginTime between :dt5 and :dt6) or (b.endTime between :dt5 and :dt6)))) and ");
                    sb.append("(NOT EXISTS (select t.user from TUserWorkTimes t where t.user.id=p.user.id and (t.workDate between :dt1 and :dt2)) or ");
                    sb.append("EXISTS (select tt.user from TUserWorkTimes tt where tt.user.id=p.user.id and tt.workDate=:dt3)) and ");
                    //sb.append("p.supportDomestic=1 and p.supportImmigration=1 and ");
                    switch(og.getNature()){
                        case 1:
                            sb.append("p.supportTourAssemble=1 and ");
                            break;
                        case 2:sb.append("p.supportTourIndependent=1 and ");
                            break;
                        case 3:sb.append("p.supportTourVip=1 and ");
                            break;
                    }
                    if(og.getNeedEmploymentCertificate()==1)sb.append("p.hasEmploymentCertificate=1 and ");
                    String s=StringUtils.substringBeforeLast(sb.toString(), " and ");
                    sql=sql+s;
                    dt1=og.getBeginTime();dt2=og.getEndTime();
                }
                break;
            case BOOKING_CATEGORY_LC:
                TUserOrderLocalGuide lc=orderLcRep.findByBooking(order);
                if(lc!=null){
                    sql="select p.user from TUserIdentityLCGuide p";
                    sb.append(" where p.user.id<>:user and p.status=1 and p.user.identity.statusLc=2 and p.user.identity.hasIdentityLocalGuide=1 and");
                    sb.append(" p.user.status<3 and p.user.receivingTaskNotice=1 and ");
                    sb.append("(NOT EXISTS (select b from TUserOrderLocalGuide b where b.booking.status in (3,4,5) and b.booking.executor.id=p.user.id and ((b.beginTime between :dt5 and :dt6) or (b.endTime between :dt5 and :dt6)))) and ");
                    sb.append("(NOT EXISTS (select t.user from TUserWorkTimes t where t.user.id=p.user.id and (t.workDate between :dt1 and :dt2)) or ");
                    sb.append("EXISTS (select tt.user from TUserWorkTimes tt where tt.user.id=p.user.id and tt.workDate=:dt3))");
                    sb.append(" and p.serviceCity.id='").append(lc.getArea().getId()).append("'");
                    if(lc.getNeedGuideCertificate()==1)sb.append(" and p.hasGuideCertificate=1");
                    sql=sql+sb.toString();
                    dt1=lc.getBeginTime();
                    dt2=lc.getEndTime();
                }
                break;
            case BOOKING_CATEGORY_DOCENTS:
                TUserOrderDocents dc=orderDcRepo.findByBooking(order);
                if(dc!=null){
                    if(dc.getType()!=1){
                        List<TUser> us=new ArrayList<>();
                        us.add(userRepo.find_admin());
                        notice=new TUserNotice();
                        notice.setSender(order.getApplicant());
                        notice.setBooking(order); notice.setCreated(new Date());
                        notice.setModified(new Date());
                        notice.setRecipients(us);
                        notice.setSendTime(new Date());notice.setType(NOTICE_TYPE_1);
                        notice.setTitle(order.getTitle());notice.setContent("--");
                        noticeRepo.save(notice);
                        return;
                    }
                    sql="select p.user from TUserIdentityDocents p";
                    sb.append(" where p.user.id<>:user and p.status=1 and p.user.identity.statusDc=2 and p.user.identity.hasIdentityDocents=1 and ");
                    sb.append("p.user.status<3 and p.user.receivingTaskNotice=1 and ");
                    sb.append("(NOT EXISTS (select t.user from TUserWorkTimes t where t.user.id=p.user.id and (t.workDate between :dt1 and :dt2)) or ");
                    sb.append("EXISTS (select tt.user from TUserWorkTimes tt where tt.user.id=p.user.id and tt.workDate=:dt3)) and ");
                    sb.append("p.supportMuseum=1 and EXISTS (select pp from p.spots pp where pp.id=").append(dc.getScenic().getId()).append(") and ");
                    if(dc.getGender()!=3)sb.append("p.user.gender=").append(dc.getGender()).append(" and ");
                    if(dc.getNationality()!=3)sb.append("p.nationality=").append(dc.getNationality()).append(" and ");
                    String s=StringUtils.substringBeforeLast(sb.toString(), " and ");
                    sql=sql+s;
                }
                break;
            case BOOKING_CATEGORY_DRV_1:
            case BOOKING_CATEGORY_DRV_2:
                TUserOrderDriver dr=orderDrRepo.findByBooking(order);
                if(dr!=null){
                    sql="select p.user from TUserIdentityDriver p";
                    sb.append(" where p.user.id<>:user and p.user.identity.statusDrv=2 and p.user.identity.hasIdentityDriver=1 and ");
                    if(dr.getStartingCity()!=null){
                        sb.append("p.area.id='").append(dr.getStartingCity().getParent().getId()).append("' and ");
                    }
                    sb.append("p.user.receivingTaskNotice=1 and ");
                    sb.append("p.user.status<3 and p.user.receivingTaskNotice=1 and ");
                    if(dr.getType()==4) {
                    } else {
                      if(dr.getCarModel()!=null)sb.append("p.models.id=").append(dr.getCarModel().getId()).append(" and ");
                      sb.append("(NOT EXISTS (select b from TUserOrderDriver b where b.booking.status in (3,4,5) and b.booking.executor.id=p.user.id and ((b.beginTime between :dt5 and :dt6) or (b.endTime between :dt5 and :dt6)))) and ");
                      dt1=dr.getBeginTime();dt2=dr.getEndTime();
                    }
                    sb.append("(NOT EXISTS (select t.user from TUserWorkTimes t where t.user.id=p.user.id and (t.workDate between :dt1 and :dt2)) or ");
                    sb.append("EXISTS (select tt.user from TUserWorkTimes tt where tt.user.id=p.user.id and tt.workDate=:dt3)) and ");
                    if(dr.getCategory()==1){
                        if(dr.getNeedLicenses()==1)sb.append("p.hasLicenses=1 and ");
                        if(dr.getNeedGuide()==1)sb.append("p.supportGuide=1 and ");
                        if(dr.getType()==1)sb.append("p.supportSightseeing=1 and ");
                        if(dr.getType()==2)sb.append("p.supportSingleCountry=1 and ");
                        if(dr.getType()==3)sb.append("p.supportMultipleCountry=1 and ");
                    }
                    String s=StringUtils.substringBeforeLast(sb.toString(), " and ");
                    sql=sql+s;
                }
                break;
        }
        if(cat==BOOKING_CATEGORY_PP){
            users=new ArrayList();
            users.add(userRepo.find_admin());
        }else {
            Date dt=order.getServiceTime();
            Date[] dts=Utils.thisMonth(dt);
            Query query=em.createQuery(sql).setParameter("user", order.getApplicant().getId())
                    .setParameter("dt1", dts[0],TemporalType.DATE)
                    .setParameter("dt2", dts[1],TemporalType.DATE)
                    .setParameter("dt3", dt,TemporalType.DATE);
            if(dt1!=null && dt2!=null)
                query.setParameter("dt5", dt1,TemporalType.DATE).setParameter("dt6", dt2,TemporalType.DATE);
            users=query.getResultList();
        }
        if(!users.isEmpty()){
            notice=new TUserNotice();
            notice.setSender(order.getApplicant());
            notice.setBooking(order); notice.setCreated(new Date());
            notice.setModified(new Date());notice.setRecipients(users);
            notice.setSendTime(new Date());notice.setType(NOTICE_TYPE_1);
            notice.setTitle(order.getTitle());notice.setContent("--");
            notice.setMessage(UUID.randomUUID().toString());
            noticeRepo.save(notice);
            jmsSender.setNotice(notice.getMessage());
            jmsSender.setType(MESSAGE_TYPE_BOOKING);
            jmsSender.send();
        }
    }

    @Override
    public void booking_notice(Object booking) {
        if(booking instanceof TUserBookingOrder){
            TUserBookingOrder bk=(TUserBookingOrder) booking;
            booking=orderRepo.findBySerialNumber(bk.getSerialNumber());
            if(booking==null){
                System.out.println("Empty Booking: "+bk.getSerialNumber());
            }else process_order_notice((TUserBookingOrder) booking);
        }
    }
    /**
     * 나의 입찰소식보기
     * api url /bkng/vws/bids
     * @param param
     * @return 
     */
    @Override
    public Map view_my_bid_notice(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        Integer start=(Integer) param.get("start");
        Integer count=(Integer) param.get("count");
        if(start==null)start=1;
        if(count==null)count=20;
        else if(count>20)count=20;
        Pageable pg=new PageRequest(start, count);
        Page<TUserNotice> pglst=noticeRepo.find_by_user(user.getMobile(), pg);
        List<TUserNotice> lst=pglst.getContent();
        List data=new ArrayList();
        if(lst!=null){
            for (TUserNotice dd : lst) {
                Map m=new HashMap();
                m.put("bbid", dd.getBooking().getId().toString());
                m.put("title", dd.getTitle());
                m.put("time",Utils.dateToString(dd.getBooking().getApplicationTime()));
                m.put("curl", getResource("booking.img.prefix", Integer.toString(dd.getBooking().getCategory())));
                long cnt=nsRepo.countByRecieverAndNoticeAndStatus(user, dd, 1);
                m.put("status", cnt>0?1:0);
                m.put("nid", dd.getId().toString());
                data.add(m);
            }
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        rm.put("total", pglst.getTotalElements());
        return rm; 
    }

    @Override
    public void journey_state_notice(Object journey) {
        if(journey!=null && (journey instanceof TUserJourney)){
            TUserJourney jr=(TUserJourney) journey;
            TUser user=jr.getUser();
            if(user.getStatus()==USER_STATE_DISABLED || user.getReceivingTaskNotice()==0)return;
            TUserNotice notice;
            List<TUser> users=new ArrayList();
            notice=new TUserNotice();
            notice.setSender(jr.getUser());notice.setType(NOTICE_TYPE_3);
            notice.setCreated(new Date());notice.setModified(new Date());
            notice.setRecipients(users);
            notice.setSendTime(new Date());
            
            if(jr.getStatus()==JOURNEY_STATE_1){
                users.add(user);notice.setTitle(getResource("journey.notice.1", jr.getNameZh()));
                notice.setSubtype(SYSTEM_NOTICE_CATEGORY_13);
            }
            if(jr.getStatus()==JOURNEY_STATE_2){
                users.add(user);notice.setTitle(getResource("journey.notice.2", jr.getNameZh()));
                notice.setSubtype(SYSTEM_NOTICE_CATEGORY_12);
            }
            notice.setContent(notice.getTitle());
            notice.setMessage(UUID.randomUUID().toString());
            noticeRepo.save(notice);
            jmsSender.setBbid(jr.getId().toString());
            jmsSender.setNotice(notice.getMessage());
            jmsSender.setType(MESSAGE_TYPE_JRS);
            jmsSender.send();
        }
    }

    @Override
    public Map journey_order_notice() {
        Calendar cal=Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        List<TUserJourneyDaily> lst=journeyDRepo.findByJourneyDate(cal.getTime());
        if(!lst.isEmpty()){
            for (TUserJourneyDaily td : lst) {
                TUserNotice notice=new TUserNotice();
                notice.setCreated(new Date());
                notice.setJourney(td);
                notice.setSendTime(new Date());
                notice.setSender(userRepo.find_admin());
                notice.setType(NOTICE_TYPE_2);
                notice.setContent(getResource("notice.journey.content", cities_names(td.getCities())));
                notice.setTitle(getResource("notice.journey.title"));
                List<TUser> users=new ArrayList<>();
                users.add(td.getJourney().getUser());
                if(td.getJourney().getTarget()!=null)users.add(td.getJourney().getTarget());
                notice.setRecipients(users);
                notice.setModified(new Date());
                notice.setSubtype(JOURNEY_NOTICE_CATEGORY_1);
                noticeRepo.save(notice);
            }
        }
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm; 
    }
    
    private String cities_names(List<TUserJourneyDailyCity> lst){
        StringBuilder sb=new StringBuilder();
        if(lst==null)return null;
        if(!lst.isEmpty()){
            for (TUserJourneyDailyCity city : lst) {
                sb.append(city.getCity().getName1()).append(",");
            }
            return StringUtils.substringBeforeLast(sb.toString(), ",");
        }
        return sb.toString();
    }
    /**
     * 행정예약통보받기
     * api: /jrn/notify/order
     * @param param
     * @return 
     */
    @Override
    public Map recieve_notice_by_journey_order(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid))return raiseError(1032);
        TUserJourney jr=userJRepo.findOne(Long.parseLong(jid));
        if(jr==null)return raiseError(1032);
        Calendar cal=Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        TUserNotice ns=noticeRepo.find_journey_order_notice(jr.getId(), cal.getTime());
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        if(ns!=null){
            rm.put("id", ns.getId().toString());
            rm.put("cnt", ns.getContent());
            if(ns.getJourney().getCities()!=null && !ns.getJourney().getCities().isEmpty()){
                rm.put("city", ns.getJourney().getCities().size());
                if(ns.getJourney().getCities().size()==1){
                    rm.put("cid", ns.getJourney().getCities().get(0).getCity().getId());
                }
            }else rm.put("city",0);
            rm.put("jdate", Long.toString(ns.getJourney().getJourneyDate().getTime()));
        }else{
            rm.put("id", "--");
        }
        return rm; 
    }

    @Override
    public Map view_city_by_journey_order(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String nid=(String) param.get("nid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(nid) || !NumberUtils.isNumber(nid)){
            return raiseError(1032);
        }
        TUserNotice notice=noticeRepo.findOne(Long.parseLong(nid));
        if(notice==null){
            return raiseError(1032);
        }
        List data=new ArrayList();
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        if(notice.getJourney()!=null){
            List<TUserJourneyDailyCity> city=dcRepo.findByJourney(notice.getJourney());
            for (TUserJourneyDailyCity ct : city) {
                Map m=new HashMap();
                List<TCityIntroduction> cct=cityRepo.findByArea(ct.getCity());
                m.put("cid", ct.getCity().getId());
                m.put("cname", ct.getCity().getName1());
                m.put("cname1", ct.getCity().getName2());
                if(!cct.isEmpty())
                m.put("image", cct.get(0).getViewUrl());
                else m.put("image", "--");
                data.add(m);
            }
            rm.put("jid", notice.getJourney().getJourney().getId().toString());
            rm.put("jdate", Long.toString(notice.getJourney().getJourneyDate().getTime()));
        }else{
            rm.put("jid", "--");
            rm.put("jdate", "0");
        }
        rm.put(DATA_KEY, data);
        return rm;
    }
    /**
     * 도시예약대상목록보기
     * @param param
     * @return 
     */
    @Override
    public Map view_city_data_jr_order(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String cid=(String) param.get("cid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(cid)){
            return raiseError(1032);
        }
        TArea area=areaRepo.findOne(cid);
        if(area==null)return raiseError(1032);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        TCityRestaurant rest=restRepo.findByLogoAndArea(1, area);
        Map m=new HashMap();
        if(rest!=null){
            m.put("id", rest.getId());
            m.put("name", Utils.valueOf(rest.getNameZh()));
            m.put("image", Utils.valueOf(rest.getHeaderUrl()));
            m.put("cat", rest.getCategories().getLevel());
        }else {
            m.put("id", -1);
        }
        rm.put("rest", m);
        m=new HashMap();
        TCityShop sh=shopRepo.findByLogoAndArea(1, area);
        if(sh!=null){
            m.put("id", sh.getId());
            m.put("name", Utils.valueOf(sh.getNameZh()));
            m.put("image", Utils.valueOf(sh.getHeaderUrl()));
        }else{
            m.put("id", -1);
        }
        rm.put("shop", m);
        List<TCityScenicSpots> lst=ssRepo.findByLogoAndArea(1, area);
        List data=new ArrayList();
        if(!lst.isEmpty()){
            for (TCityScenicSpots ss : lst) {
                Map mm=new HashMap();
                mm.put("id", ss.getId());
                mm.put("name", Utils.valueOf(ss.getNameZh()));
                mm.put("image", Utils.valueOf(ss.getHeaderUrl()));
                mm.put("dcs", ss.getHasDocents());
                data.add(mm);
            }
        }
        rm.put("ss", data);
        rm.put("city", area.getName1());
        rm.put("areaid", area.getParent().getId());
        rm.put("areaN", area.getParent().getName1());
        return rm;
    }

    @Override
    public Map view_my_journey_notice(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        Integer start=(Integer) param.get("start");
        Integer count=(Integer) param.get("count");
        if(start==null)start=1;
        if(count==null)count=20;
        else if(count>20)count=20;
        Pageable pg=new PageRequest(start, count);
        Page<TUserNotice> pglst=noticeRepo.find_by_my_journey_notice(es, pg);
        List<TUserNotice> lst=pglst.getContent();
        List data=new ArrayList();
        if(lst!=null){
            for (TUserNotice dd : lst) {
                Map m=new HashMap();
                m.put("id", dd.getId().toString());
                m.put("title", dd.getTitle());
                m.put("time",Utils.dateToString(dd.getSendTime()));
                long n=nsRepo.countByRecieverAndNoticeAndStatus(user, dd, 1);
                m.put("status",n>0?1:0);
                m.put("curl", getResource("booking.img.prefix", "jn"+Integer.toString(dd.getSubtype())));
                switch(dd.getSubtype()){
                    case JOURNEY_NOTICE_CATEGORY_1:
                        m.put("cat", "journey");
                        if(dd.getJourney().getCities()!=null && !dd.getJourney().getCities().isEmpty()){
                            m.put("city", dd.getJourney().getCities().size());
                            if(dd.getJourney().getCities().size()==1){
                                m.put("cid", dd.getJourney().getCities().get(0).getCity().getId());
                            }
                        }else rm.put("city",0);
                        m.put("jdate", Long.toString(dd.getJourney().getJourneyDate().getTime()));
                        m.put("jid", dd.getJourney().getJourney().getId().toString());
                        break;
                    default:
                        m.put("bid", dd.getBooking().getId().toString());
                        m.put("cat", "booking");
                        switch(dd.getBooking().getCategory()){
                            case BOOKING_CATEGORY_SHOP:
                            case BOOKING_CATEGORY_RESTURANT:
                            case BOOKING_CATEGORY_SS_PIAO:
                                m.put("bcat", 100);break;
                            case BOOKING_CATEGORY_DRV_1:    
                            case BOOKING_CATEGORY_DRV_2:
                            case BOOKING_CATEGORY_LG:
                            case BOOKING_CATEGORY_LC:
                            case BOOKING_CATEGORY_OG:
                                if(dd.getBooking().getStatus()==BOOKING_STATE_1)m.put("bcat", 200);
                                else m.put("bcat", 400);
                                break;
                            case BOOKING_CATEGORY_DOCENTS:
                                TUserOrderDocents to= orderDcRepo.findByBooking(dd.getBooking());
                                if(to.getType()==1){
                                    if(dd.getBooking().getStatus()==BOOKING_STATE_1)m.put("bcat", 200);
                                    else m.put("bcat", 400);
                                }else{
                                    m.put("bcat", 300);
                                } 
                                break;
                            default:
                                m.put("bcat", 300);break;
                        };break;
                }
                data.add(m);
            }
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        rm.put("total", pglst.getTotalElements());
        return rm; 
    }

    @Override
    public Map view_my_journey_system(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        Integer start=(Integer) param.get("start");
        Integer count=(Integer) param.get("count");
        if(start==null)start=1;
        if(count==null)count=20;
        else if(count>20)count=20;
        Pageable pg=new PageRequest(start, count);
        Page<TUserNotice> pglst=noticeRepo.find_by_my_system_notice(es, pg);
        List<TUserNotice> lst=pglst.getContent();
        List data=new ArrayList();
        if(lst!=null){
            for (TUserNotice dd : lst) {
                Map m=new HashMap();
                m.put("id", dd.getId().toString());
                m.put("title", dd.getTitle());
                m.put("time",Utils.dateToString(dd.getSendTime()));
                long n=nsRepo.countByRecieverAndNoticeAndStatus(user, dd, 1);
                m.put("status", n>0?1:0);
                m.put("curl", getResource("booking.img.prefix", "sn"+Integer.toString(dd.getSubtype())));
                int cat=dd.getSubtype();
                switch(cat){
                    case SYSTEM_NOTICE_CATEGORY_12:
                    case SYSTEM_NOTICE_CATEGORY_13:
                        m.put("cat", "journey");
                        m.put("state", 1001);
                        break;
                    case SYSTEM_NOTICE_CATEGORY_10:
                    case SYSTEM_NOTICE_CATEGORY_1:
                    case SYSTEM_NOTICE_CATEGORY_2:
                    case SYSTEM_NOTICE_CATEGORY_9:
                        m.put("cat", "alert");break;
                    case SYSTEM_NOTICE_CATEGORY_11:
                        m.put("cat", "skill");break;
                    case SYSTEM_NOTICE_CATEGORY_3:
                    case SYSTEM_NOTICE_CATEGORY_5:
                    case SYSTEM_NOTICE_CATEGORY_7:
                        m.put("cat", "booking");
                        m.put("bid", dd.getBooking().getId().toString());
                        switch(dd.getBooking().getCategory()){
                            case BOOKING_CATEGORY_SHOP:
                            case BOOKING_CATEGORY_RESTURANT:
                            case BOOKING_CATEGORY_SS_PIAO:
                                m.put("bcat", 100);break;
                            case BOOKING_CATEGORY_DRV_1:    
                            case BOOKING_CATEGORY_DRV_2:
                            case BOOKING_CATEGORY_LG:
                            case BOOKING_CATEGORY_LC:
                            case BOOKING_CATEGORY_OG:
                                switch(dd.getBooking().getStatus()){
                                    case BOOKING_STATE_6:
                                        m.put("bcat", 400);break;
                                    case BOOKING_STATE_7:
                                    case BOOKING_STATE_8:
                                        m.put("bcat", 200);break;
                                    default:
                                        if(dd.getBooking().getStatus()==BOOKING_STATE_1)m.put("bcat", 200);
                                        else m.put("bcat", 400);break;
                                }
                                break;
                            case BOOKING_CATEGORY_DOCENTS:
                                TUserOrderDocents to= orderDcRepo.findByBooking(dd.getBooking());
                                if(to.getType()==1){
                                    switch(dd.getBooking().getStatus()){
                                        case BOOKING_STATE_6:
                                            m.put("bcat", 400);break;
                                        case BOOKING_STATE_7:
                                        case BOOKING_STATE_8:
                                            m.put("bcat", 200);break;
                                        default:
                                            if(dd.getBooking().getStatus()==BOOKING_STATE_1)m.put("bcat", 200);
                                            else m.put("bcat", 400);break;
                                    }
                                }else{
                                    m.put("bcat", 300);
                                } 
                                break;
                            default:
                                m.put("bcat", 300);break;
                        };break;
                    case SYSTEM_NOTICE_CATEGORY_4:
                    case SYSTEM_NOTICE_CATEGORY_6:
                    case SYSTEM_NOTICE_CATEGORY_8:
                        m.put("cat", "task");
                        m.put("bid", dd.getBooking().getId().toString());
                        break;
                }
                data.add(m);
            }
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        rm.put("total", pglst.getTotalElements());
        return rm; 
    }

    @Override
    public void cancel_booking(Object booking) {
        if(booking instanceof TUserBookingOrder){
            TUserBookingOrder bk=(TUserBookingOrder) booking;
            if(bk.getApplicant().getStatus()==USER_STATE_DISABLED || bk.getApplicant().getReceivingTaskNotice()==0)return;
            TUserNotice notice=new TUserNotice();
            notice.setBooking(bk);
            notice.setCreated(new Date());
            notice.setModified(new Date());
            notice.setSendTime(new Date());
            notice.setSender(userRepo.find_admin());
            notice.setType(NOTICE_TYPE_3);
            List<TUser> users=new ArrayList<>();
            List<TUserBookingTempOrder> orders=orderTRepo.findByBooking(bk);
            for (TUserBookingTempOrder order : orders) {
                users.add(order.getBidder());
            }
            users.add(bk.getApplicant());
            notice.setRecipients(users);
            String sb = null;
            //sb.append("");
            switch(bk.getCategory()){
                case BOOKING_CATEGORY_SHOP:
                    TUserOrderShop sh=orderShRepo.findByBooking(bk);
                    if(sh!=null)sb=Utils.valueOf(sh.getShop().getNameZh());
                    break;
                case BOOKING_CATEGORY_RESTURANT:
                    TUserOrderRestaurant rs=orderRsRepo.findByBooking(bk);
                    if(rs!=null)sb=Utils.valueOf(rs.getRestaurant().getNameZh());
                    break;
                default:sb=BOOKING_CATEGORIES[bk.getCategory()-1];
            }
            notice.setTitle(getResource("notice.system.title.1",sb));
            notice.setContent(notice.getTitle());
            notice.setSubtype(SYSTEM_NOTICE_CATEGORY_1);
            notice.setMessage(UUID.randomUUID().toString());
            noticeRepo.save(notice);
            jmsSender.setNotice(notice.getMessage());
            jmsSender.setType(MESSAGE_TYPE_BOOKING_CANCEL);
            jmsSender.send();
        }
    }

    @Override
    public void user_skill_notice(Map param) {
        String uid=(String) param.get("uid");
        Integer flag=(Integer) param.get("flag");
        if(!hasText(uid) || !NumberUtils.isNumber(uid))return;
        Integer cat=(Integer) param.get("cat");
        if(cat==null)return;
        TUser user=userRepo.findOne(Long.parseLong(uid));
        if(user==null){
            return;
        }
        if(user.getStatus()==USER_STATE_DISABLED || user.getReceivingTaskNotice()==0)return;
        if(flag==null)return;
        if(cat<1 || cat>6)return;
        TUserNotice notice=new TUserNotice();
        notice.setCreated(new Date());
        notice.setModified(new Date());
        notice.setSendTime(new Date());
        notice.setSender(userRepo.find_admin());
        notice.setType(NOTICE_TYPE_3);
        List<TUser> users=new ArrayList<>();
        users.add(user);
        notice.setRecipients(users);
        notice.setMessage(UUID.randomUUID().toString());
        int ccat;
        switch(flag){
            case 1:
                notice.setTitle(getResource("notice.skill.pass",getResource("identity.title"+Integer.toString(cat))));
                notice.setContent(notice.getTitle());
                notice.setSubtype(SYSTEM_NOTICE_CATEGORY_10);
                break;
            case 2:
                notice.setTitle(getResource("notice.skill.fail",getResource("identity.title"+Integer.toString(cat))));
                notice.setContent(notice.getTitle());
                notice.setSubtype(SYSTEM_NOTICE_CATEGORY_11);
                break;
            default:return;    
        }
        ccat=notice.getSubtype();
        noticeRepo.save(notice);
        jmsSender.setNotice(notice.getMessage());
        jmsSender.setType(MESSAGE_TYPE_USER_SKILL);
        jmsSender.send();
    }
    
    @Override
    public void bid_notice(Object booking) {
        if(booking instanceof TUserBookingTempOrder){
            TUserBookingTempOrder tb=(TUserBookingTempOrder) booking;
            jmsSender.setNotice(tb.getId().toString());
            jmsSender.setType(MESSAGE_TYPE_BID_NOTICE);
            jmsSender.send();
        }
    }

    @Override
    public void notice_journey_booking() {
        StringBuilder sb=new StringBuilder();
        sb.append("select p from TUserBookingOrder p where p.journey is not null and ");
        sb.append("p.serviceTime>:date and (p.serviceTime-:date)<=3600 and (p.status=4 or p.status=5)");
        List<TUserBookingOrder> lst=em.createQuery(sb.toString()).setParameter("date", new Date(), TemporalType.TIMESTAMP)
                .getResultList();
        if(!lst.isEmpty()){
            for (TUserBookingOrder order : lst) {
                TUserNotice notice=new TUserNotice();
                notice.setBooking(order);
                notice.setCreated(new Date());
                
               // notice.setJourney(order.getJourney().get);
                
                int cat=order.getCategory();
                switch(cat){
                    case BOOKING_CATEGORY_RESTURANT:
                        
                }
            }
        }
    }
    private TUserJourney find_journey(TUserJourneyDaily daily){
        String sql="select p.journey from TUserJourneyDaily p where p.journey.id=:jj group by p.journey";
        TUserJourney jj=(TUserJourney) em.createQuery(sql).setParameter("jj", daily.getId()).getSingleResult();
        return jj;
    }
  
    @Override
    public Map confirm_notice_status(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm)){
            rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
            return rm;
        }
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String nid=(String) params.get("nid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
            return rm;
        }
        if(hasText(nid) && NumberUtils.isNumber(nid)){
            TUserNotice nt=noticeRepo.findOne(Long.parseLong(nid));
            if(nt!=null){
                TUserNoticeRelation nr=nsRepo.findByRecieverAndNotice(user, nt);
                if(nr==null){
                    nr=new TUserNoticeRelation();
                    nr.setNotice(nt);
                    nr.setReciever(user);
                    nr.setStatus(1);
                    nsRepo.save(nr);
                }
            }
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }
    
        
    
    /**
     * 행정일정예약통지보기
     * @param params
     * @return 
     */
    @Override
    public Map view_journey_daily_notice(Map params) {
        Map m=auth_apps(params, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String jid=(String) params.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        StringBuilder sb=new StringBuilder();
        Calendar cal=Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Date dt1=(Date) cal.getTime().clone();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        Date dt2=(Date) cal.getTime().clone();
        sb.append("select p.flag, count(p.id) from TUserNotice p where p.jitem.id=:jr and (p.sendTime between :dt1 and :dt2) and p.flag>0"
                + " and not exists (select 1 from TUserNoticeRelation f where f.notice.id=p.id and f.reciever.id=:user) group by p.flag"
                + " order by p.flag asc");
        List lst=em.createQuery(sb.toString()).setParameter("jr", Long.parseLong(jid))
                .setParameter("dt1", dt1).setParameter("dt2", dt2)
                .setParameter("user", user.getId()).getResultList();
        List data=new ArrayList();
        for (int i = 0; i < lst.size(); i++) {
            Object[] oo = (Object[]) lst.get(i);
            Integer flag=(Integer) oo[0];
            Long count=(Long)oo[1];
            Map m1=new HashMap();
            m1.put("cat", flag);
            m1.put("count", count.toString());
            data.add(m1);
        }
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        m.put(DATA_KEY, data);
        return m;
    }
    
    /**
     * 부류별상세보기
     * @param params
     * @return 
     */
    @Override
    public Map view_daily_notice(Map params) {
        Map m=auth_apps(params, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String jid=(String) params.get("jid");
        Integer cat=(Integer) params.get("cat");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        if(cat==null || cat<1 || cat>6)return raiseError(1032);
        StringBuilder sb=new StringBuilder();
        Calendar cal=Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Date dt1=(Date) cal.getTime().clone();
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        Date dt2=(Date) cal.getTime().clone();
        sb.append("select p from TUserNotice p where p.jitem.id=:jr and (p.sendTime between :dt1 and :dt2) and p.flag=:cat"
                + " and not exists (select 1 from TUserNoticeRelation f where f.notice.id=p.id and f.reciever.id=:user) group by p.flag"
                + " order by p.flag asc");
        TUserNotice notice;
        try{
                notice=(TUserNotice)em.createQuery(sb.toString()).
                setParameter("jr", Long.parseLong(jid)).
                 setParameter("dt1", dt1).setParameter("dt2", dt2).
                 setParameter("user", user.getId()).
                 setParameter("cat", cat).setMaxResults(1).getSingleResult();
        }catch(NoResultException ex){
            return raiseError(1032);
        }
        TUserBookingOrder booking=notice.getBooking();
        if(booking==null)return raiseError(1032);
        Map mm=new HashMap();
        mm.put("bbid", booking.getId().toString());
        switch(cat){
            case 1:case 6:
                if(booking.getStatus()==BOOKING_STATE_1)mm.put("cat", 200);
                else mm.put("cat", 400);
                break;
            case 2:case 3:case 5:
                mm.put("cat", 100);break;
            case 4:
                TUserOrderDocents to= orderDcRepo.findByBooking(booking);
                if(to.getType()==1){
                    if(booking.getStatus()==BOOKING_STATE_1)mm.put("cat", 200);
                    else mm.put("cat", 400);
                }else{
                    mm.put("cat", 300);
                } 
                break;
        }
        mm.put("title", CAT_NAME[cat-1]);
        mm.put("cnt", notice.getTitle());
        TUserNoticeRelation tr=new TUserNoticeRelation();
        tr.setNotice(notice);tr.setReciever(user);
        tr.setStatus(1);
        nsRepo.save(tr);
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        m.put(DATA_KEY, mm);
        return m;
    }

    @Override
    public Map view_notice_airinfo(Map params) {
        Map m=auth_apps(params, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String jid=(String) params.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        TUserJourney journey=userJRepo.findOne(Long.parseLong(jid));
        if(journey==null)return raiseError(1032);
        List<TUserJourneyDaily> dds=journeyDRepo.view_airinfo(journey.getId());
        StringBuilder sb=new StringBuilder();
        for (TUserJourneyDaily dd : dds) {
            if(hasText(dd.getAirinfo()))sb.append(dd.getAirinfo()).append("\n");
        }
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        m.put(DATA_KEY, sb.toString());
        return m;
    }

    @Override
    public void notice_booking_no_bidder() {
        Calendar cal=Calendar.getInstance();
        cal.setTime(new Date());
        String time=clientApp.getSystemProperty("booking.no.bidder.limit.time");
        cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(time));
        String sql="select p from TUserBookingOrder p where (FUNCTION('DATEDIFF',current_date,p.applicationTime)*24)>:dt"
                + " and p.status=1 and not exists (select pp from TUserBookingTempOrder pp where pp.booking.id=p.id)";
        String sql1="update TUserBookingOrder p set p.status=6 where (FUNCTION('DATEDIFF',current_date,p.applicationTime)*24)>:dt"
                + " and p.status=1 and not exists (select pp from TUserBookingTempOrder pp where pp.booking.id=p.id)";
        List<TUserBookingOrder> bks=em.createQuery(sql).setParameter("dt", Integer.parseInt(time)).getResultList();
        TUser admin=userRepo.find_admin();
        List<String> nts=new ArrayList<>();
        for (TUserBookingOrder bk : bks) {
           if(bk.getApplicant().getStatus()==USER_STATE_DISABLED || bk.getApplicant().getReceivingTaskNotice()==0)continue;
           TUserNotice notice=new TUserNotice();
           notice.setTitle(getResource("booking.no.bidder", BOOKING_CATEGORIES[bk.getCategory()-1]));
           notice.setContent(notice.getTitle());
           notice.setType(NOTICE_TYPE_3);
           notice.setSubtype(SYSTEM_NOTICE_CATEGORY_2);
           notice.setCreated(new Date());
           notice.setModified(new Date());
           notice.setSender(admin);
           notice.setSendTime(new Date());
           notice.setBooking(bk);
           notice.setMessage(UUID.randomUUID().toString());
           List<TUser> usrs=new ArrayList();
           usrs.add(bk.getApplicant());
           notice.setRecipients(usrs);
           noticeRepo.save(notice);
           nts.add(notice.getMessage());
        }
        String ss="";
        for (String nt : nts) {
            ss += nt + "/";
        }
        ss=StringUtils.substringBeforeLast(ss, "/");
        int n=em.createQuery(sql1).setParameter("dt", Integer.parseInt(time)).executeUpdate();
        jmsSender.setType(MESSAGE_TYPE_NO_BIDDER_BOOKING);
        jmsSender.setNotice(ss);
        jmsSender.send();
        System.out.println("Booking Cancel no bidder: "+n);
    }

    @Override
    public void notice_booking_ready_pay_state(Object booking,int state) {
        if(booking instanceof TUserBookingOrder){
            TUserBookingOrder bk=(TUserBookingOrder) booking;
            List<TUser> users=new ArrayList();
            users.add(bk.getApplicant());
            StringBuilder sbb=new StringBuilder();
            TUserNotice notice=new TUserNotice();
            String sb="";
            if(bk.getApplicant().getStatus()!=USER_STATE_DISABLED && bk.getApplicant().getReceivingTaskNotice()==1){
                notice.setBooking(bk);
                notice.setCreated(new Date());
                notice.setModified(new Date());
                if(state==1)notice.setSubtype(SYSTEM_NOTICE_CATEGORY_3);
                else notice.setSubtype(SYSTEM_NOTICE_CATEGORY_5);
                switch(bk.getCategory()){
                    case BOOKING_CATEGORY_SHOP:
                        TUserOrderShop sh=orderShRepo.findByBooking(bk);
                        if(sh!=null)sb=Utils.valueOf(sh.getShop().getNameZh());
                        break;
                    case BOOKING_CATEGORY_RESTURANT:
                        TUserOrderRestaurant rs=orderRsRepo.findByBooking(bk);
                        if(rs!=null)sb=Utils.valueOf(rs.getRestaurant().getNameZh());
                        break;
                    case BOOKING_CATEGORY_PP:
                        TUserOrderProcessCharge pc=pcRepo.findByBooking(bk);
                        if(pc!=null)sb=getResource("booking.ccc",pc.getArea().getName1());
                        break;
                    default:sb=BOOKING_CATEGORIES[bk.getCategory()-1];
                }
                if(state==1)notice.setTitle(getResource("booking.confirm.cnt", sb));
                else notice.setTitle(getResource("booking.state.1", sb));
                notice.setContent(notice.getTitle());
                notice.setMessage(UUID.randomUUID().toString());
                notice.setType(NOTICE_TYPE_3);
                notice.setRecipients(users);
                notice.setSendTime(new Date());
                notice.setSender(userRepo.find_admin());
                noticeRepo.save(notice);
                sbb.append(notice.getMessage());
            }
            if(bk.getExecutor().getStatus()!=USER_STATE_DISABLED && bk.getExecutor().getReceivingTaskNotice()==1){
                users=new ArrayList<>();
                users.add(bk.getExecutor());
                notice=new TUserNotice();
                notice.setBooking(bk);
                notice.setCreated(new Date());
                notice.setModified(new Date());
                if(state==1)notice.setSubtype(SYSTEM_NOTICE_CATEGORY_4);
                else notice.setSubtype(SYSTEM_NOTICE_CATEGORY_6);
                if(state==1)notice.setTitle(getResource("booking.confirm.cnt1", sb));
                else notice.setTitle(getResource("booking.state.2", sb));
                notice.setContent(notice.getContent());
                notice.setMessage(UUID.randomUUID().toString());
                notice.setType(NOTICE_TYPE_3);
                notice.setRecipients(users);
                notice.setSendTime(new Date());
                notice.setSender(userRepo.find_admin());
                noticeRepo.save(notice);
                if(sbb.toString().isEmpty())
                    sbb.append(notice.getMessage());
                else sbb.append("/").append(notice.getMessage());
            }
            if(hasText(sbb.toString())){
                jmsSender.setType(MESSAGE_TYPE_BOOKING_PAY_STATE);
                jmsSender.setNotice(sbb.toString());
                jmsSender.send();
            }
            
        }
    }

    @Override
    public void pay_ready_notice(Object booking) {
        if(booking instanceof TUserBookingOrder){
            TUserBookingOrder tb=(TUserBookingOrder) booking;
            jmsSender.setNotice(tb.getId().toString());
            jmsSender.setType(MESSAGE_TYPE_BID_NOTICE);
            jmsSender.send();
        }
    }

    @Override
    public Map view_notice_title(Map params) {
        Map m=auth_apps(params, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String nid=(String) params.get("nid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(nid) || !NumberUtils.isNumber(nid)){
            return raiseError(1032);
        }
        TUserNotice notice=noticeRepo.findOne(Long.parseLong(nid));
        if(notice==null)return raiseError(1032);
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        m.put("title", notice.getTitle());
        return m;
    }

    @Override
    public void pay_success_notice(Object booking) {
        if(booking instanceof TUserBookingOrder){
            TUserBookingOrder tb=(TUserBookingOrder) booking;
            StringBuilder sbb=new StringBuilder();
            if(tb.getApplicant().getStatus()!=USER_STATE_DISABLED && tb.getApplicant().getReceivingTaskNotice()==1){
                List<TUser> users=new ArrayList();
                users.add(tb.getApplicant());
                TUserNotice notice=new TUserNotice();
                notice.setBooking(tb);
                notice.setCreated(new Date());
                notice.setModified(new Date());
                notice.setRecipients(users);
                notice.setSender(userRepo.find_admin());
                notice.setSendTime(new Date());
                notice.setSubtype(SYSTEM_NOTICE_CATEGORY_5);
                notice.setType(NOTICE_TYPE_3);
                StringBuilder sb=new StringBuilder();
                switch(tb.getCategory()){
                    case BOOKING_CATEGORY_PP:
                       sb.append("[").append(tb.getArea().getName1()).append("]代买进城费");
                        break;
                    case BOOKING_CATEGORY_RESTURANT:
                       TUserOrderRestaurant rt=orderRsRepo.findByBooking(tb);
                       sb.append(rt.getRestaurant().getNameZh());
                       break;
                    case BOOKING_CATEGORY_SS_PIAO:
                        TUserOrderDocents dc=orderDcRepo.findByBooking(tb);
                        sb.append(dc.getScenic().getNameZh());
                        break;
                    default:
                        sb.append(BOOKING_CATEGORIES[tb.getCategory()-1]);
                        break;
                }
                String ss=getResource("notice.pay.ok.1", sb.toString());
                notice.setTitle(ss);notice.setContent(ss);
                notice.setMessage(UUID.randomUUID().toString());
                sbb.append(notice.getMessage()).append("/");
                noticeRepo.save(notice);
                if(tb.getExecutor()!=null){
                    users=new ArrayList();
                    users.add(tb.getExecutor());
                    notice=new TUserNotice();
                    notice.setBooking(tb);
                    notice.setCreated(new Date());
                    notice.setModified(new Date());
                    notice.setRecipients(users);
                    notice.setSender(userRepo.find_admin());
                    notice.setSendTime(new Date());
                    notice.setSubtype(SYSTEM_NOTICE_CATEGORY_6);
                    notice.setType(NOTICE_TYPE_3);
                    sb=new StringBuilder();
                    switch(tb.getCategory()){
                        case BOOKING_CATEGORY_PP:
                           sb.append("[").append(tb.getArea().getName1()).append("]代买进城费");
                            break;
                        case BOOKING_CATEGORY_RESTURANT:
                           TUserOrderRestaurant rt=orderRsRepo.findByBooking(tb);
                           sb.append(rt.getRestaurant().getNameZh());
                           break;
                        case BOOKING_CATEGORY_SS_PIAO:
                            TUserOrderDocents dc=orderDcRepo.findByBooking(tb);
                            sb.append(dc.getScenic().getNameZh());
                            break;
                        default:
                            sb.append(BOOKING_CATEGORIES[tb.getCategory()-1]);
                            break;
                    }
                    ss=getResource("notice.pay.ok.2", sb.toString());
                    notice.setTitle(ss);notice.setContent(ss);
                    notice.setMessage(UUID.randomUUID().toString());
                    sbb.append(notice.getMessage());
                    noticeRepo.save(notice);
                    
                }
                jmsSender.setNotice(sbb.toString());
                jmsSender.setType(MESSAGE_TYPE_PAY_SUCCESS_STATE);
                jmsSender.send();
                
            }
//            if(tb.get().getStatus()!=USER_STATE_DISABLED && tb.getApplicant().getReceivingTaskNotice()==1){
//                
//            }
        }
    }
}
