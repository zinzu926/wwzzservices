/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.impl;

import com.wwzz.services.api.ChatManager;
import com.wwzz.services.api.apps.AppServices;
import com.wwzz.services.domain.base.TArea;
import com.wwzz.services.domain.base.TCarModels;
import com.wwzz.services.domain.base.TCityScenicSpots;
import com.wwzz.services.domain.base.TLang;
import com.wwzz.services.domain.base.TPhoneTest;
import com.wwzz.services.domain.base.TTourismLineCategory;
import com.wwzz.services.domain.base.TTravelAgency;
import com.wwzz.services.domain.users.TUser;
import com.wwzz.services.domain.users.TUserDriverExpandedTemp;
import com.wwzz.services.domain.users.TUserDriverFilesTemp;
import com.wwzz.services.domain.users.TUserDriverTemp;
import com.wwzz.services.domain.users.TUserIdentity;
import com.wwzz.services.domain.users.TUserIdentityDocents;
import com.wwzz.services.domain.users.TUserIdentityDocentsExpanded;
import com.wwzz.services.domain.users.TUserIdentityDocentsFiles;
import com.wwzz.services.domain.users.TUserIdentityDriver;
import com.wwzz.services.domain.users.TUserIdentityDriverExpanded;
import com.wwzz.services.domain.users.TUserIdentityDriverFiles;
import com.wwzz.services.domain.users.TUserIdentityLCGuide;
import com.wwzz.services.domain.users.TUserIdentityLCGuideExpanded;
import com.wwzz.services.domain.users.TUserIdentityLCGuideFiles;
import com.wwzz.services.domain.users.TUserIdentityLongGuide;
import com.wwzz.services.domain.users.TUserIdentityLongGuideExpanded;
import com.wwzz.services.domain.users.TUserIdentityLongGuideFiles;
import com.wwzz.services.domain.users.TUserIdentityOGuide;
import com.wwzz.services.domain.users.TUserIdentityOGuideExpanded;
import com.wwzz.services.domain.users.TUserIdentityOGuideFiles;
import com.wwzz.services.domain.users.TUserIdentityOp;
import com.wwzz.services.domain.users.TUserIdentityOpExpanded;
import com.wwzz.services.domain.users.TUserOrganization;
import com.wwzz.services.domain.users.TUserPayAccount;
import com.wwzz.services.domain.users.TUserPhotos;
import com.wwzz.services.domain.users.TUserProperty;
import com.wwzz.services.domain.users.TUserWorkTimes;
import com.wwzz.services.repository.TAppsRegisterRepository;
import com.wwzz.services.repository.TAreaRepository;
import com.wwzz.services.repository.TCarModelsRepository;
import com.wwzz.services.repository.TCityScenicSpotsRepository;
import com.wwzz.services.repository.TLangRepository;
import com.wwzz.services.repository.TPhoneTestRepository;
import com.wwzz.services.repository.TSystemPropertyRepository;
import com.wwzz.services.repository.TTourismLineCategoryRepository;
import com.wwzz.services.repository.TTravelAgencyRepository;
import com.wwzz.services.repository.TUserDriverFilesTempRepository;
import com.wwzz.services.repository.TUserDriverTempRepository;
import com.wwzz.services.repository.TUserIdentityDocentsFilesRepository;
import com.wwzz.services.repository.TUserIdentityDocentsRepository;
import com.wwzz.services.repository.TUserIdentityDriverFilesRepository;
import com.wwzz.services.repository.TUserIdentityDriverRepository;
import com.wwzz.services.repository.TUserIdentityLCGuideFilesRepository;
import com.wwzz.services.repository.TUserIdentityLocalGuideRepository;
import com.wwzz.services.repository.TUserIdentityLongGuideExpandedRepository;
import com.wwzz.services.repository.TUserIdentityLongGuideFilesRepository;
import com.wwzz.services.repository.TUserIdentityLongGuideRepository;
import com.wwzz.services.repository.TUserIdentityOGuideFilesRepository;
import com.wwzz.services.repository.TUserIdentityOGuideRepository;
import com.wwzz.services.repository.TUserIdentityOpExpandedReposiotry;
import com.wwzz.services.repository.TUserIdentityOpRepository;
import com.wwzz.services.repository.TUserIdentityRepository;
import com.wwzz.services.repository.TUserOrganizationRepository;
import com.wwzz.services.repository.TUserPayAccountRepository;
import com.wwzz.services.repository.TUserPhotosRepository;
import com.wwzz.services.repository.TUserRepository;
import com.wwzz.services.repository.TUserWorkTimesRepository;
import com.wwzz.services.utils.Blowfish;
import com.wwzz.services.utils.Constants;
import com.wwzz.services.utils.Utils;
import com.wwzz.services.utils.WStringUtils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author JBoss
 */
@Service
@Transactional
public class AppServicesImpl extends ResultDataServices implements AppServices,Constants{
    @Autowired
    private TPhoneTestRepository phoneRepo;
    @Autowired
    private TUserRepository userRepo;
    
    @Autowired
    private TAreaRepository areaRepo;
    @Autowired
    private TAppsRegisterRepository appsRepo;
    @Autowired
    private TLangRepository langRepo;
    @Autowired
    private TUserIdentityRepository userIdRepo;
    @Autowired
    private TTravelAgencyRepository travelRepo;
    @Autowired
    private TTourismLineCategoryRepository lineCat;
    @Autowired
    private TUserIdentityOpRepository userOpRepo;
    @Autowired
    private TUserIdentityOpExpandedReposiotry userOpERepo;
    @Autowired
    private TUserIdentityLongGuideRepository userlgRepo;
    @Autowired
    private TUserIdentityLongGuideExpandedRepository userlgERepo;
    @Autowired
    private TUserIdentityLongGuideFilesRepository userlgFRepo;
    @Autowired
    private TUserOrganizationRepository userOrgRepo;
    @Autowired
    private TUserIdentityOGuideRepository userOgRepo;
//    @Autowired
//    private TUserIdentityOGuideExpandedRepository userOgERepo;
    @Autowired
    private TUserIdentityOGuideFilesRepository userOgFRepo;
    @Autowired
    private TUserIdentityLocalGuideRepository userLcgRepo;
    @Autowired
    private TUserIdentityLCGuideFilesRepository userLcgFRepo;
    @Autowired
    private TUserIdentityDriverRepository userDrvRepo;
    @Autowired
    private TUserIdentityDriverFilesRepository userDrvFRepo;
    @Autowired
    private TCarModelsRepository carRepo;
    @Autowired
    private TUserIdentityDocentsRepository userDcRepo;
    @Autowired
    private TUserIdentityDocentsFilesRepository userDcFRepo;
    @Autowired
    private TCityScenicSpotsRepository cityRepo;
    @Autowired
    private TUserPhotosRepository userPhRepo;
    @Autowired
    private ChatManager chatServices;
    @Autowired
    private TSystemPropertyRepository sysService;
    @Autowired
    private TUserDriverTempRepository userDrvTemp;
    @Autowired
    private TUserDriverFilesTempRepository userDrvFTemp;
    @Autowired
    private TUserWorkTimesRepository workRepo;
    @Autowired
    private TUserPayAccountRepository payRepo;
    
    private Map resultMap;
    
    public AppServicesImpl() {
        
    }

    @Override
    public Map verify_phone_code(String code, String phone,String area) {
        resultMap=new HashMap();
        TPhoneTest ph=phoneRepo.findByPhoneAndDateAndCodeAndArea(phone, new Date(),code,area);
        if(ph==null){
            return createResult(RETURN_ERROR_CODE, "verify.check.code.error", 100, "verify.check.code.error");
        }else{
            return createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        }
    }

    @Override
    public Map verify_reg_code(String code, String phone,String area) {
        TUser user=userRepo.findByMobileAndCode(area+"-"+phone, code);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "", 101, "");
        }else{
            return createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        }
    }

    @Override
    public Map view_user_info(Map params) {
        Map rm=auth_apps(params,1);
        if(!isAppAuth(rm))return rm;
        String pt=(String) params.get("partner");
        String uid=(String) params.get("uid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user!=null){
            Map m=new HashMap();
            m.put("uname", user.getName());
            m.put("uname1", user.getNameEn());
            String[] ss=user.getMobile().split("-");
            m.put("area", user.getArea().getId());
            m.put("areaN", user.getArea().getName1());
            m.put("phone", ss[1]);
            if(hasText(user.getPhone())){
                ss=user.getPhone().split("-");
                if(ss.length==2){
                    m.put("phone1", ss[1]);
                }else{
                    m.put("phone1", "--");
                }
            }else{
                m.put("phone1", "--");
            }
            m.put("gender", user.getGender());
            TArea ar=user.getResidence();
            if(ar!=null){
                m.put("city", ar.getId());
                m.put("cityN",ar.getName1());
                m.put("country", ar.getParent()==null?"--":ar.getParent().getId());
                ar=ar.getParent();
                if(ar!=null)
                   m.put("countryN", ar.getName1());
                else m.put("countryN", "--");
            }else{
                m.put("city", "--");
                m.put("cityN","--");
                m.put("country", "--");
                m.put("countryN", "--");
            }
            m.put("flag", user.getReceivingTaskNotice());
            if(hasText(user.getHeadUrl())){
                m.put("curl", user.getHeadUrl());
            }else{
                m.put("curl", "--");
            }
            if(user.getLangs()!=null){
                StringBuilder sb=new StringBuilder();
                if(!user.getLangs().isEmpty()){
                    for (int i = 0; i < user.getLangs().size(); i++) {
                        TLang lang = user.getLangs().get(i);
                        sb.append(lang.getId()).append("-");
                    }
                    String lng=org.apache.commons.lang3.StringUtils.substringBeforeLast(sb.toString(), "-");
                    m.put("langs", lng);
                }else{
                    m.put("langs", "--");
                }
            }else{
                 m.put("langs", "--");
            }
            resultMap=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
            resultMap.put("data", m);
        }else{
            resultMap=createResult(RETURN_ERROR_CODE, "general.error", 10, "general.error");
        }
        return resultMap;
    }

    @Override
    public List view_app_register(String server) {
        return appsRepo.findByServer(server);
    }

    @Override
    public List view_app_register() {
        return appsRepo.findAll();
    }

    @Override
    public List getAppRegisters() {
        return this.apps;
    }
    private List apps;

    @Override
    public void setAppRegisters(List data) {
        this.apps=data;
    }

    @Override
    public Map user_register(Map params) {
        Map rm=auth_apps(params,2);
        if(!isAppAuth(rm))return rm;
        String pt=(String) params.get("partner");
        String acode=(String) params.get("acode");
        String phone=(String) params.get("phone");
        String pass=(String) params.get("pass");
        String code=(String) params.get("code");
        String cc=(String) params.get("cc");
        String wid=(String) params.get("weid");
        if(!hasText(acode)){
            return createMessage(RETURN_ERROR_CODE, "user.reg.error", 6);
        }
        TArea area=areaRepo.findOne(acode);
        if(area==null){
            return createMessage(RETURN_ERROR_CODE, "user.reg.error", 6);
        }
        if(!hasText(phone)){
            return createMessage(RETURN_ERROR_CODE, "user.reg.error", 7);
        }
        if(!hasText(pass)){
            return createMessage(RETURN_ERROR_CODE, "user.reg.error", 8);
        }
        if(!hasText(code)){
            return createMessage(RETURN_ERROR_CODE, "user.reg.error", 9);
        }
        TPhoneTest ph=phoneRepo.findByPhoneAndDateAndCodeAndArea(phone, new Date(),code,area.getCode());
        if(ph==null){
            return createResult(RETURN_ERROR_CODE, "verify.check.code.error", 100, "verify.check.code.error");
        }
        TUser user;
        if(hasText(cc) && !cc.equals("--")){
            user=userRepo.findByMobileAndCode(area.getCode()+"-"+phone,cc);
            if(user==null){
                return createResult(RETURN_ERROR_CODE, "verify.reg.code.error", 100, "verify.reg.code.error");
            }
        }else{
            user=userRepo.findByMobile(area.getCode()+"-"+phone);
            if(user!=null){
                return createMessage(RETURN_ERROR_CODE, "user.reg.error", 10);
            }else{
                user=new TUser();user.setCreated(new Date());
            }
        }
        if(hasText(wid)){
            TUser us=userRepo.findByOpenid(wid);
            if(us!=null)return raiseError(1129);
        }
        user.setArea(area);
        user.setMobile(area.getCode()+"-"+phone);
        user.setSalt(WStringUtils.randomString(20).toUpperCase());
        user.encryptPassword(pass);
        user.setModified(new Date());
        user.setStatus(USER_STATE_INIT);
        user.setTopicid(Utils.generate_device_id());
        if(hasText(wid)){
            user.setOpenid(wid);
        }
        String chat=chatServices.add_chat_user(area.getCode()+phone, pass, "--");
        String ch=chatServices.add_chat_user("wc"+area.getCode()+phone, pass, "--");
        if(hasText(chat)){
            user.setChatid(chat);
        }
        userRepo.save(user);
        Map m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        String id=clientApp.set_uid(pt,user.getMobile());
        m.put("uid", id);
        m.put("chatid", Utils.valueOf(user.getChatid()));
        m.put("phone", phone);
        return m;
    }

    @Override
    public Map user_update(Map params) {
        return null;
    }

    @Override
    public Map user_register_info(Map params) {
        Map rm=auth_apps(params,1);
        if(!isAppAuth(rm))return rm;
        String username=(String) params.get("uname");
        String user_en=(String) params.get("uname_en");
        String acode=(String) params.get("acode");
        String phone=(String) params.get("phone");
        Integer gender=(Integer) params.get("gender");
        String residence=(String) params.get("resi");
        Integer flag=(Integer) params.get("flag");
        String pt=(String) params.get("partner");
        String langs=(String) params.get("langs");
        String uid=(String) params.get("uid");
        if(!hasText(username)){
            return createMessage(RETURN_ERROR_CODE, "user.reg.error", 1);
        }
        if(gender==null){
            return createMessage(RETURN_ERROR_CODE, "user.reg.error", 4);
        }else{
            if(gender<1 || gender>2)
            return createMessage(RETURN_ERROR_CODE, "user.reg.error", 3);
        }
        if(!hasText(residence)){
            return createMessage(RETURN_ERROR_CODE, "user.reg.error", 2);
        }
        if(!hasText(acode)){
            return createMessage(RETURN_ERROR_CODE, "user.reg.error", 6);
        }
        TArea area1=areaRepo.findOne(acode);
        if(area1==null){
            return createMessage(RETURN_ERROR_CODE, "user.reg.error", 6);
        }
        TArea area=areaRepo.findOne(residence);
        if(area==null){
            return createMessage(RETURN_ERROR_CODE, "user.reg.error", 5);
        }
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
            user.setName(username); user.setNameEn(user_en);
            user.setGender(gender);user.setResidence(area);
            user.setModified(new Date());
            user.setReceivingTaskNotice(flag);
            user.setPhone(area1.getCode()+"-"+phone);
            if(hasText(langs) && !langs.equals("--")){
              String[] sp=langs.split("-");
              if(sp.length>0){
                  List data=new ArrayList();
                  for (int i = 0; i < sp.length; i++) {
                      String s = sp[i];
                      try{
                        Long iid=Long.parseLong(s);
                        TLang tl=langRepo.findOne(iid);
                        if(tl!=null)data.add(tl);
                      }catch(NumberFormatException ex){}
                  }
                  user.setLangs(data);
              }
            }
           // user.setStatus(USER_STATE_READY);
            TUserIdentity tu=userIdRepo.findByUser(user);
            if(tu==null){
                tu=new TUserIdentity();
                tu.setAppointmentsNumber(0);
                tu.setCreated(new Date());
                tu.setHasIdentityDocents(false);
                tu.setHasIdentityDriver(false);
                tu.setHasIdentityLocalGuide(false);
                tu.setHasIdentityLongGuide(false);
                tu.setHasIdentityOp(false);
                tu.setHasIdentityOverseasLocalGuide(false);
                tu.setIrregularitiesNumber(0);
                tu.setModified(new Date());
                tu.setServiceNumber(0);
                tu.setStatusDc(USER_STATE_INIT);
                tu.setStatusDrv(USER_STATE_INIT);
                tu.setStatusLc(USER_STATE_INIT);
                tu.setStatusLg(USER_STATE_INIT);
                tu.setStatusOg(USER_STATE_INIT);
                tu.setStatusOp(USER_STATE_INIT);
                tu.setUser(user);
                userIdRepo.save(tu);
            }
            if(!hasText(user.getChatid())){
                String chat=chatServices.add_chat_user(area.getCode()+phone, user.getDecryptPassword(), "--");
                if(hasText(chat)){
                    user.setChatid(chat);
                }
            }
            userRepo.save(user);
        return createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
    }

    @Override
    public Map user_character_reg(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String pt=(String) param.get("partner");
        String uid=(String) param.get("uid");
        String store=(String) param.get("store");
        String http=(String) param.get("view");
        String es=clientApp.get_uid(pt, uid);
        if(!hasText(uid) || !hasText(http) ||!hasText(store)){
           return createMessage(RETURN_ERROR_CODE, "user.reg.error", 11);
        }
        TUser user=userRepo.findByMobile(es);
        String url;
        if(user==null){
            return createMessage(RETURN_ERROR_CODE, "user.reg.error", 11);
        }else{
            url=user.getHeadStorageUrl();
            user.setHeadStorageUrl(store);
            user.setHeadUrl(http);
            user.setModified(new Date());
            userRepo.save(user);
        }
        Map m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        if(hasText(url) && url.length()>10){
            m.put("view", url);
        }else{
            m.put("view", user.getHeadUrl());
        }
        return m;
    }

    @Override
    public String getUserId(Map param) {
        String phone=(String) param.get("uid");
        String key=(String) param.get("ak");
        TUser user=userRepo.findByMobile(phone);
        Blowfish bw=new Blowfish();
        bw.setKey(key);
        String id=bw.encryptString(Long.toString(user.getId()));
        return id;
    }

    @Override
    public Map add_user_identity_op(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String line=(String) param.get("lcat");
        Integer s1=(Integer) param.get("s1");
        Integer s2=(Integer) param.get("s2");
        Integer s3=(Integer) param.get("s3");
        Integer tv=(Integer) param.get("travel");
        List files=(List) param.get("files");
        TUserIdentityOp op;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        TUserIdentity tid=userIdRepo.findByUser(user);
        if(tid==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        int mode=0,status;
        long count=userOpRepo.countByUser(user);
        if(count==0){
            op=new TUserIdentityOp();
            op.setStatus(0);op.setUser(user);
            op.setCreated(new Date());
            status=USER_STATE_INIT;mode=1;
        }else{
            op=userOpRepo.findByUserAndStatus(user, 0);
            if(op==null){
                op=new TUserIdentityOp();
                op.setStatus(0);op.setUser(user);
                op.setCreated(new Date());
                status=USER_STATE_INIT;mode=1;
            }else{
                status=USER_STATE_READY_FOR_WAITING;
                mode=2;
            }
        }
        
        if(!hasText(line)){
            return createMessage(RETURN_ERROR_CODE, "user.op.error", 1);
        }
        String[] sp=line.split("-");
        if(sp.length<1){
            return createMessage(RETURN_ERROR_CODE, "user.op.error", 1);
        }
        if(s1==null && s2==null && s3==null){
            return createMessage(RETURN_ERROR_CODE, "user.op.error", 2);
        }
        if(tv==null){
            return createMessage(RETURN_ERROR_CODE, "user.op.error", 3);
        }
        TTravelAgency travel=travelRepo.findOne(Long.parseLong(tv.toString()));
        if(travel==null){
            return createMessage(RETURN_ERROR_CODE, "user.op.error", 3);
        }
        if(mode==1)op.setCreated(new Date());
        op.setModified(new Date());
        op.setSupportTourAssemble(s1==1);
        op.setSupportTourIndependent(s2==1);
        op.setSupportTourMice(s3==1);
        op.setTravel(travel);
        List<TTourismLineCategory> dd=new ArrayList<>();
        for (String s : sp) {
            TTourismLineCategory tc=lineCat.findOne(Long.parseLong(s));
            if(tc!=null){
                dd.add(tc);
            }
        }
        op.setLineCategories(dd);
        userOpRepo.save(op);
        if(files!=null){
            if(!files.isEmpty()){
                List<TUserIdentityOpExpanded> fs=userOpERepo.findByUserAndStatus(user, 0);
                if(tid.getStatusOp()==USER_STATE_READY){
                    if(fs.isEmpty()){
                        for (int i = 0; i < files.size(); i++) {
                            String fid = (String) files.get(i);
                            TUserIdentityOpExpanded opp=userOpERepo.findOne(fid);
                            String path=opp.getBusinessCardStorageUrl();
                            String url=opp.getBusinessCardUrl();
                            String gf=StringUtils.substringAfter(path, "/");
                            url=StringUtils.substringBefore(url, gf);
                            path=file_copy_on_fastdfs(path);
                            if(path==null)return raiseError(1032);
                            TUserIdentityOpExpanded ffs=new TUserIdentityOpExpanded();
                            ffs.setId(UUID.randomUUID().toString());
                            ffs.setBusinessCardStorageUrl(path);
                            ffs.setBusinessCardUrl(url+StringUtils.substringAfter(path, "/"));
                            ffs.setStatus(0);ffs.setUser(user);
                            ffs.setFlag(1);
                            userOpERepo.save(ffs);
                        }
                    }else{
                        for (int i = 0; i < files.size(); i++) {
                            String fid = (String) files.get(i);
                            TUserIdentityOpExpanded opp=userOpERepo.findOne(fid);
                            if(op.getStatus()==0){
                                opp.setFlag(1);userOpERepo.save(opp);continue;
                            }
                            String path=opp.getBusinessCardStorageUrl();
                            String url=opp.getBusinessCardUrl();
                            String gf=StringUtils.substringAfter(path, "/");
                            url=StringUtils.substringBefore(url, gf);
                            path=file_copy_on_fastdfs(path);
                            if(path==null)return raiseError(1032);
                            TUserIdentityOpExpanded ffs=new TUserIdentityOpExpanded();
                            ffs.setId(UUID.randomUUID().toString());
                            ffs.setBusinessCardStorageUrl(path);
                            ffs.setBusinessCardUrl(url+StringUtils.substringAfter(path, "/"));
                            ffs.setStatus(0);ffs.setUser(user);
                            ffs.setFlag(1);
                            userOpERepo.save(ffs);
                        }
                    }
                }else{
                    for (TUserIdentityOpExpanded file : fs) {
                        if(files.contains(file.getId())){
                            file.setFlag(1);
                            userOpERepo.save(file);
                        }else{
                           file_delete_on_fastdfs(file.getBusinessCardStorageUrl());
                           userOpERepo.delete(file);
                        }
                    }
                }
            }
        }
        tid.setStatusOp(status);
        tid.setHasIdentityOp(true);
        userIdRepo.save(tid);
        return createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
    }

    @Override
    public Map remove_user_identity_op(Map param) {
        Map m=auth_apps(param, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        List<TUserIdentityOp> oop=userOpRepo.findByUser(user);
        if(!oop.isEmpty()){
            List<TUserIdentityOpExpanded> tops=userOpERepo.findByUser(user);
            if(!tops.isEmpty()){
                for (TUserIdentityOpExpanded t : tops) {
                    file_delete_on_fastdfs(t.getBusinessCardStorageUrl());
                }
                userOpERepo.delete(tops);
            }
            userOpRepo.delete(oop);
            TUserIdentity tt=userIdRepo.findByUser(user);
            if(tt!=null){
                tt.setHasIdentityOp(false);
                tt.setStatusOp(-1);
                userIdRepo.save(tt);
            }
        }else{
            List<TUserIdentityOpExpanded> tops=userOpERepo.findByUser(user);
            if(!tops.isEmpty()){
                for (TUserIdentityOpExpanded t : tops) {
                    file_delete_on_fastdfs(t.getBusinessCardStorageUrl());
                }
                userOpERepo.delete(tops);
            }
        }
        return createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
    }

    @Override
    public Map add_op_mingpian_file(Map param) {
        Map m=auth_apps(param, 1);
        if(!isAppAuth(m))return m;
        String store=(String) param.get("store");
        String view=(String) param.get("view");
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        TUserIdentity tid=userIdRepo.findByUser(user);
        if(tid==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        TUserIdentityOpExpanded top=new TUserIdentityOpExpanded(UUID.randomUUID().toString());
        top.setBusinessCardStorageUrl(store);
        top.setBusinessCardUrl(view);
        top.setUser(user);
        top.setStatus(0);
        top.setFlag(0);
        userOpERepo.save(top);
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        m.put("mid", top.getId());
        m.put("view", top.getBusinessCardUrl());
        return m;
    }

    @Override
    public Map remove_op_mingpian_file(Map param) {
        Map m=auth_apps(param, 2);
        if(!isAppAuth(m))return m;
        String id=(String) param.get("iid");
        if (id == null) {
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
//        TUserIdentityOpExpanded top=userOpERepo.findOne(id);
//        if(top!=null){
//            top.setFlag(0);
//            userOpERepo.save(top);
//        }
        return createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
    }

    @Override
    public Map view_user_identity_op(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        TUserIdentity tid=userIdRepo.findByUser(user);
        if(tid==null){
            return raiseError(1000);
        }
        TUserIdentityOp op;
        if(tid.getStatusOp()==USER_STATE_READY)
            op=userOpRepo.findByUserAndStatus(user,1);
        else op=userOpRepo.findByUserAndStatus(user,0);
        if(op==null){
            return createMessage(RETURN_ERROR_CODE, "identity.title", 7);
        }
        Map m=new HashMap();
        m.put("iid", op.getId());
        m.put("travel", op.getTravel().getId());
        m.put("travelN", op.getTravel().getNameZh());
        m.put("tarea", op.getTravel().getArea().getId());
        m.put("tareaN", op.getTravel().getArea().getName1());
        m.put("s1", op.isSupportTourAssemble());
        m.put("s2", op.isSupportTourIndependent());
        m.put("s3", op.isSupportTourMice());
        List<TTourismLineCategory> cats=op.getLineCategories();
        if(cats!=null){
            StringBuilder sb=new StringBuilder();
            for (TTourismLineCategory tc : cats) {
                sb.append(tc.getId()).append("-");
            }
            m.put("lcat", org.apache.commons.lang3.StringUtils.substringBeforeLast(sb.toString(), "-"));
        }else{
            m.put("lcat", "--");
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("op", m);
        //remove_unused_file(user, 6);
        return rm;
    }
    /**
     * 스킬목록얻기
     * @param param
     * @return 
     */
    @Override
    public Map view_user_identity(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        TUserIdentity tid=userIdRepo.findByUser(user);
        if(tid==null){
            return raiseError(1107);
        }
        List data=new ArrayList();
        for(int i=1;i<=6;i++){
            Map m1=new HashMap();
            m1.put("id", i);
            m1.put("name", getResource("identity.title"+Integer.toString(i)));
            switch(i){
                case 1:
                    if(tid.isHasIdentityOp()){
                        if(tid.getStatusOp()==USER_STATE_READY)m1.put("state", 2);
                        else m1.put("state", 1);
                    }else m1.put("state", 0);
                    break;
                case 2:
                    if(tid.isHasIdentityLongGuide()){
                        if(tid.getStatusLg()==USER_STATE_READY)m1.put("state", 2);
                        else m1.put("state", 1);
                    }else m1.put("state", 0);
                    break;
                case 3:
                    if(tid.isHasIdentityOverseasLocalGuide()){
                        if(tid.getStatusOg()==USER_STATE_READY)m1.put("state", 2);
                        else m1.put("state", 1);
                    }else m1.put("state", 0);
                    break;
                case 4:
                    if(tid.isHasIdentityLocalGuide()){
                        if(tid.getStatusLc()==USER_STATE_READY)m1.put("state", 2);
                        else m1.put("state", 1);
                    }else m1.put("state", 0);
                    break;
                case 5:
                    if(tid.isHasIdentityDriver()){
                        if(tid.getStatusDrv()==USER_STATE_READY)m1.put("state", 2);
                        else m1.put("state", 1);
                    }else m1.put("state", 0);
                    break;
                case 6:
                    if(tid.isHasIdentityDocents()){
                        if(tid.getStatusDc()==USER_STATE_READY)m1.put("state", 2);
                        else m1.put("state", 1);
                    }else m1.put("state", 0);
                    break;
            }
            data.add(m1);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("data", data);
        rm.put("tid", tid.getId());
        return rm;
    }

    @Override
    public Map view_mingpian_file(Map param) {
        Map m=auth_apps(param, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        TUserIdentity tid=userIdRepo.findByUser(user);
        if(tid==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        int status=(tid.getStatusOp()==USER_STATE_READY)?1:0;
        List<TUserIdentityOpExpanded> tops=userOpERepo.findByUserAndStatusAndFlag(user,status,1);
        List data=new ArrayList();
        for (TUserIdentityOpExpanded tp : tops) {
            Map m1=new HashMap();
            m1.put("fid", tp.getId());
            m1.put("url", tp.getBusinessCardUrl());
            data.add(m1);
        }
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        m.put("data", data);
        return m;
    }
    
    @Override
    public Map view_lg_info(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        TUserIdentity tid=userIdRepo.findByUser(user);
        if(tid==null){
            return raiseError(1000);
        }
        TUserIdentityLongGuide lg;
        int status=(tid.getStatusLg()==USER_STATE_READY)?1:0;
        lg=userlgRepo.findByUserAndStatus(user,status);
        if(lg==null)return raiseError(1000);
        TUserIdentityLongGuideExpanded te=lg.getProperties();
        if(te==null)return raiseError(1000);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        Map m=new HashMap();
        m.put("iid", lg.getId());
        List<TTourismLineCategory> lst=lg.getLineCategories();
        StringBuilder sb=new StringBuilder();
        for (TTourismLineCategory t1 : lst) {
            sb.append(t1.getId()).append("-");
        }
        m.put("lcat", StringUtils.substringBeforeLast(sb.toString(), "-"));
        m.put("travel", lg.getTravelAgency().getId());
        m.put("travelN", lg.getTravelAgency().getNameZh());
        m.put("tarea", lg.getTravelAgency().getArea().getId());
        m.put("tareaN", lg.getTravelAgency().getArea().getName1());
        m.put("s1",lg.getSupportTourAssemble());
        m.put("s2",lg.getSupportTourIndependent());
        m.put("s3",lg.getSupportTourVip());
        m.put("s4",lg.getSupportDomestic());
        m.put("s5",lg.getSupportImmigration());
        m.put("gc",lg.getHasGuideCertificate());
        if(lg.getHasGuideCertificate()==1){
           List<TUserIdentityLongGuideFiles> ffs=userlgFRepo.findByUserAndStatusAndFlagAndFlag1(user,status,1,1);
           List data=new ArrayList();
            for (TUserIdentityLongGuideFiles ff : ffs) {
                Map mx=new HashMap();
                mx.put("fid", ff.getId());
                mx.put("url", ff.getFileUrl());
                data.add(mx);
            }
            m.put("gc_files", data);
        }
        if(lg.getHasLeaderCertificate()==1){
           List<TUserIdentityLongGuideFiles> ffs=userlgFRepo.findByUserAndStatusAndFlagAndFlag1(user,status, 2,1);
           List data=new ArrayList();
            for (TUserIdentityLongGuideFiles ff : ffs) {
                Map mx=new HashMap();
                mx.put("fid", ff.getId());
                mx.put("url", ff.getFileUrl());
                data.add(mx);
            }
            m.put("lc_files", data);
        }
        m.put("lc",lg.getHasLeaderCertificate());
        m.put("org",te.getOrganization().getId());
        m.put("orgN",te.getOrganization().getName());
        m.put("ed",te.getEmploymentDate()==null?null:te.getEmploymentDate().getTime());
        m.put("ep",te.getExperience());
        m.put("rf",te.getReferee());
        m.put("rs",te.getResume());
        m.put("area",te.getArea().getId());
        m.put("areaN",te.getArea().getName1());
        rm.put("info", m);
        //remove_unused_file(user, 4);
        return rm;
    }

    @Override
    public Map add_user_lg(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        TUserIdentity tid=userIdRepo.findByUser(user);
        if(tid==null){
            return raiseError(1000);
        }
        Integer s1=(Integer) param.get("s1");
        Integer s2=(Integer) param.get("s2");
        Integer s3=(Integer) param.get("s3");
        Integer tv=(Integer) param.get("travel");
        String line=(String) param.get("lcat");
        String ar=(String) param.get("area");
        Integer s4=(Integer) param.get("s4");
        Integer s5=(Integer) param.get("s5");
        Integer gc=(Integer) param.get("gc");
        Integer lc=(Integer) param.get("lc");
        Integer org=(Integer) param.get("org");
        Integer rs=(Integer) param.get("rs");
        String dt=(String) param.get("edt");
        String ref=(String) param.get("ref");
        String exp=(String) param.get("exp");
        List files=(List) param.get("files");
        
        if(!hasText(line))return raiseError(1001);
        String[] sp=line.split("-");
        if(sp.length<1){
            return raiseError(1001);
        }
        if(s1==null && s2==null && s3==null){
            return raiseError(1002);
        }
        if(s4==null || s5==null || lc==null || gc==null){
            return raiseError(1004);
        }
        if(rs==null){
            return raiseError(1006);
        }
        if(tv==null){
            return raiseError(1003);
        }
        if(org==null){
            return raiseError(1007);
        }
        if(!hasText(ar)){
            return raiseError(1008);
        }
        TArea area=areaRepo.findOne(ar);
        if(area==null){
            return raiseError(1008);
        }
        TUserOrganization to=userOrgRepo.findOne(Long.parseLong(org.toString()));
        if(to==null){
            return raiseError(1007);
        }
        TTravelAgency travel=travelRepo.findOne(Long.parseLong(tv.toString()));
        if(travel==null){
            return raiseError(1003);
        }
        TUserIdentityLongGuide lg;
        long count=userlgRepo.countByUser(user);
        int mode,status;
        if(count==0){
            lg=new TUserIdentityLongGuide();
            lg.setStatus(0);lg.setUser(user);
            lg.setCreated(new Date());mode=1;
            status=USER_STATE_INIT;
        }else{
            lg=userlgRepo.findByUserAndStatus(user,0);
            if(lg==null){
                lg=new TUserIdentityLongGuide();
                lg.setStatus(0);lg.setUser(user);
                lg.setCreated(new Date());mode=1;
            }else mode=2;
            status=USER_STATE_READY_FOR_WAITING;
        }
        TUserIdentityLongGuideExpanded lge;
        if(lg.getProperties()==null){
            lge=new TUserIdentityLongGuideExpanded();
        }else lge=lg.getProperties();
        if(mode==1)lg.setCreated(new Date());
        lg.setModified(new Date());
        lg.setSupportTourAssemble(s1);
        lg.setSupportTourIndependent(s2);
        lg.setSupportTourVip(s3);
        lg.setTravelAgency(travel);
        List<TTourismLineCategory> dd=new ArrayList<>();
        for (String s : sp) {
            TTourismLineCategory tc=lineCat.findOne(Long.parseLong(s));
            if(tc!=null){
                dd.add(tc);
            }
        }
        lg.setLineCategories(dd);
        lg.setSupportDomestic(s4);
        lg.setSupportImmigration(s5);
        lg.setHasGuideCertificate(gc);
        lg.setHasLeaderCertificate(lc);
        lge.setArea(area);
        lge.setOrganization(to);
        lge.setResume(rs);
        lge.setIdentity(lg);
        lg.setProperties(lge);
        if(dt!=null)lge.setEmploymentDate(new Date(Long.parseLong(dt)));
        if(hasText(exp))lge.setExperience(exp);
        if(hasText(ref))lge.setReferee(ref);
        if(files!=null){
            if(!files.isEmpty()){
                List<TUserIdentityLongGuideFiles> fs=userlgFRepo.findByUserAndStatus(user, 0);
                if(tid.getStatusLg()==USER_STATE_READY){
                    if(fs.isEmpty()){
                        for (int i = 0; i < files.size(); i++) {
                            String fid = (String) files.get(i);
                            TUserIdentityLongGuideFiles op=userlgFRepo.findOne(fid);
                            String path=op.getStoragePath();
                            String url=op.getFileUrl();
                            String gf=StringUtils.substringAfter(path, "/");
                            url=StringUtils.substringBefore(url, gf);
                            path=file_copy_on_fastdfs(path);
                            if(path==null)return raiseError(1032);
                            TUserIdentityLongGuideFiles ffs=new TUserIdentityLongGuideFiles();
                            ffs.setId(UUID.randomUUID().toString());
                            ffs.setStoragePath(path);ffs.setFileUrl(url+StringUtils.substringAfter(path, "/"));
                            ffs.setStatus(0);ffs.setUser(user);
                            ffs.setFlag(op.getFlag());ffs.setFlag1(1);
                            userlgFRepo.save(ffs);
                        }
                    }else{
                        for (int i = 0; i < files.size(); i++) {
                            String fid = (String) files.get(i);
                            TUserIdentityLongGuideFiles op=userlgFRepo.findOne(fid);
                            if(op.getStatus()==0){
                                op.setFlag1(1);userlgFRepo.save(op);continue;
                            }
                            String path=op.getStoragePath();
                            String url=op.getFileUrl();
                            String gf=StringUtils.substringAfter(path, "/");
                            url=StringUtils.substringBefore(url, gf);
                            path=file_copy_on_fastdfs(path);
                            if(path==null)return raiseError(1032);
                            TUserIdentityLongGuideFiles ffs=new TUserIdentityLongGuideFiles();
                            ffs.setId(UUID.randomUUID().toString());
                            ffs.setStoragePath(path);ffs.setFileUrl(url+StringUtils.substringAfter(path, "/"));
                            ffs.setStatus(0);ffs.setUser(user);
                            ffs.setFlag1(1);ffs.setFlag(op.getFlag());
                            userlgFRepo.save(ffs);
                        }
                    }
                }else{
                    for (TUserIdentityLongGuideFiles file : fs) {
                        if(files.contains(file.getId())){
                            file.setFlag1(1);
                            userlgFRepo.save(file);
                        }else{
                            file_delete_on_fastdfs(file.getStoragePath());
                            userlgFRepo.delete(file);
                        }
                    }
                }
            }
        }
        tid.setHasIdentityLongGuide(true);
        tid.setStatusLg(status);
        userIdRepo.save(tid);
        userlgRepo.save(lg);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map add_user_lg_files(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        Integer flag=(Integer) param.get("flag");
        String store=(String) param.get("store");
        String view=(String) param.get("view");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(flag==null){
            return raiseError(1004);
        }
        if(!hasText(view)){
            return raiseError(1004);
        }
        if(!hasText(store)){
            return raiseError(1004);
        }
        TUserIdentityLongGuideFiles ff=new TUserIdentityLongGuideFiles();
        ff.setId(UUID.randomUUID().toString());
        ff.setFlag(flag);
        ff.setFileUrl(view);
        ff.setStoragePath(store);
        ff.setUser(user);
        ff.setStatus(0);
        ff.setFlag1(0);
        userlgFRepo.save(ff);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("mid", ff.getId());
        rm.put("view", ff.getFileUrl());
        return rm;
    }

    @Override
    public Map remove_user_lg_files(Map param) {
        Map m=auth_apps(param, 2);
        if(!isAppAuth(m))return m;
        String id=(String) param.get("iid");
        if(!hasText(id)){
            return raiseError(1004);
        }
//        TUserIdentityLongGuideFiles ff=userlgFRepo.findOne(id);
//        if(ff!=null){
//            ff.setFlag1(0);
//            userlgFRepo.save(ff);
//        }
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return m;
    }

    @Override
    public Map remove_user_lg(Map param) {
        Map m=auth_apps(param, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        List<TUserIdentityLongGuide> oop=userlgRepo.findByUser(user);
        if(!oop.isEmpty()){
            List<TUserIdentityLongGuideFiles> tops=userlgFRepo.findByUser(user);
            for (TUserIdentityLongGuideFiles t : tops) {
                file_delete_on_fastdfs(t.getStoragePath());
            }
            userlgFRepo.delete(tops);
            userlgRepo.delete(oop);
            TUserIdentity tt=userIdRepo.findByUser(user);
            if(tt!=null){
                tt.setHasIdentityLongGuide(false);
                tt.setStatusLg(-1);
                userIdRepo.save(tt);
            }
        }
        return createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
    }

    @Override
    public Map view_og_info(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        TUserIdentity tid=userIdRepo.findByUser(user);
        if(tid==null){
            return raiseError(1000);
        }
        TUserIdentityOGuide lg;
        int status=(tid.getStatusOg()==USER_STATE_READY)?1:0;
        lg=userOgRepo.findByUserAndStatus(user, status);
        if(lg==null)return raiseError(1000);
        TUserIdentityOGuideExpanded te=lg.getProperties();
        if(te==null)return raiseError(1000);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        Map m=new HashMap();
        m.put("iid", lg.getId());
        List<TTourismLineCategory> lst=lg.getLineCategories();
        StringBuilder sb=new StringBuilder();
        for (TTourismLineCategory t1 : lst) {
            sb.append(t1.getId()).append("-");
        }
        m.put("lcat", StringUtils.substringBeforeLast(sb.toString(), "-"));
        if(lg.getTravelAgency()!=null){
            m.put("travel", lg.getTravelAgency().getId());
            m.put("travelN", lg.getTravelAgency().getNameZh());
            m.put("tarea", lg.getTravelAgency().getArea().getId());
            m.put("tareaN", lg.getTravelAgency().getArea().getName1());
        }else{
            m.put("travel", 0);
            m.put("travelN", "--");
            m.put("tarea", "--");
            m.put("tareaN", "--");
        }
        
        m.put("s1",lg.getSupportTourAssemble());
        m.put("s2",lg.getSupportTourIndependent());
        m.put("s3",lg.getSupportTourVip());
       // m.put("s4",lg.isSupportDomestic()?1:0);
       // m.put("s5",lg.isSupportImmigration()?1:0);
        m.put("ec",lg.getHasEmploymentCertificate());
        m.put("nt",lg.getNationality());
        m.put("rp",lg.getResidencePermit());
        if(lg.getHasEmploymentCertificate()==1){
           List<TUserIdentityOGuideFiles> ffs=userOgFRepo.findByUserAndStatusAndFlag(user,status,1);
           List data=new ArrayList();
            for (TUserIdentityOGuideFiles ff : ffs) {
                Map mx=new HashMap();
                mx.put("fid", ff.getId());
                mx.put("url", ff.getFileUrl());
                data.add(mx);
            }
            m.put("ec_files", data);
        }
        m.put("org",te.getOrganization().getId());
        m.put("orgN",te.getOrganization().getName());
        m.put("ed",te.getEmploymentDate()==null?null:te.getEmploymentDate().getTime());
        m.put("rf",te.getReferee()==null?"--":te.getReferee());
        m.put("rs",te.getResume());
        rm.put("info", m);
        //remove_unused_file(user, 5);
        return rm;
    }

    @Override
    public Map add_user_og(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        TUserIdentity tid=userIdRepo.findByUser(user);
        if(tid==null){
            return raiseError(1000);
        }
        Integer s1=(Integer) param.get("s1");
        Integer s2=(Integer) param.get("s2");
        Integer s3=(Integer) param.get("s3");
        Integer tv=(Integer) param.get("travel");
        String line=(String) param.get("lcat");
       // Integer s4=(Integer) param.get("s4");
       // Integer s5=(Integer) param.get("s5");
        Integer ec=(Integer) param.get("ec");
        Integer org=(Integer) param.get("org");
        Integer nt=(Integer) param.get("nt");
        Integer rp=(Integer) param.get("rp");
        Integer rs=(Integer) param.get("rs");
        String dt=(String) param.get("edt");
        String ref=(String) param.get("ref");
        List files=(List) param.get("files");
        
        
        if(!hasText(line))return raiseError(1001);
        String[] sp=line.split("-");
        if(sp.length<1){
            return raiseError(1001);
        }
        if(s1==null && s2==null && s3==null){
            return raiseError(1002);
        }
//        if(s4==null || s5==null || ec==null){
//            return raiseError(1004);
//        }
        if(rs==null){
            return raiseError(1006);
        }
        if(ec==null){
            return raiseError(1011);
        }
        if(nt==null){
            return raiseError(1009);
        }
        if(rp==null){
            return raiseError(1010);
        }
        if(org==null){
            return raiseError(1007);
        }
        TUserOrganization to=userOrgRepo.findOne(Long.parseLong(org.toString()));
        if(to==null){
            return raiseError(1007);
        }
        TTravelAgency travel=null;
        if(tv!=null && tv!=0){
            travel=travelRepo.findOne(Long.parseLong(tv.toString()));
            if(travel==null){
                return raiseError(1003);
            }
        }
        
        TUserIdentityOGuide lg;
        long count=userOgRepo.countByUser(user);
        int mode,status;
        if(count==0){
            lg=new TUserIdentityOGuide();
            lg.setCreated(new Date());
            lg.setUser(user);mode=1;
            lg.setStatus(0);
            status=USER_STATE_INIT;
        }else{
            lg=userOgRepo.findByUserAndStatus(user, 0);
            if(lg==null){
                lg=new TUserIdentityOGuide();
                lg.setCreated(new Date());
                lg.setUser(user);
                lg.setStatus(0);mode=1;
            }else mode=2;
            status=USER_STATE_READY_FOR_WAITING;
        }
        TUserIdentityOGuideExpanded lge;
        if(lg.getProperties()==null){
            lge=new TUserIdentityOGuideExpanded();
        }else lge=lg.getProperties();
        if(mode==1)lg.setCreated(new Date());
        lg.setModified(new Date());
        lg.setHasEmploymentCertificate(ec);
        lg.setNationality(nt);
        lg.setResidencePermit(rp);
        lg.setSupportTourAssemble(s1);
        lg.setSupportTourIndependent(s2);
        lg.setSupportTourVip(s3);
        lg.setTravelAgency(travel);
        List<TTourismLineCategory> dd=new ArrayList<>();
        for (String s : sp) {
            TTourismLineCategory tc=lineCat.findOne(Long.parseLong(s));
            if(tc!=null){
                dd.add(tc);
            }
        }
        lg.setLineCategories(dd);
//        lg.setSupportDomestic(s4==1);
//        lg.setSupportImmigration(s5==1);
        lge.setOrganization(to);
        lge.setResume(rs);
        if(dt!=null)lge.setEmploymentDate(new Date(Long.parseLong(dt)));
        if(hasText(ref))lge.setReferee(ref);
        lge.setIdentity(lg);
        lg.setProperties(lge);
        if(files!=null){
            if(!files.isEmpty()){
                List<TUserIdentityOGuideFiles> fs=userOgFRepo.findByUserAndStatus(user, 0);
                if(tid.getStatusOg()==USER_STATE_READY){
                    if(fs.isEmpty()){
                        for (int i = 0; i < files.size(); i++) {
                            String fid = (String) files.get(i);
                            TUserIdentityOGuideFiles op=userOgFRepo.findOne(fid);
                            String path=op.getStoragePath();
                            String url=op.getFileUrl();
                            String gf=StringUtils.substringAfter(path, "/");
                            url=StringUtils.substringBefore(url, gf);
                            path=file_copy_on_fastdfs(path);
                            if(path==null)return raiseError(1032);
                            TUserIdentityOGuideFiles ffs=new TUserIdentityOGuideFiles();
                            ffs.setId(UUID.randomUUID().toString());
                            ffs.setStoragePath(path);ffs.setFileUrl(url+StringUtils.substringAfter(path, "/"));
                            ffs.setStatus(0);ffs.setUser(user);
                            ffs.setFlag(1);
                            userOgFRepo.save(ffs);
                        }
                    }else{
                        for (int i = 0; i < files.size(); i++) {
                            String fid = (String) files.get(i);
                            TUserIdentityOGuideFiles op=userOgFRepo.findOne(fid);
                            if(op.getStatus()==0){
                                op.setFlag(1);userOgFRepo.save(op);continue;
                            }
                            String path=op.getStoragePath();
                            String url=op.getFileUrl();
                            String gf=StringUtils.substringAfter(path, "/");
                            url=StringUtils.substringBefore(url, gf);
                            path=file_copy_on_fastdfs(path);
                            if(path==null)return raiseError(1032);
                            TUserIdentityOGuideFiles ffs=new TUserIdentityOGuideFiles();
                            ffs.setId(UUID.randomUUID().toString());
                            ffs.setStoragePath(path);ffs.setFileUrl(url+StringUtils.substringAfter(path, "/"));
                            ffs.setStatus(0);ffs.setUser(user);
                            ffs.setFlag(1);
                            userOgFRepo.save(ffs);
                        }
                    }
                }else{
                    for (TUserIdentityOGuideFiles file : fs) {
                        if(files.contains(file.getId())){
                            file.setFlag(1);
                            userOgFRepo.save(file);
                        }else{
                            file_delete_on_fastdfs(file.getStoragePath());
                            userOgFRepo.delete(file);
                        }
                    }
                }
            }
        }
        userOgRepo.save(lg);
        tid.setStatusOg(status);
        tid.setHasIdentityOverseasLocalGuide(true);
        userIdRepo.save(tid);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map add_user_og_files(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String store=(String) param.get("store");
        String view=(String) param.get("view");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(view)){
            return raiseError(1004);
        }
        if(!hasText(store)){
            return raiseError(1004);
        }
        TUserIdentityOGuideFiles ff=new TUserIdentityOGuideFiles();
        ff.setId(UUID.randomUUID().toString());
        ff.setFileUrl(view);
        ff.setStoragePath(store);
        ff.setUser(user);
        ff.setStatus(0);
        ff.setFlag(0);
        userOgFRepo.save(ff);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("mid", ff.getId());
        rm.put("view", ff.getFileUrl());
        return rm;
    }

    @Override
    public Map remove_user_og_files(Map param) {
        Map m=auth_apps(param, 2);
        if(!isAppAuth(m))return m;
        String id=(String) param.get("iid");
        if(!hasText(id)){
            return raiseError(1004);
        }
//        TUserIdentityOGuideFiles ff=userOgFRepo.findOne(id);
//        if(ff!=null){
//            ff.setFlag(0);
//            userOgFRepo.save(ff);
//        }
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return m;
    }

    @Override
    public Map remove_user_og(Map param) {
        Map m=auth_apps(param, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        List<TUserIdentityOGuide> oop=userOgRepo.findByUser(user);
        if(!oop.isEmpty()){
            List<TUserIdentityOGuideFiles> tops=userOgFRepo.findByUser(user);
            for (TUserIdentityOGuideFiles t : tops) {
                file_delete_on_fastdfs(t.getStoragePath());
            }
            userOgFRepo.delete(tops);
            userOgRepo.delete(oop);
            TUserIdentity tt=userIdRepo.findByUser(user);
            if(tt!=null){
                tt.setHasIdentityOverseasLocalGuide(false);
                tt.setStatusOg(-1);
                userIdRepo.save(tt);
            }
        }
        return createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
    }

    @Override
    public Map view_lcg_info(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        TUserIdentity tid=userIdRepo.findByUser(user);
        if(tid==null){
            return raiseError(1000);
        }
        TUserIdentityLCGuide lg;
        int status=(tid.getStatusLc()==USER_STATE_READY)?1:0;
        lg=userLcgRepo.findByUserAndStatus(user,status);
        if(lg==null)return raiseError(1000);
        TUserIdentityLCGuideExpanded te=lg.getProperties();
        if(te==null)return raiseError(1000);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        Map m=new HashMap();
        m.put("gc", lg.getHasGuideCertificate());
         if(lg.getHasGuideCertificate()==1){
           List<TUserIdentityLCGuideFiles> ffs=userLcgFRepo.findByUserAndStatusAndFlagAndFlag1(user, status, 1,1);
           List data=new ArrayList();
            for (TUserIdentityLCGuideFiles ff : ffs) {
                Map mx=new HashMap();
                mx.put("fid", ff.getId());
                mx.put("url", ff.getFileUrl());
                data.add(mx);
            }
            m.put("gc_files", data);
        }
        m.put("pass", lg.getHasPassport());
        if(lg.getHasPassport()==1){
           List<TUserIdentityLCGuideFiles> ffs=userLcgFRepo.findByUserAndStatusAndFlagAndFlag1(user, status,2,1);
           if(!ffs.isEmpty()){
                TUserIdentityLCGuideFiles ff=ffs.get(0);
                Map mx=new HashMap();
                mx.put("fid", ff.getId());
                mx.put("url", ff.getFileUrl());
                m.put("pass_files", mx);
           }
        }
        m.put("resi", lg.getHasResidence());
        if(lg.getHasResidence()==1){
           List<TUserIdentityLCGuideFiles> ffs=userLcgFRepo.findByUserAndStatusAndFlagAndFlag1(user, status,3,1);
           if(!ffs.isEmpty()){
                TUserIdentityLCGuideFiles ff=ffs.get(0);
                Map mx=new HashMap();
                mx.put("fid", ff.getId());
                mx.put("url", ff.getFileUrl());
                m.put("resi_files", mx);
           }
        }
        m.put("org", te.getOrganization().getId());
        m.put("orgN", te.getOrganization().getName());
        m.put("ed",te.getEmploymentDate()==null?null:te.getEmploymentDate().getTime());
        m.put("rf", te.getReferee());
        m.put("cid", lg.getServiceCity().getParent().getId());
        m.put("ccid", lg.getServiceCity().getId());
        m.put("cname", lg.getServiceCity().getParent().getName1());
        m.put("ccname", lg.getServiceCity().getName1());
        rm.put("info", m);
        //remove_unused_file(user, 3);
        return rm;
    }

    @Override
    public Map add_user_lcg(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        TUserIdentity tid=userIdRepo.findByUser(user);
        if(tid==null){
            return raiseError(1000);
        }
        String scity=(String) param.get("sct");
        Integer gc=(Integer) param.get("gc");
        Integer pass=(Integer) param.get("pass");
        Integer resi=(Integer) param.get("resi");
        Integer org=(Integer) param.get("org");
        String dt=(String) param.get("edt");
        String ref=(String) param.get("ref");
        List files=(List) param.get("files");
        if(!hasText(scity)){
            return raiseError(1128);
        }
        TArea ar=areaRepo.findOne(scity);
        if(ar==null){
            return raiseError(1128);
        }
        if(gc==null || pass==null || resi==null){
            return raiseError(1012);
        }
        if(org==null){
            return raiseError(1007);
        }
        TUserOrganization to=userOrgRepo.findOne(Long.parseLong(org.toString()));
        if(to==null){
            return raiseError(1007);
        }
        TUserIdentityLCGuide lg;
        long count=userLcgRepo.countByUser(user);
        int status;
        if(count==0){
            lg=new TUserIdentityLCGuide();
            lg.setStatus(0);lg.setUser(user);
            status=USER_STATE_INIT;
        }else{
            lg=userLcgRepo.findByUserAndStatus(user,0);
            if(lg==null){
                lg=new TUserIdentityLCGuide();
                lg.setStatus(0);lg.setUser(user);
            }
            status=USER_STATE_READY_FOR_WAITING;
        }
        TUserIdentityLCGuideExpanded lge;
        if(lg.getProperties()==null){
            lge=new TUserIdentityLCGuideExpanded();
        }else lge=lg.getProperties();
        lg.setHasGuideCertificate(gc);
        lg.setHasPassport(pass);
        lg.setHasResidence(resi);
        lge.setOrganization(to);
        if(hasText(ref))lge.setReferee(ref);
        if(hasText(dt))lge.setEmploymentDate(new Date(Long.parseLong(dt)));
        lg.setProperties(lge);
        lg.setServiceCity(ar);
        lge.setIdentity(lg);
        if(files!=null){
            if(!files.isEmpty()){
                List<TUserIdentityLCGuideFiles> fs=userLcgFRepo.findByUserAndStatus(user, 0);
                if(tid.getStatusLc()==USER_STATE_READY){
                    if(fs.isEmpty()){
                        for (int i = 0; i < files.size(); i++) {
                            String fid = (String) files.get(i);
                            TUserIdentityLCGuideFiles op=userLcgFRepo.findOne(fid);
                            String path=op.getStoragePath();
                            String url=op.getFileUrl();
                            String gf=StringUtils.substringAfter(path, "/");
                            url=StringUtils.substringBefore(url, gf);
                            path=file_copy_on_fastdfs(path);
                            if(path==null)return raiseError(1032);
                            TUserIdentityLCGuideFiles ffs=new TUserIdentityLCGuideFiles();
                            ffs.setId(UUID.randomUUID().toString());
                            ffs.setStoragePath(path);ffs.setFileUrl(url+StringUtils.substringAfter(path, "/"));
                            ffs.setStatus(0);ffs.setUser(user);
                            ffs.setFlag(op.getFlag());ffs.setFlag1(1);
                            userLcgFRepo.save(ffs);
                        }
                    }else{
                        for (int i = 0; i < files.size(); i++) {
                            String fid = (String) files.get(i);
                            TUserIdentityLCGuideFiles op=userLcgFRepo.findOne(fid);
                            if(op.getStatus()==0){
                                op.setFlag1(1);userLcgFRepo.save(op);continue;
                            }
                            String path=op.getStoragePath();
                            String url=op.getFileUrl();
                            String gf=StringUtils.substringAfter(path, "/");
                            url=StringUtils.substringBefore(url, gf);
                            path=file_copy_on_fastdfs(path);
                            if(path==null)return raiseError(1032);
                            TUserIdentityLCGuideFiles ffs=new TUserIdentityLCGuideFiles();
                            ffs.setId(UUID.randomUUID().toString());
                            ffs.setStoragePath(path);ffs.setFileUrl(url+StringUtils.substringAfter(path, "/"));
                            ffs.setStatus(0);ffs.setUser(user);
                            ffs.setFlag1(1);ffs.setFlag(op.getFlag());
                            userLcgFRepo.save(ffs);
                        }
                    }
                }else{
                    for (TUserIdentityLCGuideFiles file : fs) {
                        if(files.contains(file.getId())){
                            file.setFlag1(1);
                            userLcgFRepo.save(file);
                        }else{
                            file_delete_on_fastdfs(file.getStoragePath());
                            userLcgFRepo.delete(file);
                        }
                    }
                }
            }
        }
        tid.setHasIdentityLocalGuide(true);
        tid.setStatusLc(status);
        userIdRepo.save(tid);
        userLcgRepo.save(lg);
        return createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
    }

    @Override
    public Map add_user_lcg_files(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String store=(String) param.get("store");
        String view=(String) param.get("view");
        Integer flag=(Integer) param.get("flag");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(view)){
            return raiseError(1004);
        }
        if(flag==null){
            return raiseError(1032);
        }
        if(!hasText(store)){
            return raiseError(1004);
        }
        TUserIdentityLCGuideFiles ff;
        if(flag==1){
            ff=new TUserIdentityLCGuideFiles();
            ff.setId(UUID.randomUUID().toString());
        }else{
            List<TUserIdentityLCGuideFiles> ffs=userLcgFRepo.findByUserAndFlag(user, flag);
            if(ffs.isEmpty()){
                ff=new TUserIdentityLCGuideFiles();
                ff.setId(UUID.randomUUID().toString());
            }
            else{
                ff=ffs.get(0);
            }
        }
        ff.setFileUrl(view);
        ff.setStoragePath(store);
        ff.setUser(user);
        ff.setFlag(flag);
        ff.setStatus(0);
        ff.setFlag1(0);
        userLcgFRepo.save(ff);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("mid", ff.getId());
        rm.put("view", ff.getFileUrl());
        return rm;
    }

    @Override
    public Map remove_user_lcg_files(Map param) {
        Map m=auth_apps(param, 2);
        if(!isAppAuth(m))return m;
        String id=(String) param.get("iid");
        if(!hasText(id)){
            return raiseError(1004);
        }
//        TUserIdentityLCGuideFiles ff=userLcgFRepo.findOne(id);
//        if(ff!=null){
//            ff.setFlag1(0);
//            userLcgFRepo.save(ff);
//        }
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return m;
    }

    @Override
    public Map remove_user_lcg(Map param) {
        Map m=auth_apps(param, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        List<TUserIdentityLCGuide> oop=userLcgRepo.findByUser(user);
        if(!oop.isEmpty()){
            List<TUserIdentityLCGuideFiles> tops=userLcgFRepo.findByUser(user);
            for (TUserIdentityLCGuideFiles t : tops) {
                file_delete_on_fastdfs(t.getStoragePath());
            }
            userLcgFRepo.delete(tops);
            userLcgRepo.delete(oop);
            TUserIdentity tt=userIdRepo.findByUser(user);
            if(tt!=null){
                tt.setHasIdentityLocalGuide(false);
                tt.setStatusLg(-1);
                userIdRepo.save(tt);
            }
        }
        return createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
    }

    @Override
    public Map view_driver_info(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        TUserIdentity tid=userIdRepo.findByUser(user);
        if(tid==null){
            return raiseError(1032);
        }
        if(tid.getStatusDrv()!=USER_STATE_READY)return view_driver_temp_info(param);
        TUserIdentityDriver lg=userDrvRepo.findByUser(user);
        if(lg==null)return raiseError(1000);
        TUserIdentityDriverExpanded te=lg.getProperties();
        if(te==null)return raiseError(1000);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        Map m=new HashMap();
        m.put("model", lg.getModels().getId());
        m.put("modelN", lg.getModels().getModel());
        TUserIdentityDriverFiles df;
        if(te.isHasPicture1()){
            df=userDrvFRepo.findByUserAndFlag(user, 1);
            if(df!=null){
                m.put("f1", df.getFileUrl());
                m.put("f1m", df.getId());
            }else m.put("f1", "--");
        }else m.put("f1", "--");
        if(te.isHasPicture2()){
            df=userDrvFRepo.findByUserAndFlag(user, 2);
            if(df!=null){
                m.put("f2", df.getFileUrl());
                m.put("f2m", df.getId());
            }else m.put("f2", "--");
        }else m.put("f2", "--");
        if(te.isHasPicture3()){
            df=userDrvFRepo.findByUserAndFlag(user, 3);
            if(df!=null){
                m.put("f3", df.getFileUrl());
                m.put("f3m", df.getId());
            }else m.put("f3", "--");
        }else m.put("f3", "--");
        if(te.isHasPicture4()){
            df=userDrvFRepo.findByUserAndFlag(user, 4);
            if(df!=null){
                m.put("f4", df.getFileUrl());
                m.put("f4m", df.getId());
            }else m.put("f4", "--");
        }else m.put("f4", "--");
        if(te.isHasPicture5()){
            df=userDrvFRepo.findByUserAndFlag(user, 5);
            if(df!=null){
                m.put("f5", df.getFileUrl());
                m.put("f5m", df.getId());
            }else m.put("f5", "--");
        }else m.put("f5", "--");
        if(te.isHasPicture6()){
            df=userDrvFRepo.findByUserAndFlag(user, 6);
            if(df!=null){
                m.put("f6", df.getFileUrl());
                m.put("f6m", df.getId());
            }else m.put("f6", "--");
        }else m.put("f6", "--");
        if(te.isHasPicture7()){
            df=userDrvFRepo.findByUserAndFlag(user, 7);
            if(df!=null){
                m.put("f7", df.getFileUrl());
                m.put("f7m", df.getId());
            }else m.put("f7", "--");
        }else m.put("f7", "--");
        m.put("owner", lg.isIsCarOwner()?1:0);
        m.put("band", te.getVehicleBrand());
        m.put("orgN", te.getOrganization().getName());
        m.put("org", te.getOrganization().getId());
        m.put("ed",te.getEmploymentDate()==null?null:te.getEmploymentDate().getTime());
        m.put("rf", te.getReferee());
        m.put("drv_time", te.getDrivingTime().getTime());
        m.put("vpd", te.getVehiclePurchaseDate()!=null?te.getVehiclePurchaseDate().getTime():null);
        m.put("vc", lg.getVehicleCondition());
        m.put("hc", lg.isHasConditioning()?1:0);
        m.put("lc", lg.isHasLicenses()?1:0);
        m.put("af", lg.getAffiliated());
        m.put("s1", lg.isSupportMultipleCountry()?1:0);
        m.put("s2", lg.isSupportSightseeing()?1:0);
        m.put("s3", lg.isSupportSingleCountry()?1:0);
        m.put("s4", lg.isSupportService()?1:0);
        if(lg.getArea()!=null){
            m.put("area", lg.getArea().getId());
            m.put("areaN", lg.getArea().getName1());
        }else{
            m.put("area", "--");
            m.put("areaN", "--");
        }
        rm.put("info", m);
        return rm;
    }

    @Override
    public Map add_user_driver(Map param) {
        return add_user_driver_temp(param);
    }
   
    @Override
    public Map add_user_driver_files(Map param) {
        return add_user_driver_temp_files(param);
    }

    @Override
    public Map remove_user_driver_files(Map param) {
        return remove_user_driver_temp_files(param);
    }

    @Override
    public Map remove_user_driver(Map param) {
        Map m=auth_apps(param, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        TUserIdentity tt=userIdRepo.findByUser(user);
        if(tt==null)return raiseError(1032);
        if(tt.getStatusDrv()==USER_STATE_READY){
            TUserIdentityDriver oop=userDrvRepo.findByUser(user);
            if(oop!=null){
                List<TUserIdentityDriverFiles> tops=userDrvFRepo.findByUser(user);
                for (TUserIdentityDriverFiles t : tops) {
                    file_delete_on_fastdfs(t.getStoragePath());
                }
                oop.setProperties(null);
                userDrvFRepo.delete(tops);
                userDrvRepo.delete(oop);
            }
            TUserDriverTemp opt=userDrvTemp.findByUser(user);
            if(opt!=null){
                userDrvTemp.delete(opt);
                List dd=userDrvFTemp.findByUser(user);
                if(!dd.isEmpty())userDrvFTemp.delete(dd);
            }
        }
        
        tt.setHasIdentityDriver(false);
        tt.setStatusDrv(-1);
        userIdRepo.save(tt);
        return createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
    }

    @Override
    public Map view_docent_info(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        TUserIdentity tid=userIdRepo.findByUser(user);
        if(tid==null){
            return raiseError(1000);
        }
        TUserIdentityDocents dc;
        int status;
        if(tid.getStatusDc()==USER_STATE_READY)status=1;
        else status=0;
        dc=userDcRepo.findByUserAndStatus(user,status);
        if(dc==null)return raiseError(1000);
        TUserIdentityDocentsExpanded te=dc.getProperties();
        if(te==null)return raiseError(1000);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        Map m=new HashMap();
        m.put("nt", dc.getNationality());
        m.put("org", te.getOrganization().getId());
        m.put("orgN", te.getOrganization().getName());
        if(te.getEmploymentDate()!=null){
            m.put("ed", te.getEmploymentDate().getTime());
        }else m.put("ed", null);
        if(hasText(te.getReferee())){
            m.put("rf", te.getReferee());
        }else m.put("rf", "--");
        List<TCityScenicSpots> ccs=dc.getSpots();
        if(ccs!=null){
            StringBuilder sb=new StringBuilder();
            for (TCityScenicSpots cc : ccs) {
               sb.append(cc.getId()).append("-");
            }
            m.put("cats", StringUtils.substringBeforeLast(sb.toString(), "-"));
        }else m.put("cats", "--");
        List<TUserIdentityDocentsFiles> dcfs=userDcFRepo.findByUserAndStatusAndFlag(user,status,1);
        List data=new ArrayList();
        for (TUserIdentityDocentsFiles dcf : dcfs) {
            Map m1=new HashMap();
            m1.put("fid", dcf.getId());
            m1.put("url", dcf.getFileUrl());
            data.add(m1);
        }
        m.put("files", data);
        rm.put("info", m);
        //remove_unused_file(user, 1);
        return rm;
    }

    @Override
    public Map add_user_docent(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        TUserIdentity tid=userIdRepo.findByUser(user);
        if(tid==null){
            return raiseError(1000);
        }
        Integer nt=(Integer) param.get("nt");
        Integer org=(Integer) param.get("org");
        List files=(List) param.get("files");
        if(nt==null){
            return raiseError(1009);
        }
        if(org==null){
            return raiseError(1007);
        }
        TUserOrganization oog=userOrgRepo.findOne(Long.parseLong(org.toString()));
        if(oog==null){
            return raiseError(1007);
        }
        TUserIdentityDocents td;
        long count=userDcRepo.countByUser(user);
        int status;
        TUserIdentityDocentsExpanded tde;
        if(count==0){
           td=new TUserIdentityDocents();
           td.setUser(user); td.setStatus(0);
           status=USER_STATE_INIT;
        }else{
            td=userDcRepo.findByUserAndStatus(user, 0);
            if(td==null)td=new TUserIdentityDocents();
            td.setUser(user); 
            td.setStatus(0);
            status=USER_STATE_READY_FOR_WAITING;
        }
        if(td.getProperties()==null){
            tde=new TUserIdentityDocentsExpanded();
        }else tde=td.getProperties();
        String cats=(String) param.get("cats");
        if(hasText(cats) && !cats.equals("--")){
          String[] sp=cats.split("-");
          if(sp.length>0){
              List data=new ArrayList();
              for (int i = 0; i < sp.length; i++) {
                  String s = sp[i];
                  try{
                    Long iid=Long.parseLong(s);
                    TCityScenicSpots tl=cityRepo.findOne(iid);
                    if(tl!=null)data.add(tl);
                  }catch(NumberFormatException ex){}
              }
              td.setSpots(data);
          }
        }else{
           return raiseError(1021);
        }
        td.setSupportMuseum(true);
        tde.setOrganization(oog);
        td.setNationality(nt);
        String dt=(String) param.get("edt");
        String ref=(String) param.get("ref");
        if(hasText(ref))tde.setReferee(ref);
        if(hasText(dt))tde.setEmploymentDate(new Date(Long.parseLong(dt)));
        td.setProperties(tde);
        tde.setIdentity(td);
        if(files!=null){
            if(!files.isEmpty()){
                List<TUserIdentityDocentsFiles> fs=userDcFRepo.findByUserAndStatus(user, 0);
                if(tid.getStatusDc()==USER_STATE_READY){
                    if(fs.isEmpty()){
                        for (int i = 0; i < files.size(); i++) {
                            String fid = (String) files.get(i);
                            TUserIdentityDocentsFiles op=userDcFRepo.findOne(fid);
                            String path=op.getStoragePath();
                            String url=op.getFileUrl();
                            String gf=StringUtils.substringAfter(path, "/");
                            url=StringUtils.substringBefore(url, gf);
                            path=file_copy_on_fastdfs(path);
                            if(path==null)return raiseError(1032);
                            TUserIdentityDocentsFiles ffs=new TUserIdentityDocentsFiles();
                            ffs.setId(UUID.randomUUID().toString());
                            ffs.setStoragePath(path);ffs.setFileUrl(url+StringUtils.substringAfter(path, "/"));
                            ffs.setStatus(0);ffs.setUser(user);
                            ffs.setFlag(1);
                            userDcFRepo.save(ffs);
                        }
                    }else{
                        for (int i = 0; i < files.size(); i++) {
                            String fid = (String) files.get(i);
                            TUserIdentityDocentsFiles op=userDcFRepo.findOne(fid);
                            if(op.getStatus()==0){
                                op.setFlag(1);userDcFRepo.save(op);continue;
                            }
                            String path=op.getStoragePath();
                            String url=op.getFileUrl();
                            String gf=StringUtils.substringAfter(path, "/");
                            url=StringUtils.substringBefore(url, gf);
                            path=file_copy_on_fastdfs(path);
                            if(path==null)return raiseError(1032);
                            TUserIdentityDocentsFiles ffs=new TUserIdentityDocentsFiles();
                            ffs.setId(UUID.randomUUID().toString());
                            ffs.setStoragePath(path);ffs.setFileUrl(url+StringUtils.substringAfter(path, "/"));
                            ffs.setStatus(0);ffs.setUser(user);
                            ffs.setFlag(1);
                            userDcFRepo.save(ffs);
                        }
                    }
                }else{
                    for (TUserIdentityDocentsFiles file : fs) {
                        if(files.contains(file.getId())){
                            file.setFlag(1);
                            userDcFRepo.save(file);
                        }else{
                            file_delete_on_fastdfs(file.getStoragePath());
                           userDcFRepo.delete(file);
                        }
                    }
                }
            }
        }
        userDcRepo.save(td);
        tid.setStatusDc(status);
        tid.setHasIdentityDocents(true);
        userIdRepo.save(tid);
        return createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
    }

    @Override
    public Map add_user_docent_files(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String store=(String) param.get("store");
        String view=(String) param.get("view");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(view)){
            return raiseError(1004);
        }
        if(!hasText(store)){
            return raiseError(1004);
        }
        TUserIdentityDocentsFiles ff=new TUserIdentityDocentsFiles();
        ff.setId(UUID.randomUUID().toString());
        ff.setFileUrl(view);
        ff.setStoragePath(store);
        ff.setUser(user);
        ff.setStatus(0);
        ff.setFlag(0);
        userDcFRepo.save(ff);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("mid", ff.getId());
        rm.put("view", ff.getFileUrl());
        return rm; 
    }

    @Override
    public Map remove_user_docent_files(Map param) {
        Map m=auth_apps(param, 2);
        if(!isAppAuth(m))return m;
        String id=(String) param.get("iid");
        if(!hasText(id)){
            return raiseError(1004);
        }
//        TUserIdentityDocentsFiles ff=userDcFRepo.findOne(id);
//        if(ff!=null){
//            ff.setFlag(0);
//            userDcFRepo.save(ff);
//        }
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return m;
    }

    @Override
    public Map remove_user_docent(Map param) {
        Map m=auth_apps(param, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        List<TUserIdentityDocents> oop=userDcRepo.findByUser(user);
        List<TUserIdentityDocentsFiles> ffs=userDcFRepo.findByUser(user);
        for (TUserIdentityDocentsFiles ff : ffs) {
            file_delete_on_fastdfs(ff.getStoragePath());
        }
        userDcFRepo.delete(ffs);
        if(!oop.isEmpty()){
          userDcRepo.delete(oop);
        }
        TUserIdentity tt=userIdRepo.findByUser(user);
        if(tt!=null){
            tt.setHasIdentityDocents(false);
            tt.setStatusDc(-1);
            userIdRepo.save(tt);
        }
        return createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
    }

    @Override
    public Map add_user_intoduce(Map param) {
        Map m=auth_apps(param, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String intro=(String) param.get("intro");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        TUserProperty tp=user.getProperties();
        if(tp==null){
            tp=new TUserProperty();
            tp.setCreated(new Date());
            tp.setModified(new Date());
            tp.setRegate(new Date());
            tp.setUser(user);
        }else{
            tp.setModified(new Date());
        }
        tp.setIntroduce(intro);
        user.setProperties(tp);
        userRepo.save(user);
        return createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
    }

    @Override
    public Map add_user_photo(Map param) {
        Map m=auth_apps(param, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String store=(String) param.get("store");
        String view=(String) param.get("view");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        TUserPhotos tp=new TUserPhotos();
        tp.setId(UUID.randomUUID().toString());
        tp.setPhotoUrl(view);
        tp.setStoragePath(store);
        tp.setUser(user);
        userPhRepo.save(tp);
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        m.put("fid", tp.getId());
        m.put("view", tp.getPhotoUrl());
        return m;
    }

    @Override
    public Map remove_user_photo(Map param) {
        Map m=auth_apps(param, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        String id=(String) param.get("iid");
        if(!hasText(id)){
            return raiseError(1004);
        }
        TUserPhotos ff=userPhRepo.findOne(id);
        if(ff!=null){
            String f=ff.getStoragePath();
            userPhRepo.delete(ff);
            file_delete_on_fastdfs(f);
        }
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return m;
    }

    @Override
    public Map view_user_photo_list(Map param) {
        Map m=auth_apps(param, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        List<TUserPhotos> phs=userPhRepo.findByUser(user);
        List data=new ArrayList();
        for (TUserPhotos ph : phs) {
            Map m1=new HashMap();
            m1.put("pid", ph.getId());
            m1.put("view", ph.getPhotoUrl());
            data.add(m1);
        }
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        if(user.getProperties()!=null){
            m.put("intro", user.getProperties().getIntroduce());
        }else m.put("intro", "");
        m.put(DATA_KEY, data);
        return m;
    }

    @Override
    public Map change_password(Map params) {
        Map rm=auth_apps(params,2);
        if(!isAppAuth(rm))return rm;
        String pt=(String) params.get("partner");
        String acode=(String) params.get("acode");
        String phone=(String) params.get("phone");
        String pass=(String) params.get("pass");
        String code=(String) params.get("code");
        if(!hasText(acode)){
            return createMessage(RETURN_ERROR_CODE, "user.reg.error", 6);
        }
        TArea area=areaRepo.findOne(acode);
        if(area==null){
            return createMessage(RETURN_ERROR_CODE, "user.reg.error", 6);
        }
        if(!hasText(phone)){
            return createMessage(RETURN_ERROR_CODE, "user.reg.error", 7);
        }
        if(!hasText(pass)){
            return createMessage(RETURN_ERROR_CODE, "user.reg.error", 8);
        }
        if(!hasText(code)){
            return createMessage(RETURN_ERROR_CODE, "user.reg.error", 9);
        }
        TPhoneTest ph=phoneRepo.findByPhoneAndDateAndCodeAndArea(phone, new Date(),code,area.getCode());
        if(ph==null){
            return createResult(RETURN_ERROR_CODE, "verify.check.code.error", 100, "verify.check.code.error");
        }
        TUser user=userRepo.findByMobile(area.getCode()+"-"+phone);
        if(user==null){
            return raiseError(1000);
        }
        user.encryptPassword(pass);
        user.setModified(new Date());
        userRepo.save(user);
        Map m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        String id=clientApp.set_uid(pt,user.getMobile());
        m.put("uid", id);
        return m;
    }

    @Override
    public Map register_device_apple(Map param) {
        Map m=auth_apps(param, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String device=(String) param.get("device");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(device)){
            return raiseError(1032);
        }
        List<TUser> users=userRepo.findByDeviceid(device);
        if(!users.isEmpty()){
            for (TUser usr : users) {
                usr.setDeviceid(null);
                userRepo.save(usr);
            }
        }
        user.setDeviceid(device);
        userRepo.save(user);
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return m;
    }

    @Override
    public List load_system_property() {
        return sysService.findAll();
    }
    
    private void remove_unused_file(TUser user,int cat){
        switch(cat){
            //Docent
            case 1:
                List<TUserIdentityDocentsFiles> ffs=userDcFRepo.findByUserAndStatusAndFlag(user, 0, 0);
                userDcFRepo.delete(ffs);
                for (TUserIdentityDocentsFiles ff  : ffs) {
                    file_delete_on_fastdfs(ff.getStoragePath());
                }
                break;
            //Local Guide    
            case 3:
                List<TUserIdentityLCGuideFiles> lfs=userLcgFRepo.findByUserAndStatusAndFlag1(user, 0, 0);
                userLcgFRepo.delete(lfs);
                for (TUserIdentityLCGuideFiles ff  : lfs) {
                    file_delete_on_fastdfs(ff.getStoragePath());
                }
                break;
            //Long Guide    
            case 4:
                List<TUserIdentityLongGuideFiles> lgfs=userlgFRepo.findByUserAndStatusAndFlag1(user, 0, 0);
                userlgFRepo.delete(lgfs);
                for (TUserIdentityLongGuideFiles ff  : lgfs) {
                    file_delete_on_fastdfs(ff.getStoragePath());
                }
                break;
            //Oversal Guide
            case 5:
                List<TUserIdentityOGuideFiles> ogfs=userOgFRepo.findByUserAndStatusAndFlag(user, 0, 0);
                userOgFRepo.delete(ogfs);
                for (TUserIdentityOGuideFiles ff  : ogfs) {
                    file_delete_on_fastdfs(ff.getStoragePath());
                }
                break;
            //Op    
            case 6:
                List<TUserIdentityOpExpanded> opfs=userOpERepo.findByUserAndStatusAndFlag(user, 0, 0);
                userOpERepo.delete(opfs);
                for (TUserIdentityOpExpanded ff  : opfs) {
                    file_delete_on_fastdfs(ff.getBusinessCardStorageUrl());
                }
                break;
        }
    }

    @Override
    public void preprocess_drv_file(Map params) {
         Map rm=auth_apps(params,1);
        if(!isAppAuth(rm))return;
        String pt=(String) params.get("partner");
        String uid=(String) params.get("uid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user!=null){
            remove_unused_file(user, 2);
        }
    }
    
    private Map add_user_driver_temp(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        TUserIdentity tid=userIdRepo.findByUser(user);
        if(tid==null){
            return raiseError(1000);
        }
        Integer model=(Integer) param.get("model");
        Integer org=(Integer) param.get("org");
        Integer owner=(Integer) param.get("owner");
        Integer f1=(Integer) param.get("f1");
        Integer f2=(Integer) param.get("f2");
        Integer f3=(Integer) param.get("f3");
        Integer f4=(Integer) param.get("f4");
        Integer f5=(Integer) param.get("f5");
        Integer f6=(Integer) param.get("f6");
        Integer f7=(Integer) param.get("f7");
        Integer vc=(Integer) param.get("vc");
        Integer hc=(Integer) param.get("hc");
        Integer lc=(Integer) param.get("lc");
        Integer af=(Integer) param.get("af");
        Integer s1=(Integer) param.get("s1");
        Integer s2=(Integer) param.get("s2");
        Integer s3=(Integer) param.get("s3");
        Integer s4=(Integer) param.get("s4");
        String drvt=(String) param.get("drv_time");
        String band=(String) param.get("band");
        String vpd=(String) param.get("vpd");
        String dt=(String) param.get("edt");
        String ref=(String) param.get("ref");
        String ar=(String) param.get("area");
        if(model==null){
            return raiseError(1013);
        }
        TCarModels car=carRepo.findOne(model);
        if(car==null){
            return raiseError(1013);
        }
        TUserOrganization oog=userOrgRepo.findOne(Long.parseLong(org.toString()));
        if(oog==null){
            return raiseError(1007);
        }
        if(f1==null || f2==null || f3==null || f4==null || f5==null || f6==null || f7==null){
            return raiseError(1004);
        }
        if(vc==null){
            return raiseError(1014);
        }
        if(hc==null){
            return raiseError(1015);
        }
        if(lc==null){
            return raiseError(1016);
        }
        if(af==null){
            return raiseError(1017);
        }
        if(s1==null || s2==null || s3==null || s4==null){
            return raiseError(1020);
        }
        TUserDriverTemp td=userDrvTemp.findByUser(user);
        TUserDriverExpandedTemp tde;
        if(td==null){
            td=new TUserDriverTemp();
            td.setUser(user);td.setCreated(new Date());
        }
        if(td.getProperties()==null){
            tde=new TUserDriverExpandedTemp();
           
        }else tde=td.getProperties();
        td.setModified(new Date());
        td.setModels(car);
        td.setIsCarOwner(owner==1);
        td.setAffiliated(af);
        tde.setHasPicture1(f1==1);tde.setHasPicture2(f2==1);
        tde.setHasPicture3(f3==1);tde.setHasPicture4(f4==1);
        tde.setHasPicture5(f5==1);tde.setHasPicture6(f6==1);
        tde.setHasPicture7(f7==1);
        if(hasText(band))tde.setVehicleBrand(band);
        if(hasText(vpd))tde.setVehiclePurchaseDate(new Date(Long.parseLong(vpd)));
        td.setVehicleCondition(vc);
        td.setHasConditioning(hc==1);
        td.setHasLicenses(lc==1);
        tde.setDrivingTime(new Date(Long.parseLong(drvt)));
        tde.setOrganization(oog);
        td.setSupportMultipleCountry(s1==1);
        td.setSupportSightseeing(s2==1);
        td.setSupportSingleCountry(s3==1);
        td.setSupportService(s4==1);
        td.setSupportGuide(f3==1);
        if(hasText(ar)){
            TArea area=areaRepo.findOne(ar);
            if(area!=null)td.setArea(area);
        }
        if(hasText(dt))tde.setEmploymentDate(new Date(Long.parseLong(dt)));
        if(hasText(ref))tde.setReferee(ref);
        tde.setIdentity(td);
        td.setProperties(tde);
        TUserDriverFilesTemp df;
        userDrvTemp.save(td);
        List<TUserDriverFilesTemp> dfs=userDrvFTemp.findByUser(user);
        if(dfs!=null){
            for (TUserDriverFilesTemp dff : dfs) {
                int fg=dff.getFlag();
                String ph=null;
                switch(fg){
                    case 1:if(!tde.isHasPicture1())ph=dff.getStoragePath();break;
                    case 2:if(!tde.isHasPicture2())ph=dff.getStoragePath();break;
                    case 3:if(!tde.isHasPicture3())ph=dff.getStoragePath();break;
                    case 4:if(!tde.isHasPicture4())ph=dff.getStoragePath();break;
                    case 5:if(!tde.isHasPicture5())ph=dff.getStoragePath();break;
                    case 6:if(!tde.isHasPicture6())ph=dff.getStoragePath();break;
                    case 7:if(!tde.isHasPicture7())ph=dff.getStoragePath();break;
                }
                if(hasText(ph)){
                    userDrvFTemp.delete(dff);
                    file_delete_on_fastdfs(ph);
                }
            }
        }
        if(userDrvRepo.findByUser(user)==null)tid.setStatusDrv(USER_STATE_INIT);
        else tid.setStatusDrv(USER_STATE_READY_FOR_WAITING);
        tid.setHasIdentityDriver(true);
        userIdRepo.save(tid);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }
    
    private Map add_user_driver_temp_files(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String store=(String) param.get("store");
        String view=(String) param.get("view");
        Integer flag=(Integer) param.get("flag");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(view)){
            return raiseError(1004);
        }
        if(!hasText(store)){
            return raiseError(1004);
        }
        if(flag==null){
            return raiseError(1004);
        }
        TUserDriverFilesTemp ff=userDrvFTemp.findByUserAndFlag(user, flag);
        if(ff==null){
            ff=new TUserDriverFilesTemp();
            ff.setId(UUID.randomUUID().toString());
        }
        ff.setFileUrl(view);
        ff.setStoragePath(store);
        ff.setUser(user);
        ff.setFlag(flag);
        userDrvFTemp.save(ff);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("mid", ff.getId());
        rm.put("view", ff.getFileUrl());
        return rm; 
    }
    
    private Map remove_user_driver_temp_files(Map param) {
        Map m=auth_apps(param, 2);
        if(!isAppAuth(m))return m;
        String id=(String) param.get("iid");
        if(!hasText(id)){
            return raiseError(1004);
        }
//        TUserDriverFilesTemp ff=userDrvFTemp.findOne(id);
//        if(ff!=null){
//            ff.setStatus(0);
//            userDrvFTemp.save(ff);
//        }
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return m;
    }
    
    private Map view_driver_temp_info(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        TUserDriverTemp lg=userDrvTemp.findByUser(user);
        if(lg==null)return raiseError(1000);
        TUserDriverExpandedTemp te=lg.getProperties();
        if(te==null)return raiseError(1000);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        Map m=new HashMap();
        m.put("model", lg.getModels().getId());
        m.put("modelN", lg.getModels().getModel());
        TUserDriverFilesTemp df;
        if(te.isHasPicture1()){
            df=userDrvFTemp.findByUserAndFlag(user, 1);
            if(df!=null){
                m.put("f1", df.getFileUrl());
                m.put("f1m", df.getId());
            }else m.put("f1", "--");
        }else m.put("f1", "--");
        if(te.isHasPicture2()){
            df=userDrvFTemp.findByUserAndFlag(user, 2);
            if(df!=null){
                m.put("f2", df.getFileUrl());
                m.put("f2m", df.getId());
            }else m.put("f2", "--");
        }else m.put("f2", "--");
        if(te.isHasPicture3()){
            df=userDrvFTemp.findByUserAndFlag(user, 3);
            if(df!=null){
                m.put("f3", df.getFileUrl());
                m.put("f3m", df.getId());
            }else m.put("f3", "--");
        }else m.put("f3", "--");
        if(te.isHasPicture4()){
            df=userDrvFTemp.findByUserAndFlag(user, 4);
            if(df!=null){
                m.put("f4", df.getFileUrl());
                m.put("f4m", df.getId());
            }else m.put("f4", "--");
        }else m.put("f4", "--");
        if(te.isHasPicture5()){
            df=userDrvFTemp.findByUserAndFlag(user, 5);
            if(df!=null){
                m.put("f5", df.getFileUrl());
                m.put("f5m", df.getId());
            }else m.put("f5", "--");
        }else m.put("f5", "--");
        if(te.isHasPicture6()){
            df=userDrvFTemp.findByUserAndFlag(user, 6);
            if(df!=null){
                m.put("f6", df.getFileUrl());
                m.put("f6m", df.getId());
            }else m.put("f6", "--");
        }else m.put("f6", "--");
        if(te.isHasPicture7()){
            df=userDrvFTemp.findByUserAndFlag(user, 7);
            if(df!=null){
                m.put("f7", df.getFileUrl());
                m.put("f7m", df.getId());
            }else m.put("f7", "--");
        }else m.put("f7", "--");
        m.put("owner", lg.isIsCarOwner()?1:0);
        m.put("band", te.getVehicleBrand());
        m.put("orgN", te.getOrganization().getName());
        m.put("org", te.getOrganization().getId());
        m.put("ed",te.getEmploymentDate()==null?null:te.getEmploymentDate().getTime());
        m.put("rf", te.getReferee());
        m.put("drv_time", te.getDrivingTime().getTime());
        m.put("vpd", te.getVehiclePurchaseDate()!=null?te.getVehiclePurchaseDate().getTime():null);
        m.put("vc", lg.getVehicleCondition());
        m.put("hc", lg.isHasConditioning()?1:0);
        m.put("lc", lg.isHasLicenses()?1:0);
        m.put("af", lg.getAffiliated());
        m.put("s1", lg.isSupportMultipleCountry()?1:0);
        m.put("s2", lg.isSupportSightseeing()?1:0);
        m.put("s3", lg.isSupportSingleCountry()?1:0);
        m.put("s4", lg.isSupportService()?1:0);
        if(lg.getArea()!=null){
            m.put("area", lg.getArea().getId());
            m.put("areaN", lg.getArea().getName1());
        }else{
            m.put("area", "--");
            m.put("areaN", "--");
        }
        rm.put("info", m);
        return rm;
    }

    @Override
    public Map add_user_work_time(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        List dates=(List)param.get("wdate");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(dates!=null){
          if(!dates.isEmpty()){
              List<TUserWorkTimes> wts=new ArrayList<>();
                for (int i = 0; i < dates.size(); i++) {
                    String dts = (String) dates.get(i);
                    if(!NumberUtils.isNumber(dts))return raiseError(1146);
                    TUserWorkTimes wt=new TUserWorkTimes();
                    wt.setUser(user);
                    wt.setWorkDate(new Date(Long.parseLong(dts)));
                    wts.add(wt);
                }
                workRepo.delete(workRepo.findByUser(user));
                workRepo.save(wts);
          }else{    
              workRepo.delete(workRepo.findByUser(user));
          }
        }else{
            workRepo.delete(workRepo.findByUser(user));
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map view_user_work_time(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        Calendar cal=Calendar.getInstance();
        Date from=cal.getTime();
        cal.add(Calendar.YEAR, 1);
        Date to=(Date) cal.getTime().clone();
        List<TUserWorkTimes> lst=workRepo.findByUserAndWorkDateBetweenOrderByWorkDateAsc(user, from, to);
        List data=new ArrayList();
        for (TUserWorkTimes dt : lst) {
            data.add(Long.toString(dt.getWorkDate().getTime()));
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public Map register_alipay_account(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String account=(String) param.get("account");
        String aname=(String) param.get("aname");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(account))return raiseError(1152);
        if(!hasText(aname))return raiseError(1153);
        TUserPayAccount acc1=payRepo.findByAlipay(account);
        TUserPayAccount acc=payRepo.findByUser(user);
        if(acc==null){
            acc=new TUserPayAccount();
            acc.setCreated(new Date());
        }else{
            if(acc1!=null && !acc1.getId().equals(acc.getId())){
                return raiseError(1151);
            }
        }
        acc.setAlipayName(aname);
        acc.setAlipay(account);
        acc.setUser(user);
        acc.setModified(new Date());
        payRepo.save(acc);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map view_alipay_account(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        String pay="",payname="";
        TUserPayAccount acc=payRepo.findByUser(user);
        if(acc!=null){
            pay=Utils.valueOf(acc.getAlipay());
            payname=Utils.valueOf(acc.getAlipayName());
        }
        rm.put("account", pay);
        rm.put("aname", payname);
        return rm;
    }

    @Override
    public Map view_file_server_conf() {
        Map m=new HashMap();
        m.put("connect_timeout", clientApp.getSystemProperty("file.server.connect_timeout"));
        m.put("network_timeout", clientApp.getSystemProperty("file.server.network_timeout"));
        m.put("charset", clientApp.getSystemProperty("file.server.charset"));
        m.put("http.tracker_http_port", clientApp.getSystemProperty("file.server.http.tracker_http_port"));
        m.put("http.anti_steal_token", clientApp.getSystemProperty("file.server.http.anti_steal_token"));
        m.put("tracker_server", clientApp.getUrlByFile("file.server.tracker_server"));
        m.put("http.prefix", clientApp.getUrlByFile("http.prefix"));
        m.put("file.type.prefix", clientApp.getUrl("file.type.prefix"));
        return m;
    }
}