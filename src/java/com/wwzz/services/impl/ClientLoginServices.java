/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.impl;

import com.wwzz.services.api.ChatManager;
import com.wwzz.services.api.apps.LoginServices;
import com.wwzz.services.domain.base.TArea;
import com.wwzz.services.domain.users.TUser;
import com.wwzz.services.repository.TAreaRepository;
import com.wwzz.services.repository.TUserRepository;
import com.wwzz.services.utils.Utils;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author JBoss
 */
@Service
@Transactional
public class ClientLoginServices extends ResultDataServices implements LoginServices{
    @Autowired
    private TUserRepository userRepo;
    @Autowired
    private TAreaRepository areaRepo;
    @Autowired
    private ChatManager chatServices;
    @Override
    public Map app_login(Map param) {
        String pt=(String) param.get("partner");
        String acode=(String) param.get("acode");
        String phone=(String) param.get("pm");
        String pass=(String) param.get("pass");
        if(!hasText(pt)){
            return createMessage(RETURN_ERROR_CODE, "user.login.error", 1);
        }
        String key=clientApp.getAppKey(pt);
        if(key==null){
            return createMessage(RETURN_ERROR_CODE, "user.reg.error", 1);
        }
        if(!hasText(acode)){
            return createMessage(RETURN_ERROR_CODE, "user.login.error", 1);
        }
        if(!hasText(phone)){
            return createMessage(RETURN_ERROR_CODE, "user.login.error", 2);
        }
        if(!hasText(pass)){
            return createMessage(RETURN_ERROR_CODE, "user.login.error", 3);
        }
        TUser user=userRepo.findByMobile(acode+"-"+phone);
        if(user==null){
            return createMessage(RETURN_ERROR_CODE, "user.login.error", 4);
        }else{
           if(!pass.equals(user.getDecryptPassword())){
               return createMessage(RETURN_ERROR_CODE, "user.login.error", 5);
           }
           if(user.getStatus()==USER_STATE_DISABLED){
                return raiseError(1127);
           }
           if(clientApp.isWeixin(pt)){
               String wid=(String) param.get("weid");
               //if(!hasText(wid))return raiseError(1127);
               if(hasText(wid) && user.getOpenid()==null){
                   user.setOpenid(wid);
               }
               //else if(!wid.equals(user.getOpenid()))return raiseError(1127);
           }
           if(!hasText(user.getChatid())){
               String chat=StringUtils.remove(user.getMobile(), "-");
               String cc=chatServices.add_chat_user(chat, user.getDecryptPassword(), "--");
               String cc1=chatServices.add_chat_user("wc"+chat, user.getDecryptPassword(), "--");
                if(hasText(cc)){
                    user.setChatid(cc);
                }
           }
           user.setLastLogin(new Date());
           if(!hasText(user.getTopicid()))user.setTopicid(Utils.generate_device_id());
           userRepo.save(user);
           Map m=new HashMap();
           m.put("uid", clientApp.set_uid(pt, user.getMobile()));
           m.put("uname", user.getName());
           m.put("unameEn", user.getNameEn());
           m.put("character", user.getHeadUrl());
           m.put("flag", user.getReceivingTaskNotice());
           m.put("pm", user.getMobile());
           m.put("acode", user.getArea().getId());
           m.put("acodeN", user.getArea().getName1());
           m.put("sl", user.getCode()!=null?user.getCode():"--");
           m.put("chatid", Utils.valueOf(user.getChatid()));
           m.put("topic", clientApp.get_topic_info(pt,user.getTopicid()));
           if(user.getResidence()!=null){
               m.put("city", user.getResidence().getId());
               m.put("cityN", user.getResidence().getName1());
               TArea ta=user.getResidence().getParent();
               if(ta!=null){
                    m.put("country", ta.getId());
                    m.put("countryN", ta.getName1());
               }else{
                    m.put("country", "--");
                    m.put("countryN", "--");
               }
           }else{
               m.put("city", "--");
               m.put("cityN", "--");
               m.put("country", "--");
               m.put("countryN", "--");
           }
           m.put("pdate",clientApp.getSystemProperty("booking.pay.expired.time"));
           Map mp=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
           mp.put("info", m);
           return mp;
        }
    }

    @Override
    public Map silent_login(Map param) {
        String pt=(String) param.get("partner");
        String uid=(String) param.get("uid");
        if(!hasText(pt)){
            return createMessage(RETURN_ERROR_CODE, "user.login.error", 1);
        }
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }else{
           if(user.getStatus()==USER_STATE_DISABLED){
                return raiseError(1127);
           }
           if(!hasText(user.getChatid())){
               String chat=StringUtils.remove(user.getMobile(), "-");
               String cc=chatServices.add_chat_user(chat, user.getDecryptPassword(), "--");
               String cc1=chatServices.add_chat_user("wc"+chat, user.getDecryptPassword(), "--");
                if(hasText(cc)){
                    user.setChatid(cc);
                }
           }
           user.setLastLogin(new Date());
           if(!hasText(user.getTopicid()))user.setTopicid(Utils.generate_device_id());
           userRepo.save(user);
           Map m=new HashMap();
           m.put("uid", clientApp.set_uid(pt, user.getMobile()));
           m.put("uname", Utils.valueOf(user.getName()));
           m.put("unameEn", Utils.valueOf(user.getNameEn()));
           m.put("character", Utils.valueOf(user.getHeadUrl()));
           m.put("flag", user.getReceivingTaskNotice());
           m.put("pm", Utils.valueOf(user.getMobile()));
           m.put("acode", user.getArea().getId());
           m.put("acodeN", user.getArea().getName1());
           m.put("sl", Utils.valueOf(user.getCode()));
           m.put("chatid", Utils.valueOf(user.getChatid()));
           m.put("topic", clientApp.get_topic_info(pt,user.getTopicid()));
           if(user.getResidence()!=null){
               m.put("city", user.getResidence().getId());
               m.put("cityN", user.getResidence().getName1());
               TArea ta=user.getResidence().getParent();
               if(ta!=null){
                    m.put("country", ta.getId());
                    m.put("countryN", ta.getName1());
               }else{
                    m.put("country", "--");
                    m.put("countryN", "--");
               }
           }else{
               m.put("city", "--");
               m.put("cityN", "--");
               m.put("country", "--");
               m.put("countryN", "--");
           }
           m.put("pdate",clientApp.getSystemProperty("booking.pay.expired.time"));
           Map mp=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
           mp.put("info", m);
           return mp;
        }
    }

    @Override
    public Map get_user_info_by_openid(Map param) {
        String pt=(String) param.get("partner");
        if(!hasText(pt)){
            return createMessage(RETURN_ERROR_CODE, "user.login.error", 1);
        }
        String openid=(String) param.get("wid");
        if(!hasText(openid)){
            return raiseError(1032);
        }
        TUser user=userRepo.findByOpenid(openid);
        if(user==null){
            return raiseError(1000);
        }
        if(user.getStatus()==USER_STATE_DISABLED){
                return raiseError(1127);
        }
        String chat=StringUtils.remove(user.getMobile(), "-");
        String cc=chatServices.add_chat_user(chat, user.getDecryptPassword(), "--");
        String cc1=chatServices.add_chat_user("wc"+chat, user.getDecryptPassword(), "--");
         if(hasText(cc)){
             user.setChatid(cc);
         }
        user.setLastLogin(new Date());
        userRepo.save(user);
        Map m=new HashMap();
        m.put("uid", clientApp.set_uid(pt, user.getMobile()));
        m.put("uname", Utils.valueOf(user.getName()));
        m.put("unameEn", Utils.valueOf(user.getNameEn()));
        m.put("character", Utils.valueOf(user.getHeadUrl()));
        m.put("flag", user.getReceivingTaskNotice());
        m.put("pm", Utils.valueOf(user.getMobile()));
        m.put("acode", user.getArea().getId());
        m.put("acodeN", user.getArea().getName1());
        m.put("sl", Utils.valueOf(user.getCode()));
        if(user.getResidence()!=null){
            m.put("city", user.getResidence().getId());
            m.put("cityN", user.getResidence().getName1());
            TArea ta=user.getResidence().getParent();
            if(ta!=null){
                 m.put("country", ta.getId());
                 m.put("countryN", ta.getName1());
            }else{
                 m.put("country", "--");
                 m.put("countryN", "--");
            }
        }else{
            m.put("city", "--");
            m.put("cityN", "--");
            m.put("country", "--");
            m.put("countryN", "--");
        }
        m.put("pdate",clientApp.getSystemProperty("booking.pay.expired.time"));
        Map mp=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        mp.put("info", m);
        return mp;
    }

    @Override
    public Map iphone_logout(Map param) {
        String pt=(String) param.get("partner");
        String uid=(String) param.get("uid");
        if(!hasText(pt)){
            return createMessage(RETURN_ERROR_CODE, "user.login.error", 1);
        }
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(hasText(user.getDeviceid())){
            user.setDeviceid(null);
            userRepo.save(user);
        }
        Map mp=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return mp;
    }
    
}
