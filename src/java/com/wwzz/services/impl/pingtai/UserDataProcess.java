/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.impl.pingtai;

import com.wwzz.services.api.ChatManager;
import com.wwzz.services.api.SMServices;
import com.wwzz.services.api.UserNoticeServices;
import com.wwzz.services.api.pingtai.UserServices;
import com.wwzz.services.domain.admin.TAuthority;
import com.wwzz.services.domain.admin.TRole;
import com.wwzz.services.domain.admin.TStaff;
import com.wwzz.services.domain.base.TArea;
import com.wwzz.services.domain.base.TCarModels;
import com.wwzz.services.domain.base.TCityScenicSpots;
import com.wwzz.services.domain.base.TLang;
import com.wwzz.services.domain.base.TTourismLineCategory;
import com.wwzz.services.domain.base.TTravelAgency;
import com.wwzz.services.domain.users.TGroup;
import com.wwzz.services.domain.users.TGroupuserRelation;
import com.wwzz.services.domain.users.TUser;
import com.wwzz.services.domain.users.TUserDriverExpandedTemp;
import com.wwzz.services.domain.users.TUserDriverFilesTemp;
import com.wwzz.services.domain.users.TUserDriverTemp;
import com.wwzz.services.domain.users.TUserIdentity;
import com.wwzz.services.domain.users.TUserIdentityDocents;
import com.wwzz.services.domain.users.TUserIdentityDocentsExpanded;
import com.wwzz.services.domain.users.TUserIdentityDocentsFiles;
import com.wwzz.services.domain.users.TUserIdentityDriver;
import com.wwzz.services.domain.users.TUserIdentityDriverExpanded;
import com.wwzz.services.domain.users.TUserIdentityDriverFiles;
import com.wwzz.services.domain.users.TUserIdentityLCGuide;
import com.wwzz.services.domain.users.TUserIdentityLCGuideExpanded;
import com.wwzz.services.domain.users.TUserIdentityLCGuideFiles;
import com.wwzz.services.domain.users.TUserIdentityLongGuide;
import com.wwzz.services.domain.users.TUserIdentityLongGuideExpanded;
import com.wwzz.services.domain.users.TUserIdentityLongGuideFiles;
import com.wwzz.services.domain.users.TUserIdentityOGuide;
import com.wwzz.services.domain.users.TUserIdentityOGuideExpanded;
import com.wwzz.services.domain.users.TUserIdentityOGuideFiles;
import com.wwzz.services.domain.users.TUserIdentityOp;
import com.wwzz.services.domain.users.TUserIdentityOpExpanded;
import com.wwzz.services.domain.users.TUserOrganization;
import com.wwzz.services.domain.users.TUserProperty;
import com.wwzz.services.impl.ResultDataServices;
import com.wwzz.services.repository.TAreaRepository;
import com.wwzz.services.repository.TAuthorityRepository;
import com.wwzz.services.repository.TCarModelsRepository;
import com.wwzz.services.repository.TCityScenicSpotsRepository;
import com.wwzz.services.repository.TGroupRepository;
import com.wwzz.services.repository.TGroupuserRelationRepository;
import com.wwzz.services.repository.TLangRepository;
import com.wwzz.services.repository.TRoleRepository;
import com.wwzz.services.repository.TStaffRepository;
import com.wwzz.services.repository.TTourismLineCategoryRepository;
import com.wwzz.services.repository.TTravelAgencyRepository;
import com.wwzz.services.repository.TUserDriverExpandedTempRepository;
import com.wwzz.services.repository.TUserDriverFilesTempRepository;
import com.wwzz.services.repository.TUserDriverTempRepository;
import com.wwzz.services.repository.TUserIdentityDocentsExpandedRepository;
import com.wwzz.services.repository.TUserIdentityDocentsFilesRepository;
import com.wwzz.services.repository.TUserIdentityDocentsRepository;
import com.wwzz.services.repository.TUserIdentityDriverExpandedRepository;
import com.wwzz.services.repository.TUserIdentityDriverFilesRepository;
import com.wwzz.services.repository.TUserIdentityDriverRepository;
import com.wwzz.services.repository.TUserIdentityLCGuideExpandedRepository;
import com.wwzz.services.repository.TUserIdentityLCGuideFilesRepository;
import com.wwzz.services.repository.TUserIdentityLocalGuideRepository;
import com.wwzz.services.repository.TUserIdentityLongGuideExpandedRepository;
import com.wwzz.services.repository.TUserIdentityLongGuideFilesRepository;
import com.wwzz.services.repository.TUserIdentityLongGuideRepository;
import com.wwzz.services.repository.TUserIdentityOGuideExpandedRepository;
import com.wwzz.services.repository.TUserIdentityOGuideFilesRepository;
import com.wwzz.services.repository.TUserIdentityOpExpandedReposiotry;
import com.wwzz.services.repository.TUserIdentityOpRepository;
import com.wwzz.services.repository.TUserIdentityOverseaslGuideRepository;
import com.wwzz.services.repository.TUserIdentityRepository;
import com.wwzz.services.repository.TUserOrganizationRepository;
import com.wwzz.services.repository.TUserPropertyRepository;
import com.wwzz.services.repository.TUserRepository;
import com.wwzz.services.utils.Constants;
import static com.wwzz.services.utils.Constants.RETURN_SUCCESS_CODE;
import com.wwzz.services.utils.CheckRequest;
import com.wwzz.services.utils.WStringUtils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author tiger
 * This class is user management service one that do main process such as database read, add, delete, update etc.
 * This implements UserService interface.
 */
@Service
@Transactional
public class UserDataProcess extends ResultDataServices implements UserServices, Constants{
    
    @Value("#{serverinfo['apps.key']}")
    private String key;
     
      @Autowired
    private TAreaRepository areaRepository;
    
    @Autowired
    private TLangRepository langRepository;
    
    @Autowired
    private TTravelAgencyRepository travelRepository;
    
    @Autowired
    private TTourismLineCategoryRepository tcRepository;
     
     @Autowired
    private TUserIdentityDriverRepository udrRepository;
    
    @Autowired
    private TUserIdentityDriverExpandedRepository uddeRepository;
    
    @Autowired
    private TUserIdentityDriverFilesRepository uddfRepository;
    
    @Autowired
    private TUserIdentityLCGuideExpandedRepository uleRepository;
    
    @Autowired
    private TUserIdentityLCGuideFilesRepository ulfRepository;
    
    @Autowired
    private TUserIdentityLongGuideExpandedRepository ugeRepository;
    
    @Autowired
    private TUserIdentityLongGuideFilesRepository ugfRepository;
    
    @Autowired
    private TUserIdentityOGuideExpandedRepository uoeRepository;
    
    @Autowired
    private TUserIdentityOGuideFilesRepository uofRepository;
    
    @Autowired
    private TUserIdentityOpExpandedReposiotry uopeRepository;
    
    @Autowired
    private TGroupRepository groupRepository;
    
    @Autowired
    private SMServices smservices;
    
    @Autowired
    private TUserRepository userRepository;
    
     @Autowired
    private TUserIdentityOpRepository uoRepository;
    
    @Autowired
    private TUserIdentityDocentsRepository udRepository;
    
    @Autowired
    private TUserIdentityLocalGuideRepository userLocalRepository;
    
    @Autowired
    private TUserIdentityLongGuideRepository userLongRepository;
    
    @Autowired
    private TUserIdentityOverseaslGuideRepository udsRepository;
    
    @Autowired
    private TUserPropertyRepository upRepository;
    
    @Autowired
    private TUserIdentityRepository uiRepository;
    
     @Autowired
    private TUserOrganizationRepository orgRepository;
     
      @Autowired
    private TCityScenicSpotsRepository ssRepository;
      
       @Autowired
    private TUserIdentityDocentsExpandedRepository udeRepository;
    
    @Autowired
    private TUserIdentityDocentsFilesRepository udfRepository;
    
    @Autowired
    private TUserIdentityOpExpandedReposiotry opeRepository;
    
       @Autowired
    private TCarModelsRepository modelRepository;
       
       @Autowired
       private TStaffRepository staffRepository;
       
       @Autowired
       private TRoleRepository roleRepository;
       
       @Autowired
       private TAuthorityRepository authRepository;
       
       @Autowired
       private UserNoticeServices noticeServices;
       
       @Autowired
       private TUserDriverTempRepository dtempRepository;
       
       @Autowired
       private TUserDriverExpandedTempRepository detempRepository;
       
       @Autowired
       private TUserDriverFilesTempRepository dftempRepository;
       
       @Autowired
        private ChatManager chatServices;
       
       @Autowired
       private TGroupuserRelationRepository grRepository;
       
       @PersistenceContext
       private EntityManager em;
    
    private Map add(Map map){
        map.put("partner", "11111111111111111111");
        Calendar cal=Calendar.getInstance();
        cal.setTime(new Date());
        map.put("timestamp", cal.getTime().getTime());
        CheckRequest cr=new CheckRequest(key, map);
        map.put("sign", cr.hashParams());
        return map;
    }
    
    /**
     * Add user group.
     * @param map HashMap
     * @return map HashMap
     */
      @Override
    public Map ugroup_add(Map map) {
        Map m=(Map) map.get("data");
        Map rm=new HashMap();
        TGroup lang=new TGroup();
       lang.setCreated(new Date());
       lang.setModified(new Date());
       lang.setName((String) m.get("name"));
       if((String)m.get("description")!=null)lang.setDescription((String) m.get("description"));
       TGroup gr=groupRepository.findByName(lang.getName());
       if(gr==null){
           groupRepository.save(lang);
             rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
             Map mmm=new HashMap();
             mmm.put("id", lang.getId());
             rm.put("data", mmm);
       }else {
             rm=raiseError(2000);
             rm.put("success", false);
       }
       return add(rm);
    }
    
    /**
     * Delete user group.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map ugroup_delete(Map map) {
        Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        try{
            List list=grRepository.findByGroup(id);
            if(list!=null && !list.isEmpty()){
                rm=raiseError(2007);
                rm.put("success", false);
                return add(rm);
            }
            groupRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read user group.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map ugroup_read(Map model) {
          Map rm=new HashMap();
             Map map=(Map) model.get("data");
          String key=(String) map.get("key");
          List<TGroup> list=new ArrayList<>();
          if(key!=null)list=groupRepository.findByNameContaining(key);
          else list=groupRepository.findAll();
          List lst=new ArrayList();
        for(TGroup air:list){
            Map mm=new HashMap();
            mm.put("id", air.getId().toString());
            mm.put("name", air.getName());
            mm.put("description", air.getDescription());
            lst.add(mm);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("data", lst);
        return add(rm);
    }
    
    /**
     * Update user group.
     * @param map HashMap
     * @return map HashMap
     */
        @Override
    public Map ugroup_update(Map map) {
       Map rm=new HashMap();
       Map m=(Map) map.get("data");
       TGroup lang=groupRepository.findOne(Long.valueOf((String) m.get("id")));
       if((String)m.get("name")!=null)lang.setName((String) m.get("name_zh"));
        if((String)m.get("description")!=null)lang.setDescription((String) m.get("description"));
       TGroup gr=groupRepository.findByName(lang.getName());
       if((String)m.get("name")!=null){
           if(gr!=null){
               if(gr.getName().equals(lang.getName())){
                   rm=raiseError(2000);
                   rm.put("success", false);
               }else{
                   groupRepository.save(lang);
                   rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
               }
           }else{
               groupRepository.save(lang);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
           }
       }else{
           groupRepository.save(lang);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
       }
       return add(rm);
    }

    /**
     * Add user.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map uset_add(Map map) {
         Map rm=new HashMap();
        Map m=(Map) map.get("data");
        String id=(String) m.get("residence_id");
        TArea tt=areaRepository.findOne(id);
        String aid=(String)m.get("area_id");
        TArea area=areaRepository.findOne(aid);
        String cas=(String) m.get("cas");
        List<TLang> list=new ArrayList<>();
        String[] cass=cas.substring(1).split(",");
        for (String cas1 : cass) {
            Long cid = Long.valueOf(cas1);
            TLang cc=langRepository.findOne(cid);
            list.add(cc);
        }
        String mobile=(String) m.get("mobile");
        TUser ta=new TUser();
        ta.setCreated(new Date());
        ta.setName((String) m.get("name"));
        ta.setModified(new Date());
        if(m.containsKey("nameEn"))ta.setNameEn((String)m.get("nameEn"));
        ta.setGender((Integer)m.get("gender"));
        ta.setStatus(1);
        ta.setReceivingTaskNotice((Boolean)m.get("receivingTaskNotice")?1:0);
        ta.setSalt(WStringUtils.randomString(20).toUpperCase());
        ta.encryptPassword((String)m.get("password"));
        ta.setArea(area);
        ta.setMobile(area.getCode()+'-'+mobile);
        ta.setResidence(tt);
      
        if(m.containsKey("phone") && ((String)m.get("phone")!=null))ta.setPhone(area.getCode()+"-"+(String)m.get("phone"));
         if(m.containsKey("icon_url") && !((String) m.get("icon_url")).isEmpty())ta.setHeadUrl((String) m.get("icon_url"));
         if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
             ta.setHeadStorageUrl((String) m.get("idonotknow"));
         }
        TUser muser=userRepository.findByMobileAndArea(ta.getMobile(), ta.getArea());
        if(muser!=null){
            rm=raiseError(2000);
            rm.put("success", false);
            return add(rm);
        }
     
       ta.setLangs(list);
       if(!hasText(ta.getChatid())){
                String chat=chatServices.add_chat_user(area.getCode()+mobile, ta.getDecryptPassword(), "--");
                String ch=chatServices.add_chat_user("wc"+area.getCode()+mobile, ta.getDecryptPassword(), "--");
                if(hasText(chat)){
                    ta.setChatid(chat);
                }
                if(hasText(ch)){
                    ta.setOpenid(ch);
                }
               userRepository.save(ta);
       } 
       TUser ppa=userRepository.findByMobile(area.getCode()+"-"+mobile);
       TUserIdentity tud=new TUserIdentity();
       tud.setCreated(new Date());
       tud.setModified(new Date());
       tud.setHasIdentityDocents(false);
       tud.setHasIdentityDriver(false);
       tud.setHasIdentityLocalGuide(false);
       tud.setHasIdentityLongGuide(false);
       tud.setHasIdentityOp(false);
       tud.setHasIdentityOverseasLocalGuide(false);
       tud.setStatusDc(USER_STATE_INIT);
       tud.setStatusDrv(USER_STATE_INIT);
       tud.setStatusLc(USER_STATE_INIT);
       tud.setStatusLg(USER_STATE_INIT);
       tud.setStatusOg(USER_STATE_INIT);
       tud.setStatusOp(USER_STATE_INIT);
       tud.setIrregularitiesNumber(0);
       tud.setServiceNumber(0);
       tud.setUser(ppa);
       uiRepository.save(tud);
       
       if(m.containsKey("group_id")){
            String gid=m.get("group_id").toString();
            if(gid!=null && !gid.isEmpty() && !gid.contains("-")){
                TGroupuserRelation gr=new TGroupuserRelation();
                gr.setGroup(Long.valueOf(gid));
                gr.setUser(ta.getId());;
                grRepository.save(gr);
            }
        }
       rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
       Map mmm=new HashMap();
       mmm.put("id", ta.getId());
       mmm.put("created", WStringUtils.dateToString(ta.getCreated()));
       rm.put("data", mmm);
        
        return add(rm);
    }

    /**
     * Delete user
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map uset_delete(Map map) {
        Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        try{
            TUser user=userRepository.findOne(id);
            TUserProperty up=upRepository.findByUser(user);
            upRepository.delete(up);
            userRepository.delete(user);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read user.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map uset_read(Map model) {
        
         Map rm=new HashMap();
         Map map=(Map) model.get("data");
         String key=(String) map.get("key");
         int cat=Integer.valueOf(map.get("scat").toString());
         int start=(int) map.get("page");
          if(start<1)start=1;
          int count=15;
          if(hasText(key)){
            key="%"+key.toLowerCase()+"%";
          }else{
            key="%%";
         }
          int total=0;
         Pageable page=new PageRequest(start-1, count);
        Page<TUser> dt=null;
//         if(cat==1){
//             total=userRepository.count_user_by_mobile_zstatus(key, 0).intValue();
//             if(total<(start-1)*count){
//                 page=new PageRequest(0, count);
//             }
//             dt=userRepository.find_user_by_mobile_zstatus(key, 0, page);
//         }else
         if(cat==2){
             total=userRepository.count_user_by_mobile_status(key).intValue();
             if(total<(start-1)*count){
                 page=new PageRequest(0, count);
             }
             dt=userRepository.find_user_by_mobile_status(key, page);
         }else if(cat==3){
             total=userRepository.countByMobileContainingAndStatusOrderByCreatedDesc(key, cat).intValue();
             if(total<(start-1)*count){
                 page=new PageRequest(0, count);
             }
             dt=userRepository.findByMobileContainingAndStatusOrderByCreatedDesc(key, cat, page);
         }else{
             total=userRepository.countByMobileContainingOrderByCreatedDesc(key).intValue();
             if(total<(start-1)*count){
                 page=new PageRequest(0, count);
             }
             dt=userRepository.findByMobileContainingOrderByCreatedDesc(key, page);
         }
         List<TUser> list=dt.getContent();
        List lst=new ArrayList();
        for(TUser air:list){
            Map mm=new HashMap();
            mm.put("id", air.getId().toString());
            mm.put("name", air.getName());
            mm.put("nameEn", air.getNameEn());
            mm.put("gender", air.getGender());
            try{
            if(air.getStatus()!=3){
                int status=2;
                TUserIdentity ui=air.getIdentity();
                if(ui==null){
                    mm.put("status", 10);
                    mm.put("statusDc", true);
                    mm.put("statusDrv", true);
                    mm.put("statusLc", true);
                    mm.put("statusLg", true);
                    mm.put("statusOg", true);
                    mm.put("statusOp", true);
                }
                else{
                      if(ui.isHasIdentityDocents() && ui.getStatusDc()!=2){
                           mm.put("statusDc", false);
                            status=1;
                        }else mm.put("statusDc", true);
                        if(ui.isHasIdentityDriver() && ui.getStatusDrv()!=2){
                            mm.put("statusDrv", false);
                            status=1;
                        } else mm.put("statusDrv", true);
                        if(ui.isHasIdentityLocalGuide() && ui.getStatusLc()!=2){
                            mm.put("statusLc", false);
                            status=1;
                        }else mm.put("statusLc", true);
                        if(ui.isHasIdentityLongGuide() && ui.getStatusLg()!=2){
                            mm.put("statusLg", false);
                            status=1;
                        }else mm.put("statusLg", true);
                        if(ui.isHasIdentityOverseasLocalGuide() && ui.getStatusOg()!=2){
                            mm.put("statusOg", false);
                            status=1;
                        }else mm.put("statusOg", true);
                        if(ui.isHasIdentityOp() && ui.getStatusOp()!=2){
                            mm.put("statusOp", false);
                            status=1;
                        }else mm.put("statusOp", true);
                        mm.put("status", status);
                }
            }
            if(air.getStatus()==3) mm.put("status", 3);
            mm.put("receivingTaskNotice", (air.getReceivingTaskNotice()==1));
            mm.put("mobile", air.getMobile().substring(air.getMobile().indexOf("-")+1));
            if(air.getPhone()!=null && air.getPhone().contains("-")){
                int st=air.getPhone().indexOf("-");
                if(air.getPhone().length()>st)mm.put("phone", air.getPhone().substring(st+1));
            }
            if(air.getArea()!=null)mm.put("area_id", air.getArea().getId());
            mm.put("lastLogin", air.getLastLogin());
            if(air.getResidence()!=null)mm.put("residence_id", air.getResidence().getId());
            if(air.getResidence()!=null)mm.put("residence_name", air.getResidence().getName1());
            else mm.put("residence_name", "");
            mm.put("created", WStringUtils.dateToString(air.getCreated()));
            mm.put("modified", WStringUtils.dateToString(air.getModified()));
            mm.put("icon_url", air.getHeadUrl());
            mm.put("code", air.getCode());
            TGroupuserRelation groups=grRepository.findByUser(air.getId());
           if(groups!=null){
                    mm.put("group_id", groups.getGroup());
            }
            String st_ca="";
            for(TLang tcc:air.getLangs()){
                st_ca=st_ca+tcc.getNameZh()+" , ";
            }
            mm.put("cas_name", st_ca);
            lst.add(mm);
            }catch(Exception exx){
                String sssss="";
            }
        }
        
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("data", lst);
        rm.put("total", total);
        return add(rm);
    }

    /**
     * Update user.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map uset_update(Map map) {
        Map m=(Map) map.get("data");
         Map rm=new HashMap();
         Long id=Long.valueOf((String)m.get("id"));
         TUser ta=userRepository.findOne(id);
         if(m.containsKey("status")){
             ta.setStatus((Integer)m.get("status"));
             userRepository.save(ta);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            return add(rm);
         }
         
         if(m.containsKey("code")){
             Integer val;
             try{
                 val=userRepository.findMaxValue();
             }catch(Exception e){
                 val=0;
             }
             val++;
             String mval="";
             if(val>10000)mval=val.toString();
             else{
                    for(int i=0;i<4-val.toString().length();i++){
                           mval=mval+String.valueOf(0);
                    }
                    mval=mval+val.toString();
             }
             String sval="SL-"+ta.getArea().getCode()+"-"+mval;
             ta.setCode(sval);
            Map mmm= smservices.send_sms_to(ta.getMobile().replace("-", ""), getResource("phone.test.msg", sval));
            userRepository.save(ta);
            
            return add(mmm);
            
         }
         
         if(m.containsKey("icon_url") && !((String) m.get("icon_url")).isEmpty())ta.setHeadUrl((String) m.get("icon_url"));
        if(m.containsKey("area_id")){
            String aid=(String)m.get("area_id");
            TArea area=areaRepository.findOne(aid);
            ta.setArea(area);
        }
        
        
        if(m.containsKey("name"))ta.setName((String) m.get("name"));
        if(m.containsKey("nameEn"))ta.setNameEn((String)m.get("nameEn"));
        ta.setModified(new Date());
        if(m.containsKey("gender"))ta.setGender((Integer)m.get("gender"));
        
        if(m.containsKey("receivingTaskNotice"))ta.setReceivingTaskNotice((Boolean)m.get("receivingTaskNotice")?1:0);
        
        if(m.containsKey("mobile"))ta.setMobile(ta.getArea().getCode()+"-"+(String)m.get("mobile"));
        
        if(m.containsKey("phone"))ta.setPhone(ta.getArea().getCode()+"-"+(String)m.get("phone"));
        TUser muser=userRepository.findByMobileAndArea(ta.getMobile(), ta.getArea());
        if(muser!=null){
            if(!muser.getId().equals(ta.getId())){
                rm=raiseError(2000);
                rm.put("success", false);
                return add(rm);
            }
        }
        if(m.containsKey("cas")){
            String cas=(String) m.get("cas");
            List<TLang> list=new ArrayList<>();
            String[] cass=cas.substring(1).split(",");
            for (String cas1 : cass) {
                Long cid = Long.valueOf(cas1);
                TLang cc=langRepository.findOne(cid);
                list.add(cc);
            }
            ta.setLangs(list);
        }
        if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
              if(ta.getHeadStorageUrl()!=null)file_delete_on_fastdfs(ta.getHeadStorageUrl());
               String donot=(String) m.get("idonotknow");
               ta.setHeadStorageUrl(clientApp.get_uid((String)map.get("partner"), donot));
         }
        
        userRepository.save(ta);
        
       if(m.containsKey("group_id")){
            String gid=(String) m.get("group_id");
            if(gid!=null && !gid.isEmpty() && !gid.contains("-")){
                TGroupuserRelation ugr=grRepository.findByUser(ta.getId());
                if(ugr==null){
                    ugr=new TGroupuserRelation();
                    ugr.setUser(ta.getId());
                }
                ugr.setGroup(Long.valueOf(gid));
                grRepository.save(ugr);
            }
        }
       rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
       return add(rm);
    }

    /**
     * Add user property.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map upr_add(Map map) {
         Map m=(Map) map.get("data");
       Map rm=new HashMap();
       
       String code=m.get("user_id").toString();
       TUser ta=userRepository.findOne(Long.valueOf(code));
       if(ta==null){
           rm=raiseError(2005);
           rm.put("success", false);
       }else{
            TUserProperty up=upRepository.findByUser(ta);
            if(up==null){
                up=new TUserProperty();
                up.setCreated(new Date());
                up.setRegate(new Date());
            }
                 if(m.containsKey("nationality"))up.setNationality((String)m.get("nationality"));
                if(m.containsKey("birthday")){
                    Long dd=Long.valueOf((String)m.get("birthday"))*1000;
                    Date td=new Date();
                    td.setTime(dd);
                    up.setBirthday(td);
                }
                if(m.containsKey("idcard"))up.setIdcard((String)m.get("idcard"));
                if(m.containsKey("passport"))up.setPassport((String)m.get("passport"));
                if(m.containsKey("education"))up.setEducation((String)m.get("education"));
                if(m.containsKey("introduce"))up.setIntroduce((String)m.get("introduce"));
                if(m.containsKey("talents"))up.setTalents((String)m.get("talents"));
                if(m.containsKey("hobby"))up.setHobby((String)m.get("hobby"));
                if(m.containsKey("opinion"))up.setOpinion((String)m.get("opinion"));
                up.setModified(new Date());
                up.setUser(ta);
                upRepository.save(up);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
        return add(rm);
    }

    /**
     * Read user property.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map upr_read(Map model) {
          Map rm=new HashMap();
          Map map=(Map) model.get("data");
          String rid=(String) map.get("rid");
          TUser res=userRepository.findOne(Long.valueOf(rid));
          TUserProperty up=upRepository.findByUser(res);
          Map mm=new HashMap();
          if(up!=null){
              mm.put("id", up.getId());
              mm.put("nationality", up.getNationality());
            mm.put("birthday", up.getBirthday());
            mm.put("idcard", up.getIdcard());
            mm.put("passport", up.getPassport());
            mm.put("education", up.getEducation());
            mm.put("introduce", up.getIntroduce());
            mm.put("talents", up.getTalents());
            mm.put("hobby", up.getHobby());
            mm.put("opinion", up.getOpinion());
            mm.put("proposer", up.getProposer());
            mm.put("regDate", WStringUtils.dateToString(up.getRegate()));
            mm.put("consumpAmount", up.getConsumpAmount());
            mm.put("revenueAmount", up.getRevenueAmount());
          }
         
         rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         rm.put("data", mm);
          return add(rm);
    }

    /**
     * Add user identity.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map udn_add(Map map) {
         Map m=(Map) map.get("data");
       Map rm=new HashMap();
       
       String code= m.get("user_id").toString();
       TUser ta=userRepository.findOne(Long.valueOf(code));
       if(ta==null){
           rm=raiseError(2005);
           rm.put("success", false);
       }else{
            TUserIdentity up=uiRepository.findByUser(ta);
            if(up==null){
                up=new TUserIdentity();
                up.setCreated(new Date());
                up.setAppointmentsNumber(0);
                up.setHasIdentityDocents(false);
                up.setHasIdentityDriver(false);
                up.setHasIdentityLocalGuide(false);
                up.setHasIdentityLongGuide(false);
                up.setHasIdentityOp(false);
                up.setHasIdentityOverseasLocalGuide(false);
                up.setIrregularitiesNumber(0);
                up.setServiceNumber(0);
            }
                 if(m.containsKey("hasIdentityOp"))up.setHasIdentityOp((Boolean)m.get("hasIdentityOp"));
                 if(m.containsKey("hasIdentityLongGuide"))up.setHasIdentityLongGuide((Boolean)m.get("hasIdentityLongGuide"));
                if(m.containsKey("hasIdentityOverseasLocalGuide"))up.setHasIdentityOverseasLocalGuide((Boolean)m.get("hasIdentityOverseasLocalGuide"));
                if(m.containsKey("hasIdentityLocalGuide"))up.setHasIdentityLocalGuide((Boolean)m.get("hasIdentityLocalGuide"));
                if(m.containsKey("hasIdentityDriver"))up.setHasIdentityDriver((Boolean)m.get("hasIdentityDriver"));
                if(m.containsKey("hasIdentityDocents"))up.setHasIdentityDocents((Boolean)m.get("hasIdentityDocents"));
                up.setModified(new Date());
               up.setUser(ta);
               uiRepository.save(up);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
        return add(rm);
    }

    /**
     * Read user identity.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map udn_read(Map model) {
          Map rm=new HashMap();
          Map map=(Map) model.get("data");
          String rid=(String) map.get("rid");
          TUser res=userRepository.findOne(Long.valueOf(rid));
          TUserIdentity up=uiRepository.findByUser(res);
          Map mm=new HashMap();
          if(up!=null){
              mm.put("id", up.getId());
              mm.put("hasIdentityOp", up.isHasIdentityOp());
            mm.put("hasIdentityLongGuide", up.isHasIdentityLongGuide());
            mm.put("hasIdentityOverseasLocalGuide", up.isHasIdentityOverseasLocalGuide());
            mm.put("hasIdentityLocalGuide", up.isHasIdentityLocalGuide());
            mm.put("hasIdentityDriver", up.isHasIdentityDriver());
            mm.put("hasIdentityDocents", up.isHasIdentityDocents());
            mm.put("irregularitiesNumber", up.getIrregularitiesNumber());
            mm.put("appointmentsNumber", up.getAppointmentsNumber());
            mm.put("serviceNumber", up.getServiceNumber());
            mm.put("created", WStringUtils.dateToString(up.getCreated()));
            mm.put("modified", WStringUtils.dateToString(up.getModified()));
          }
         rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         rm.put("data", mm);
          return add(rm);
    }

    /**
     * Add user identity.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map udo_add(Map map) {
        Map m=(Map) map.get("data");
       Map rm=new HashMap();
       
       String code=m.get("user_id").toString();
       TUser ta=userRepository.findOne(Long.valueOf(code));
       
       if(ta==null){
           rm=raiseError(2005);
           rm.put("success", false);
       }else{
           if(m.containsKey("flag")){
               Integer bb=(Integer) m.get("flag");
               if(bb==100 || bb==200){
                   if(bb==100){
                       TUserIdentityOp oop=uoRepository.findByUserAndStatus(ta, 1);
                       if(oop!=null)uoRepository.delete(oop);
                       TUserIdentityOp kop=uoRepository.findByUserAndStatus(ta, 0);
                       kop.setStatus(1);
                       uoRepository.save(kop);
                       List<TUserIdentityOpExpanded> dfs=uopeRepository.findByUserAndStatus(ta, 1);
                       if(dfs!=null && !dfs.isEmpty())uopeRepository.delete(dfs);
                       List<TUserIdentityOpExpanded> dffs=uopeRepository.findByUserAndStatus(ta, 0);
                       for(TUserIdentityOpExpanded ffs:dffs){
                           ffs.setStatus(1);
                           uopeRepository.save(ffs);
                       }
                       TUserIdentity udd=uiRepository.findByUser(ta);
                       udd.setStatusOp(2);
                       udd.setHasIdentityOp(true);
                       uiRepository.save(udd);
                       
                   }else{
                       TUserIdentityOp kop=uoRepository.findByUserAndStatus(ta, 0);
                       uoRepository.delete(kop);
                       List<TUserIdentityOpExpanded> dfs=uopeRepository.findByUserAndStatus(ta, 0);
                       if(dfs!=null && !dfs.isEmpty())uopeRepository.delete(dfs);
                       List<TUserIdentityOpExpanded> fs=uopeRepository.findByUserAndStatus(ta, 1);
                       for(TUserIdentityOpExpanded of:fs){
                           of.setFlag(1);
                           uopeRepository.save(of);
                       }
                       if(dfs!=null && !dfs.isEmpty())uopeRepository.delete(dfs);
                       TUserIdentityOp ddss=uoRepository.findByUserAndStatus(ta, 1);
                       TUserIdentity uu=uiRepository.findByUser(ta);
                        if(ddss==null){
                            uu.setHasIdentityOp(false);
                            uu.setStatusOp(0);
                        }else{
                            uu.setHasIdentityOp(true);
                            uu.setStatusOp(2);
                        }
                        uiRepository.save(uu);
                   }
                    Map tm=new HashMap();
                    tm.put("partner", map.get("partner"));
                    tm.put("uid", ta.getId().toString());
                    tm.put("flag", bb==100?1:2);
                    tm.put("cat", 1);
                    noticeServices.user_skill_notice(tm);
                    rm.put("success", true);
                    return add(rm);
               }
              
           }
           int cid=(int) m.get("cid");
           TUserIdentityOp up=uoRepository.findByUserAndStatus(ta,cid);
            if(up==null){
                    up=new TUserIdentityOp();
                    up.setCreated(new Date());
                    up.setStatus(cid);
            }
            if(m.containsKey("tid")){
                String tid=(String)m.get("tid");
                 TTravelAgency tra=travelRepository.findOne(Long.valueOf(tid));
                  up.setTravel(tra);
            }
                 if(m.containsKey("supportTourAssemble"))up.setSupportTourAssemble((Boolean)m.get("supportTourAssemble"));
                 if(m.containsKey("supportTourIndependent"))up.setSupportTourIndependent((Boolean)m.get("supportTourIndependent"));
                if(m.containsKey("supportTourMice"))up.setSupportTourMice((Boolean)m.get("supportTourMice"));
                if(m.containsKey("tor_ids")){
                    List objs=(List) m.get("tor_ids");
                    List<TTourismLineCategory> lines=new ArrayList<>();
                    for(int i=0;i<objs.size();i++){
                        String ids=(String) objs.get(i);
                        TTourismLineCategory ca=tcRepository.findOne(Long.valueOf(ids));
                        lines.add(ca);
                    }
                    up.setLineCategories(lines);
                }
                up.setModified(new Date());
                
               up.setUser(ta);
               uoRepository.save(up);
               
               if(cid==1){
                            TUserIdentity udd=uiRepository.findByUser(ta);
                            udd.setStatusOp(2);
                            udd.setHasIdentityOp(true);
                            uiRepository.save(udd);
                        }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
        return add(rm);
    }

    /**
     * Read user identity.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map udo_read(Map model) {
         Map rm=new HashMap();
         Map map=(Map) model.get("data");
          String rid=(String) map.get("rid");
          TUser res=userRepository.findOne(Long.valueOf(rid));
          List datas=new ArrayList();
          for(int i=0;i<2;i++){
              TUserIdentityOp up=uoRepository.findByUserAndStatus(res,1-i);
                Map mm=new HashMap();
                if(up!=null){
                  mm.put("id", up.getId());
                  mm.put("supportTourAssemble", up.isSupportTourAssemble());
                  mm.put("supportTourIndependent", up.isSupportTourIndependent());
                  mm.put("supportTourMice", up.isSupportTourMice());
                  mm.put("tid", up.getTravel().getId());
                  mm.put("created", WStringUtils.dateToString(up.getCreated()));
                  mm.put("modified", WStringUtils.dateToString(up.getModified()));
                  if(up.getLineCategories()!=null){
                      List<String> tors=new ArrayList<>();
                      for(TTourismLineCategory ca:up.getLineCategories()){
                          tors.add(ca.getId().toString());
                      }
                      mm.put("tor_ids", tors);
                  }
                  mm.put("cid", up.getStatus());
                  datas.add(mm);
                }
          }
          
         rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         rm.put("data", datas);
          return add(rm);
    }

    /**
     * Add user identity docent.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map udd_add(Map map) {
         Map m=(Map) map.get("data");
       Map rm=new HashMap();
       String code=m.get("user_id").toString();
       TUser ta=userRepository.findOne(Long.valueOf(code));
       if(ta==null){
           rm=raiseError(2005);
           rm.put("success", false);
       }else{
           try{
         if(m.containsKey("flag")){
               Integer bb=(Integer) m.get("flag");
               if(bb==100 || bb==200){
                    if(bb==100){
                       TUserIdentityDocents oop=udRepository.findByUserAndStatus(ta, 1);
                       if(oop!=null){
                           udRepository.delete(oop);
                           List<TUserIdentityDocentsFiles> dds=udfRepository.findByUserAndStatus(ta, 1);
                           udfRepository.delete(dds);
                       }
                       TUserIdentityDocents kop=udRepository.findByUserAndStatus(ta, 0);
                       kop.setStatus(1);
                       udRepository.save(kop);
                       TUserIdentity udd=uiRepository.findByUser(ta);
                       udd.setStatusDc(2);
                       udd.setHasIdentityDocents(true);
                       uiRepository.save(udd);
                       List<TUserIdentityDocentsFiles> ddfs=udfRepository.findByUserAndStatus(ta, 1);
                       if(ddfs!=null && !ddfs.isEmpty() )udfRepository.delete(ddfs);
                       List<TUserIdentityDocentsFiles> dds=udfRepository.findByUserAndStatus(ta, 0);
                       for(TUserIdentityDocentsFiles ff:dds){
                           ff.setStatus(1);
                           udfRepository.save(ff);
                       }
                   }else{
                       TUserIdentityDocents kop=udRepository.findByUserAndStatus(ta, 0);
                       udRepository.delete(kop);
                        List<TUserIdentityDocentsFiles> dds=udfRepository.findByUserAndStatus(ta, 0);
                       udfRepository.delete(dds);
                       List<TUserIdentityDocentsFiles> fs=udfRepository.findByUserAndStatus(ta, 1);
                       for(TUserIdentityDocentsFiles f:fs){
                           f.setFlag(1);
                           udfRepository.save(f);
                       }
                        TUserIdentityDocents ddss=udRepository.findByUserAndStatus(ta, 1);
                        TUserIdentity uu=uiRepository.findByUser(ta);
                        if(ddss==null ){
                            uu.setHasIdentityDocents(false);
                            uu.setStatusDc(0);
                        }else{
                            uu.setHasIdentityDocents(true);
                            uu.setStatusDc(2);
                        }
                        uiRepository.save(uu);
                   }
                    Map tm=new HashMap();
                    tm.put("partner", map.get("partner"));
                    tm.put("uid", ta.getId().toString());
                    tm.put("flag", bb==100?1:2);
                    tm.put("cat", 6);
                    noticeServices.user_skill_notice(tm);
                    rm.put("success", true);
                    return add(rm);
               }
           }
                 int cid=(int) m.get("cid");
                    TUserIdentityDocents up=udRepository.findByUserAndStatus(ta,cid);
                    TUserIdentityDocentsExpanded ude;
                    if(up==null){
                            up=new TUserIdentityDocents();
                            ude=new TUserIdentityDocentsExpanded();
                            up.setStatus(cid);
                    }else{
                        ude=up.getProperties();
                        if(ude==null)ude=new TUserIdentityDocentsExpanded();
                    }
                       if(m.containsKey("oid")){
                             String oid=(String) m.get("oid");
                             TUserOrganization org=orgRepository.findOne(Long.valueOf(oid));
                              ude.setOrganization(org);
                         }
                        if(m.containsKey("edate")){

                            Long dd=Long.valueOf((String)m.get("edate"))*1000;
                            Date td=new Date();
                            td.setTime(dd);
                            ude.setEmploymentDate(td);
                        }
                        if(m.containsKey("nationality"))up.setNationality((Integer)m.get("nationality"));
                        if(m.containsKey("rman"))ude.setReferee((String)m.get("rman"));
                        if(m.containsKey("museums")){
                            List objs=(List) m.get("museums");
                            List<TCityScenicSpots> lines=new ArrayList<>();
                            for(int i=0;i<objs.size();i++){
                                String ids=(String) objs.get(i);
                                TCityScenicSpots ca=ssRepository.findOne(Long.valueOf(ids));
                                lines.add(ca);
                            }
                            up.setSpots(lines);
                        }
                        up.setUser(ta);
                       udRepository.save(up);
                       ude.setIdentity(up);
                        udeRepository.save(ude);
                        if(cid==1){
                            TUserIdentity udd=uiRepository.findByUser(ta);
                            udd.setStatusDc(2);
                            udd.setHasIdentityDocents(true);
                            uiRepository.save(udd);
                        }
                        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                    }catch(Exception e)
                    {
                         rm=raiseError(3000);
                         rm.put("success", false);
                    }
       }
        return add(rm);
    }

    /**
     * Read user identity docent.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map udd_read(Map model) {
         Map rm=new HashMap();
         Map map=(Map) model.get("data");
          String rid=(String) map.get("rid");
          TUser res=userRepository.findOne(Long.valueOf(rid));
          List datas=new ArrayList();
          for(int i=0;i<2;i++){
            TUserIdentityDocents up=udRepository.findByUserAndStatus(res,1-i);
            Map mm=new HashMap();
            if(up!=null){
                mm.put("id", up.getId());
                mm.put("oid", up.getProperties().getOrganization().getId().toString());
                mm.put("nationality", up.getNationality());
                mm.put("rman", up.getProperties().getReferee());
               if(up.getSpots()!=null){
                  List<String> tors=new ArrayList<>();
                  for(TCityScenicSpots ca:up.getSpots()){
                      tors.add(ca.getId().toString());
                  }
                  mm.put("edate", up.getProperties().getEmploymentDate());
                  mm.put("museums", tors);
              }
               mm.put("cid", up.getStatus());
              datas.add(mm);
            }
          }
         rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         rm.put("data", datas);
          return add(rm);
    }

    /**
     * Add user identity local guide.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map udl_add(Map map) {
       Map m=(Map) map.get("data");
       Map rm=new HashMap();
       String code=m.get("user_id").toString();
       TUser ta=userRepository.findOne(Long.valueOf(code));
       if(ta==null){
           rm=raiseError(2005);
           rm.put("success", false);
       }else{
             if(m.containsKey("flag")){
               Integer bb=(Integer) m.get("flag");
               if(bb==100 || bb==200){
                   if(bb==100){
                       TUserIdentityLCGuide oop=userLocalRepository.findByUserAndStatus(ta, 1);
                       if(oop!=null){
                           userLocalRepository.delete(oop);
                           List<TUserIdentityLCGuideFiles>dds=ulfRepository.findByUserAndStatus(ta, 1);
                           ulfRepository.delete(dds);
                       }
                       TUserIdentityLCGuide kop=userLocalRepository.findByUserAndStatus(ta, 0);
                       kop.setStatus(1);
                       userLocalRepository.save(kop);
                       TUserIdentity udd=uiRepository.findByUser(ta);
                       udd.setStatusLc(2);
                       udd.setHasIdentityLocalGuide(true);
                       uiRepository.save(udd);
                       List<TUserIdentityLCGuideFiles>ddfs=ulfRepository.findByUserAndStatus(ta, 1);
                       if(ddfs!=null && !ddfs.isEmpty())ulfRepository.delete(ddfs);
                       List<TUserIdentityLCGuideFiles>dds=ulfRepository.findByUserAndStatus(ta, 0);
                       for(TUserIdentityLCGuideFiles ff:dds){
                           ff.setStatus(1);
                           ulfRepository.save(ff);
                       }
                       
                   }else{
                       TUserIdentityLCGuide kop=userLocalRepository.findByUserAndStatus(ta, 0);
                       userLocalRepository.delete(kop);
                       List<TUserIdentityLCGuideFiles>dds=ulfRepository.findByUserAndStatus(ta, 0);
                       ulfRepository.delete(dds);
                       List<TUserIdentityLCGuideFiles>fs=ulfRepository.findByUserAndStatus(ta, 1);
                       for(TUserIdentityLCGuideFiles f:fs){
                           f.setFlag1(1);
                           ulfRepository.save(f);
                       }
                        TUserIdentityLCGuide ddss=userLocalRepository.findByUserAndStatus(ta, 1);
                        TUserIdentity uu=uiRepository.findByUser(ta);
                        if(ddss==null){
                            uu.setHasIdentityLocalGuide(false);
                            uu.setStatusLc(0);
                        }else{
                            uu.setHasIdentityLocalGuide(true);
                            uu.setStatusLc(2);
                        }
                        uiRepository.save(uu);
                   }
                    Map tm=new HashMap();
                    tm.put("partner", map.get("partner"));
                    tm.put("uid", ta.getId().toString());
                    tm.put("flag", bb==100?1:2);
                    tm.put("cat", 4);
                    noticeServices.user_skill_notice(tm);
                    rm.put("success", true);
                    return add(rm);
               }
              
           }
             int cid=(int) m.get("cid");
            TUserIdentityLCGuide up=userLocalRepository.findByUserAndStatus(ta,cid);
            TUserIdentityLCGuideExpanded ue;
            if(up==null){
                    up=new TUserIdentityLCGuide();
                    ue=new TUserIdentityLCGuideExpanded();
                    up.setStatus(cid);
            }else{
                ue=up.getProperties();
                if(ue==null)ue=new TUserIdentityLCGuideExpanded();
            }
                 if(m.containsKey("hasGuideCertificate"))up.setHasGuideCertificate((Boolean)m.get("hasGuideCertificate")?1:0);
                 if(m.containsKey("hasPassport"))up.setHasPassport((Boolean)m.get("hasPassport")?1:0);
                 if(m.containsKey("hasResidence"))up.setHasResidence((Boolean)m.get("hasResidence")?1:0);
                 if(m.containsKey("area_code")){
                     String area_code=(String) m.get("area_code");
                     TArea area=areaRepository.findOne(area_code);
                     up.setServiceCity(area);
                 }
                    if(m.containsKey("org")){
                             String oid=(String) m.get("org");
                             TUserOrganization org=orgRepository.findOne(Long.valueOf(oid));
                              ue.setOrganization(org);
                         }
                        if(m.containsKey("edate")){
                            Long dd=Long.valueOf((String)m.get("edate"))*1000;
                            Date td=new Date();
                            td.setTime(dd);
                            ue.setEmploymentDate(td);
                        }
                        if(m.containsKey("rman"))ue.setReferee((String)m.get("rman"));
               up.setUser(ta);
               userLocalRepository.save(up);
               ue.setIdentity(up);
               uleRepository.save(ue);
               if(cid==1){
                            TUserIdentity udd=uiRepository.findByUser(ta);
                            udd.setStatusLc(2);
                            udd.setHasIdentityLocalGuide(true);
                            uiRepository.save(udd);
                        }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
        return add(rm);
    }

    /**
     * Read user identity local guide.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map udl_read(Map model) {
        Map rm=new HashMap();
        Map map=(Map) model.get("data");
          String rid=(String) map.get("rid");
          TUser res=userRepository.findOne(Long.valueOf(rid));
           List datas=new ArrayList();
          for(int i=0;i<2;i++){
                TUserIdentityLCGuide up=userLocalRepository.findByUserAndStatus(res,1-i);
                Map mm=new HashMap();
                if(up!=null){
                    mm.put("id", up.getId());
                    mm.put("hasGuideCertificate", up.getHasGuideCertificate()==1);
                    mm.put("hasPassport", up.getHasPassport()==1);
                    mm.put("hasResidence", up.getHasResidence()==1);
                    if(up.getServiceCity()!=null)mm.put("area_code", up.getServiceCity().getId());
                    else mm.put("area_code", "");
                    TUserIdentityLCGuideExpanded ue=up.getProperties();
                    if(ue!=null){
                        mm.put("edate", ue.getEmploymentDate());
                        mm.put("rman", ue.getReferee());
                        if(ue.getOrganization()!=null)mm.put("org", ue.getOrganization().getId());
                    }
                    mm.put("cid", up.getStatus());
                    datas.add(mm);
                }
          }
         rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         rm.put("data", datas);
          return add(rm);
    }

    /**
     * Add user identity long guide.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map udg_add(Map map) {
        Map m=(Map) map.get("data");
       Map rm=new HashMap();
       String code=m.get("user_id").toString();
       TUser ta=userRepository.findOne(Long.valueOf(code));
       if(ta==null){
           rm=raiseError(2005);
           rm.put("success", false);
       }else{
            if(m.containsKey("flag")){
               Integer bb=(Integer) m.get("flag");
               if(bb==100 || bb==200){
                    if(bb==100){
                       TUserIdentityLongGuide oop=userLongRepository.findByUserAndStatus(ta, 1);
                       if(oop!=null)userLongRepository.delete(oop);
                       TUserIdentityLongGuide kop=userLongRepository.findByUserAndStatus(ta, 0);
                       kop.setStatus(1);
                       userLongRepository.save(kop);
                       TUserIdentity udd=uiRepository.findByUser(ta);
                       udd.setStatusLg(2);
                       udd.setHasIdentityLongGuide(true);
                       uiRepository.save(udd);
                       List<TUserIdentityLongGuideFiles>dds=ugfRepository.findByUserAndStatus(ta, 1);
                       if(dds!=null && !dds.isEmpty())ugfRepository.delete(dds);
                       List<TUserIdentityLongGuideFiles>ddfs=ugfRepository.findByUserAndStatus(ta, 0);
                       for(TUserIdentityLongGuideFiles ff:ddfs){
                           ff.setStatus(1);
                           ugfRepository.save(ff);
                       }
                   }else{
                       TUserIdentityLongGuide kop=userLongRepository.findByUserAndStatus(ta, 0);
                       userLongRepository.delete(kop);
                       List<TUserIdentityLongGuideFiles>ddfs=ugfRepository.findByUserAndStatus(ta, 0);
                       ugfRepository.delete(ddfs);
                       List<TUserIdentityLongGuideFiles>fs=ugfRepository.findByUserAndStatus(ta, 1);
                       for(TUserIdentityLongGuideFiles f:fs){
                           f.setFlag1(1);
                           ugfRepository.save(f);
                       }
                        TUserIdentityLongGuide ddss=userLongRepository.findByUserAndStatus(ta, 1);
                        TUserIdentity uu=uiRepository.findByUser(ta);
                        if(ddss==null){
                            uu.setHasIdentityLongGuide(false);
                            uu.setStatusLg(0);
                        }else{
                            uu.setStatusLg(2);
                            uu.setHasIdentityLongGuide(true);
                        }
                        uiRepository.save(uu);
                   }
                    Map tm=new HashMap();
                    tm.put("partner", map.get("partner"));
                    tm.put("uid", ta.getId().toString());
                    tm.put("flag", bb==100?1:2);
                    tm.put("cat", 2);
                    noticeServices.user_skill_notice(tm);
                    rm.put("success", true);
                    return add(rm);
               }
              
           }
            int cid=(int) m.get("cid");
            TUserIdentityLongGuide up=userLongRepository.findByUserAndStatus(ta,cid);
            TUserIdentityLongGuideExpanded ue;
            if(up==null){
                    up=new TUserIdentityLongGuide();
                    ue=new TUserIdentityLongGuideExpanded();
                    up.setCreated(new Date());
                    up.setStatus(cid);
            }else{
                ue=up.getProperties();
                if(ue==null)ue=new TUserIdentityLongGuideExpanded();
            }
            if(m.containsKey("tid")){
                String tid=m.get("tid").toString();
                 TTravelAgency tra=travelRepository.findOne(Long.valueOf(tid));
                  up.setTravelAgency(tra);
            }
                 if(m.containsKey("supportTourAssemble"))up.setSupportTourAssemble((Boolean)m.get("supportTourAssemble")?1:0);
                 if(m.containsKey("supportTourIndependent"))up.setSupportTourIndependent((Boolean)m.get("supportTourIndependent")?1:0);
                 if(m.containsKey("supportTourVip"))up.setSupportTourVip((Boolean)m.get("supportTourVip")?1:0);
                if(m.containsKey("hasGuideCertificate"))up.setHasGuideCertificate((Boolean)m.get("hasGuideCertificate")?1:0);
                if(m.containsKey("hasLeaderCertificate"))up.setHasLeaderCertificate((Boolean)m.get("hasLeaderCertificate")?1:0);
                  if(m.containsKey("org")){
                             String oid=m.get("org").toString();
                             TUserOrganization org=orgRepository.findOne(Long.valueOf(oid));
                              ue.setOrganization(org);
                  }
                  if(m.containsKey("edate")){
                            Long dd=Long.valueOf((String)m.get("edate"))*1000;
                            Date td=new Date();
                            td.setTime(dd);
                            ue.setEmploymentDate(td);
                  }
                  if(m.containsKey("rman"))ue.setReferee((String)m.get("rman"));
                  if(m.containsKey("graduate"))ue.setResume((int)m.get("graduate"));
                  if(m.containsKey("area_code")){
                      String aid=(String) m.get("area_code");
                      TArea area=areaRepository.findOne(aid);
                      ue.setArea(area);
                  }
                  if(m.containsKey("experience")){
                      ue.setExperience((String)m.get("experience"));
                  }
                  if(m.containsKey("line_ids")){
                      List<String> lines=(List) m.get("line_ids");
                      List<TTourismLineCategory> cas=new ArrayList<>();
                      for(int i=0;i<lines.size();i++){
                          String ii=lines.get(i);
                          TTourismLineCategory cc=tcRepository.findOne(Long.valueOf(ii));
                          cas.add(cc);
                      }
                      up.setLineCategories(cas);
                  }
                up.setModified(new Date());
               up.setUser(ta);
               userLongRepository.save(up);
               ue.setIdentity(up);
               ugeRepository.save(ue);
               if(cid==1){
                            TUserIdentity udd=uiRepository.findByUser(ta);
                            udd.setStatusLg(2);
                            udd.setHasIdentityLongGuide(true);
                            uiRepository.save(udd);
                        }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
        return add(rm);
    }

    /**
     * Read user identity long guide.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map udg_read(Map model) {
           Map rm=new HashMap();
           Map map=(Map) model.get("data");
          String rid=map.get("rid").toString();
          TUser res=userRepository.findOne(Long.valueOf(rid));
          List data=new ArrayList();
          for(int i=0;i<2;i++){
                TUserIdentityLongGuide up=userLongRepository.findByUserAndStatus(res,1-i);
                Map mm=new HashMap();
                if(up!=null){
                      mm.put("id", up.getId());
                      mm.put("supportTourAssemble", up.getSupportTourAssemble()==1);
                      mm.put("supportTourIndependent", up.getSupportTourIndependent()==1);
                      mm.put("supportTourVip", up.getSupportTourVip()==1);
                      mm.put("hasGuideCertificate", up.getHasGuideCertificate()==1);
                      mm.put("hasLeaderCertificate", up.getHasLeaderCertificate()==1);
                      mm.put("tid", up.getTravelAgency().getId());
                      List<TTourismLineCategory>cas =up.getLineCategories();
                      List datas=new ArrayList();
                      for(TTourismLineCategory c:cas){
                          datas.add(c.getId());
                      }
                      mm.put("line_ids", datas);
                      TUserIdentityLongGuideExpanded ue=up.getProperties();
                      if(ue!=null){
                          mm.put("edate", ue.getEmploymentDate());
                          mm.put("area_code", ue.getArea().getId());
                          mm.put("rman", ue.getReferee());
                          mm.put("graduate", ue.getResume());
                          mm.put("org", ue.getOrganization().getId());
                          mm.put("experience", ue.getExperience());
                      }
                      mm.put("cid", up.getStatus());
                      data.add(mm);
                    }
          }
         rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         rm.put("data", data);
          return add(rm);
    }

    /**
     * Add user identity overseal guide.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map uds_add(Map map) {
       Map m=(Map) map.get("data");
       Map rm=new HashMap();
       String code=m.get("user_id").toString();
       TUser ta=userRepository.findOne(Long.valueOf(code));
       if(ta==null){
           rm=raiseError(2005);
           rm.put("success", false);
       }else{
              if(m.containsKey("flag")){
               Integer bb=(Integer) m.get("flag");
               if(bb==100 || bb==200){
                    if(bb==100){
                       TUserIdentityOGuide oop=udsRepository.findByUserAndStatus(ta, 1);
                       if(oop!=null)udsRepository.delete(oop);
                       TUserIdentityOGuide kop=udsRepository.findByUserAndStatus(ta, 0);
                       kop.setStatus(1);
                       udsRepository.save(kop);
                       TUserIdentity udd=uiRepository.findByUser(ta);
                       udd.setStatusOg(2);
                       udd.setHasIdentityOverseasLocalGuide(true);
                       uiRepository.save(udd);
                       List<TUserIdentityOGuideFiles>ddfs=uofRepository.findByUserAndStatus(ta, 1);
                       if(ddfs!=null && !ddfs.isEmpty())uofRepository.delete(ddfs);
                       List<TUserIdentityOGuideFiles>dds=uofRepository.findByUserAndStatus(ta, 0);
                       for(TUserIdentityOGuideFiles ff:dds){
                           ff.setStatus(1);
                           uofRepository.save(ff);
                       }
                   }else{
                       TUserIdentityOGuide kop=udsRepository.findByUserAndStatus(ta, 0);
                       udsRepository.delete(kop);
                       List<TUserIdentityOGuideFiles>dds=uofRepository.findByUserAndStatus(ta, 0);
                       uofRepository.delete(dds);
                       List<TUserIdentityOGuideFiles>fs=uofRepository.findByUserAndStatus(ta, 1);
                       for(TUserIdentityOGuideFiles f:fs){
                           f.setFlag(1);
                           uofRepository.save(f);
                       }
                        TUserIdentityOGuide ddss=udsRepository.findByUserAndStatus(ta, 1);
                        TUserIdentity uu=uiRepository.findByUser(ta);
                        if(ddss==null){
                            uu.setHasIdentityOverseasLocalGuide(false);
                            uu.setStatusOg(0);
                        }else{
                            uu.setHasIdentityOverseasLocalGuide(true);
                            uu.setStatusOg(2);
                        }
                        uiRepository.save(uu);
                   }
                    Map tm=new HashMap();
                    tm.put("partner", map.get("partner"));
                    tm.put("uid", ta.getId().toString());
                    tm.put("flag", bb==100?1:2);
                    tm.put("cat", 3);
                    noticeServices.user_skill_notice(tm);
                    rm.put("success", true);
                    return add(rm);
               }
           }
            int cid=(int) m.get("cid");
            TUserIdentityOGuide up=udsRepository.findByUserAndStatus(ta,cid);
            TUserIdentityOGuideExpanded oe;
            if(up==null){
                    up=new TUserIdentityOGuide();
                    oe=new TUserIdentityOGuideExpanded();
                    up.setCreated(new Date());
                    up.setStatus(cid);
            }else{
                oe=up.getProperties();
                if(oe==null)oe=new TUserIdentityOGuideExpanded();
            }
            if(m.containsKey("tid")){
                String tid=(String)m.get("tid");
                 TTravelAgency tra=travelRepository.findOne(Long.valueOf(tid));
                  up.setTravelAgency(tra);
            }
                 if(m.containsKey("supportTourAssemble"))up.setSupportTourAssemble((Boolean)m.get("supportTourAssemble")?1:0);
                 if(m.containsKey("supportTourIndependent"))up.setSupportTourIndependent((Boolean)m.get("supportTourIndependent")?1:0);
                 if(m.containsKey("supportTourVip"))up.setSupportTourVip((Boolean)m.get("supportTourVip")?1:0);
                if(m.containsKey("hasEmploymentCertificate"))up.setHasEmploymentCertificate((Boolean)m.get("hasEmploymentCertificate")?1:0);
                if(m.containsKey("nationality"))up.setNationality((Integer)m.get("nationality"));
                if(m.containsKey("residencePermit"))up.setResidencePermit((Integer)m.get("residencePermit"));
                if(m.containsKey(("line_ids"))){
                    List<String>datas=(List<String>) m.get("line_ids");
                    List<TTourismLineCategory> lines=new ArrayList<>();
                    for(String s:datas){
                        TTourismLineCategory c=tcRepository.findOne(Long.valueOf(s));
                        lines.add(c);
                    }
                    up.setLineCategories(lines);
                }
                
                  if(m.containsKey("org")){
                             String oid=m.get("org").toString();
                             TUserOrganization org=orgRepository.findOne(Long.valueOf(oid));
                              oe.setOrganization(org);
                  }
                  if(m.containsKey("edate")){
                            Long dd=Long.valueOf((String)m.get("edate"))*1000;
                            Date td=new Date();
                            td.setTime(dd);
                            oe.setEmploymentDate(td);
                  }
                  if(m.containsKey("rman"))oe.setReferee((String)m.get("rman"));
                  if(m.containsKey("graduate"))oe.setResume((int)m.get("graduate"));
                  
                  
                up.setModified(new Date());
                up.setUser(ta);
                udsRepository.save(up);
                
                oe.setIdentity(up);
                uoeRepository.save(oe);
                if(cid==1){
                            TUserIdentity udd=uiRepository.findByUser(ta);
                            udd.setStatusOg(2);
                            udd.setHasIdentityOverseasLocalGuide(true);
                            uiRepository.save(udd);
                        }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
        return add(rm);
    }

    /**
     * Read user identity overseal guide.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map uds_read(Map model) {
          Map rm=new HashMap();
          Map map=(Map) model.get("data");
          String rid=(String) map.get("rid");
          TUser res=userRepository.findOne(Long.valueOf(rid));
          List data=new ArrayList();
          for(int i=0;i<2;i++){
                TUserIdentityOGuide up=udsRepository.findByUserAndStatus(res,1-i);
                Map mm=new HashMap();
                if(up!=null){
                  mm.put("id", up.getId());
                  mm.put("supportTourAssemble", up.getSupportTourAssemble()==1);
                  mm.put("supportTourIndependent", up.getSupportTourIndependent()==1);
                  mm.put("supportTourVip", up.getSupportTourVip()==1);
                  mm.put("nationality", up.getNationality());
                  mm.put("residencePermit", up.getResidencePermit());
                  mm.put("hasEmploymentCertificate", up.getHasEmploymentCertificate()==1);
                  mm.put("tid", up.getTravelAgency().getId());
                  if(up.getLineCategories()!=null){
                       List<TTourismLineCategory>cas =up.getLineCategories();
                      List datas=new ArrayList();
                      for(TTourismLineCategory c:cas){
                          datas.add(c.getId());
                      }
                      mm.put("line_ids", datas);
                  }
                try{
                      TUserIdentityOGuideExpanded oe=up.getProperties();
                      if(oe!=null){

                          mm.put("edate", oe.getEmploymentDate());
                          mm.put("rman", oe.getReferee());
                          mm.put("graduate", oe.getResume());
                          mm.put("org", oe.getOrganization().getId());
                      }
                }catch(Exception e){

                }
                mm.put("cid", up.getStatus());
                data.add(mm);
             }
          }
         rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         rm.put("data", data);
          return add(rm);
    }
    
    /**
     * Add user identity op file.
     * @param map HashMap
     * @return map HashMap
     */
     @Override
    public Map op_add(Map map) {
        Map rm=new HashMap();
        Map m=(Map) map.get("data");
        String uid=m.get("user_id").toString();
        String icon_url=(String) m.get("icon_url");
        String icon_surl=(String) m.get("icon_surl");
        TUser tt=userRepository.findOne(Long.valueOf(uid));
    
        TUserIdentityOpExpanded ta=new TUserIdentityOpExpanded(UUID.randomUUID().toString());
        ta.setUser(tt);
        
        ta.setBusinessCardStorageUrl(clientApp.get_uid((String)map.get("partner"), icon_surl));
        ta.setBusinessCardUrl(icon_url);
        ta.setStatus(1);
        ta.setFlag(1);
        opeRepository.save(ta);
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        Map mmm=new HashMap();
        mmm.put("id", ta.getId());
        mmm.put("icon_url", ta.getBusinessCardUrl());
        rm.put("data", mmm);
        
        return add(rm);
    }

    /**
     * Delete user identity op file.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map op_delete(Map map) {
        Map model=(Map) map.get("data");
         String id=(String) model.get("id");
       Map rm=new HashMap();
        try{
            opeRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }
     
    /**
     * Read user identity op file.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map op_read(Map model) {
        Map map=(Map) model.get("data");
       String uid=(String) map.get("uid");
       Map rm=new HashMap();
       TUser user=userRepository.findOne(Long.valueOf(uid));
       List<TUserIdentityOpExpanded> list=opeRepository.findByUser(user);
       List lst=new ArrayList();
       for(TUserIdentityOpExpanded op:list){
           if(op.getStatus()==0 && op.getFlag()==0)continue;
           Map m=new HashMap();
           m.put("id", op.getId());
           m.put("icon_url", op.getBusinessCardUrl());
           m.put("op_name", "图片");
           m.put("status", op.getStatus());
           lst.add(m);
       }
       rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
       rm.put("data", lst);
       return add(rm);
    }
    
    /**
     * Add user identity docent file.
     * @param map HashMap
     * @return map HashMap
     */
      @Override
    public Map uddup_add(Map map) {
        Map rm=new HashMap();
        Map m=(Map) map.get("data");
        String uid=m.get("user_id").toString();
        String icon_url=(String) m.get("icon_url");
        String icon_surl=(String) m.get("icon_surl");
        TUser tt=userRepository.findOne(Long.valueOf(uid));
        TUserIdentityDocentsFiles ta=new TUserIdentityDocentsFiles(UUID.randomUUID().toString());
        ta.setUser(tt);
        ta.setStoragePath(clientApp.get_uid((String)map.get("partner"), icon_surl));
        ta.setFileUrl(icon_url);
        ta.setStatus(1);
        ta.setFlag(1);
        udfRepository.save(ta);
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        Map mmm=new HashMap();
        mmm.put("id", ta.getId());
        mmm.put("icon_url", ta.getFileUrl());
        rm.put("data", mmm);
        
        return add(rm);
    }

    /**
     * Delete user identity docent file.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map uddup_delete(Map map) {
        Map model=(Map) map.get("data");
         String id=(String) model.get("id");
       Map rm=new HashMap();
        try{
            udfRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read user identity docent file.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map uddup_read(Map model) {
        Map map=(Map) model.get("data");
       String uid=(String) map.get("uid");
       Map rm=new HashMap();
       TUser user=userRepository.findOne(Long.valueOf(uid));
       List<TUserIdentityDocentsFiles> list=udfRepository.findByUser(user);
       List lst=new ArrayList();
       for(TUserIdentityDocentsFiles op:list){
           if(op.getStatus()==0 && op.getFlag()==0)continue;
           Map m=new HashMap();
           m.put("id", op.getId());
           m.put("icon_url", op.getFileUrl());
           m.put("img_name", "图片");
           m.put("status", op.getStatus());
           lst.add(m);
       }
       rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
       rm.put("data", lst);
       return add(rm);
    }
    
    /**
     * Add user identity local guide file.
     * @param map HashMap
     * @return map HashMap
     */
      @Override
    public Map udlup_add(Map map) {
        Map rm=new HashMap();
        Map m=(Map) map.get("data");
        String uid=m.get("user_id").toString();
        String icon_url=(String) m.get("icon_url");
        String icon_surl=(String) m.get("icon_surl");
        Integer flag=(Integer) m.get("flag");
        TUser tt=userRepository.findOne(Long.valueOf(uid));
        TUserIdentityLCGuideFiles ta=new TUserIdentityLCGuideFiles(UUID.randomUUID().toString());
        if(flag!=1){
            List<TUserIdentityLCGuideFiles> pp=ulfRepository.findByUserAndStatusAndFlag(tt, 1, flag);
            if(pp!=null && !pp.isEmpty()){
               TUserIdentityLCGuideFiles po=pp.get(0);
               ulfRepository.delete(po);
            }
        }
        ta.setUser(tt);
        ta.setStoragePath(clientApp.get_uid((String)map.get("partner"), icon_surl));
        ta.setFileUrl(icon_url);
        ta.setFlag(flag);
        ta.setStatus(1);
        ta.setFlag1(1);
        ulfRepository.save(ta);
        TUserIdentityLCGuide lc=userLocalRepository.findByUserAndStatus(tt,1);
        switch(flag){
            case 1:
                lc.setHasGuideCertificate(1);
                break;
            case 2:
                lc.setHasPassport(1);
                break;
            case 3:
                lc.setHasResidence(1);
                break;
        }
        userLocalRepository.save(lc);
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        Map mmm=new HashMap();
        mmm.put("id", ta.getId());
        mmm.put("icon_url", ta.getFileUrl());
        rm.put("data", mmm);
        
        return add(rm);
    }

    /**
     * Delete user identity local guide file.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map udlup_delete(Map map) {
         Map model=(Map) map.get("data");
         String id=(String) model.get("id");
       Map rm=new HashMap();
        try{
            ulfRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read user identity local guide file.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map udlup_read(Map model) {
         Map map=(Map) model.get("data");
       String uid=(String) map.get("uid");
       Map rm=new HashMap();
       TUser user=userRepository.findOne(Long.valueOf(uid));
       List<TUserIdentityLCGuideFiles> list=ulfRepository.findByUser(user);
       List lst=new ArrayList();
       for(TUserIdentityLCGuideFiles op:list){
           if(op.getStatus()==0 && op.getFlag()==0)continue;
           Map m=new HashMap();
           m.put("id", op.getId());
           m.put("icon_url", op.getFileUrl());
           m.put("img_name", "图片");
           m.put("status", op.getStatus());
           lst.add(m);
       }
       rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
       rm.put("data", lst);
       return add(rm);
    }

    /**
     * Add user identity long guide 领队证照片 file.
     * @param map
     * @return map
     */
    @Override
    public Map udgc_add(Map map) {
         Map rm=new HashMap();
        Map m=(Map) map.get("data");
        String uid=m.get("user_id").toString();
        String icon_url=(String) m.get("icon_url");
        String icon_surl=(String) m.get("icon_surl");
        TUser tt=userRepository.findOne(Long.valueOf(uid));
        TUserIdentityLongGuideFiles ta=new TUserIdentityLongGuideFiles(UUID.randomUUID().toString());
        ta.setUser(tt);
        ta.setStoragePath(clientApp.get_uid((String)map.get("partner"), icon_surl));
        ta.setFileUrl(icon_url);
        ta.setFlag(2);
        ta.setStatus(1);
        ta.setFlag1(1);
        ugfRepository.save(ta);
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        Map mmm=new HashMap();
        mmm.put("id", ta.getId());
        mmm.put("icon_url", ta.getFileUrl());
        rm.put("data", mmm);
        
        return add(rm);
    }

    /**
     * Delete user identity long guide 领队证照片 file.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map udgc_delete(Map map) {
          Map model=(Map) map.get("data");
         String id=(String) model.get("id");
       Map rm=new HashMap();
        try{
            ugfRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read user identity long guide 领队证照片 file.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map udgc_read(Map model) {
         Map map=(Map) model.get("data");
       String uid=(String) map.get("uid");
       Map rm=new HashMap();
       TUser user=userRepository.findOne(Long.valueOf(uid));
       List<TUserIdentityLongGuideFiles> list=ugfRepository.findByUserAndFlag(user,2);
       List lst=new ArrayList();
       for(TUserIdentityLongGuideFiles op:list){
           if(op.getStatus()==0 && op.getFlag1()==0)continue;
           Map m=new HashMap();
           m.put("id", op.getId());
           m.put("icon_url", op.getFileUrl());
           m.put("img_name", "图片");
           m.put("status", op.getStatus());
           lst.add(m);
       }
       rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
       rm.put("data", lst);
       return add(rm);
    }

    /**
     * Add user identity long guide 导游证照片 file.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map udgg_add(Map map) {
         Map rm=new HashMap();
        Map m=(Map) map.get("data");
        String uid=m.get("user_id").toString();
        String icon_url=(String) m.get("icon_url");
        String icon_surl=(String) m.get("icon_surl");
        TUser tt=userRepository.findOne(Long.valueOf(uid));
        TUserIdentityLongGuideFiles ta=new TUserIdentityLongGuideFiles(UUID.randomUUID().toString());
        ta.setUser(tt);
        ta.setStoragePath(clientApp.get_uid((String)map.get("partner"), icon_surl));
        ta.setFileUrl(icon_url);
        ta.setStatus(1);
        ta.setFlag(1);
        ta.setFlag1(1);
        ugfRepository.save(ta);
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        Map mmm=new HashMap();
        mmm.put("id", ta.getId());
        mmm.put("icon_url", ta.getFileUrl());
        rm.put("data", mmm);
        return add(rm);
    }

    /**
     * Delete user identity long guide 导游证照片 file.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map udgg_delete(Map map) {
         Map model=(Map) map.get("data");
         String id=(String) model.get("id");
       Map rm=new HashMap();
        try{
            ugfRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /** user identity long guide 导游证照片 file read. */
    /**
     * Read user identity long guide 导游证照片 file.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map udgg_read(Map model) {
         Map map=(Map) model.get("data");
       String uid=(String) map.get("uid");
       Map rm=new HashMap();
       TUser user=userRepository.findOne(Long.valueOf(uid));
       List<TUserIdentityLongGuideFiles> list=ugfRepository.findByUserAndFlag(user,1);
       List lst=new ArrayList();
       for(TUserIdentityLongGuideFiles op:list){
           if(op.getStatus()==0 && op.getFlag1()==0)continue;
           Map m=new HashMap();
           m.put("id", op.getId());
           m.put("icon_url", op.getFileUrl());
           m.put("img_name", "图片");
           m.put("status", op.getStatus());
           lst.add(m);
       }
       rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
       rm.put("data", lst);
       return add(rm);
    }

    /**
     * Add user identity overseal guide file.
     * @param map
     * @return map
     */
    @Override
    public Map udsup_add(Map map) {
         Map rm=new HashMap();
        Map m=(Map) map.get("data");
        String uid=m.get("user_id").toString();
        String icon_url=(String) m.get("icon_url");
        String icon_surl=(String) m.get("icon_surl");
        TUser tt=userRepository.findOne(Long.valueOf(uid));
        TUserIdentityOGuideFiles ta=new TUserIdentityOGuideFiles(UUID.randomUUID().toString());
        ta.setUser(tt);
        ta.setStoragePath(clientApp.get_uid((String)map.get("partner"), icon_surl));
        ta.setFileUrl(icon_url);
        ta.setStatus(1);
        ta.setFlag(1);
        
        uofRepository.save(ta);
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        Map mmm=new HashMap();
        mmm.put("id", ta.getId());
        mmm.put("icon_url", ta.getFileUrl());
        rm.put("data", mmm);
        return add(rm);
    }

    /**
     * Delete user identity overseal guide file.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map udsup_delete(Map map) {
        Map model=(Map) map.get("data");
         String id=(String) model.get("id");
       Map rm=new HashMap();
        try{
            uofRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read user identity overseal guide file.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map udsup_read(Map model) {
        Map map=(Map) model.get("data");
       String uid= map.get("uid").toString();
       Map rm=new HashMap();
       TUser user=userRepository.findOne(Long.valueOf(uid));
       List<TUserIdentityOGuideFiles> list=uofRepository.findByUser(user);
       List lst=new ArrayList();
       for(TUserIdentityOGuideFiles op:list){
           if(op.getStatus()==0 && op.getFlag()==0)continue;
           Map m=new HashMap();
           m.put("id", op.getId());
           m.put("icon_url", op.getFileUrl());
           m.put("img_name", "图片");
           m.put("status", op.getStatus());
           lst.add(m);
       }
       rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
       rm.put("data", lst);
       return add(rm);
    }
    
    /**
     * Add user identity driver.
     * @param map HashMap
     * @return map HashMap
     */
      @Override
    public Map udr_add(Map map) {
        Map m=(Map) map.get("data");
       Map rm=new HashMap();
       String code=m.get("user_id").toString();
       TUser ta=userRepository.findOne(Long.valueOf(code));
       if(ta==null){
           rm=raiseError(2005);
           rm.put("success", false);
       }else{
              if(m.containsKey("flag")){
               Integer bb=(Integer) m.get("flag");
               if(bb==100 || bb==200){
                   if(bb==100){
                       TUserIdentityDriver oop=udrRepository.findByUser(ta);
                       if(oop!=null)udrRepository.delete(oop);
                       TUserDriverTemp kop=dtempRepository.findByUser(ta);
                       TUserIdentityDriver ddd=new TUserIdentityDriver();
                       ddd.setUser(ta);
                       ddd.setAffiliated(kop.getAffiliated());
                       ddd.setCreated(new Date());
                       ddd.setHasConditioning(kop.isHasConditioning());
                       ddd.setHasLicenses(kop.isHasLicenses());
                       ddd.setIsCarOwner(kop.isIsCarOwner());
                       ddd.setModels(kop.getModels());
                       ddd.setModified(new Date());
                       ddd.setSupportGuide(kop.isSupportGuide());
                       ddd.setSupportMultipleCountry(kop.isSupportMultipleCountry());
                       ddd.setSupportService(kop.isSupportService());
                       ddd.setSupportSightseeing(kop.isSupportSightseeing());
                       ddd.setSupportSingleCountry(kop.isSupportSingleCountry());
                       ddd.setVehicleCondition(kop.getVehicleCondition());
                       ddd.setArea(kop.getArea());
                       udrRepository.save(ddd);
                       TUserIdentityDriverExpanded eee=new TUserIdentityDriverExpanded();
                       TUserDriverExpandedTemp ett=kop.getProperties();
                       if(ett!=null){
                            eee.setDrivingTime(ett.getDrivingTime());
                            eee.setEmploymentDate(ett.getEmploymentDate());
                            eee.setHasPicture1(ett.isHasPicture1());
                            eee.setHasPicture2(ett.isHasPicture2());
                            eee.setHasPicture3(ett.isHasPicture3());
                            eee.setHasPicture4(ett.isHasPicture4());
                            eee.setHasPicture5(ett.isHasPicture5());
                            eee.setHasPicture6(ett.isHasPicture6());
                            eee.setHasPicture7(ett.isHasPicture7());
                            eee.setIdentity(ddd);
                            eee.setOrganization(ett.getOrganization());
                            eee.setReferee(ett.getReferee());
                            eee.setVehicleBrand(ett.getVehicleBrand());
                            eee.setVehiclePurchaseDate(ett.getVehiclePurchaseDate());
                            uddeRepository.save(eee);
                       }
                       //dtempRepository.delete(kop);
                       TUserIdentity udd=uiRepository.findByUser(ta);
                       udd.setStatusDrv(2);
                       udd.setHasIdentityDriver(true);
                       uiRepository.save(udd);
                       List<TUserIdentityDriverFiles>ddfs=uddfRepository.findByUser(ta);
                       if(ddfs!=null && !ddfs.isEmpty())uddfRepository.delete(ddfs);
                       List<TUserDriverFilesTemp>dds=dftempRepository.findByUser(ta);
                       for(TUserDriverFilesTemp ff:dds){
                           TUserIdentityDriverFiles dff=new TUserIdentityDriverFiles();
                           dff.setId(UUID.randomUUID().toString());
                           dff.setFileUrl(ff.getFileUrl());
                           dff.setFlag(ff.getFlag());
                           dff.setStoragePath(ff.getStoragePath());
                           dff.setUser(ta);
                           uddfRepository.save(dff);
                       }
                       //dftempRepository.delete(dds);
                   }else{
                       TUserDriverTemp kop=dtempRepository.findByUser(ta);
                       dtempRepository.delete(kop);
                       List<TUserDriverFilesTemp>dds=dftempRepository.findByUser(ta);
                       dftempRepository.delete(dds);
                       TUserIdentityDriver ddss=udrRepository.findByUser(ta);
                       TUserIdentity uu=uiRepository.findByUser(ta);
                        if(ddss==null){
                            uu.setHasIdentityDriver(false);
                            uu.setStatusDrv(0);
                        }else{
                            uu.setHasIdentityDriver(true);
                            uu.setStatusDrv(2);
                        }
                        uiRepository.save(uu);
                   }
                    Map tm=new HashMap();
                    tm.put("partner", map.get("partner"));
                    tm.put("uid", ta.getId().toString());
                    tm.put("flag", bb==100?1:2);
                    tm.put("cat", 5);
                    noticeServices.user_skill_notice(tm);
                    rm.put("success", true);
                    return add(rm);
               }
              
           }
              int cid=(int) m.get("cid");
              if(cid==1){
                   TUserIdentityDriver up=udrRepository.findByUser(ta);
                    TUserIdentityDriverExpanded oe;
                    if(up==null){
                            up=new TUserIdentityDriver();
                            oe=new TUserIdentityDriverExpanded();
                            up.setCreated(new Date());
                    }else{
                        oe=up.getProperties();
                        if(oe==null)oe=new TUserIdentityDriverExpanded();
                    }
                         if(m.containsKey("supportSightseeing"))up.setSupportSightseeing((Boolean)m.get("supportSightseeing"));
                         if(m.containsKey("supportSingleCountry"))up.setSupportSingleCountry((Boolean)m.get("supportSingleCountry"));
                         if(m.containsKey("supportMultipleCountry"))up.setSupportMultipleCountry((Boolean)m.get("supportMultipleCountry"));
                        if(m.containsKey("isCarOwner"))up.setIsCarOwner((Boolean)m.get("isCarOwner"));
                        if(m.containsKey("hasConditioning"))up.setHasConditioning((Boolean)m.get("hasConditioning"));
                        if(m.containsKey("hasLicenses"))up.setHasLicenses((Boolean)m.get("hasLicenses"));
                        if(m.containsKey("affiliated"))up.setAffiliated((int)m.get("affiliated"));
                        if(m.containsKey("supportService"))up.setSupportService((int)m.get("supportService")==1);
                        if(m.containsKey("vehicleCondition"))up.setVehicleCondition((int)m.get("vehicleCondition"));
                        if(m.containsKey("models")){
                            String mid=m.get("models").toString();
                            TCarModels model=modelRepository.findOne(Integer.valueOf(mid));
                            up.setModels(model);
                        }
                        if(m.containsKey("vehicleBrand")){
                            oe.setVehicleBrand((String)m.get("vehicleBrand"));
                        }
                        if(m.containsKey("vehiclePurchaseDate")){
                                    Long dd=Long.valueOf((String)m.get("vehiclePurchaseDate"))*1000;
                                    Date td=new Date();
                                    td.setTime(dd);
                                    oe.setVehiclePurchaseDate(td);
                        }
                          if(m.containsKey("org")){
                                     String oid=m.get("org").toString();
                                     TUserOrganization org=orgRepository.findOne(Long.valueOf(oid));
                                      oe.setOrganization(org);
                          }
                          if(m.containsKey("edate")){
                                    Long dd=Long.valueOf((String)m.get("edate"))*1000;
                                    Date td=new Date();
                                    td.setTime(dd);
                                    oe.setEmploymentDate(td);
                          }
                          if(m.containsKey("drivingTime")){
                                    Long dd=Long.valueOf((String)m.get("drivingTime"))*1000;
                                    Date td=new Date();
                                    td.setTime(dd);
                                    oe.setDrivingTime(td);
                          }
                          if(m.containsKey("rman"))oe.setReferee((String)m.get("rman"));
                          if(m.containsKey("area_code")){
                              String area_code=(String) m.get("area_code");
                              TArea area=areaRepository.findOne(area_code);
                              up.setArea(area);
                          }
                        up.setModified(new Date());
                        up.setUser(ta);
                        udrRepository.save(up);

                        oe.setIdentity(up);
                        uddeRepository.save(oe);
                        
                            TUserIdentity udd=uiRepository.findByUser(ta);
                            udd.setStatusDrv(2);
                            udd.setHasIdentityDriver(true);
                            uiRepository.save(udd);
                        
              }else{
                  TUserDriverTemp up=dtempRepository.findByUser(ta);
                    TUserDriverExpandedTemp oe;
                    if(up==null){
                            up=new TUserDriverTemp();
                            oe=new TUserDriverExpandedTemp();
                            up.setCreated(new Date());
                    }else{
                        oe=up.getProperties();
                        if(oe==null)oe=new TUserDriverExpandedTemp();
                    }
                         if(m.containsKey("supportSightseeing"))up.setSupportSightseeing((Boolean)m.get("supportSightseeing"));
                         if(m.containsKey("supportSingleCountry"))up.setSupportSingleCountry((Boolean)m.get("supportSingleCountry"));
                         if(m.containsKey("supportMultipleCountry"))up.setSupportMultipleCountry((Boolean)m.get("supportMultipleCountry"));
                        if(m.containsKey("isCarOwner"))up.setIsCarOwner((Boolean)m.get("isCarOwner"));
                        if(m.containsKey("hasConditioning"))up.setHasConditioning((Boolean)m.get("hasConditioning"));
                        if(m.containsKey("hasLicenses"))up.setHasLicenses((Boolean)m.get("hasLicenses"));
                        if(m.containsKey("affiliated"))up.setAffiliated((int)m.get("affiliated"));
                        if(m.containsKey("supportService"))up.setSupportService((int)m.get("supportService")==1);
                        if(m.containsKey("vehicleCondition"))up.setVehicleCondition((int)m.get("vehicleCondition"));
                        if(m.containsKey("models")){
                            String mid=m.get("models").toString();
                            TCarModels model=modelRepository.findOne(Integer.valueOf(mid));
                            up.setModels(model);
                        }
                        if(m.containsKey("vehicleBrand")){
                            oe.setVehicleBrand((String)m.get("vehicleBrand"));
                        }
                        if(m.containsKey("vehiclePurchaseDate")){
                                    Long dd=Long.valueOf((String)m.get("vehiclePurchaseDate"))*1000;
                                    Date td=new Date();
                                    td.setTime(dd);
                                    oe.setVehiclePurchaseDate(td);
                        }
                          if(m.containsKey("org")){
                                     String oid=m.get("org").toString();
                                     TUserOrganization org=orgRepository.findOne(Long.valueOf(oid));
                                      oe.setOrganization(org);
                          }
                          if(m.containsKey("edate")){
                                    Long dd=Long.valueOf((String)m.get("edate"))*1000;
                                    Date td=new Date();
                                    td.setTime(dd);
                                    oe.setEmploymentDate(td);
                          }
                          if(m.containsKey("drivingTime")){
                                    Long dd=Long.valueOf((String)m.get("drivingTime"))*1000;
                                    Date td=new Date();
                                    td.setTime(dd);
                                    oe.setDrivingTime(td);
                          }
                          if(m.containsKey("rman"))oe.setReferee((String)m.get("rman"));
                          if(m.containsKey("area_code")){
                              String area_code=(String) m.get("area_code");
                              TArea area=areaRepository.findOne(area_code);
                              up.setArea(area);
                          }


                        up.setModified(new Date());
                        up.setUser(ta);
                        dtempRepository.save(up);

                        oe.setIdentity(up);
                        detempRepository.save(oe);
              }
           
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
        return add(rm);
    }

    /**
     * Read user identity driver.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map udr_read(Map model) {
         Map rm=new HashMap();
          Map map=(Map) model.get("data");
          String rid=(String) map.get("rid");
          TUser res=userRepository.findOne(Long.valueOf(rid));
          List data=new ArrayList();
                TUserIdentityDriver up=udrRepository.findByUser(res);
                Map mm=new HashMap();
                if(up!=null){
                  mm.put("id", up.getId());
                  mm.put("supportSightseeing", up.isSupportSightseeing());
                  mm.put("supportSingleCountry", up.isSupportSingleCountry());
                  mm.put("supportMultipleCountry", up.isSupportMultipleCountry());
                  mm.put("isCarOwner", up.isIsCarOwner());
                  mm.put("hasConditioning", up.isHasConditioning());
                  mm.put("hasLicenses", up.isHasLicenses());
                  mm.put("affiliated", up.getAffiliated());
                  mm.put("supportService", up.isSupportService()?1:0);
                  mm.put("vehicleCondition", up.getVehicleCondition());
                  mm.put("models", up.getModels().getId());
                 if(up.getArea()!=null)mm.put("area_code", up.getArea().getId());
                    try{
                          TUserIdentityDriverExpanded oe=up.getProperties();
                          if(oe!=null){
                              mm.put("edate", oe.getEmploymentDate());
                              mm.put("rman", oe.getReferee());
                              mm.put("drivingTime", oe.getDrivingTime());
                              mm.put("org", oe.getOrganization().getId());
                              
                              mm.put("vehiclePurchaseDate", oe.getVehiclePurchaseDate());
                              mm.put("vehicleBrand", oe.getVehicleBrand());
                          }
                    }catch(Exception e){

                    }
                    mm.put("cid", 1);
                    data.add(mm);
                }
          
                if(res.getIdentity()!=null && res.getIdentity().getStatusDrv()!=2){
                      TUserDriverTemp pp=dtempRepository.findByUser(res);
                        mm=new HashMap();
                        if(pp!=null){
                          mm.put("id", pp.getId());
                          mm.put("supportSightseeing", pp.isSupportSightseeing());
                          mm.put("supportSingleCountry", pp.isSupportSingleCountry());
                          mm.put("supportMultipleCountry", pp.isSupportMultipleCountry());
                          mm.put("isCarOwner", pp.isIsCarOwner());
                          mm.put("hasConditioning", pp.isHasConditioning());
                          mm.put("hasLicenses", pp.isHasLicenses());
                          mm.put("affiliated", pp.getAffiliated());
                          mm.put("supportService", pp.isSupportService()?1:0);
                          mm.put("vehicleCondition", pp.getVehicleCondition());
                          mm.put("models", pp.getModels().getId());
                          mm.put("area_code", pp.getArea().getId());
                            try{
                                  TUserDriverExpandedTemp oe=pp.getProperties();
                                  if(oe!=null){
                                      mm.put("edate", oe.getEmploymentDate());
                                      mm.put("rman", oe.getReferee());
                                      mm.put("drivingTime", oe.getDrivingTime());
                                      mm.put("org", oe.getOrganization().getId());
                                      mm.put("vehiclePurchaseDate", oe.getVehiclePurchaseDate());
                                      mm.put("vehicleBrand", oe.getVehicleBrand());
                                  }
                            }catch(Exception e){

                            }
                            mm.put("cid", 0);
                            data.add(mm);
                        }
                }
              
         rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         rm.put("data", data);
         return add(rm);
    }

    /**
     * Add user identity driver file.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map udrup_add(Map map) {
         Map rm=new HashMap();
        Map m=(Map) map.get("data");
        String uid=m.get("user_id").toString();
        String icon_url=(String) m.get("icon_url");
        String icon_surl=(String) m.get("icon_surl");
        int flag=(int)m.get("flag");
        TUser tt=userRepository.findOne(Long.valueOf(uid));
        TUserIdentityDriverFiles pp=uddfRepository.findByUserAndFlag(tt, flag);
        if(pp!=null)uddfRepository.delete(pp);
        TUserIdentityDriverFiles ta=new TUserIdentityDriverFiles(UUID.randomUUID().toString());
        ta.setUser(tt);
        ta.setStoragePath(clientApp.get_uid((String)map.get("partner"), icon_surl));
        ta.setFileUrl(icon_url);
        ta.setFlag(flag);
        TUserIdentityDriver dr=udrRepository.findByUser(tt);
        if(dr==null){
            rm=raiseError(3000);
            rm.put("success", false);
            return add(rm);
        }
        TUserIdentityDriverExpanded de=dr.getProperties();
        if(de==null){
            rm=raiseError(3000);
            rm.put("success", false);
            return add(rm);
        }
        switch(flag){
            case 1:
                de.setHasPicture1(true);
                break;
            case 2:
                de.setHasPicture2(true);
                break;
            case 3:
                de.setHasPicture3(true);
                break;
            case 4:
                de.setHasPicture4(true);
                break;
            case 5:
                de.setHasPicture5(true);
                break;
            case 6:
                de.setHasPicture6(true);
                break;
            case 7:
                de.setHasPicture7(true);
                break;
        }
        uddeRepository.save(de);
//        ta.setStatus(2);
        uddfRepository.save(ta);
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        Map mmm=new HashMap();
        mmm.put("id", ta.getId());
        mmm.put("icon_url", ta.getFileUrl());
        String img_name="";
        switch(flag){
            case 1:
                img_name="车辆照片（内饰）";
                break;
           case 2:
                img_name="车辆照片（外45度角）";
                break;
           case 3:
                img_name="商业保险照片";
                break;
           case 4:
                img_name="营业执照照片";
                break;
           case 5:
                img_name="行驶证照片";
                break;
           case 6:
                img_name="运营许可照片";
                break;
           case 7:
               img_name="居留证照片";
               break;
        }
        mmm.put("img_name", img_name);
        rm.put("data", mmm);
        return add(rm);
    }

    /**
     * Delete user identity driver file.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map udrup_delete(Map map) {
         Map model=(Map) map.get("data");
         String id=(String) model.get("id");
       Map rm=new HashMap();
        try{
            uddfRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read user identity driver file.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map udrup_read(Map model) {
        Map map=(Map) model.get("data");
       String uid=(String) map.get("uid");
       Map rm=new HashMap();
       TUser user=userRepository.findOne(Long.valueOf(uid));
       List<TUserIdentityDriverFiles> list=uddfRepository.findByUser(user);
       List lst=new ArrayList();
       for(TUserIdentityDriverFiles op:list){
           
           Map m=new HashMap();
           m.put("id", op.getId());
           m.put("icon_url", op.getFileUrl());
           m.put("status", 1);
           String img_name="";
           switch(op.getFlag()){
                case 1:
                    img_name="车辆照片（内饰）";
                    break;
               case 2:
                    img_name="车辆照片（外45度角）";
                    break;
               case 3:
                    img_name="商业保险照片";
                    break;
               case 4:
                    img_name="驾驶执照照片";
                    break;
               case 5:
                    img_name="行驶证照片";
                    break;
               case 6:
                    img_name="运营许可照片";
                    break;
               case 7:
                   img_name="居留证照片";
                   break;
            }
            m.put("img_name", img_name);
            m.put("flag", op.getFlag());
           lst.add(m);
       }
       if(user.getIdentity()!=null && user.getIdentity().getStatusDrv()!=2){
             List<TUserDriverFilesTemp> pst=dftempRepository.findByUser(user);
            for(TUserDriverFilesTemp op:pst){
    //           if(op.getStatus()==0 && op.getFlag1()==0)continue;
               Map m=new HashMap();
               m.put("id", op.getId());
               m.put("icon_url", op.getFileUrl());
               m.put("status", 0);
               String img_name="";
               switch(op.getFlag()){
                    case 1:
                        img_name="车辆照片（内饰）";
                        break;
                   case 2:
                        img_name="车辆照片（外45度角）";
                        break;
                   case 3:
                        img_name="商业保险照片";
                        break;
                   case 4:
                        img_name="营业执照照片";
                        break;
                   case 5:
                        img_name="行驶证照片";
                        break;
                   case 6:
                        img_name="运营许可照片";
                        break;
                   case 7:
                       img_name="居留证照片";
                       break;
                }
                m.put("img_name", img_name);
                m.put("flag", op.getFlag());
               lst.add(m);
           }
       }
     
       rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
       rm.put("data", lst);
       return add(rm);
    }

    /**
     *Read user information(password,salt) with mobile. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map userinfo_read(Map map) {
        Map rm=new HashMap();
        String mobile=(String) map.get("data");
        TStaff user=staffRepository.findByMobile(mobile);
        if(user==null){
            rm=raiseError(1000);
            rm.put("success", false);
            return add(rm);
        }
        if(!user.isEnabled()){
            rm=raiseError(5000);
            rm.put("success", false);
            return add(rm);
        }
        Map m=new HashMap();
        m.put("name", user.getName());
        m.put("id", mobile);
        m.put("pass", user.getDecryptPassword());
        m.put("s", user.getSalt());
        List<Map> list=new ArrayList<>();
        for(TRole role:user.getRoles()){
            Map mk=new HashMap();
            mk.put("rname", role.getName());
            list.add(mk);
        }
        Map mmm=new HashMap();
        mmm.put("rname", "USER");
        list.add(mmm);
        m.put("roles", list);
        m.put("success", true);
//        m.put("", map)
        return add(m);
    }

    /**
     * Read user role with mobile.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map roles_read(Map map) {
        Map rm=new HashMap();
        List<TRole> list=roleRepository.findAll();
        List data=new ArrayList();
        for(TRole role:list){
            List<TAuthority> auths=role.getAuthorities();
            for(TAuthority auth:auths){
                Map m=new HashMap();
                m.put("role", role.getName());
                if(auth.getLevel()==0 && auth.getSort()==1)m.put("url", "/manager");
                else m.put("url", "/manager/"+auth.getMark());
                m.put("method", auth.getChain());
                data.add(m);
            }
        }
        rm.put("data", data);
        return add(rm);
    }

    @Override
    public List find_role_all() {
        return roleRepository.findAll();
    }

    /**
     * Read urls for user mobile.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map url_read(Map model) {
         Map map=(Map) model.get("data");
         int cid=(int) map.get("cid");
         Map rm=new HashMap();
         String username=(String) map.get("name");
           if(username==null || username.isEmpty()){
                    rm=raiseError(2008);
                    rm.put("success", false);
                    return add(rm);
          }
         TStaff sta=staffRepository.findByName(username);
         Map rmap=new HashMap();
         for(TRole role:sta.getRoles()){
             for(TAuthority a:role.getAuthorities()){
                 if(rmap.containsKey(a.getName()))continue;
                 rmap.put(a.getName(), a);
             }
         }
         List<TAuthority> lst=authRepository.findByLevel(cid);
         List list=new ArrayList();
         for(TAuthority auth:lst){
             if(!rmap.containsKey(auth.getName()))continue;
             Map m=new HashMap();
             m.put("cls", auth.getCls());
             m.put("title", auth.getName());
             m.put("url", "/wwzzmng/manager/"+auth.getMark());
             list.add(m);
         }
         rm.put("data", list);
         return add(rm);
    }

    /**
     * Read user skill state. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map ust_read(Map model) {
         Map map=(Map) model.get("data");
       String uid=map.get("uid").toString();
       int cid=Integer.valueOf(map.get("cid").toString());
       Map rm=new HashMap();
       TUser user=userRepository.findOne(Long.valueOf(uid));
       List list=new ArrayList();
       Map mm=new HashMap();
       if(user.getIdentity()!=null){
             switch(cid){
                    case 1:
                        mm.put("sflag", user.getIdentity().getStatusOp()!=1);
                        break;
                    case 2:
                        mm.put("sflag", user.getIdentity().getStatusDc()!=1);
                        break;
                    case 3:
                        mm.put("sflag", user.getIdentity().getStatusDrv()!=1);
                        break;
                    case 4:
                        mm.put("sflag", user.getIdentity().getStatusLc()!=1);
                        break;
                    case 5:
                        mm.put("sflag", user.getIdentity().getStatusLg()!=1);
                        break;
                    case 6:
                        mm.put("sflag", user.getIdentity().getStatusOg()!=1);
                        break;
                }
                TUserIdentity ui=uiRepository.findByUser(user);
                if((ui.getStatusOp()==2) && (ui.getStatusLc()==2) && (ui.getStatusDc()==2) && (ui.getStatusDrv()==2) && (ui.getStatusLg()==2) && (ui.getStatusOg()==2))mm.put("tflag", true);
                else mm.put("tflag", false);
       }else{
           mm.put("tflag", false);
           mm.put("sflag", true);
       }
       list.add(mm);
       rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
       rm.put("data", list);
       return add(rm);
    }
}
