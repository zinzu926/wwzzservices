
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.impl.pingtai;

import com.wwzz.services.api.UserNoticeServices;
import com.wwzz.services.api.pingtai.BaseDataServices;
import com.wwzz.services.domain.base.TAirport;
import com.wwzz.services.domain.base.TArea;
import com.wwzz.services.domain.base.TCarModels;
import com.wwzz.services.domain.base.TCategoryLogo;
import com.wwzz.services.domain.base.TCityIntroduction;
import com.wwzz.services.domain.base.TCityRestaurant;
import com.wwzz.services.domain.base.TCityRestaurantCategory;
import com.wwzz.services.domain.base.TCityRestaurantPackage;
import com.wwzz.services.domain.base.TCityRestaurantProperty;
import com.wwzz.services.domain.base.TCityScenicSpotCategory;
import com.wwzz.services.domain.base.TCityScenicSpotDocent;
import com.wwzz.services.domain.base.TCityScenicSpots;
import com.wwzz.services.domain.base.TCityScenicSpotsProperty;
import com.wwzz.services.domain.base.TCityShop;
import com.wwzz.services.domain.base.TCityShopCategory;
import com.wwzz.services.domain.base.TCityShopProperty;
import com.wwzz.services.domain.base.TDocentsSchedule;
import com.wwzz.services.domain.base.TDocentsScheduleAssign;
import com.wwzz.services.domain.base.TDocentsScheduleItems;
import com.wwzz.services.domain.base.TLang;
import com.wwzz.services.domain.base.TProcessChargeCategory;
import com.wwzz.services.domain.base.TTourismLine;
import com.wwzz.services.domain.base.TTourismLineCategory;
import com.wwzz.services.domain.base.TTravelAgency;
import com.wwzz.services.domain.base.TUserDocents;
import com.wwzz.services.domain.booking.TUserOrderDriver;
import com.wwzz.services.domain.journey.TUserJourney;
import com.wwzz.services.domain.journey.TUserJourneyCarInfo;
import com.wwzz.services.domain.journey.TUserJourneyDaily;
import com.wwzz.services.domain.journey.TUserJourneyDailyCity;
import com.wwzz.services.domain.journey.TUserJourneyDailyDetail;
import com.wwzz.services.domain.journey.TUserJourneyData;
import com.wwzz.services.domain.journey.TUserJourneyFiles;
import com.wwzz.services.domain.journey.TUserJourneyHotelInfo;
import com.wwzz.services.domain.journey.TUserJourneyRoomsList;
import com.wwzz.services.domain.news.TNews;
import com.wwzz.services.domain.users.TUser;
import com.wwzz.services.domain.users.TUserIdentity;
import com.wwzz.services.domain.users.TUserIdentityDocents;
import com.wwzz.services.domain.users.TUserIdentityDocentsExpanded;
import com.wwzz.services.domain.users.TUserIdentityDocentsFiles;
import com.wwzz.services.domain.users.TUserIdentityDriver;
import com.wwzz.services.domain.users.TUserIdentityDriverExpanded;
import com.wwzz.services.domain.users.TUserIdentityDriverFiles;
import com.wwzz.services.domain.users.TUserIdentityLCGuide;
import com.wwzz.services.domain.users.TUserIdentityLCGuideExpanded;
import com.wwzz.services.domain.users.TUserIdentityLCGuideFiles;
import com.wwzz.services.domain.users.TUserIdentityLongGuide;
import com.wwzz.services.domain.users.TUserIdentityLongGuideExpanded;
import com.wwzz.services.domain.users.TUserIdentityLongGuideFiles;
import com.wwzz.services.domain.users.TUserIdentityOp;
import com.wwzz.services.domain.users.TUserIdentityOGuide;
import com.wwzz.services.domain.users.TUserIdentityOGuideExpanded;
import com.wwzz.services.domain.users.TUserIdentityOGuideFiles;
import com.wwzz.services.domain.users.TUserIdentityOpExpanded;
import com.wwzz.services.domain.users.TUserOrganization;
import com.wwzz.services.error.ErrorController;
import com.wwzz.services.impl.ResultDataServices;
import com.wwzz.services.repository.TAirportRepository;
import com.wwzz.services.repository.TAreaRepository;
import com.wwzz.services.repository.TCarModelsRepository;
import com.wwzz.services.repository.TCategoryLogoRepository;
import com.wwzz.services.repository.TCityIntroductionRepository;
import com.wwzz.services.repository.TCityRestaurantCategoryRepository;
import com.wwzz.services.repository.TCityRestaurantPackageRepository;
import com.wwzz.services.repository.TCityRestaurantPropertyRepository;
import com.wwzz.services.repository.TCityRestaurantRepository;
import com.wwzz.services.repository.TCityScenicSpotCategoryRepository;
import com.wwzz.services.repository.TCityScenicSpotDocentRepository;
import com.wwzz.services.repository.TCityScenicSpotsPropertyRepository;
import com.wwzz.services.repository.TCityScenicSpotsRepository;
import com.wwzz.services.repository.TCityShopCategoryRepository;
import com.wwzz.services.repository.TCityShopPropertyRepository;
import com.wwzz.services.repository.TCityShopRepository;
import com.wwzz.services.repository.TDocentsScheduleAssignRepository;
import com.wwzz.services.repository.TDocentsScheduleItemsRepository;
import com.wwzz.services.repository.TDocentsScheduleRepository;
import com.wwzz.services.repository.TLangRepository;
import com.wwzz.services.repository.TNewsRepository;
import com.wwzz.services.repository.TProcessChargeCategoryRepository;
import com.wwzz.services.repository.TTourismLineCategoryRepository;
import com.wwzz.services.repository.TTourismLineRepository;
import com.wwzz.services.repository.TTravelAgencyRepository;
import com.wwzz.services.repository.TUserDocentsRepository;
import com.wwzz.services.repository.TUserIdentityDocentsExpandedRepository;
import com.wwzz.services.repository.TUserIdentityDocentsFilesRepository;
import com.wwzz.services.repository.TUserIdentityDocentsRepository;
import com.wwzz.services.repository.TUserIdentityDriverExpandedRepository;
import com.wwzz.services.repository.TUserIdentityDriverFilesRepository;
import com.wwzz.services.repository.TUserIdentityDriverRepository;
import com.wwzz.services.repository.TUserIdentityLCGuideExpandedRepository;
import com.wwzz.services.repository.TUserIdentityLCGuideFilesRepository;
import com.wwzz.services.repository.TUserIdentityLocalGuideRepository;
import com.wwzz.services.repository.TUserIdentityLongGuideExpandedRepository;
import com.wwzz.services.repository.TUserIdentityLongGuideFilesRepository;
import com.wwzz.services.repository.TUserIdentityLongGuideRepository;
import com.wwzz.services.repository.TUserIdentityOGuideExpandedRepository;
import com.wwzz.services.repository.TUserIdentityOGuideFilesRepository;
import com.wwzz.services.repository.TUserIdentityOpExpandedReposiotry;
import com.wwzz.services.repository.TUserIdentityOpRepository;
import com.wwzz.services.repository.TUserIdentityOverseaslGuideRepository;
import com.wwzz.services.repository.TUserIdentityRepository;
import com.wwzz.services.repository.TUserJourneyCarInfoRepository;
import com.wwzz.services.repository.TUserJourneyDailyDetailRepository;
import com.wwzz.services.repository.TUserJourneyDailyRepository;
import com.wwzz.services.repository.TUserJourneyFilesRepository;
import com.wwzz.services.repository.TUserJourneyHotelInfoRepository;
import com.wwzz.services.repository.TUserJourneyRepository;
import com.wwzz.services.repository.TUserJourneyRoomsListRepository;
import com.wwzz.services.repository.TUserOrganizationRepository;
import com.wwzz.services.repository.TUserRepository;
import com.wwzz.services.repository.TUserJourneyDailyCityRepository;
import com.wwzz.services.repository.TUserJourneyDataRepository;
import com.wwzz.services.repository.TUserOrderDocentsRepository;
import com.wwzz.services.repository.TUserOrderDriverRepository;
import com.wwzz.services.repository.TUserOrderProcessRepository;
import com.wwzz.services.repository.TUserOrderRestaurantRepository;
import com.wwzz.services.repository.TUserOrderShopRepository;
import com.wwzz.services.utils.Constants;
import com.wwzz.services.utils.CheckRequest;
import com.wwzz.services.utils.WStringUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.logging.Level;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 *
 * @author tiger
 * This class is a base data management service class that 
 * do process base data such as area,language,airport etc.
 */
@Service
@Transactional
 public class BaseDataProcess extends ResultDataServices implements BaseDataServices, Constants{
    
    @Autowired
    private TAreaRepository areaRepository;
    @Autowired
    private TLangRepository langRepo;
    @Autowired
    private ErrorController errors;
    private Map resultMap;
    
    @Autowired
    private TLangRepository langRepository;
    
    @Autowired
    private TAirportRepository airportRepository;
    
    @Autowired
    private TTravelAgencyRepository travelRepository;
    
    @Autowired
    private TTourismLineCategoryRepository tcRepository;
    
    @Autowired
    private TTourismLineRepository lineRepository;
    @Autowired
    private TProcessChargeCategoryRepository priceRepository;
    
    @Autowired
    private TCityIntroductionRepository cityRepository;
    
    @Value("#{serverinfo['apps.key']}")
    private String key;
    
    @Value("#{serverinfo['excel.log.dir']}")
    private String log_dir;
    
    @Autowired
    private TCityRestaurantCategoryRepository rescRepository;
    
    @Autowired
    private TCityRestaurantRepository resRepository;
    
    @Autowired
    private TCityRestaurantPackageRepository repRepository;
    
    @Autowired
    private TCityRestaurantPropertyRepository rerRepository;
    
    @Autowired
    private TCityScenicSpotCategoryRepository sscRepository;
    
    @Autowired
    private TCityScenicSpotsRepository ssRepository;
    
    @Autowired
    private TCityScenicSpotDocentRepository ssdRepository;
    
    @Autowired
    private TCityScenicSpotsPropertyRepository ssrRepository;
    
    @Autowired
    private TCityShopCategoryRepository shcRepository;
    
    @Autowired
    private TCityShopRepository shopRepository;
    
    @Autowired
    private TCityShopPropertyRepository shrRepository;
    
    @Autowired
    private TUserRepository userRepository;
    
    @Autowired
    private TUserIdentityRepository uiRepository;
    
    @Autowired
    private TUserIdentityOpRepository uoRepository;
    
    @Autowired
    private TUserIdentityDocentsRepository udRepository;
    
    @Autowired
    private TUserIdentityLocalGuideRepository userLocalRepository;
    
    @Autowired
    private TUserIdentityLongGuideRepository userLongRepository;
    
    @Autowired
    private TUserIdentityOverseaslGuideRepository udsRepository;
    
    @Autowired
    private TNewsRepository newsRepository;
    
    @Autowired
    private TUserOrganizationRepository orgRepository;
    
    @Autowired
    private TUserIdentityDocentsExpandedRepository udeRepository;
    
    @Autowired
    private TUserIdentityDocentsFilesRepository udfRepository;
    
    @Autowired
    private TCarModelsRepository modelRepository;
    
    @Autowired
    private TUserIdentityDriverRepository udrRepository;
    
    @Autowired
    private TUserIdentityDriverExpandedRepository uddeRepository;
    
    @Autowired
    private TUserIdentityDriverFilesRepository uddfRepository;
    
    @Autowired
    private TUserIdentityLCGuideExpandedRepository uleRepository;
    
    @Autowired
    private TUserIdentityLCGuideFilesRepository ulfRepository;
    
    @Autowired
    private TUserIdentityLongGuideExpandedRepository ugeRepository;
    
    @Autowired
    private TUserIdentityLongGuideFilesRepository ugfRepository;
    
    @Autowired
    private TUserIdentityOGuideExpandedRepository uoeRepository;
    
    @Autowired
    private TUserIdentityOGuideFilesRepository uofRepository;
    
    @Autowired
    private TUserIdentityOpExpandedReposiotry uopeRepository;
    
    @Autowired
    private TUserJourneyRepository jouRepository;
    
    @Autowired
    private TUserJourneyDailyRepository jdRepository;
    
    @Autowired
    private TUserJourneyDailyDetailRepository jddRepository;
    
    @Autowired
    private TUserJourneyHotelInfoRepository jhotelRepository;
    
    @Autowired
    private TUserJourneyRoomsListRepository jroomRepository;
    
    @Autowired
    private TUserJourneyCarInfoRepository jcarRepository;
    
    @Autowired
    private TUserJourneyFilesRepository jfileRepository;
    
    @Autowired
    private TCategoryLogoRepository logoRepository;
    
    @Autowired
    private TUserJourneyDailyCityRepository dcRepository;
    
    @Autowired
    private TUserJourneyDataRepository joudRepository;
    
    @Autowired
    private UserNoticeServices noticeServices;
    
    @Autowired
    private TUserOrderDriverRepository odRepository;
    
    @Autowired
    private TUserOrderProcessRepository opcRepository;
    
    @Autowired
    private TUserOrderRestaurantRepository orsRepository;
    
    @Autowired
    private TUserOrderShopRepository oshRepository;
    
    @Autowired
    private TUserOrderDocentsRepository odsRepository;
    
    @Autowired
    private TDocentsScheduleRepository doiRepository;
    
    @Autowired
    private TUserDocentsRepository dodRepository;
    
    @Autowired
    private TDocentsScheduleAssignRepository dotRepository;
    
    @Autowired
    private TDocentsScheduleItemsRepository doddRepository;
    
    
    private  Logger logger = LoggerFactory.getLogger("ddlx-log");
    
    /**
     * Add area. 
     * @param map HashMap including area information
     * @return map HashMap
     */
    @Override
    @Transactional
    public Map locale_add(Map map) {
        Map m=(Map) map.get("data");
        TArea ta=new TArea(UUID.randomUUID().toString());
        ta.setCode((String) m.get("area_code"));
        ta.setCreated(new Date());
        if(m.containsKey("full_cn"))ta.setFname1((String) m.get("full_cn"));
        if(m.containsKey("full_en"))ta.setFname2((String) m.get("full_en"));
        if(m.containsKey("full_tw"))ta.setFname3((String) m.get("full_tw"));
        String pid=(String) m.get("parentId");
        TArea area;
         if(pid.equals("--")){
             area=null;
             ta.setLevel(1);
         }
         else {
             area=areaRepository.findOne(pid);
             if(area.getParent()==null)ta.setLevel(2);
             else ta.setLevel(3);
         }
        ta.setParent(area);
        ta.setName1((String) m.get("text"));
        if(m.containsKey("name_en"))ta.setName2((String) m.get("name_en"));
        if(m.containsKey("name_tw"))ta.setName3((String) m.get("name_tw"));
        if(m.containsKey("sort"))ta.setSort(Long.valueOf(m.get("sort").toString()));
        ta.setModified(new Date());
        ta.setSort(100000);
        List lst=areaRepository.findByName1(ta.getName1());
        Map rm;
        if(lst==null || lst.size()<1){
           areaRepository.save(ta);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            Map mmm=new HashMap();
             mmm.put("id", ta.getId());
             rm.put("data", mmm);
        }else{
            rm= raiseError(2000);
            rm.put("success", false);
        }
        return add(rm);
    }
    
    /**
     * Delete area.
     * @param id String
     * @return map HashMap
     */
    @Transactional
    @Override
    public Map locale_delete(String id) {
        Map rm=new HashMap();
        try{
            TArea area=areaRepository.findOne(id);
            List lst=travelRepository.findByArea(area);
            if(lst!=null && !lst.isEmpty()){
                rm=raiseError(2007);
                rm.put("success", false);
                return add(rm);
            }
            lst=resRepository.findByArea(area);
            if(lst!=null && !lst.isEmpty()){
                rm=raiseError(2007);
                rm.put("success", false);
                return add(rm);
            }
            areaRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception ex){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read area.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map locale_read(Map map) {
        Map rm=new HashMap();
        Map kmap=(Map) map.get("data");
        String pid=kmap!=null?(String)kmap.get("node"):null;
        String key=kmap!=null?(String) kmap.get("key"):"";
        List<TArea> list;
       List lst=new ArrayList();
       if(key==null || key.trim().isEmpty()){
            TArea pa;
            if(pid!=null)pa=areaRepository.findOne(pid);
            else pa=null;
            list=areaRepository.findByParent(pa);
            for(TArea ar:list){
                lst.add(myMap(ar));
            }
        }else{
            Sort sort=new Sort(Sort.Direction.ASC, "level");
            list=areaRepository.findByName1Containing(key,sort);
            Map<String,Map> vm=new HashMap();
            for(TArea a:list){
                switch(a.getLevel()){
                    case 1:
                        Map am=new HashMap();
                        am.put("name", a);
                        vm.put(a.getName1(), am);
                        break;
                    case 2:{
                                if(vm.containsKey(a.getParent().getName1())){
                                    Map cm=vm.get(a.getParent().getName1());
                                    Map aam=new HashMap();
                                    aam.put("name", a);
                                    cm.put(a.getName1(), aam);
                                    vm.put(a.getParent().getName1(), cm);
                                }else{
                                    Map aam=new HashMap();
                                    aam.put("name", a);
                                    Map cm=new HashMap();
                                    cm.put(a.getName1(), aam);
                                    cm.put("name", a.getParent());
                                    vm.put(a.getParent().getName1(), cm);
                                }
                            }break;
                    case 3:{
                           if(vm.containsKey(a.getParent().getParent().getName1())){
                               Map cm=vm.get(a.getParent().getParent().getName1());
                               if(cm.containsKey(a.getParent().getName1())){
                                   Map ccm=(Map) cm.get(a.getParent().getName1());
                                   Map aam=new HashMap();
                                   aam.put("name", a);
                                   ccm.put(a.getName1(), aam);
                                   cm.put(a.getParent().getName1(), ccm);
                               }else{
                                   Map aam=new HashMap();
                                   aam.put("name", a);
                                   Map ccm=new HashMap();
                                   ccm.put("name", a.getParent());
                                   ccm.put(a.getName1(),aam);
                                   cm.put(a.getParent().getName1(),ccm);
                               }
                               vm.put(a.getParent().getParent().getName1(), cm);
                           }else{
                               Map aam=new HashMap();
                               aam.put("name", a);
                               Map ccm=new HashMap();
                               ccm.put("name", a.getParent());
                               ccm.put(a.getName1(), aam);
                               Map cm=new HashMap();
                               cm.put("name", a.getParent().getParent());
                               cm.put(a.getParent().getName1(), ccm);
                               vm.put(a.getParent().getParent().getName1(), cm);
                           }
                    }break;
                }
            }
            for(String s:vm.keySet()){
                Map sm=vm.get(s);
                TArea nm=(TArea) sm.get("name");
                Map m1=myMap(nm);
                m1.put("expanded", true);
                if(sm.keySet().size()>1){
                    List c2=new ArrayList();
                    for(Object sc:sm.keySet()){
                          if(sc.toString().equals("name"))continue;
                          Map scm=(Map) sm.get(sc);
                          TArea ns=(TArea) scm.get("name");
                          Map m2=myMap(ns);
                          if((boolean)m2.get("leaf")){
                              c2.add(m2);
                              continue;
                          }
                          m2.put("expanded", true);
                          if(scm.keySet().size()>1){
                              List c3=new ArrayList();
                              for(Object oo:scm.keySet()){
                                  if(oo.toString().equals("name"))continue;
                                  Map ssm=(Map) scm.get(oo);
                                  TArea np=(TArea) ssm.get("name");
                                  Map m3=myMap(np);
                                  c3.add(m3);
                              }
                              m2.put("children", c3);
                          }else{
                              if(!(boolean)m2.get("leaf"))m2.put("loaded", true);
                              m2.put("expanded", false);
                          }
                          c2.add(m2);
                    }
                    m1.put("children", c2);
                }else{
                    m1.put("loaded", true);
                }
                lst.add(m1);
            }
        }
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("children", lst);
        rm.put("data", lst);
        return add(rm);
    }

    private Map myMap(TArea ta){
        Map m=new HashMap();
            boolean bb=false;
            m.put("text", ta.getName1());
            m.put("name_en", ta.getName2());
            m.put("name_tw", ta.getName3());
            m.put("full_cn", ta.getFname1());
            m.put("full_en", ta.getFname2());
            m.put("full_tw", ta.getFname3());
            m.put("sort", Long.valueOf(ta.getSort()).intValue());
            m.put("id", ta.getId());
            m.put("flag", ta.getFlag()==1);
            if(ta.getParent()!=null)m.put("parentId", ta.getParent().getId());
            else m.put("parentId","--");
             m.put("area_code", ta.getCode());
            if(ta.getCreated()!=null)m.put("c_date", WStringUtils.dateToString(ta.getCreated()));
            if(ta.getModified()!=null)m.put("m_date", WStringUtils.dateToString(ta.getModified()));
            if(ta.getLevel()==2 && !ta.getParent().getName1().equals("中国"))bb=true;
            if(ta.getLevel()==3)bb=true;
            m.put("leaf", bb);
        return m;
    }
    
    /**
     * Update area.
     * @param map HashMap
     * @return map HashMap
     */
    @Transactional
    @Override
    public Map locale_update(Map map) {
         Map m=(Map) map.get("data");
         Map rm=new HashMap();
         String id=(String) m.get("id");
         TArea ta=areaRepository.findOne(id);
         if((String)m.get("text")!=null)ta.setName1((String) m.get("text"));
         if(m.containsKey("name_en"))ta.setName2((String) m.get("name_en"));
         String sss=(String) m.get("name_tw");
         if(m.containsKey("name_tw"))ta.setName3(sss);
         if(m.containsKey("full_en"))ta.setFname2((String) m.get("full_en"));
         if(m.containsKey("full_cn"))ta.setFname1((String) m.get("full_cn"));
         if(m.containsKey("full_tw"))ta.setFname3((String) m.get("full_tw"));
         if(m.containsKey("area_code")){
             
             ta.setCode((String) m.get("area_code"));
         }
         if(m.containsKey("flag"))ta.setFlag((Boolean)m.get("flag")?1:0);
         if(m.containsKey("sort"))ta.setSort(Long.valueOf(m.get("sort").toString()));
         ta.setModified(new Date());
         
         List lst=areaRepository.findByName1(ta.getName1());
         if((String)m.get("text")!=null){
             if(lst.size()>0){
                 TArea ttt=(TArea) lst.get(0);
                 if(ttt.getName1().equals(ta.getName1())){
                        rm=raiseError(2000);
                        rm.put("success", false);
                 }else{
                      areaRepository.save(ta);
                       rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                 }
            }else{
                areaRepository.save(ta);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
         }else{
             areaRepository.save(ta);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         }
        return add(rm);
    }
    
    /**
     * Add language.
     * @param map HashMap
     * @return map HashMap
     */
    @Transactional
    @Override
    public Map lang_add(Map map) {
        Map m=(Map) map.get("data");
        Map rm=new HashMap();
       TLang lang=new TLang();
       lang.setCreated(new Date());
       lang.setModified(new Date());
       lang.setNameZh((String) m.get("name_zh"));
       if((String)m.get("name_en")!=null)lang.setNameEn((String) m.get("name_en"));
       if((String)m.get("name_tw")!=null)lang.setNameTw((String) m.get("name_tw"));
       if((String)m.get("full_zh")!=null)lang.setDescriptionZh((String) m.get("full_zh"));
       if((String)m.get("full_en")!=null)lang.setDescriptionEn((String) m.get("full_en"));
       if((String)m.get("full_tw")!=null)lang.setDescriptionTw((String) m.get("full_tw"));
       List lst=langRepository.findByNameZh(lang.getNameZh());
       if(lst==null || lst.size()<1){
           langRepository.save(lang);
             rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
             Map mmm=new HashMap();
             mmm.put("id", lang.getId());
             rm.put("data", mmm);
       }else {
           rm=raiseError(2000);
           rm.put("success", false);
       }
       return add(rm);    }

    /**
     * Delete language. 
     * @param map HashMap
     * @return map HashMap
     */
    @Transactional
    @Override
    public Map lang_delete(Map map) {
        Map model=(Map) map.get("data");
        Long id=Long.valueOf( model.get("id").toString());
        Map rm=new HashMap();
        try{
            TLang lang=langRepository.findOne(id);
            List lst=resRepository.findByLanguage(lang);
            if(lst!=null && !lst.isEmpty()){
                rm=raiseError(2007);
                rm.put("success", false);
                 return add(rm);
            }
            langRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read language.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map lang_read(Map map) {
          Map rm=new HashMap();
          Map m=(Map) map.get("data");
          String ke=(String) m.get("key");
          if(ke==null)ke="";
        List<TLang> list=langRepository.findByNameZhContaining(ke);
        List lst=new ArrayList();
        for(TLang lang:list){
            Map mm=new HashMap();
            mm.put("id", lang.getId().toString());
            mm.put("name_zh", lang.getNameZh());
            mm.put("name_en", lang.getNameEn());
            mm.put("name_tw", lang.getNameTw());
            mm.put("full_zh", lang.getDescriptionZh());
            mm.put("full_en", lang.getDescriptionEn());
            mm.put("full_tw", lang.getDescriptionTw());
            lst.add(mm);
        }
        
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("data", lst);
        return add(rm);
    }

    /**
     * Update language.
     * @param map HashMap
     * @return map HashMap
     */
    @Transactional
    @Override
    public Map lang_update(Map map) {
        Map rm=new HashMap();
        Map m=(Map) map.get("data");
       TLang lang=langRepository.findOne(Long.valueOf((String) m.get("id")));
       if((String)m.get("name_zh")!=null)lang.setNameZh((String) m.get("name_zh"));
        if((String)m.get("name_en")!=null)lang.setNameEn((String) m.get("name_en"));
       if((String)m.get("name_tw")!=null)lang.setNameTw((String) m.get("name_tw"));
       if((String)m.get("full_zh")!=null)lang.setDescriptionZh((String) m.get("full_zh"));
       if((String)m.get("full_en")!=null)lang.setDescriptionEn((String) m.get("full_en"));
       if((String)m.get("full_tw")!=null)lang.setDescriptionTw((String) m.get("full_tw"));
       List lst=langRepository.findByNameZh(lang.getNameZh());
       if((String)m.get("name_zh")!=null){
           if(lst.size()>0){
               TLang ttt=(TLang) lst.get(0);
               if(ttt.getNameZh().equals(lang.getNameZh())){
                   rm=raiseError(2000);
                   rm.put("success", false);
               }else{
                   langRepository.save(lang);
                   rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
               }
           }else{
               langRepository.save(lang);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
           }
       }else{
           langRepository.save(lang);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
       }
       return add(rm);
    }

    /**
     * 
     * @param level
     * @return map
     */
    @Override
    public Map findAreaByLevel(String level) {
        resultMap=errors.createResultMap(RETURN_CODE_OK, "", ERROR_CODE_OK, "");
        Sort sort=new Sort(Sort.Direction.ASC, "name1");
  //      resultMap.put(DATA_KEY, areaRepository.findByLevel(level,sort));
        return resultMap;
    }

    /**
     * 
     * @return map
     */
    @Override
    public Map findLangAll() {
        resultMap=errors.createResultMap(RETURN_CODE_OK, "", ERROR_CODE_OK, "");
        resultMap.put(DATA_KEY, langRepo.findAll());
        return resultMap;
    }
    
    /**
     *  Add airport.
     * @param map HashMap
     * @return map HashMap
     */
   @Override
    public Map air_add(Map map) {
       Map m=(Map) map.get("data");
       Map rm=new HashMap();
       String code=(String) m.get("area_code");
       TArea ta=areaRepository.findOne(code);
       if(ta==null){
           rm=raiseError(2001);
           rm.put("success", false);
       }else{
            TAirport air=new TAirport();
            air.setCreated(new Date());
            air.setModified(new Date());
            air.setNameZh((String) m.get("name_zh"));
            if((String)m.get("name_en")!=null)air.setNameEn((String) m.get("name_en"));
            if((String)m.get("name_tw")!=null)air.setNameTw((String) m.get("name_tw"));
            if((String)m.get("full_zh")!=null)air.setDescriptionZh((String) m.get("full_zh"));
            if((String)m.get("full_en")!=null)air.setDescriptionEn((String) m.get("full_en"));
            if((String)m.get("full_tw")!=null)air.setDescriptionTw((String) m.get("full_tw"));
             List lst=airportRepository.findByNameZhAndArea(air.getNameZh(),ta);
            if(lst==null || lst.size()<1){
                air.setArea(ta);
                airportRepository.save(air);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                  Map mmm=new HashMap();
                  mmm.put("id", air.getId());
                  rm.put("data", mmm);
            }else {
                     rm=raiseError(2000);
                     rm.put("success", false);
            }
       }
        return add(rm);
    }
    
    /**
     * Delete airport.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map air_delete(Map map) {
        Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        try{
            TAirport air=airportRepository.findOne(id);
            List<TUserOrderDriver> as=odRepository.findByAirport(air);
            if(as!=null && !as.isEmpty()){
                rm=raiseError(2007);
                rm.put("success", false);
                return add(rm);
            }
            airportRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read airport.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map air_read(Map model) {
          Map rm=new HashMap();
          Map map=(Map) model.get("data");
          String cid=(String) map.get("cid");
          String key=(String) map.get("key");
          if(key==null)key="";
          List<TAirport> list=new ArrayList<>();
          if(cid==null || cid.isEmpty()){
              list=airportRepository.findByNameZhContainingOrNameEnContaining(key, key);
          }else{
              TArea ta=areaRepository.findOne(cid);
              List<TArea> tlist=new ArrayList<>();
              tlist.add(ta);
              if(ta.getParent()==null){
                  List<TArea> clist=areaRepository.findByCid(ta.getId());
                  for(TArea ch:clist){
                      tlist.add(ch);
                      List<TArea> cclist=areaRepository.findByCid(ch.getId());
                      tlist.addAll(cclist);
                  }
              }else {
                  List<TArea> ttlist=areaRepository.findByCid(ta.getId());
                  tlist.addAll(ttlist);
              }
              for(TArea tt:tlist){
                  list.addAll(airportRepository.findByArea(tt));
              }
          }
        
        List lst=new ArrayList();
        for(TAirport air:list){
            Map mm=new HashMap();
            mm.put("id", air.getId().toString());
            mm.put("name_zh", air.getNameZh());
            mm.put("name_en", air.getNameEn());
            mm.put("name_tw", air.getNameTw());
            mm.put("full_zh", air.getDescriptionZh());
            mm.put("full_en", air.getDescriptionEn());
            mm.put("full_tw", air.getDescriptionTw());
            mm.put("area_code", air.getArea().getId());
            mm.put("area_codename", air.getArea().getName1());
            lst.add(mm);
        }
        
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("data", lst);
        return add(rm);
    }

    /**
     * Update airport.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map air_update(Map map) {
          Map rm=new HashMap();
          Map m=(Map) map.get("data");
          TAirport air=airportRepository.findOne(Long.valueOf((String) m.get("id")));
          if((String)m.get("name_zh")!=null)air.setNameZh((String) m.get("name_zh"));
            if((String)m.get("name_en")!=null)air.setNameEn((String) m.get("name_en"));
            if((String)m.get("name_tw")!=null)air.setNameTw((String) m.get("name_tw"));
            if((String)m.get("full_zh")!=null)air.setDescriptionZh((String) m.get("full_zh"));
            if((String)m.get("full_en")!=null)air.setDescriptionEn((String) m.get("full_en"));
            if((String)m.get("full_tw")!=null)air.setDescriptionTw((String) m.get("full_tw"));
            List lst=airportRepository.findByNameZhAndArea(air.getNameZh(),air.getArea());
            if((String)m.get("name_zh")!=null){
                if(lst.size()>0){
                    TAirport ttt=(TAirport) lst.get(0);
                    if(ttt.getNameZh().equals(air.getNameZh())){
                        rm=raiseError(2000);
                        rm.put("success", false);
                    }else{
                        airportRepository.save(air);
                         rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                    }
                }else{
                    airportRepository.save(air);
                     rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                }
            }else{
                airportRepository.save(air);
                     rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
          return add(rm);
    }
    
    /**
     * Add travelAgency.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map tra_add(Map map) {
        Map m=(Map) map.get("data");
       Map rm=new HashMap();
       
       String code=(String) m.get("area_code");
       TArea ta=areaRepository.findOne(code);
       if(ta==null){
           rm=raiseError(2001);
           rm.put("success", false);
       }else{
            TTravelAgency tra=new TTravelAgency();
            tra.setNameZh((String) m.get("name_zh"));
            if((String)m.get("name_en")!=null)tra.setNameEn((String) m.get("name_en"));
            if((String)m.get("name_tw")!=null)tra.setNameTw((String) m.get("name_tw"));
            if((String)m.get("full_zh")!=null)tra.setDescriptionZh((String) m.get("full_zh"));
            if((String)m.get("full_en")!=null)tra.setDescriptionEn((String) m.get("full_en"));
            if((String)m.get("full_tw")!=null)tra.setDescriptionTw((String) m.get("full_tw"));
            List lst=travelRepository.findByNameZhAndArea(tra.getNameZh(),ta);
            if(lst==null || lst.size()<1){
                tra.setArea(ta);
                travelRepository.save(tra);
                 rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                  Map mmm=new HashMap();
                  mmm.put("id", tra.getId());
                  rm.put("data", mmm);
            }else {
                     rm=raiseError(2000);
                     rm.put("success", false);
            }
       }
       return add(rm);
    }

    /**
     * Delete travelAgency.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map tra_delete(Map map) {
        Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        try{
            TTravelAgency tra=travelRepository.findOne(id);
            List lst=jouRepository.findByTravelAgency(tra);
            if(lst!=null && !lst.isEmpty()){
                rm=raiseError(2007);
                rm.put("success", false);
                 return add(rm);
            }
            travelRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read travelAgency.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map tra_read(Map model) {
        Map rm=new HashMap();
        Map map=(Map) model.get("data");
          String cid=(String) map.get("cid");
          String key=(String) map.get("key");
          boolean flag=map.get("page")==null;
          int start=0;
          if(!flag)start=(int) map.get("page");
          if(start<0)start=1;
          int count=15;
          int total=0;
          if(hasText(key)){
            key="%"+key.toLowerCase()+"%";
          }else{
            key="%%";
         }
          
          List<TTravelAgency> list=new ArrayList<>();
          if(flag){
              list=travelRepository.findAll();
          }else{
              Pageable page=new PageRequest(start-1, count);
              Page dt;
                  if(cid==null || cid.isEmpty()){
                      total=travelRepository.countByNameZhContainingOrNameEnContaining(key, key).intValue();
                         if(total<(start-1)*count){
                          page=new PageRequest(0, count);
                      }
                        dt=travelRepository.findByNameZhContainingOrNameEnContaining(key, key,page);
                        
                    }else{
                        TArea ta=areaRepository.findOne(cid);
                        List<TArea> tlist=new ArrayList<>();
                        tlist.add(ta);
                        if(ta.getParent()==null){
                            List<TArea> clist=areaRepository.findByCid(ta.getId());
                            for(TArea ch:clist){
                                tlist.add(ch);
                                List<TArea> cclist=areaRepository.findByCid(ch.getId());
                                tlist.addAll(cclist);
                            }
                        }else {
                            List<TArea> ttlist=areaRepository.findByCid(ta.getId());
                            tlist.addAll(ttlist);
                        }
                        total=travelRepository.countByAreas(tlist);
                         if(total<(start-1)*count){
                           page=new PageRequest(0, count);
                        }
                         dt=travelRepository.findByAreas(tlist, page);
                       
                    }
                  list=dt.getContent();
          }
      
          
        List lst=new ArrayList();
        for(TTravelAgency air:list){
            Map mm=new HashMap();
            mm.put("id", air.getId().toString());
            mm.put("name_zh", air.getNameZh());
            mm.put("name_en", air.getNameEn());
            mm.put("name_tw", air.getNameTw());
            mm.put("full_zh", air.getDescriptionZh());
            mm.put("full_en", air.getDescriptionEn());
            mm.put("full_tw", air.getDescriptionTw());
            mm.put("area_code", air.getArea().getId());
            mm.put("area_codename", air.getArea().getName1());
            mm.put("license", air.getLicense());
            mm.put("phone", air.getPhone());
            mm.put("icon_url", air.getImageUrl());
            lst.add(mm);
        }
         rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("data", lst);
        rm.put("total", total);
        return add(rm);
    }

    /**
     *  Update travelAgency.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map tra_update(Map map) {
        Map rm=new HashMap();
          Map m=(Map) map.get("data");
          TTravelAgency air=travelRepository.findOne(Long.valueOf((String) m.get("id")));
          if((String)m.get("name_zh")!=null)air.setNameZh((String) m.get("name_zh"));
            if((String)m.get("name_en")!=null)air.setNameEn((String) m.get("name_en"));
            if((String)m.get("name_tw")!=null)air.setNameTw((String) m.get("name_tw"));
            if((String)m.get("full_zh")!=null)air.setDescriptionZh((String) m.get("full_zh"));
            if((String)m.get("full_en")!=null)air.setDescriptionEn((String) m.get("full_en"));
            if((String)m.get("full_tw")!=null)air.setDescriptionTw((String) m.get("full_tw"));
            if((String)m.get("phone")!=null)air.setPhone((String)m.get("phone"));
            if((String)m.get("license")!=null)air.setLicense((String)m.get("license"));
            if((String)m.get("icon_url")!=null)air.setImageUrl((String)m.get("icon_url"));
             List lst=travelRepository.findByNameZhAndArea(air.getNameZh(),air.getArea());
            if((String)m.get("name_zh")!=null){
                if(lst.size()>0){
                    TTravelAgency ttt=(TTravelAgency) lst.get(0);
                    if(ttt.getNameZh().equals(air.getNameZh())){
                        rm=raiseError(2000);
                        rm.put("success", false);
                    }else{
                         if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                            if(air.getImageStorage()!=null)file_delete_on_fastdfs(air.getImageStorage());
                            String donot=(String) m.get("idonotknow");
                            air.setImageStorage(clientApp.get_uid((String)map.get("partner"), donot));
                        }
                        travelRepository.save(air);
                        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                    }
                }else{
                    if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                            if(air.getImageStorage()!=null)file_delete_on_fastdfs(air.getImageStorage());
                            String donot=(String) m.get("idonotknow");
                            air.setImageStorage(clientApp.get_uid((String)map.get("partner"), donot));
                        }
                    travelRepository.save(air);
                     rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                }
            }else{
                if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                            if(air.getImageStorage()!=null)file_delete_on_fastdfs(air.getImageStorage());
                            String donot=(String) m.get("idonotknow");
                            air.setImageStorage(clientApp.get_uid((String)map.get("partner"), donot));
                        }
                travelRepository.save(air);
                     rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
          return add(rm);
    }
    
    
     private Map add(Map map){
        map.put("partner", "11111111111111111111");
        Calendar cal=Calendar.getInstance();
        cal.setTime(new Date());
        map.put("timestamp", cal.getTime().getTime());
        CheckRequest cr=new CheckRequest(key, map);
        map.put("sign", cr.hashParams());
        return map;
    }

     
     /**
      * Add tourism line category. 
      * @param map HashMap
      * @return map HashMap
      */
    @Transactional
    @Override
    public Map tor_add(Map map) {
       Map m=(Map) map.get("data");
       Map rm=new HashMap();
       TTourismLineCategory tc=new TTourismLineCategory();
       tc.setCreated(new Date());
       tc.setModified(new Date());
       tc.setNameZh((String) m.get("name_zh"));
       if((String)m.get("name_en")!=null)tc.setNameEn((String) m.get("name_en"));
       if((String)m.get("name_tw")!=null)tc.setNameTw((String) m.get("name_tw"));
       if((String)m.get("full_zh")!=null)tc.setDescriptionZh((String) m.get("full_zh"));
       if((String)m.get("full_en")!=null)tc.setDescriptionEn((String) m.get("full_en"));
       if((String)m.get("full_tw")!=null)tc.setDescriptionTw((String) m.get("full_tw"));
       
      List lst=tcRepository.findByNameZh(tc.getNameZh());
       if(lst==null || lst.size()<1){
           tcRepository.save(tc);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
             Map mmm=new HashMap();
                  mmm.put("id", tc.getId());
                  rm.put("data", mmm);
       }else {
                rm=raiseError(2000);
                rm.put("success", false);
       }
        return add(rm);
    }

    /**
     *  Delete tourism line category.
     * @param map HashMap
     * @return map HashMap
     */
    @Transactional
    @Override
    public Map tor_delete(Map map) {
        Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        TTourismLineCategory tc=tcRepository.findOne(id);
        List<TTourismLine> tts=lineRepository.findByCategory(tc);
        Map rm=new HashMap();
        if(tts.size()>0){
                rm=raiseError(2000);
                rm.put("success", false);
        }else{
           try{
                List<TUserIdentityLongGuide> as=userLongRepository.findByCid(id);
                if(as!=null && !as.isEmpty()){
                    rm=raiseError(2007);
                    rm.put("success", false);
                    return add(rm);
                }
                tcRepository.delete(id);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }catch(Exception e){
                rm=raiseError(3000);
                rm.put("success", false);
            }
        }
        return add(rm);
    }

    /**
     * Read tourism line category.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map tor_read(Map model) {
        Map rm=new HashMap();
        Map map=(Map) model.get("data");
        String key=(String) map.get("key");
         List<TTourismLineCategory> list;
        if(key==null)key="";
        list=tcRepository.findByNameZhContainingOrNameEnContaining(key, key);
        List lst=new ArrayList();
        for(TTourismLineCategory lang:list){
            Map mm=new HashMap();
            mm.put("id", lang.getId().toString());
            mm.put("name_zh", lang.getNameZh());
            mm.put("name_en", lang.getNameEn());
            mm.put("name_tw", lang.getNameTw());
            mm.put("full_zh", lang.getDescriptionZh());
            mm.put("full_en", lang.getDescriptionEn());
            mm.put("full_tw", lang.getDescriptionTw());
            lst.add(mm);
        }
        
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("data", lst);
        return add(rm);
    }

    /**
     * Update tourism line category. 
     * @param map HashMap
     * @return map HashMap
     */
    @Transactional
    @Override
    public Map tor_update(Map map) {
        Map rm=new HashMap();
        Map m=(Map) map.get("data");
       TTourismLineCategory lang=tcRepository.findOne(Long.valueOf((String) m.get("id")));
       if((String)m.get("name_zh")!=null)lang.setNameZh((String) m.get("name_zh"));
        if((String)m.get("name_en")!=null)lang.setNameEn((String) m.get("name_en"));
       if((String)m.get("name_tw")!=null)lang.setNameTw((String) m.get("name_tw"));
       if((String)m.get("full_zh")!=null)lang.setDescriptionZh((String) m.get("full_zh"));
       if((String)m.get("full_en")!=null)lang.setDescriptionEn((String) m.get("full_en"));
       if((String)m.get("full_tw")!=null)lang.setDescriptionTw((String) m.get("full_tw"));
       
        List lst=tcRepository.findByNameZh(lang.getNameZh());
       if((String)m.get("name_zh")!=null){
           if(lst.size()>0){
               TTourismLineCategory ttt=(TTourismLineCategory) lst.get(0);
               if(ttt.getNameZh().equals(lang.getNameZh())){
                   rm=raiseError(2000);
                   rm.put("success", false);
               }else{
                   tcRepository.save(lang);
                    rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
               }
           }else{
               tcRepository.save(lang);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
           }
       }else{
            tcRepository.save(lang);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
       }
       return add(rm);
    }

    /**
     * Add tourism line. 
     * @param map HashMap
     * @return map HashMap
     */
    @Transactional
    @Override
    public Map line_add(Map map) {
        Map m=(Map) map.get("data");
       Map rm=new HashMap();
       
       String code=(String) m.get("category_id");
       TTourismLineCategory ta=tcRepository.findOne(Long.valueOf(code));
       if(ta==null){
           rm=createResult(RETURN_ERROR_CODE, "operation.fail", 500, "operation.fail", "false");
       }else{
            TTourismLine tra=new TTourismLine();
            tra.setNameZh((String) m.get("name_zh"));
            if((String)m.get("name_en")!=null)tra.setNameEn((String) m.get("name_en"));
            if((String)m.get("name_tw")!=null)tra.setNameTw((String) m.get("name_tw"));
            if((String)m.get("full_zh")!=null)tra.setDescriptionZh((String) m.get("full_zh"));
            if((String)m.get("full_en")!=null)tra.setDescriptionEn((String) m.get("full_en"));
            if((String)m.get("full_tw")!=null)tra.setDescriptionTw((String) m.get("full_tw"));
            tra.setCategory(ta);
            tra.setCreated(new Date());
            tra.setModified(new Date());
            List lst=lineRepository.findByNameZhAndCategory(tra.getNameZh(),ta);
            if(lst==null || lst.size()<1){
                try{
                lineRepository.save(tra);
                }catch(Exception e){
                    String ss="";
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                  Map mmm=new HashMap();
                  mmm.put("id", tra.getId());
                  rm.put("data", mmm);
            }else {
                     rm=createResult(RETURN_ERROR_CODE, "already.exist", 500, "already.exist", "false");
            }
       }
       
        return add(rm);
    }

    /**
     * Delete tourism line. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map line_delete(Map map) {
         Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        try{
            lineRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=createResult(RETURN_ERROR_CODE, "operation.fail", 500, "operation.fail", "false");
        }
        return add(rm);
    }

    /**
     * Read tourism line.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map line_read(Map model) {
          Map rm=new HashMap();
          Map map=(Map) model.get("data");
          String cid=(String) map.get("cid");
          String key=(String) map.get("key");
          List<TTourismLine> list=new ArrayList<>();
          if(cid==null || cid.isEmpty()){
              list=lineRepository.findByNameZhContainingOrNameEnContaining(key, key);
          }else{
             TTourismLineCategory tc=tcRepository.findOne(Long.valueOf(cid));
              list=lineRepository.findByCategory(tc);
          }
        List lst=new ArrayList();
        for(TTourismLine air:list){
            Map mm=new HashMap();
            mm.put("id", air.getId().toString());
            mm.put("name_zh", air.getNameZh());
            mm.put("name_en", air.getNameEn());
            mm.put("name_tw", air.getNameTw());
            mm.put("full_zh", air.getDescriptionZh());
            mm.put("full_en", air.getDescriptionEn());
            mm.put("full_tw", air.getDescriptionTw());
            mm.put("category_id", air.getCategory().getId());
            mm.put("category_name", air.getCategory().getNameZh());
            lst.add(mm);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("data", lst);
        return add(rm);
    }

    /**
     * Update tourism line. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map line_update(Map map) {
         Map rm=new HashMap();
          Map m=(Map) map.get("data");
          TTourismLine air=lineRepository.findOne(Long.valueOf((String) m.get("id")));
          if((String)m.get("name_zh")!=null)air.setNameZh((String) m.get("name_zh"));
            if((String)m.get("name_en")!=null)air.setNameEn((String) m.get("name_en"));
            if((String)m.get("name_tw")!=null)air.setNameTw((String) m.get("name_tw"));
            if((String)m.get("full_zh")!=null)air.setDescriptionZh((String) m.get("full_zh"));
            if((String)m.get("full_en")!=null)air.setDescriptionEn((String) m.get("full_en"));
            if((String)m.get("full_tw")!=null)air.setDescriptionTw((String) m.get("full_tw"));
            air.setModified(new Date());
        List lst=lineRepository.findByNameZhAndCategory(air.getNameZh(),air.getCategory());
            if((String)m.get("name_zh")!=null){
                if(lst.size()>0){
                    TTourismLine ttt=(TTourismLine) lst.get(0);
                    if(ttt.getNameZh().equals(air.getNameZh())){
                        rm=createResult(RETURN_ERROR_CODE, "already.exist", 500, "already.exist", "false");
                    }else{
                        lineRepository.save(air);
                         rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                    }
                }else{
                    lineRepository.save(air);
                     rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                }
            }else{
                lineRepository.save(air);
                     rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
          return add(rm);
    }

    /**
     * Add process charge category.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map cm_add(Map map) {
       Map m=(Map) map.get("data");
       Map rm=new HashMap();
       String code=(String) m.get("area_code");
       TArea ta=areaRepository.findOne(code);
       if(ta==null){
           rm.put("return_code", true);
            rm.put("return_msg", "Failure");
            rm.put("success", false);
       }else{
            TProcessChargeCategory tra=new TProcessChargeCategory();
            String pid=(String) m.get("parentId");
            TProcessChargeCategory pa=pid.equals("-1")?null:priceRepository.findOne(Long.valueOf(pid));
            tra.setNameZh((String) m.get("text"));
            if((String)m.get("name_en")!=null)tra.setNameEn((String) m.get("name_en"));
            if((String)m.get("name_tw")!=null)tra.setNameTw((String) m.get("name_tw"));
            if((String)m.get("full_zh")!=null)tra.setDescriptionZh((String) m.get("full_zh"));
            if((String)m.get("full_en")!=null)tra.setDescriptionEn((String) m.get("full_en"));
            if((String)m.get("full_tw")!=null)tra.setDescriptionTw((String) m.get("full_tw"));
            tra.setParent(pa);
            if(m.containsKey("price")){
                
               if(m.get("price")!=null)tra.setPrice(Float.valueOf(m.get("price").toString()));
            }
            if(pa==null)tra.setLevel(0);
            else tra.setLevel(pa.getLevel()+1);
//            List lst=priceRepository.findByNameZhAndArea(tra.getNameZh(),ta);
//            if(lst==null || lst.size()<1){
                tra.setArea(ta);
                priceRepository.save(tra);
                 rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                  Map mmm=new HashMap();
                  mmm.put("id", tra.getId());
                  rm.put("data", mmm);
//            }else {
//                     rm=raiseError(2000);
//                     rm.put("success", false);
//            }
       }
       
        return add(rm);
    }

    /**
     * Delete process charge category. 
     * @param iid String 
     * @return map HashMap
     */
    @Override
    public Map cm_delete(String iid) {
        Long id=Long.valueOf(iid);
        Map rm=new HashMap();
        try{
            TProcessChargeCategory pc=priceRepository.findOne(id);
            List as=opcRepository.findByProcessCharge(pc);
               if(as!=null && !as.isEmpty()){
                    rm=raiseError(2007);
                    rm.put("success", false);
                    return add(rm);
                }
            priceRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read process charge category.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map cm_read(Map model) {
          Map rm=new HashMap();
          Map map=(Map) model.get("data");
          String cid=(String) map.get("node");
          if(cid.equals("root"))cid="-1";
          String key=(String) map.get("key");
          List<TProcessChargeCategory> list=new ArrayList<>();
          if(!cid.contains("-")){
              TProcessChargeCategory tc=priceRepository.findOne(Long.valueOf(cid));
              if(tc!=null)list=priceRepository.findByParent(tc);
          }else{
              TArea area=areaRepository.findOne(cid);
              list=priceRepository.findByAreaAndLevel(area, 0);
          }
        
         List lst=new ArrayList();
         for(TProcessChargeCategory air:list){
            Map mm=new HashMap();
            mm.put("id", air.getId().toString());
            mm.put("text", air.getNameZh());
            mm.put("name_en", air.getNameEn());
            mm.put("name_tw", air.getNameTw());
            mm.put("full_zh", air.getDescriptionZh());
            mm.put("full_en", air.getDescriptionEn());
            mm.put("full_tw", air.getDescriptionTw());
            mm.put("area_code", air.getArea().getId());
            mm.put("area_codename", air.getArea().getName1());
            mm.put("price", air.getPrice()==null?"":air.getPrice());
            mm.put("leaf", (air.getPrice() != null));
            mm.put("parentId", air.getParent()==null?"-1":air.getParent().getId().toString());
            lst.add(mm);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("data", lst);
        return add(rm);
    }

    /**
     * Update process charge category.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map cm_update(Map map) {
        Map rm=new HashMap();
          Map m=(Map) map.get("data");
          TProcessChargeCategory air=priceRepository.findOne(Long.valueOf((String) m.get("id")));
          if((String)m.get("text")!=null)air.setNameZh((String) m.get("text"));
            if((String)m.get("name_en")!=null)air.setNameEn((String) m.get("name_en"));
            if((String)m.get("name_tw")!=null)air.setNameTw((String) m.get("name_tw"));
            if((String)m.get("full_zh")!=null)air.setDescriptionZh((String) m.get("full_zh"));
            if((String)m.get("full_en")!=null)air.setDescriptionEn((String) m.get("full_en"));
            if((String)m.get("full_tw")!=null)air.setDescriptionTw((String) m.get("full_tw"));
            if(m.containsKey("price"))air.setPrice(Float.valueOf(m.get("price").toString()));
            List lst=priceRepository.findByNameZhAndArea(air.getNameZh(),air.getArea());
            if((String)m.get("name_zh")!=null){
                if(lst.size()>0){
                    TProcessChargeCategory ttt=(TProcessChargeCategory) lst.get(0);
                    if(ttt.getNameZh().equals(air.getNameZh())){
                        rm=createResult(RETURN_ERROR_CODE, "already.exist", 500, "already.exist", "false");
                    }else{
                        priceRepository.save(air);
                        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                    }
                }else{
                    priceRepository.save(air);
                     rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                }
            }else{
                priceRepository.save(air);
                    rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
          return add(rm);
    }

    /**
     * Add city introduction.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map city_add(Map map) {
       Map m=(Map) map.get("data");
       Map rm=new HashMap();
       
       String code=(String) m.get("area_code");
       TArea ta=areaRepository.findOne(code);
       if(ta==null){
           rm=raiseError(2001);
           rm.put("success", false);
       }else{
            TCityIntroduction tra;
           List<TCityIntroduction> tras=cityRepository.findByArea(ta);
            if(tras==null || tras.isEmpty()){
                tra=new TCityIntroduction();
            }
            else tra=tras.get(0);
            
            if((String)m.get("full_zh")!=null)tra.setIntroduction((String) m.get("full_zh"));
            if((String)m.get("full_en")!=null)tra.setIntroduction1((String) m.get("full_en"));
            if((String)m.get("full_tw")!=null)tra.setIntroduction2((String) m.get("full_tw"));
            if(m.containsKey("logo"))tra.setLogo((Boolean)m.get("logo")?1:0);
            if(m.containsKey("icon_url") && ((String) m.get("icon_url"))!=null && !((String) m.get("icon_url")).isEmpty())tra.setViewUrl((String) m.get("icon_url"));
            if(m.containsKey("idonotknow") && ((String) m.get("idonotknow"))!=null && !((String) m.get("idonotknow")).isEmpty()){
                            if(tra.getStoragePath()!=null)file_delete_on_fastdfs(tra.getStoragePath());
                           String donot=(String) m.get("idonotknow");
                            tra.setStoragePath(clientApp.get_uid((String)map.get("partner"), donot));
           }
            if(m.containsKey("home")){
                Boolean b=(Boolean) m.get("home");
                if(b){
                     List<TCityIntroduction> befs=cityRepository.findByHome(1);
                     if(befs!=null && !befs.isEmpty()){
                         TCityIntroduction bef=befs.get(0);
                         bef.setHome(0);
                         cityRepository.save(bef);
                     }
                     tra.setHome(1);
                }else tra.setHome(0);
            }
            tra.setArea(ta);
            cityRepository.save(tra);
           rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
           Map mmm=new HashMap();
           mmm.put("id", tra.getId());
           rm.put("data", mmm);
       }
       
        return add(rm);
    }

    /**
     * Read city introduction.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map city_read(Map model) {
          Map rm=new HashMap();
          Map map=(Map) model.get("data");
          String cid=(String) map.get("cid");
          TArea ta=areaRepository.findOne(cid);
          List<TCityIntroduction> list=cityRepository.findByArea(ta);
          List lst=new ArrayList();
        if(list!=null && !list.isEmpty()){    
                for(TCityIntroduction air:list){
                    Map mm=new HashMap();
                    mm.put("id", air.getId().toString());
                    mm.put("full_zh", air.getIntroduction());
                    mm.put("full_en", air.getIntroduction1());
                    mm.put("full_tw", air.getIntroduction2());
                    mm.put("area_code", air.getArea().getId());
                    mm.put("area_codename", air.getArea().getName1());
                    mm.put(("logo"), air.getLogo()==1);
                    mm.put("home", air.getHome()==1);
                    mm.put(("icon_url"), air.getViewUrl());
                    lst.add(mm);
                }
        }
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("data", lst);
        return add(rm);
    }

    /**
     * Add restaurant category.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map resc_add(Map map) {
        Map rm=new HashMap();
        Map m=(Map) map.get("data");
        TCityRestaurantCategory ta=new TCityRestaurantCategory();
        ta.setLevel("--");
        ta.setSort(0);
        ta.setCreated(new Date());
        ta.setDescription((String) m.get("full_cn"));
        ta.setDescription1((String) m.get("full_en"));
        ta.setDescription2((String) m.get("full_tw"));
        ta.setParent(Long.valueOf((String)m.get("p_id")));
        ta.setNameZh((String) m.get("text"));
        ta.setNameEn((String) m.get("name_en"));
        ta.setNameTw((String) m.get("name_tw"));
        ta.setModified(new Date());
        List lst=rescRepository.findByNameZhAndParent(ta.getNameZh(), ta.getParent());
        if(lst==null || lst.size()<1){
           rescRepository.save(ta);
           rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            Map mmm=new HashMap();
             mmm.put("id", ta.getId());
             rm.put("data", mmm);
        }else{
             rm=raiseError(2000);
             rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Delete restaurant category.
     * @param iid String
     * @return map HashMap
     */
    @Override
    public Map resc_delete(String iid) {
        Map rm=new HashMap();
        Long id=Long.valueOf(iid);
        TCityRestaurantCategory ta=rescRepository.findOne(id);
        List<TCityRestaurantCategory> datas=new ArrayList<>();
        datas.add(ta);
        if(ta.getParent()==-1){
            List lst=rescRepository.findByParent(id);    
            for(int i=0;i<lst.size();i++){
                TCityRestaurantCategory taa=(TCityRestaurantCategory) lst.get(i);
                List list=rescRepository.findByParent(taa.getId());
                datas.add(taa);
                datas.addAll(list);
            }
        }else{
            List lst=rescRepository.findByParent(id);
            datas.addAll(lst);
        }
        
        List as=resRepository.findByCategories(datas);
            if(as!=null && !as.isEmpty()){
                    rm=raiseError(2007);
                    rm.put("success", false);
                    return add(rm);
                }
        
        for(int j=0;j<datas.size();j++){
            try{
                TCityRestaurantCategory ttta=datas.get(j);
                if(ttta.getIconStorage()!=null)file_delete_on_fastdfs(ttta.getIconStorage());
                rescRepository.delete(ttta);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }catch(Exception e){ }
        }
        return add(rm);
    }

    /**
     * Read restaurant category. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map resc_read(Map map) {
        Map rm=new HashMap();
        Map kmap=(Map) map.get("data");
        String ppid=(String)kmap.get("node");
        Long pid=Long.valueOf(ppid);
        List<TCityRestaurantCategory> list=rescRepository.findByParent(pid);
        List lst=new ArrayList();
        for(TCityRestaurantCategory ta:list){
            Map m=new HashMap();
            m.put("text", ta.getNameZh());
            m.put("leaf", false);
            m.put("name_en", ta.getNameEn());
            m.put("name_tw", ta.getNameTw());
            m.put("full_cn", ta.getDescription());
            m.put("full_en", ta.getDescription1());
            m.put("full_tw", ta.getDescription2());
            m.put("id", ta.getId());
            m.put("p_id", ta.getParent());
            m.put("area_code", ta.getLevel());
            m.put("sort", ta.getSort());
            m.put("icon_url", ta.getIconUrl());
            if(ta.getCreated()!=null)m.put("c_date", WStringUtils.dateToString(ta.getCreated()));
            if(ta.getModified()!=null)m.put("m_date", WStringUtils.dateToString(ta.getModified()));
            lst.add(m);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("children", lst);
        rm.put("data", lst);
        return add(rm);
    }

    /**
     * Update restaurant category. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map resc_update(Map map) {
        Map m=(Map) map.get("data");
         Map rm=new HashMap();
         Long id=Long.valueOf((String)m.get("id"));
         TCityRestaurantCategory ta=rescRepository.findOne(id);
         if((String)m.get("text")!=null)ta.setNameZh((String) m.get("text"));
         if(m.containsKey("name_en"))ta.setNameEn((String) m.get("name_en"));
         String sss=(String) m.get("name_tw");
         if(m.containsKey("name_tw"))ta.setNameTw(sss);
         if(m.containsKey("full_en"))ta.setDescription1((String) m.get("full_en"));
         if(m.containsKey("full_cn"))ta.setDescription((String) m.get("full_cn"));
         if(m.containsKey("full_tw"))ta.setDescription2((String) m.get("full_tw"));
         if(m.containsKey("area_code"))ta.setLevel((String) m.get("area_code"));
         if(m.containsKey("sort")){
             int ss= (int) m.get("sort");
             if(ss>0)ta.setSort(ss);
         }
        
         if(m.containsKey("icon_url") && !((String) m.get("icon_url")).isEmpty())ta.setIconUrl((String) m.get("icon_url"));
         ta.setModified(new Date());
         
         List lst=rescRepository.findByNameZhAndParent(ta.getNameZh(), ta.getParent());
         if((String)m.get("text")!=null){
             if(lst.size()>0){
                 TCityRestaurantCategory ttt=(TCityRestaurantCategory) lst.get(0);
                 if(ttt.getNameZh().equals(ta.getNameZh())){
                        rm=raiseError(2000);
                        rm.put("success", false);
                 }else{
                      if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                        if(ta.getIconStorage()!=null)file_delete_on_fastdfs(ta.getIconStorage());
                        String donot=(String) m.get("idonotknow");
                        ta.setIconStorage(clientApp.get_uid((String)map.get("partner"), donot));
                    }
                      rescRepository.save(ta);
                        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                 }
            }else{
                  if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                        if(ta.getIconStorage()!=null)file_delete_on_fastdfs(ta.getIconStorage());
                        String donot=(String) m.get("idonotknow");
                        ta.setIconStorage(clientApp.get_uid((String)map.get("partner"), donot));
                    }
                rescRepository.save(ta);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
         }else{
              if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                    if(ta.getIconStorage()!=null)file_delete_on_fastdfs(ta.getIconStorage());
                    String donot=(String) m.get("idonotknow");
                    ta.setIconStorage(clientApp.get_uid((String)map.get("partner"), donot));
                }
             rescRepository.save(ta);
               rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         }
        return add(rm);
    }

    /**
     * Read restaurant category.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map rcc_read(Map map) {
        Map rm=new HashMap();
        Map kmap=(Map) map.get("data");
        String ppid=(String)kmap.get("node");
        Long pid=Long.valueOf(ppid);
        List<TCityRestaurantCategory> list=rescRepository.findByParent(pid);
        List lst=new ArrayList();
        for(TCityRestaurantCategory ta:list){
            Map m=new HashMap();
            m.put("text", ta.getNameZh());
            m.put("leaf", false);
            m.put("name_en", ta.getNameEn());
            m.put("name_tw", ta.getNameTw());
            m.put("full_cn", ta.getDescription());
            m.put("full_en", ta.getDescription1());
            m.put("full_tw", ta.getDescription2());
            m.put("checked", false);
            m.put("id", ta.getId());
            m.put("p_id", ta.getParent());
            m.put("area_code", ta.getLevel());
            m.put("sort", ta.getSort());
            m.put("icon_url", ta.getIconUrl());
            if(ta.getCreated()!=null)m.put("c_date", WStringUtils.dateToString(ta.getCreated()));
            if(ta.getModified()!=null)m.put("m_date", WStringUtils.dateToString(ta.getModified()));
            lst.add(m);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("children", lst);
        rm.put("data", lst);
        return add(rm);
    }

    /**
     * Add restaurant.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map res_add(Map map) {
        Map rm=new HashMap();
        Map m=(Map) map.get("data");
        String id=(String) m.get("area_code");
        TArea tt=areaRepository.findOne(id);
        TCityRestaurant ta=new TCityRestaurant();
        ta.setCreated(new Date());
        ta.setNameZh((String) m.get("name_zh"));
        ta.setRestaurantLevel(1);
        ta.setHasCStores(3);
        ta.setHasChineseMenu(3);
        ta.setHasGroupDinner(3);
        ta.setHasParkBus(3);
        ta.setHasRecommend(2);
        ta.setHasReservation(3);
        ta.setHasSpecialDinner(3);
        ta.setHasTakeOut(3);
        ta.setModified(new Date());
        ta.setLogo(0);
        ta.setArea(tt);
        List lst=resRepository.findByNameZhAndArea(ta.getNameZh(), tt);
        if(lst==null || lst.size()<1){
           resRepository.save(ta);
           rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            Map mmm=new HashMap();
             mmm.put("id", ta.getId());
             rm.put("data", mmm);
        }else{
             rm=raiseError(2000);
             rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Delete restaurant. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map res_delete(Map map) {
         Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        TCityRestaurant res=resRepository.findOne(id);
        Map rm=new HashMap();
        List as=repRepository.findByRestaurant(res);
        if(as!=null && !as.isEmpty()){
                    rm=raiseError(2007);
                    rm.put("success", false);
                    return add(rm);
        }
        try{
            if(res.getImageStorage()!=null){
                file_delete_on_fastdfs(res.getImageStorage());
            }
            TCityRestaurantProperty rpr=rerRepository.findByRestaurant(res);
            resRepository.delete(id);
            if(rpr!=null)rerRepository.delete(rpr);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read restaurant.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map res_read(Map model) {
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        Map map=(Map) model.get("data");
          String pid=(String) map.get("pid");
          String key=(String) map.get("key");
          Boolean rec=(Boolean) map.get("rec");
          Boolean logo=(Boolean)map.get("logo");
          int start=(int) map.get("page");
          if(start<0)return rm;
          int count=15;
          int total=0;
          if(hasText(key)){
            key="%"+key.toLowerCase()+"%";
          }else{
            key="%%";
         }
         Pageable page=new PageRequest(start-1, count);
         TArea area=areaRepository.findOne(pid);
         Page<TCityRestaurant> dt=null;
         if(area.getParent()==null){
             if(logo && rec){
                 total=resRepository.count_restaurant_by_name_logo_rec_country(key, pid);
                  if(total<(start-1)*count){
                           page=new PageRequest(0, count);
                  }
                 dt=resRepository.find_resturant_by_name_logo_rec_country(key, pid, page);
             }else if(logo && !rec){
                 total=resRepository.count_restaurant_by_name_logo_country(key, pid);
                  if(total<(start-1)*count){
                           page=new PageRequest(0, count);
                  }
                 dt=resRepository.find_resturant_by_name_logo_country(key, pid, page);
             }else if(!logo && rec){
                 total=resRepository.count_restaurant_by_name_rec_country(key, pid);
                  if(total<(start-1)*count){
                           page=new PageRequest(0, count);
                  }
                 dt=resRepository.find_resturant_by_name_rec_country(key, pid, page);
             }else{
                 total=resRepository.count_restaurant_by_name_country(key, pid);
                  if(total<(start-1)*count){
                           page=new PageRequest(0, count);
                  }
                 dt=resRepository.find_resturant_by_name_country(key, pid, page);
             }
         }
         else {
             if(logo && rec){
                 total=resRepository.count_restaurant_by_name_logo_rec_city(key, pid);
                 if(total<(start-1)*count){
                           page=new PageRequest(0, count);
                  }
                 dt=resRepository.find_resturant_by_name_logo_rec_city(key, pid, page);
             }else if(logo && !rec){
                 total=resRepository.count_restaurant_by_name_logo_city(key, pid);
                  if(total<(start-1)*count){
                           page=new PageRequest(0, count);
                  }
                 dt=resRepository.find_resturant_by_name_logo_city(key, pid, page);
             }else if(!logo && rec){
                 total=resRepository.count_restaurant_by_name_rec_city(key, pid);
                  if(total<(start-1)*count){
                           page=new PageRequest(0, count);
                  }
                 dt=resRepository.find_resturant_by_name_rec_city(key, pid, page);
             }else{
                 total=resRepository.count_restaurant_by_name_city(key, pid);
                  if(total<(start-1)*count){
                           page=new PageRequest(0, count);
                  }
                 dt=resRepository.find_resturant_by_name_city(key, pid, page);
             }
         }
         List<TCityRestaurant> list=dt.getContent();
         List lst=new ArrayList();
        for(TCityRestaurant air:list){
            Map mm=new HashMap();
            mm.put("id", air.getId().toString());
            mm.put("name_zh", air.getNameZh());
            mm.put("name_en", air.getNameEn());
            mm.put("name_tw", air.getNameTw());
            mm.put("phone_number", air.getPhone());
            mm.put("parkbus", air.isHasParkBus());
            mm.put("cstores", air.isHasCStores());
            mm.put("reservation", air.isHasReservation());
            mm.put("takeout", air.isHasTakeOut());
            mm.put("rlevel", air.getRestaurantLevel());
            mm.put("cmenu", air.isHasChineseMenu());
            mm.put("union_pay", air.isSupportUnionpay());
            mm.put("group_dinner", air.isHasGroupDinner());
            mm.put("special_menu", air.isHasSpecialDinner());
            mm.put("icon_url", air.getViewUrl());
            mm.put("head_url", air.getHeaderUrl());
            mm.put("small_url", air.getImageUrl());
            if(air.getCategories()!=null)mm.put("cat_des", air.getCategories().getNameZh());
            mm.put("recommend", (air.isHasRecommend()==1));
            mm.put("logo", (air.getLogo()==1));
            if(air.getLanguage()!=null)mm.put("language", air.getLanguage().getId().toString());
            lst.add(mm);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("data", lst);
        rm.put("total", total);
        return add(rm);
    }

    /**
     * Update restaurant. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map res_update(Map map) {
         Map m=(Map) map.get("data");
         Map rm=new HashMap();
         Long id=Long.valueOf((String)m.get("id"));
         TCityRestaurant ta=resRepository.findOne(id);
         if((String)m.get("name_zh")!=null)ta.setNameZh((String) m.get("name_zh"));
         if(m.containsKey("name_en"))ta.setNameEn((String) m.get("name_en"));
         String sss=(String) m.get("name_tw");
         if(m.containsKey("name_tw"))ta.setNameTw(sss);
         if(m.containsKey("cstores"))ta.setHasCStores((int) m.get("cstores"));
         if(m.containsKey("cmenu"))ta.setHasChineseMenu((int) m.get("cmenu"));
         if(m.containsKey("group_dinner"))ta.setHasGroupDinner((int) m.get("group_dinner"));
         if(m.containsKey("parkbus"))ta.setHasParkBus((int) m.get("parkbus"));
         if(m.containsKey("recommend")){
             boolean ff=(boolean) m.get("recommend");
             ta.setHasRecommend(ff?1:2);
             if(ta.isHasRecommend()!=1 && ff)ta.setRecommendTime(new Date());
         }
         if(m.containsKey("logo")){
             boolean logo=(boolean) m.get("logo");
             if(logo && (ta.getLogo()!=1)){
                 TCityRestaurant rest=resRepository.findByLogoAndArea(1, ta.getArea());
                 if(rest!=null){
                     rest.setLogo(0);
                     resRepository.save(rest);
                 }
             }
             ta.setLogo(logo?1:0);
         }
         if(m.containsKey("cas")){
            String cas=(String) m.get("cas");
           TCityRestaurantCategory cc=rescRepository.findOne(Long.valueOf(cas));
            ta.setCategories(cc);
         }
         if(m.containsKey("reservation"))ta.setHasReservation((int) m.get("reservation"));
         if(m.containsKey("special_menu"))ta.setHasSpecialDinner((int) m.get("special_menu"));
         if(m.containsKey("takeout"))ta.setHasTakeOut((int)m.get("takeout"));
         if(m.containsKey("icon_url") && !((String) m.get("icon_url")).isEmpty())ta.setViewUrl((String) m.get("icon_url"));
         if(m.containsKey("head_url") && !((String) m.get("head_url")).isEmpty())ta.setHeaderUrl((String) m.get("head_url"));
         if(m.containsKey("small_url") && !((String) m.get("small_url")).isEmpty())ta.setImageUrl((String) m.get("small_url"));
         if(m.containsKey("language") && (String)m.get("language")!=null){
             Long lang_id=Long.valueOf((String) m.get("language"));
             TLang tg=langRepository.findOne(lang_id);
             ta.setLanguage(tg);
         }
         if(m.containsKey("phone_number"))ta.setPhone((String) m.get("phone_number"));
         if(m.containsKey("rlevel"))ta.setRestaurantLevel((int) m.get("rlevel"));
         if(m.containsKey("union_pay"))ta.setSupportUnionpay((int) m.get("union_pay"));
         
         ta.setModified(new Date());
         
         List lst=resRepository.findByNameZhAndArea(ta.getNameZh(), ta.getArea());
         if((String)m.get("name_zh")!=null){
             if(lst.size()>0){
                 TCityRestaurant ttt=(TCityRestaurant) lst.get(0);
                 if((ttt.getNameZh().equals(ta.getNameZh()) && (ttt.getId()!=ta.getId()))){
                        rm=raiseError(2000);
                        rm.put("success", false);
                 }else{
                     if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                        if(ta.getStoragePath()!=null)file_delete_on_fastdfs(ta.getStoragePath());
                        String donot=(String) m.get("idonotknow");
                        ta.setStoragePath(clientApp.get_uid((String)map.get("partner"), donot));
                        
                    }
                     if(m.containsKey("head_path") && !((String) m.get("head_path")).isEmpty()){
                        if(ta.getHeaderPath()!=null)file_delete_on_fastdfs(ta.getHeaderPath());
                        String donot=(String) m.get("head_path");
                        ta.setHeaderPath(clientApp.get_uid((String)map.get("partner"), donot));
                     }
                     if(m.containsKey("small_path") && !((String) m.get("small_path")).isEmpty()){
                        if(ta.getImageStorage()!=null)file_delete_on_fastdfs(ta.getImageStorage());
                        String donot=(String) m.get("small_path");
                        ta.setImageStorage(clientApp.get_uid((String)map.get("partner"), donot));
                     }
                        resRepository.save(ta);
                        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                        if(ta.getCategories()!=null)rm.put("cat_des", ta.getCategories().getNameZh());
                 }
            }else{
                 if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                        if(ta.getStoragePath()!=null)file_delete_on_fastdfs(ta.getStoragePath());
                        String donot=(String) m.get("idonotknow");
                        ta.setStoragePath(clientApp.get_uid((String)map.get("partner"), donot));
                    }
                     if(m.containsKey("head_path") && !((String) m.get("head_path")).isEmpty()){
                        if(ta.getHeaderPath()!=null)file_delete_on_fastdfs(ta.getHeaderPath());
                        String donot=(String) m.get("head_path");
                        ta.setHeaderPath(clientApp.get_uid((String)map.get("partner"), donot));
                     }
                     if(m.containsKey("small_path") && !((String) m.get("small_path")).isEmpty()){
                        if(ta.getImageStorage()!=null)file_delete_on_fastdfs(ta.getImageStorage());
                         String donot=(String) m.get("small_path");
                        ta.setImageStorage(clientApp.get_uid((String)map.get("partner"), donot));
                     }
                resRepository.save(ta);
               rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
         }else{
            if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                        if(ta.getStoragePath()!=null)file_delete_on_fastdfs(ta.getStoragePath());
                        String donot=(String) m.get("idonotknow");
                        ta.setStoragePath(clientApp.get_uid((String)map.get("partner"), donot));
                    }
                     if(m.containsKey("head_path") && !((String) m.get("head_path")).isEmpty()){
                        if(ta.getHeaderPath()!=null)file_delete_on_fastdfs(ta.getHeaderPath());
                        String donot=(String) m.get("head_path");
                        ta.setHeaderPath(clientApp.get_uid((String)map.get("partner"), donot));
                     }
                     if(m.containsKey("small_path") && !((String) m.get("small_path")).isEmpty()){
                        if(ta.getImageStorage()!=null)file_delete_on_fastdfs(ta.getImageStorage());
                         String donot=(String) m.get("small_path");
                        ta.setImageStorage(clientApp.get_uid((String)map.get("partner"), donot));
                     }
             resRepository.save(ta);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         }
        return add(rm);
    }

    /**
     * Add restaurant package.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map rep_add(Map map) {
       Map m=(Map) map.get("data");
       Map rm=new HashMap();
       
       String code=(String) m.get("res_id");
       TCityRestaurant ta=resRepository.findOne(Long.valueOf(code));
       if(ta==null){
           rm=raiseError(2002);
           rm.put("success", false);
       }else{
            TCityRestaurantPackage air=new TCityRestaurantPackage();
            air.setNameZh((String) m.get("name_zh"));
            if((String)m.get("name_en")!=null)air.setNameEn((String) m.get("name_en"));
            if((String)m.get("name_tw")!=null)air.setNameTw((String) m.get("name_tw"));
            air.setReservationNumber((Integer) m.get("reservation_number"));
            air.setAdultPrice(Float.valueOf(m.get("adult_price").toString()));
            air.setChildrenPrice(Float.valueOf(m.get("children_price").toString()));
            air.setIncentives((Integer)m.get("incentives"));
            List lst=repRepository.findByNameZhAndRestaurant(air.getNameZh(),ta);
            if(lst==null || lst.size()<1){
                air.setRestaurant(ta);
                repRepository.save(air);
                 rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                  Map mmm=new HashMap();
                  mmm.put("id", air.getId());
                  rm.put("data", mmm);
            }else {
                     rm=raiseError(2000);
                     rm.put("success", false);
            }
       }
        return add(rm);
    }

    /**
     * Delete restaurant package. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map rep_delete(Map map) {
        Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        try{
            TCityRestaurantPackage rep=repRepository.findOne(id);
            List as=orsRepository.findByPackSet(rep);
            if(as!=null && !as.isEmpty()){
                    rm=raiseError(2007);
                    rm.put("success", false);
                    return add(rm);
            }
            repRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read restaurant package.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map rep_read(Map model) {
        Map rm=new HashMap();
        Map map=(Map) model.get("data");
          String cid=(String) map.get("rid");
          String key=(String) map.get("key");
          List<TCityRestaurantPackage> list=new ArrayList<>();
          if(cid==null || cid.isEmpty()){
              list=repRepository.findByNameZhContainingOrNameEnContaining(key, key);
          }else{
              TCityRestaurant ta=resRepository.findOne(Long.valueOf(cid));
              list.addAll(repRepository.findByRestaurant(ta));
          }
        
        List lst=new ArrayList();
        for(TCityRestaurantPackage air:list){
            Map mm=new HashMap();
            mm.put("id", air.getId().toString());
            mm.put("name_zh", air.getNameZh());
            mm.put("name_en", air.getNameEn());
            mm.put("name_tw", air.getNameTw());
            mm.put("res_id", air.getRestaurant().getId());
            mm.put("res_name", air.getRestaurant().getNameZh());
            mm.put("reservation_number", air.getReservationNumber());
            mm.put("adult_price", air.getAdultPrice());
            mm.put("children_price", air.getChildrenPrice());
            mm.put("incentives", air.getIncentives());
            lst.add(mm);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("data", lst);
        
        return add(rm);
    }

    /**
     * Update restaurant package. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map rep_update(Map map) {
         Map rm=new HashMap();
          Map m=(Map) map.get("data");
          TCityRestaurantPackage air=repRepository.findOne(Long.valueOf((String) m.get("id")));
          List as=orsRepository.findByPackSet(air);
            if(as!=null && !as.isEmpty()){
                    rm=raiseError(2007);
                    rm.put("success", false);
                    return add(rm);
            }
          if((String)m.get("name_zh")!=null)air.setNameZh((String) m.get("name_zh"));
            if((String)m.get("name_en")!=null)air.setNameEn((String) m.get("name_en"));
            if((String)m.get("name_tw")!=null)air.setNameTw((String) m.get("name_tw"));
            if(m.containsKey("reservation_number"))air.setReservationNumber((Integer) m.get("reservation_number"));
            if(m.containsKey("adult_price"))air.setAdultPrice(Float.valueOf(m.get("adult_price").toString()));
            if(m.containsKey("children_price"))air.setChildrenPrice(Float.valueOf(m.get("children_price").toString()));
            if(m.containsKey("incentives"))air.setIncentives((Integer)m.get("incentives"));
            
            List lst=repRepository.findByNameZhAndRestaurant(air.getNameZh(),air.getRestaurant());
            if((String)m.get("name_zh")!=null){
                if(lst.size()>0){
                    TCityRestaurantPackage ttt=(TCityRestaurantPackage) lst.get(0);
                    if(ttt.getNameZh().equals(air.getNameZh())){
                        rm=raiseError(2000);
                        rm.put("success", false);
                    }else{
                        repRepository.save(air);
                         rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                    }
                }else{
                    repRepository.save(air);
                     rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                }
            }else{
                     repRepository.save(air);
                     rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
          return add(rm);
    }

    /**
     * Add restaurant property. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map rer_add(Map map) {
       Map m=(Map) map.get("data");
       Map rm=new HashMap();
       String code=(String) m.get("res_id");
       TCityRestaurant ta=resRepository.findOne(Long.valueOf(code));
       if(ta==null){
           rm=raiseError(2002);
           rm.put("success", false);
       }else{
            TCityRestaurantProperty air=rerRepository.findByRestaurant(ta);
            if(air==null)air=new TCityRestaurantProperty();
            if((String)m.get("contact_zh")!=null)air.setContactName((String) m.get("contact_zh"));
            if((String)m.get("contact_en")!=null)air.setContactName1((String) m.get("contact_en"));
            if((String)m.get("contact_tw")!=null)air.setContactName2((String) m.get("contact_tw"));
            if(m.containsKey("prtype"))air.setType((Integer) m.get("prtype"));
            if((String)m.get("web_site")!=null)air.setWebsite((String) m.get("web_site"));
            if((String)m.get("taste_zh")!=null)air.setTaste((String) m.get("taste_zh"));
            if((String)m.get("taste_en")!=null)air.setTaste1((String) m.get("taste_en"));
            if((String)m.get("taste_tw")!=null)air.setTaste2((String) m.get("taste_tw"));
            if((String)m.get("consumptionPerPerson")!=null)air.setConsumptionPerPerson((String) m.get("consumptionPerPerson"));
            if((String)m.get("area")!=null)air.setArea((String) m.get("area"));
            if(m.containsKey("environment"))air.setEnvironment((Integer) m.get("environment"));
            if((Integer)m.get("service_attitude")!=null)air.setServiceAttitude(Integer.valueOf(m.get("service_attitude").toString()));
            if((Integer)m.get("waiting_time")!=null)air.setWaitingTimeForTable(Integer.valueOf(m.get("waiting_time").toString()));
            if((Integer)m.get("waiting_food")!=null)air.setWaitingTimeForFood(Integer.valueOf(m.get("waiting_food").toString()));
            if((Integer)m.get("distance")!=null)air.setDistanceToAttractions((Integer) m.get("distance"));
            if((Integer)m.get("parking_size")!=null)air.setParkingSize((Integer) m.get("parking_size"));
            if((Integer)m.get("surroundings")!=null)air.setSurroundings((Integer)m.get("surroundings"));
            if((Integer)m.get("has_wifi")!=null)air.setHasWifi((Integer) m.get("has_wifi"));
            if((String)m.get("circum_zh")!=null)air.setCircumjacentTrafficZh((String) m.get("circum_zh"));
            if((String)m.get("circum_en")!=null)air.setCircumjacentTrafficEn((String) m.get("circum_en"));
            if((String)m.get("circum_tw")!=null)air.setCircumjacentTrafficTw((String) m.get("circum_tw"));
            if((Integer)m.get("toilet_situation")!=null)air.setToiletSituation((Integer) m.get("toilet_situation"));
            if((String)m.get("brief_zh")!=null)air.setBriefIntroductionZh((String) m.get("brief_zh"));
            if((String)m.get("brief_en")!=null)air.setBriefIntroductionEn((String) m.get("brief_en"));
            if((String)m.get("brief_tw")!=null)air.setBriefIntroductionTw((String) m.get("brief_tw"));
            if((String)m.get("address_zh")!=null)air.setBriefIntroductionZh((String) m.get("brief_zh"));
            if((String)m.get("brief_en")!=null)air.setBriefIntroductionEn((String) m.get("brief_en"));
            if((String)m.get("brief_tw")!=null)air.setBriefIntroductionTw((String) m.get("brief_tw"));
            if((String)m.get("intro_zh")!=null)air.setIntroductionZh((String) m.get("intro_zh"));
            if((String)m.get("intro_en")!=null)air.setIntroductionEn((String) m.get("intro_en"));
            if((String)m.get("intro_tw")!=null)air.setIntroductionTw((String) m.get("intro_tw"));
           
                air.setRestaurant(ta);
                rerRepository.save(air);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
        return add(rm);
    }

    /**
     * Read restaurant property.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map repr_read(Map model) {
          Map rm=new HashMap();
          Map map=(Map) model.get("data");
          String rid=(String) map.get("rid");
          TCityRestaurant res=resRepository.findOne(Long.valueOf(rid));
          TCityRestaurantProperty pr=rerRepository.findByRestaurant(res);
          Map m=new HashMap();
          if(pr!=null){
              m.put("id", pr.getId());
              m.put("contact_zh", pr.getContactName());
              m.put("contact_en", pr.getContactName1());
              m.put("contact_tw", pr.getContactName2());
              m.put("prtype", pr.getType());
              m.put("web_site", pr.getWebsite());
              m.put("taste_zh", pr.getTaste());
              m.put("taste_en", pr.getTaste1());
              m.put("taste_tw", pr.getTaste2());
              m.put("consumptionPerPerson", pr.getConsumptionPerPerson());
              m.put("area", pr.getArea());
              m.put("environment", pr.getEnvironment());
              m.put("service_attitude", pr.getServiceAttitude());
              m.put("waiting_time", pr.getWaitingTimeForTable());
              m.put("waiting_food", pr.getWaitingTimeForFood());
              m.put("distance", pr.getDistanceToAttractions());
              m.put("parking_size", pr.getParkingSize());
              m.put("surroundings", pr.getSurroundings());
              m.put("has_wifi", pr.isHasWifi());
              m.put("circum_zh", pr.getCircumjacentTrafficZh());
              m.put("circum_en", pr.getCircumjacentTrafficEn());
              m.put("circum_tw", pr.getCircumjacentTrafficTw());
              m.put("toilet_situation", pr.getToiletSituation());
              m.put("brief_zh", pr.getBriefIntroductionZh());
              m.put("brief_en", pr.getBriefIntroductionEn());
              m.put("brief_tw", pr.getBriefIntroductionTw());
              m.put("intro_zh", pr.getIntroductionZh());
              m.put("intro_en", pr.getIntroductionEn());
              m.put("intro_tw", pr.getIntroductionTw());
          }
         
         rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         rm.put("data", m);
          return add(rm);
    }

    /**
     * Add scenic spot category. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map ssc_add(Map map) {
         Map rm=new HashMap();
        Map m=(Map) map.get("data");
        TCityScenicSpotCategory ta=new TCityScenicSpotCategory();
        ta.setLevel("--");
        ta.setSort(0);
        ta.setCreated(new Date());
        String pid=(String)m.get("p_id");
        ta.setParent(Long.valueOf(pid));
        ta.setNameZh((String) m.get("text"));
        ta.setModified(new Date());
        List lst=sscRepository.findByNameZhAndParent(ta.getNameZh(), ta.getParent());
        if(lst==null || lst.size()<1){
           sscRepository.save(ta);
           rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            Map mmm=new HashMap();
             mmm.put("id", ta.getId());
             rm.put("data", mmm);
        }else{
             rm=raiseError(2000);
             rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Delete scenic spot category.
     * @param iid String
     * @return map HashMap
     */
    @Override
    public Map ssc_delete(String iid) {
        Map rm=new HashMap();
        Long id=Long.valueOf(iid);
        TCityScenicSpotCategory ta=sscRepository.findOne(id);
        List<TCityScenicSpotCategory> datas=new ArrayList<>();
        datas.add(ta);
        if(ta.getParent()==-1){
            List lst=sscRepository.findByParent(id);    
            for(int i=0;i<lst.size();i++){
                TCityScenicSpotCategory taa=(TCityScenicSpotCategory) lst.get(i);
                List list=sscRepository.findByParent(taa.getId());
                datas.add(taa);
                datas.addAll(list);
            }
        }else{
            List lst=sscRepository.findByParent(id);
            datas.addAll(lst);
        }
        
        List as=ssRepository.findByCategories(datas);
           if(as!=null && !as.isEmpty()){
                    rm=raiseError(2007);
                    rm.put("success", false);
                    return add(rm);
            }
        
        for(int j=0;j<datas.size();j++){
            try{
                TCityScenicSpotCategory ttcc=datas.get(j);
                if(ttcc.getIconStorage()!=null)file_delete_on_fastdfs(ttcc.getIconStorage());
                sscRepository.delete(ttcc);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }catch(Exception e){ }
        }
        return add(rm);
    }
    
    /**
     * Read scenic spot category.  
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map ssc_read(Map map) {
        Map rm=new HashMap();
        Map kmap=(Map) map.get("data");
        String ppid=(String)kmap.get("node");
        Long pid=Long.valueOf(ppid);
        List<TCityScenicSpotCategory> list=sscRepository.findByParent(pid);
        List lst=new ArrayList();
        for(TCityScenicSpotCategory ta:list){
            Map m=new HashMap();
            m.put("text", ta.getNameZh());
            m.put("leaf", false);
            m.put("name_en", ta.getNameEn());
            m.put("name_tw", ta.getNameTw());
            m.put("full_cn", ta.getDescriptionZh());
            m.put("full_en", ta.getDescriptionEn());
            m.put("full_tw", ta.getDescriptionTw());
            m.put("id", ta.getId());
            m.put("p_id", ta.getParent());
            m.put("area_code", ta.getLevel());
            m.put("sort", ta.getSort());
            m.put("icon_url", ta.getIconUrl());
            if(ta.getCreated()!=null)m.put("c_date", WStringUtils.dateToString(ta.getCreated()));
            if(ta.getModified()!=null)m.put("m_date", WStringUtils.dateToString(ta.getModified()));
            lst.add(m);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("children", lst);
        rm.put("data", lst);
        
        return add(rm);
    }

    /**
     * Update scenic spot category. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map ssc_update(Map map) {
         Map m=(Map) map.get("data");
         Map rm=new HashMap();
         Long id=Long.valueOf((String)m.get("id"));
         TCityScenicSpotCategory ta=sscRepository.findOne(id);
         if((String)m.get("text")!=null)ta.setNameZh((String) m.get("text"));
         if(m.containsKey("name_en"))ta.setNameEn((String) m.get("name_en"));
         String sss=(String) m.get("name_tw");
         if(m.containsKey("name_tw"))ta.setNameTw(sss);
         if(m.containsKey("full_en"))ta.setDescriptionEn((String) m.get("full_en"));
         if(m.containsKey("full_cn"))ta.setDescriptionZh((String) m.get("full_cn"));
         if(m.containsKey("full_tw"))ta.setDescriptionTw((String) m.get("full_tw"));
         if(m.containsKey("area_code"))ta.setLevel((String) m.get("area_code"));
        
         if(m.containsKey("icon_url") && !((String) m.get("icon_url")).isEmpty())ta.setIconUrl((String) m.get("icon_url"));
         if(m.containsKey("sort")){
             int ss= Integer.valueOf((String)m.get("sort"));
             if(ss>0)ta.setSort(ss);
         }
         ta.setModified(new Date());
         
         List lst=sscRepository.findByNameZhAndParent(ta.getNameZh(), ta.getParent());
         if((String)m.get("text")!=null){
             if(lst.size()>0){
                 TCityScenicSpotCategory ttt=(TCityScenicSpotCategory) lst.get(0);
                 if(ttt.getNameZh().equals(ta.getNameZh())){
                        rm=raiseError(2000);
                        rm.put("success", false);
                 }else{
                      if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                            if(ta.getIconStorage()!=null)file_delete_on_fastdfs(ta.getIconStorage());
                            String donot=(String) m.get("idonotknow");
                            ta.setIconStorage(clientApp.get_uid((String)map.get("partner"), donot));
                        }
                      sscRepository.save(ta);
                        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                 }
            }else{
                  if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                        if(ta.getIconStorage()!=null)file_delete_on_fastdfs(ta.getIconStorage());
                            String donot=(String) m.get("idonotknow");
                            ta.setIconStorage(clientApp.get_uid((String)map.get("partner"), donot));
                    }
                sscRepository.save(ta);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
         }else{
              if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                    if(ta.getIconStorage()!=null)file_delete_on_fastdfs(ta.getIconStorage());
                        String donot=(String) m.get("idonotknow");
                        ta.setIconStorage(clientApp.get_uid((String)map.get("partner"), donot));
                }
             sscRepository.save(ta);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         }
        return add(rm);
    }

    /**
     * Read scenic spot category. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map scc_read(Map map) {
        Map rm=new HashMap();
        Map kmap=(Map) map.get("data");
        String ppid=(String)kmap.get("node");
        if(ppid.equals("--") || ppid.equals("root"))ppid="-1";
        Long pid=Long.valueOf(ppid);
        List<TCityScenicSpotCategory> list=sscRepository.findByParent(pid);
        List lst=new ArrayList();
        for(TCityScenicSpotCategory ta:list){
            Map m=new HashMap();
            m.put("text", ta.getNameZh());
            m.put("leaf", false);
            m.put("name_en", ta.getNameEn());
            m.put("name_tw", ta.getNameTw());
            m.put("full_cn", ta.getDescriptionZh());
            m.put("full_en", ta.getDescriptionEn());
            m.put("full_tw", ta.getDescriptionTw());
            m.put("id", ta.getId());
            m.put("p_id", ta.getParent());
            m.put("area_code", ta.getLevel());
            m.put("sort", ta.getSort());
            m.put("icon_url", ta.getIconUrl());
            m.put("checked", false);
            lst.add(m);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("children", lst);
        rm.put("data", lst);
        
        return add(rm);
    }

    /**
     * Add scenic spot. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map ss_add(Map map) {
        Map rm=new HashMap();
        Map m=(Map) map.get("data");
        String id=(String) m.get("area_code");
        TArea tt=areaRepository.findOne(id);
        TCityScenicSpots ta=new TCityScenicSpots();
        ta.setCreated(new Date());
        ta.setNameZh((String) m.get("name_zh"));
        ta.setHasTicketReservation(3);
        ta.setHasRecommend(2);
        ta.setHasDocentReservation(3);
        ta.setModified(new Date());
        ta.setLogo(0);
        ta.setArea(tt);
        ta.setHasDocents(0);
        List lst=ssRepository.findByNameZhAndArea(ta.getNameZh(), tt);
        if(lst==null || lst.size()<1){
           ssRepository.save(ta);
           rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            Map mmm=new HashMap();
             mmm.put("id", ta.getId());
             rm.put("data", mmm);
        }else{
             rm=raiseError(2000);
             rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Delete scenic spot. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map ss_delete(Map map) {
         Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        TCityScenicSpots ss=ssRepository.findOne(id);
        Map rm=new HashMap();
        List as=ssdRepository.findByScenic(ss);
        if(as!=null && !as.isEmpty()){
                    rm=raiseError(2007);
                    rm.put("success", false);
                    return add(rm);
        }
        try{
             if(ss.getImageStorage()!=null){
                 file_delete_on_fastdfs(ss.getImageStorage());
             }
            ssRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read scenic spot. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map ss_read(Map model) {
        Map rm=new HashMap();
        Map map=(Map) model.get("data");
          String pid=(String) map.get("pid");
          Boolean doc=(Boolean)map.get("rid");
          String key=(String) map.get("key");
          Boolean rec=(Boolean) map.get("rec");
          Boolean logo=(Boolean)map.get("logo");
          int start=(int) map.get("page");
          if(start<0)return rm;
          int count=15;
          if(hasText(key)){
            key="%"+key.toLowerCase()+"%";
          }else{
            key="%%";
         }
          int total=0;
         Pageable page=new PageRequest(start-1, count);
         TArea area=areaRepository.findOne(pid);
         Page<TCityScenicSpots> dt=null;
         if(doc){
             total=ssRepository.countByHasDocents(1);
             if(total<(start-1)*count){
                  page=new PageRequest(0, count);
             }
             dt=ssRepository.findByHasDocents(1, page);
         }else{
              if(area.getParent()==null){
                    if(logo && rec){
                        total=ssRepository.count_ss_by_name_logo_rec_country(key, pid);
                        if(total<(start-1)*count){
                            page=new PageRequest(0, count);
                        }
                        dt=ssRepository.find_ss_by_name_logo_rec_country(key, pid, page);
                    }else if(logo && !rec){
                        total=ssRepository.count_ss_by_name_logo_country(key, pid);
                        if(total<(start-1)*count){
                            page=new PageRequest(0, count);
                        }
                        dt=ssRepository.find_ss_by_name_logo_country(key, pid, page);
                    }else if(!logo && rec){
                        total=ssRepository.count_ss_by_name_rec_country(key, pid);
                        if(total<(start-1)*count){
                            page=new PageRequest(0, count);
                        }
                        dt=ssRepository.find_ss_by_name_rec_country(key, pid, page);
                    }else{
                        total=ssRepository.count_ss_by_name_country(key, pid);
                        if(total<(start-1)*count){
                            page=new PageRequest(0, count);
                        }
                        dt=ssRepository.find_ss_by_name_country(key, pid, page);
                    }
                }
                else {
                    if(logo && rec){
                        total=ssRepository.count_ss_by_name_logo_rec_city(key, pid);
                        if(total<(start-1)*count){
                            page=new PageRequest(0, count);
                        }
                        dt=ssRepository.find_ss_by_name_logo_rec_city(key, pid, page);
                    }else if(logo && !rec){
                        total=ssRepository.count_ss_by_name_logo_city(key, pid);
                        if(total<(start-1)*count){
                            page=new PageRequest(0, count);
                        }
                        dt=ssRepository.find_ss_by_name_logo_city(key, pid, page);
                    }else if(!logo && rec){
                        total=ssRepository.count_ss_by_name_rec_city(key, pid);
                        if(total<(start-1)*count){
                            page=new PageRequest(0, count);
                        }
                        dt=ssRepository.find_ss_by_name_rec_city(key, pid, page);
                    }else{
                        total=ssRepository.count_ss_by_name_city(key, pid);
                        if(total<(start-1)*count){
                            page=new PageRequest(0, count);
                        }
                        dt=ssRepository.find_ss_by_name_city(key, pid, page);
                    }
                }
         }
        
         List<TCityScenicSpots> list=dt.getContent();
        
        List lst=new ArrayList();
        for(TCityScenicSpots air:list){
            Map mm=new HashMap();
            mm.put("id", air.getId().toString());
            mm.put("name_zh", air.getNameZh());
            mm.put("name_en", air.getNameEn());
            mm.put("name_tw", air.getNameTw());
            mm.put("hasTicketReservation", air.isHasTicketReservation());
            mm.put("hasDocentReservation", air.isHasDocentReservation());
            mm.put("hasDocent", air.getHasDocents()==1);
            mm.put("icon_url", air.getImageUrl());
            mm.put("head_url", air.getHeaderUrl());
            String st_ca="";
            for(TCityScenicSpotCategory tcc:air.getCategories()){
                st_ca=st_ca+tcc.getNameZh()+" , ";
            }
            mm.put("cat_des", st_ca);
            mm.put("hasRecommend", air.isHasRecommend()==1);
            mm.put("logo", air.getLogo()==1);
            lst.add(mm);
        }
        
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("data", lst);
        rm.put("total",total);
        return add(rm);
    }

    /**
     * Update scenic spot. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map ss_update(Map map) {
        Map m=(Map) map.get("data");
         Map rm=new HashMap();
         Long id=Long.valueOf((String)m.get("id"));
         TCityScenicSpots ta=ssRepository.findOne(id);
         if((String)m.get("name_zh")!=null)ta.setNameZh((String) m.get("name_zh"));
         if(m.containsKey("name_en"))ta.setNameEn((String) m.get("name_en"));
         String sss=(String) m.get("name_tw");
         if(m.containsKey("name_tw"))ta.setNameTw(sss);
         if(m.containsKey("hasDocentReservation"))ta.setHasDocentReservation((int) m.get("hasDocentReservation"));
         if(m.containsKey("hasTicketReservation"))ta.setHasTicketReservation((int) m.get("hasTicketReservation"));
         if(m.containsKey("icon_url") && !((String) m.get("icon_url")).isEmpty())ta.setImageUrl((String) m.get("icon_url"));
        if(m.containsKey("head_url") && !((String) m.get("head_url")).isEmpty())ta.setHeaderUrl((String) m.get("head_url"));
        if(m.containsKey("hasDocent"))ta.setHasDocents((Boolean)m.get("hasDocent")?1:0);
       if(m.containsKey("hasRecommend")){
             boolean ff=(boolean) m.get("hasRecommend");
             ta.setHasRecommend(ff?1:2);
             if(ta.isHasRecommend()!=1 && ff)ta.setRecommendTime(new Date());
         }
         if(m.containsKey("logo")){
             boolean logo=(boolean) m.get("logo");
             ta.setLogo(logo?1:0);
         }
           if(m.containsKey("cas")){
            String cas=(String) m.get("cas");
            List<TCityScenicSpotCategory> clist=new ArrayList<>();
            
            String[] cass=cas.substring(1).split(",");
            for (String cas1 : cass) {
                Long cid = Long.valueOf(cas1);
                TCityScenicSpotCategory cc=sscRepository.findOne(cid);
                clist.add(cc);
            }
            ta.setCategories(clist);
         }
         ta.setModified(new Date());
         List lst=ssRepository.findByNameZhAndArea(ta.getNameZh(), ta.getArea());
         if((String)m.get("name_zh")!=null){
             if(lst.size()>0){
                 TCityScenicSpots ttt=(TCityScenicSpots) lst.get(0);
                 if((ttt.getNameZh().equals(ta.getNameZh()) && (ttt.getId()!=ta.getId()))){
                        rm=raiseError(2000);
                        rm.put("success", false);
                 }else{
                     if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                        if(ta.getImageStorage()!=null)file_delete_on_fastdfs(ta.getImageStorage());
                            String donot=(String) m.get("idonotknow");
                            ta.setImageStorage(clientApp.get_uid((String)map.get("partner"), donot));
                        
                    }
                    if(m.containsKey("head_path") && !((String) m.get("head_path")).isEmpty()){
                        if(ta.getHeaderPath()!=null)file_delete_on_fastdfs(ta.getHeaderPath());
                            String donot=(String) m.get("head_path");
                            ta.setHeaderPath(clientApp.get_uid((String)map.get("partner"), donot));
                        
                    }
                      ssRepository.save(ta);
                        String st_ca="";
                        for(TCityScenicSpotCategory tcc:ta.getCategories()){
                            st_ca=st_ca+" , "+tcc.getNameZh();
                        }
                        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                        rm.put("cat_des", st_ca.substring(2));
                 }
            }else{
                 if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                        if(ta.getImageStorage()!=null)file_delete_on_fastdfs(ta.getImageStorage());
                            String donot=(String) m.get("idonotknow");
                            ta.setImageStorage(clientApp.get_uid((String)map.get("partner"), donot));
                    }
                     if(m.containsKey("head_path") && !((String) m.get("head_path")).isEmpty()){
                        if(ta.getHeaderPath()!=null)file_delete_on_fastdfs(ta.getHeaderPath());
                            String donot=(String) m.get("head_path");
                            ta.setHeaderPath(clientApp.get_uid((String)map.get("partner"), donot));
                    }
                ssRepository.save(ta);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
         }else{
             if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                        if(ta.getImageStorage()!=null)file_delete_on_fastdfs(ta.getImageStorage());
                                String donot=(String) m.get("idonotknow");
                            ta.setImageStorage(clientApp.get_uid((String)map.get("partner"), donot));
              }
            if(m.containsKey("head_path") && !((String) m.get("head_path")).isEmpty()){
                        if(ta.getHeaderPath()!=null)file_delete_on_fastdfs(ta.getHeaderPath());
                                String donot=(String) m.get("head_path");
                            ta.setHeaderPath(clientApp.get_uid((String)map.get("partner"), donot));
            }
             ssRepository.save(ta);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         }
        return add(rm);
    }

    /**
     * Add scenic spot property. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map ssr_add(Map map) {
        Map m=(Map) map.get("data");
       Map rm=new HashMap();
       String code=(String) m.get("res_id");
       TCityScenicSpots ta=ssRepository.findOne(Long.valueOf(code));
       if(ta==null){
           rm=raiseError(2003);
           rm.put("success", false);
       }else{
            TCityScenicSpotsProperty air=ssrRepository.findByScenic(ta);
            if(air==null)air=new TCityScenicSpotsProperty();
            if((String)m.get("web_site")!=null)air.setWebsite((String) m.get("web_site"));
            if((String)m.get("openTime")!=null)air.setOpenTime((String) m.get("openTime"));
            if((String)m.get("locationZh")!=null)air.setLocationZh((String) m.get("locationZh"));
            if((String)m.get("locationEn")!=null)air.setLocationEn((String) m.get("locationEn"));
            if((String)m.get("locationTw")!=null)air.setLocationTw((String) m.get("locationTw"));
            if(m.containsKey("adultTicketPrice")){
                air.setAdultTicketPrice(Float.valueOf(m.get("adultTicketPrice").toString()));
            }
            if(m.containsKey("childrenTicketPrice"))air.setChildrenTicketPrice(Float.valueOf(m.get("childrenTicketPrice").toString()));
            if(m.containsKey("adultService"))air.setAdultService(Float.valueOf(m.get("adultService").toString()));
            if(m.containsKey("childService"))air.setChildService(Float.valueOf(m.get("childService").toString()));
            if((String)m.get("phone")!=null)air.setPhone((String) m.get("phone"));
            if((String)m.get("intro_zh")!=null)air.setIntroductionZh((String) m.get("intro_zh"));
            if((String)m.get("intro_en")!=null)air.setIntroductionEn((String) m.get("intro_en"));
            if((String)m.get("intro_tw")!=null)air.setIntroductionTw((String) m.get("intro_tw"));
                air.setScenic(ta);
                ssrRepository.save(air);
                 rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
        return add(rm);
    }

    /**
     * Read scenic spot property.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map ssr_read(Map model) {
        Map rm=new HashMap();
        Map map=(Map) model.get("data");
          String rid=(String) map.get("rid");
          TCityScenicSpots res=ssRepository.findOne(Long.valueOf(rid));
          TCityScenicSpotsProperty pr=ssrRepository.findByScenic(res);
          Map m=new HashMap();
          if(pr!=null){
              m.put("id", pr.getId());
              m.put("web_site", pr.getWebsite());
              m.put("locationZh", pr.getLocationZh());
              m.put("locationEn", pr.getLocationEn());
              m.put("locationTw", pr.getLocationTw());
              m.put("openTime", pr.getOpenTime());
              m.put("adultTicketPrice", pr.getAdultTicketPrice());
              m.put("childrenTicketPrice", pr.getChildrenTicketPrice());
              m.put("adultService", pr.getAdultService());
              m.put("childService", pr.getChildService());
              m.put("phone", pr.getPhone());
              m.put("intro_zh", pr.getIntroductionZh());
              m.put("intro_en", pr.getIntroductionEn());
              m.put("intro_tw", pr.getIntroductionTw());
          }
         
         rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         rm.put("data", m);
          return add(rm);
    }

    /**
     * Add shop category. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map shc_add(Map map) {
        Map rm=new HashMap();
        Map m=(Map) map.get("data");
        TCityShopCategory ta=new TCityShopCategory();
        ta.setLevel("--");
        ta.setSort(0);
        ta.setParent(Long.valueOf((String)m.get("p_id")));
        ta.setNameZh((String) m.get("text"));
        List lst=shcRepository.findByNameZhAndParent(ta.getNameZh(), ta.getParent());
        if(lst==null || lst.size()<1){
           shcRepository.save(ta);
           rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            Map mmm=new HashMap();
             mmm.put("id", ta.getId());
             rm.put("data", mmm);
        }else{
             rm=raiseError(2000);
             rm.put("success", false);
        }
        return add(rm);
    }

    /**
     *  Delete shop category. 
     * @param iid String
     * @return map HashMap
     */
    @Override
    public Map shc_delete(String iid) {
        Map rm=new HashMap();
        Long id=Long.valueOf(iid);
        TCityShopCategory ta=shcRepository.findOne(id);
        List<TCityShopCategory> datas=new ArrayList<>();
        datas.add(ta);
        if(ta.getParent()==-1){
            List lst=shcRepository.findByParent(id);    
            for(int i=0;i<lst.size();i++){
                TCityShopCategory taa=(TCityShopCategory) lst.get(i);
                List list=shcRepository.findByParent(taa.getId());
                datas.add(taa);
                datas.addAll(list);
            }
        }else{
            List lst=shcRepository.findByParent(id);
            datas.addAll(lst);
        }
        
        List as=shopRepository.findByCategories(datas);
        if(as!=null && !as.isEmpty()){
                    rm=raiseError(2007);
                    rm.put("success", false);
                    return add(rm);
        }
        
        for(int j=0;j<datas.size();j++){
            try{
                TCityShopCategory tsc=datas.get(j);
                if(tsc.getIconStorage()!=null)file_delete_on_fastdfs(tsc.getIconStorage());
                shcRepository.delete(tsc);
            }catch(Exception e){ 
            
            }
        }
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        return add(rm);
    }

    /**
     * Read shop category. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map shc_read(Map map) {
         Map rm=new HashMap();
        Map kmap=(Map) map.get("data");
        String ppid=(String)kmap.get("node");
        Long pid=Long.valueOf(ppid);
        List<TCityShopCategory> list=shcRepository.findByParent(pid);
        List lst=new ArrayList();
        for(TCityShopCategory ta:list){
            Map m=new HashMap();
            m.put("text", ta.getNameZh());
            m.put("leaf", false);
            m.put("name_en", ta.getNameEn());
            m.put("name_tw", ta.getNameTw());
            m.put("full_cn", ta.getDescriptionZh());
            m.put("full_en", ta.getDescriptionEn());
            m.put("full_tw", ta.getDescriptionTw());
            m.put("id", ta.getId());
            m.put("p_id", ta.getParent());
            m.put("area_code", ta.getLevel());
            m.put("sort", ta.getSort());
            m.put("icon_url", ta.getIconUrl());
            lst.add(m);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("children", lst);
        rm.put("data", lst);
        
        return add(rm);
    }

    /**
     * Update shop category. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map shc_update(Map map) {
       Map m=(Map) map.get("data");
         Map rm=new HashMap();
         Long id=Long.valueOf((String)m.get("id"));
         TCityShopCategory ta=shcRepository.findOne(id);
         if((String)m.get("text")!=null)ta.setNameZh((String) m.get("text"));
         if(m.containsKey("name_en"))ta.setNameEn((String) m.get("name_en"));
         String sss=(String) m.get("name_tw");
         if(m.containsKey("name_tw"))ta.setNameTw(sss);
         if(m.containsKey("full_en"))ta.setDescriptionEn((String) m.get("full_en"));
         if(m.containsKey("full_cn"))ta.setDescriptionZh((String) m.get("full_cn"));
         if(m.containsKey("full_tw"))ta.setDescriptionTw((String) m.get("full_tw"));
         if(m.containsKey("area_code"))ta.setLevel((String) m.get("area_code"));
         if(m.containsKey("icon_url") && !((String) m.get("icon_url")).isEmpty())ta.setIconUrl((String) m.get("icon_url"));
         
         if(m.containsKey("sort")){
             int ss= Integer.valueOf((String)m.get("sort"));
             if(ss>0)ta.setSort(ss);
         }
         
         List lst=shcRepository.findByNameZhAndParent(ta.getNameZh(), ta.getParent());
         if((String)m.get("text")!=null){
             if(lst.size()>0){
                 TCityShopCategory ttt=(TCityShopCategory) lst.get(0);
                 if(ttt.getNameZh().equals(ta.getNameZh())){
                        rm=raiseError(2000);
                        rm.put("success", false);
                 }else{
                     if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                            if(ta.getIconStorage()!=null)file_delete_on_fastdfs(ta.getIconStorage());
                                 String donot=(String) m.get("idonotknow");
                                 ta.setIconStorage(clientApp.get_uid((String)map.get("partner"), donot));
                       }
                      shcRepository.save(ta);
                        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                 }
            }else{
                 if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                        if(ta.getIconStorage()!=null)file_delete_on_fastdfs(ta.getIconStorage());
                             String donot=(String) m.get("idonotknow");
                            ta.setIconStorage(clientApp.get_uid((String)map.get("partner"), donot));
                   }
                shcRepository.save(ta);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
         }else{
             if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                    if(ta.getIconStorage()!=null)file_delete_on_fastdfs(ta.getIconStorage());
                         String donot=(String) m.get("idonotknow");
                            ta.setIconStorage(clientApp.get_uid((String)map.get("partner"), donot));
               }
             shcRepository.save(ta);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         }
        return add(rm);
    }

    /**
     * Read shop category.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map shcc_read(Map map) {
        Map rm=new HashMap();
        Map kmap=(Map) map.get("data");
        String ppid=(String)kmap.get("node");
        Long pid=Long.valueOf(ppid);
        List<TCityShopCategory> list=shcRepository.findByParent(pid);
        List lst=new ArrayList();
        for(TCityShopCategory ta:list){
            Map m=new HashMap();
            m.put("text", ta.getNameZh());
            m.put("leaf", false);
            m.put("name_en", ta.getNameEn());
            m.put("name_tw", ta.getNameTw());
            m.put("full_cn", ta.getDescriptionZh());
            m.put("full_en", ta.getDescriptionEn());
            m.put("full_tw", ta.getDescriptionTw());
            m.put("id", ta.getId());
            m.put("p_id", ta.getParent());
            m.put("area_code", ta.getLevel());
            m.put("sort", ta.getSort());
            m.put("icon_url", ta.getIconUrl());
            m.put("checked", false);
            lst.add(m);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("children", lst);
        rm.put("data", lst);
        
        return add(rm);
    }

    /**
     * Add shop. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map sh_add(Map map) {
        Map rm=new HashMap();
        Map m=(Map) map.get("data");
        String id=(String) m.get("area_code");
        TArea tt=areaRepository.findOne(id);
        TCityShop ta=new TCityShop();
        ta.setCreated(new Date());
        ta.setNameZh((String) m.get("name_zh"));
        ta.setHasParkBus(3);
        ta.setHasRecommend(2);
        ta.setSupportUnionpay(3);
        ta.setModified(new Date());
        ta.setLogo(0);
        ta.setArea(tt);
        List lst=shopRepository.findByNameZhAndArea(ta.getNameZh(), tt);
        if(lst==null || lst.size()<1){
           shopRepository.save(ta);
           rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            Map mmm=new HashMap();
             mmm.put("id", ta.getId());
             rm.put("data", mmm);
        }else{
             rm=raiseError(2000);
             rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Delete shop. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map sh_delete(Map map) {
         Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        TCityShop shop=shopRepository.findOne(id);
        Map rm=new HashMap();
        List as=oshRepository.findByShop(shop);
        if(as!=null && !as.isEmpty()){
                    rm=raiseError(2007);
                    rm.put("success", false);
                    return add(rm);
        }
        try{
            TCityShopProperty shr=shrRepository.findByShop(shop);
            if(shop.getImageStorage()!=null)file_delete_on_fastdfs(shop.getImageStorage());
            shopRepository.delete(id);
            if(shr!=null)shrRepository.delete(shr);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read shop. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map sh_read(Map model) {
        Map rm=new HashMap();
           Map map=(Map) model.get("data");
          String pid=(String) map.get("pid");
          String key=(String) map.get("key");
          Boolean rec=(Boolean)map.get("rec");
          Boolean logo=(Boolean)map.get("logo");
         int start=(int) map.get("page");
          if(start<0)return rm;
          int count=15;
          if(hasText(key)){
            key="%"+key.toLowerCase()+"%";
          }else{
            key="%%";
         }
          int total=0;
          Pageable page=new PageRequest(start-1, count);
         TArea area=areaRepository.findOne(pid);
         Page<TCityShop> dt=null;
         if(area.getParent()==null){
             if(logo && rec){
                 total=shopRepository.count_shop_by_name_logo_rec_country(key, pid);
                 if(total<(start-1)*count){
                            page=new PageRequest(0, count);
                 }
                 dt=shopRepository.find_shop_by_name_logo_rec_country(key, pid, page);
             }else if(logo && !rec){
                 total=shopRepository.count_shop_by_name_logo_country(key, pid);
                 if(total<(start-1)*count){
                            page=new PageRequest(0, count);
                 }
                 dt=shopRepository.find_shop_by_name_logo_country(key, pid, page);
             }else if(!logo && rec){
                 total=shopRepository.count_shop_by_name_rec_country(key, pid);
                 if(total<(start-1)*count){
                            page=new PageRequest(0, count);
                 }
                 dt=shopRepository.find_shop_by_name_rec_country(key, pid, page);
             }else{
                 total=shopRepository.count_shop_by_name_country(key, pid);
                 if(total<(start-1)*count){
                            page=new PageRequest(0, count);
                 }
                 dt=shopRepository.find_shop_by_name_country(key, pid, page);
             }
         }
         else {
             if(logo && rec){
                 total=shopRepository.count_shop_by_name_logo_rec_city(key, pid);
                 if(total<(start-1)*count){
                            page=new PageRequest(0, count);
                 }
                 dt=shopRepository.find_shop_by_name_logo_rec_city(key, pid, page);
             }else if(logo && !rec){
                 total=shopRepository.count_shop_by_name_logo_city(key, pid);
                  if(total<(start-1)*count){
                            page=new PageRequest(0, count);
                 }
                 dt=shopRepository.find_shop_by_name_logo_city(key, pid, page);
                 
             }else if(!logo && rec){
                 total=shopRepository.count_shop_by_name_rec_city(key, pid);
                 if(total<(start-1)*count){
                            page=new PageRequest(0, count);
                 }
                 dt=shopRepository.find_shop_by_name_rec_city(key, pid, page);
             }else{
                 total=shopRepository.count_shop_by_name_city(key, pid);
                 if(total<(start-1)*count){
                            page=new PageRequest(0, count);
                 }
                 dt=shopRepository.find_shop_by_name_city(key, pid, page);
             }
         }
         List<TCityShop> list=dt.getContent();
        
        List lst=new ArrayList();
        for(TCityShop air:list){
            Map mm=new HashMap();
            mm.put("id", air.getId().toString());
            mm.put("name_zh", air.getNameZh());
            mm.put("name_en", air.getNameEn());
            mm.put("name_tw", air.getNameTw());
            mm.put("hasParkBus", air.isHasParkBus());
            mm.put("supportUnionpay", air.isSupportUnionpay());
            mm.put("icon_url", air.getViewUrl());
            mm.put("small_url", air.getImageUrl());
            mm.put("head_url", air.getHeaderUrl());
            mm.put("phone", air.getPhone());
            String st_ca="";
            for(TCityShopCategory tcc:air.getCategories()){
                st_ca=st_ca+tcc.getNameZh()+" , ";
            }
            mm.put("cat_des", st_ca);
            mm.put("hasRecommend", air.isHasRecommend()==1);
            mm.put("logo", air.getLogo()==1);
            lst.add(mm);
        }
        
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("data", lst);
        rm.put("total", total);
        return add(rm);
    }

    /**
     * Update shop. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map sh_update(Map map) {
         Map m=(Map) map.get("data");
         Map rm=new HashMap();
         Long id=Long.valueOf((String)m.get("id"));
         TCityShop ta=shopRepository.findOne(id);
         if((String)m.get("name_zh")!=null)ta.setNameZh((String) m.get("name_zh"));
         if(m.containsKey("name_en"))ta.setNameEn((String) m.get("name_en"));
         String sss=(String) m.get("name_tw");
         if(m.containsKey("name_tw"))ta.setNameTw(sss);
         if(m.containsKey("phone"))ta.setPhone((String) m.get("phone"));
         if(m.containsKey("hasParkBus"))ta.setHasParkBus((int) m.get("hasParkBus"));
         if(m.containsKey("supportUnionpay"))ta.setSupportUnionpay((int) m.get("supportUnionpay"));
         if(m.containsKey("icon_url") && !((String) m.get("icon_url")).isEmpty())ta.setViewUrl((String) m.get("icon_url"));
         if(m.containsKey("head_url") && !((String) m.get("head_url")).isEmpty())ta.setHeaderUrl((String) m.get("head_url"));
         if(m.containsKey("small_url") && !((String) m.get("small_url")).isEmpty())ta.setImageUrl((String) m.get("small_url"));
        if(m.containsKey("hasRecommend")){
             boolean ff=(boolean) m.get("hasRecommend");
             ta.setHasRecommend(ff?1:2);
             if(ta.isHasRecommend()!=1 && ff)ta.setRecommendTime(new Date());
         }
         if(m.containsKey("logo")){
             boolean logo=(boolean) m.get("logo");
             if(logo && (ta.getLogo()!=1)){
                 TCityShop rest=shopRepository.findByLogoAndArea(1, ta.getArea());
                 if(rest!=null){
                     rest.setLogo(0);
                     shopRepository.save(rest);
                 }
             }
             ta.setLogo(logo?1:0);
         }
         if(m.containsKey("cas")){
            String cas=(String) m.get("cas");
            List<TCityShopCategory> clist=new ArrayList<>();
            String[] cass=cas.substring(1).split(",");
            for (String cas1 : cass) {
                Long cid = Long.valueOf(cas1);
                TCityShopCategory cc=shcRepository.findOne(cid);
                clist.add(cc);
            }
            ta.setCategories(clist);
         }
         ta.setModified(new Date());
         List lst=shopRepository.findByNameZhAndArea(ta.getNameZh(), ta.getArea());
         if((String)m.get("name_zh")!=null){
             if(lst.size()>0){
                 TCityShop ttt=(TCityShop) lst.get(0);
                 if((ttt.getNameZh().equals(ta.getNameZh()) && (!Objects.equals(ttt.getId(), ta.getId())))){
                        rm=raiseError(2000);
                        rm.put("success", false);
                 }else{
                     if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                        if(ta.getStoragePath()!=null)file_delete_on_fastdfs(ta.getStoragePath());
                        String donot=(String) m.get("idonotknow");
                        ta.setStoragePath(clientApp.get_uid((String)map.get("partner"), donot));
                    }
                    if(m.containsKey("head_path") && !((String) m.get("head_path")).isEmpty()){
                        if(ta.getHeaderPath()!=null)file_delete_on_fastdfs(ta.getHeaderPath());
                        String donot=(String) m.get("head_path");
                        ta.setHeaderPath(clientApp.get_uid((String)map.get("partner"), donot));
                    }
                    if(m.containsKey("small_path") && !((String) m.get("small_path")).isEmpty()){
                        if(ta.getImageStorage()!=null)file_delete_on_fastdfs(ta.getImageStorage());
                        String donot=(String) m.get("small_path");
                        ta.setImageStorage(clientApp.get_uid((String)map.get("partner"), donot));
                        
                    }
                        shopRepository.save(ta);
                        String st_ca="";
                        for(TCityShopCategory tcc:ta.getCategories()){
                            st_ca=st_ca+" , "+tcc.getNameZh();
                        }
                        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                        rm.put("cat_des", st_ca.substring(2));
                 }
            }else{
                 if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                        if(ta.getStoragePath()!=null)file_delete_on_fastdfs(ta.getStoragePath());
                        String donot=(String) m.get("idonotknow");
                        ta.setStoragePath(clientApp.get_uid((String)map.get("partner"), donot));
                    }
                    if(m.containsKey("head_path") && !((String) m.get("head_path")).isEmpty()){
                        if(ta.getHeaderPath()!=null)file_delete_on_fastdfs(ta.getHeaderPath());
                        String donot=(String) m.get("head_path");
                        ta.setHeaderPath(clientApp.get_uid((String)map.get("partner"), donot));
                    }
                    if(m.containsKey("small_path") && !((String) m.get("small_path")).isEmpty()){
                        if(ta.getImageStorage()!=null)file_delete_on_fastdfs(ta.getImageStorage());
                        String donot=(String) m.get("small_path");
                        ta.setImageStorage(clientApp.get_uid((String)map.get("partner"), donot));
                    }
                shopRepository.save(ta);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
         }else{
               if(m.containsKey("idonotknow") && !((String) m.get("idonotknow")).isEmpty()){
                        if(ta.getStoragePath()!=null)file_delete_on_fastdfs(ta.getStoragePath());
                         String donot=(String) m.get("idonotknow");
                        ta.setStoragePath(clientApp.get_uid((String)map.get("partner"), donot));
                    }
                    if(m.containsKey("head_path") && !((String) m.get("head_path")).isEmpty()){
                        if(ta.getHeaderPath()!=null)file_delete_on_fastdfs(ta.getHeaderPath());
                         String donot=(String) m.get("head_path");
                        ta.setHeaderPath(clientApp.get_uid((String)map.get("partner"), donot));
                    }
                    if(m.containsKey("small_path") && !((String) m.get("small_path")).isEmpty()){
                        if(ta.getImageStorage()!=null)file_delete_on_fastdfs(ta.getImageStorage());
                        String donot=(String) m.get("small_path");
                        ta.setImageStorage(clientApp.get_uid((String)map.get("partner"), donot));
                    }
             shopRepository.save(ta);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         }
        return add(rm);
    }

    /**
     * Add shop property. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map shr_add(Map map) {
        Map m=(Map) map.get("data");
       Map rm=new HashMap();
       
       String code=(String) m.get("res_id");
       TCityShop ta=shopRepository.findOne(Long.valueOf(code));
       if(ta==null){
           rm=raiseError(2004);
           rm.put("success", false);
       }else{
            TCityShopProperty air=shrRepository.findByShop(ta);
            if(air==null)air=new TCityShopProperty();
            if((String)m.get("contact_zh")!=null)air.setContactNameZh((String) m.get("contact_zh"));
            if((String)m.get("contact_en")!=null)air.setContactNameEn((String) m.get("contact_en"));
            if((String)m.get("contact_tw")!=null)air.setContactNameTw((String) m.get("contact_tw"));
            if((String)m.get("web_site")!=null)air.setWebsite((String) m.get("web_site"));
            if((String)m.get("address_zh")!=null)air.setAddressZh((String) m.get("address_zh"));
            if((String)m.get("address_en")!=null)air.setAddressZh((String) m.get("address_en"));
            if((String)m.get("address_tw")!=null)air.setAddressZh((String) m.get("address_tw"));
            if((String)m.get("parkingSize")!=null)air.setParkingSize((Integer) m.get("parkingSize"));
            if((String)m.get("circum_zh")!=null)air.setCircumjacentTrafficZh((String) m.get("circum_zh"));
            if((String)m.get("circum_en")!=null)air.setCircumjacentTrafficEn((String) m.get("circum_en"));
            if((String)m.get("circum_tw")!=null)air.setCircumjacentTrafficTw((String) m.get("circum_tw"));
            if((String)m.get("public_zh")!=null)air.setPublicPraiseZh((String) m.get("public_zh"));
            if((String)m.get("public_en")!=null)air.setPublicPraiseEn((String) m.get("public_en"));
            if((String)m.get("public_tw")!=null)air.setPublicPraiseTw((String) m.get("public_tw"));
            if((String)m.get("intro_zh")!=null)air.setIntroductionZh((String) m.get("intro_zh"));
            if((String)m.get("intro_en")!=null)air.setIntroductionEn((String) m.get("intro_en"));
            if((String)m.get("intro_tw")!=null)air.setIntroductionTw((String) m.get("intro_tw"));
                air.setShop(ta);
                shrRepository.save(air);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
        return add(rm);
    }

    /**
     * Read shop property. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map shr_read(Map model) {
        Map rm=new HashMap();
        Map map=(Map) model.get("data");
          String rid=(String) map.get("rid");
          TCityShop res=shopRepository.findOne(Long.valueOf(rid));
          TCityShopProperty pr=shrRepository.findByShop(res);
          Map m=new HashMap();
          if(pr!=null){
              m.put("id", pr.getId());
              m.put("contact_zh", pr.getContactNameZh());
              m.put("contact_en", pr.getContactNameEn());
              m.put("contact_tw", pr.getContactNameTw());
              m.put("web_site", pr.getWebsite());
              m.put("address_zh", pr.getAddressZh());
              m.put("address_en", pr.getAddressEn());
              m.put("address_tw", pr.getAddressTw());
              m.put("parkingSize", pr.getParkingSize());
              m.put("circum_zh", pr.getCircumjacentTrafficZh());
              m.put("circum_en", pr.getCircumjacentTrafficEn());
              m.put("circum_tw", pr.getCircumjacentTrafficTw());
              m.put("public_zh", pr.getPublicPraiseZh());
              m.put("public_en", pr.getPublicPraiseEn());
              m.put("public_tw", pr.getPublicPraiseTw());
              m.put("intro_zh", pr.getIntroductionZh());
              m.put("intro_en", pr.getIntroductionEn());
              m.put("intro_tw", pr.getIntroductionTw());
          }
         
         rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         rm.put("data", m);
          return add(rm);
    }

    /**
     * Add scenic spot docent.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map ssd_add(Map map) {
        Map m=(Map) map.get("data");
       Map rm=new HashMap();
       
       String code=(String) m.get("res_id");
       TCityScenicSpots ta=ssRepository.findOne(Long.valueOf(code));
       if(ta==null){
           rm=raiseError(2003);
           rm.put("success", false);
       }else{
            TCityScenicSpotDocent air=new TCityScenicSpotDocent();
            air.setDocentNumber((Integer) m.get("docentNumber"));
            air.setDocentTimes((Integer) m.get("docentTimes"));
            air.setLimitedNumber((Integer) m.get("limitedNumber"));
            air.setAdultPrice(Float.valueOf(m.get("adult_price").toString()));
            air.setChildrenPrice(Float.valueOf(m.get("children_price").toString()));
            air.setApm(m.get("apm").toString());
            int dhour=(int) m.get("dhour");
            int minutes=(int) m.get("minutes");
            int seconds=0;
            String ddt=dhour+":"+minutes+":"+seconds;
              if(m.containsKey("docentTime")){
                Long dd=Long.valueOf((String)m.get("docentTime"))*1000;
                Date td=new Date();
                td.setTime(dd);
                air.setDocentTime(td);
            }
            air.setDocentItems(ddt);
            air.setReserverNumber(0);
            air.setRemainNumber(air.getLimitedNumber());
//            TCityScenicSpotDocent ssd=ssdRepository.findByScenicAndDocentTimes(ta, air.getDocentTimes());
//            if(ssd==null){
                air.setScenic(ta);
                ssdRepository.save(air);
                ta.setHasDocents(1);
                ssRepository.save(ta);
                 rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                  Map mmm=new HashMap();
                  mmm.put("id", air.getId());
                  mmm.put("dtems", air.getDocentItems());
                  rm.put("data", mmm);
//            }else {
//                   rm=raiseError(2000);
//                   rm.put("success", false);
//            }
       }
        return add(rm);
    }

    /**
     * Delete scenic spot docent. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map ssd_delete(Map map) {
        Map model=(Map) map.get("data");
        Map rm=new HashMap();
        Long id=Long.valueOf((String) model.get("id"));
        TCityScenicSpotDocent ssd=ssdRepository.findOne(id);
//        List as=doiRepository.findByDocent(ssd);
//        if(as!=null && !as.isEmpty()){
//                    rm=raiseError(2007);
//                    rm.put("success", false);
//                    return add(rm);
//        }
        TCityScenicSpots ss=ssd.getScenic();
        List lst=ssdRepository.findByScenic(ss);
        if(lst.size()==1){
            ss.setHasDocents(0);
            ssRepository.save(ss);
        }
        try{
            ssdRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read scenic spot docent. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map ssd_read(Map model) {
         Map rm=new HashMap();
            Map map=(Map) model.get("data");
          String cid=(String) map.get("cid");
          String key=(String) map.get("key");
          List<TCityScenicSpotDocent> list=new ArrayList<>();
          TCityScenicSpots ta=ssRepository.findOne(Long.valueOf(cid));
          list.addAll(ssdRepository.findByScenic(ta));
          
        List lst=new ArrayList();
        for(TCityScenicSpotDocent air:list){
            Map mm=new HashMap();
            mm.put("id", air.getId().toString());
            mm.put("docentTime", air.getDocentTime());
            mm.put("docentTimes", air.getDocentTimes());
            mm.put("limitedNumber", air.getLimitedNumber());
            mm.put("remainNumber", air.getRemainNumber());
            mm.put("reserverNumber", air.getReserverNumber());
            mm.put("flag", air.getReserverNumber()>0);
            mm.put("res_id", air.getScenic().getId());
            mm.put("res_name", air.getScenic().getNameZh());
            String ds=air.getDocentItems();
            if(ds!=null && ds.contains(":")){
                String[]dss=ds.split(":");
                mm.put("dhour", dss[0]);
                mm.put("minutes", dss[1]);
                mm.put("seconds", dss[2]);
            }
            mm.put("dtems", air.getDocentItems());
            mm.put("apm", air.getApm());
            mm.put("adult_price", air.getAdultPrice());
            mm.put("children_price", air.getChildrenPrice());
            mm.put("docentNumber", air.getDocentNumber());
            lst.add(mm);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("data", lst);
        return add(rm);
    }

    /**
     * Update scenic spot docent. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map ssd_update(Map map) {
        Map rm=new HashMap();
          Map m=(Map) map.get("data");
          TCityScenicSpotDocent air=ssdRepository.findOne(Long.valueOf((String) m.get("id")));
            if(m.containsKey("docentNumber"))air.setDocentNumber((Integer) m.get("docentNumber"));
            if(m.containsKey("docentTimes"))air.setDocentTimes((Integer) m.get("docentTimes"));
            if(m.containsKey("limitedNumber")){
                air.setLimitedNumber((Integer) m.get("limitedNumber"));
                air.setRemainNumber((Integer) m.get("limitedNumber"));
            }
            
            if(m.containsKey("apm"))air.setApm((String)m.get("apm"));
            if(m.containsKey("adult_price"))air.setAdultPrice(Float.valueOf(m.get("adult_price").toString()));
            if(m.containsKey("children_price"))air.setChildrenPrice(Float.valueOf(m.get("children_price").toString()));
            if(m.containsKey("docentTime")){
                Long dd=Long.valueOf((String)m.get("docentTime"))*1000;
                Date td=new Date();
                td.setTime(dd);
                air.setDocentTime(td);
            }
            int dhour=m.containsKey("dhour")?(int) m.get("dhour"):0;
            int minutes=m.containsKey("minutes")?(int) m.get("minutes"):0;
            int seconds=m.containsKey("seconds")?(int) m.get("seconds"):0;
            
            String ds=air.getDocentItems();
            if(ds!=null && ds.contains(":")){
                String[]dss=ds.split(":");
                if(dhour==0)dhour=Integer.valueOf(dss[0]);
                if(minutes==0)minutes=Integer.valueOf(dss[1]);
                if(seconds==0)seconds=Integer.valueOf(dss[2]);
            }
            String ddt=dhour+":"+minutes+":"+seconds;
            //air.setDocentTime(new Date());
            air.setDocentItems(ddt);
            try{
            TCityScenicSpotDocent ssd=ssdRepository.findByScenicAndDocentTimes(air.getScenic(),air.getDocentTimes());
                if(ssd==null){
                    ssdRepository.save(air);
                    rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                   Map mmm=new HashMap();
                  mmm.put("dtems", air.getDocentItems());
                  rm.put("data", mmm);
                }else{
                    if(!ssd.getId().equals(air.getId())){
                            rm=raiseError(2000);
                          rm.put("success", false);
                    }
                    else{
                      ssdRepository.save(air);
                      rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");  
                      Map mmm=new HashMap();
                     mmm.put("dtems", air.getDocentItems());
                      rm.put("data", mmm);
                    }
                }
            }catch(Exception e){
                rm=raiseError(3000);
                rm.put("success", false);
            }
          return add(rm);
    }

    /**
     * Add news. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map news_add(Map map) {
        Map rm=new HashMap();
        Map m=(Map) map.get("data");
        String id=(String) m.get("area_code");
        TArea tt=areaRepository.findOne(id);
        TNews ta=new TNews();
        ta.setCreated(new Date());
        ta.setTitle((String) m.get("name_zh"));
        if(m.containsKey("name_en"))ta.setTitle1((String) m.get("name_en"));
        if(m.containsKey("name_tw"))ta.setTitle2((String) m.get("name_tw"));
        if(m.containsKey("content_zh"))ta.setContent((String) m.get("content_zh"));
        if(m.containsKey("content_en"))ta.setContent1((String) m.get("content_en"));
        if(m.containsKey("content_tw"))ta.setContent2((String) m.get("content_tw"));
        
        ta.setIsRecommend((Boolean) m.get("isRecommend"));
        ta.setStatus((Boolean) m.get("status")?1:0);
        ta.setArea(tt);
        ta.setModified(new Date());
        ta.setPraiseNumber((Integer)m.get("praiseNumber"));
        if(ta.isIsRecommend())ta.setRecommendTime(new Date());
        if(ta.getStatus()==1)ta.setPublishTime(new Date());
        String user_id=(String) m.get("user_id");
        TUser user=userRepository.findOne(Long.valueOf(user_id));
        ta.setUser(user);
        
        List lst=newsRepository.findByTitleAndUser(ta.getTitle(),user );
        if(lst==null || lst.size()<1){
           newsRepository.save(ta);
           rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
           Map mmm=new HashMap();
           mmm.put("id", ta.getId());
           rm.put("data", mmm);
        }else{
           rm=raiseError(2000);
           rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Delete news. 
     * @param map
     * @return map
     */
    @Override
    public Map news_delete(Map map) {
        Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        try{
            newsRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read news. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map news_read(Map model) {
        Map rm=new HashMap();
        Map map=(Map) model.get("data");
        String pid=(String) map.get("pid");
        String key=(String) map.get("key");
         int start=(int) map.get("page");
          if(start<0)return rm;
          int count=15;
          if(hasText(key)){
            key="%"+key.toLowerCase()+"%";
          }else{
            key="%%";
         }
          int total=0;
          Pageable page=new PageRequest(start-1, count);
         TArea area=areaRepository.findOne(pid);
         Page<TNews> dt=null;
         if(area.getParent()==null){
             dt=newsRepository.find_news_by_name_country(key, pid, page);
             total=newsRepository.count_news_by_name_country(key, pid);
         }
         else {
             dt=newsRepository.find_news_by_name_city(key, pid, page);
             total=newsRepository.count_news_by_name_city(key, pid);
         }
         List<TNews> list=dt.getContent();
        
        List lst=new ArrayList();
        for(TNews air:list){
            Map mm=new HashMap();
            mm.put("id", air.getId().toString());
            mm.put("name_zh", air.getTitle());
            mm.put("name_en", air.getTitle1());
            mm.put("name_tw", air.getTitle2());
            mm.put("content_zh", air.getContent());
            mm.put("content_en", air.getContent1());
            mm.put("content_tw", air.getContent2());
            mm.put("isRecommend", air.isIsRecommend());
            mm.put("status", air.getStatus()==1);
            mm.put("praiseNumber", air.getPraiseNumber());
            mm.put("user_name", air.getUser().getName());
            mm.put("recommendTime", WStringUtils.dateToString(air.getRecommendTime()));
            mm.put("publishTime", WStringUtils.dateToString(air.getPublishTime()));
            mm.put("created", WStringUtils.dateToString(air.getCreated()));
            mm.put("modified", WStringUtils.dateToString(air.getModified()));
            mm.put("area_code", air.getArea().getName1());
            lst.add(mm);
        }
        
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("data", lst);
        rm.put("total", total);
        return add(rm);
    }

    /**
     * Update news. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map news_update(Map map) {
        Map m=(Map) map.get("data");
         Map rm=new HashMap();
         Long id=Long.valueOf((String)m.get("id"));
         TNews ta=newsRepository.findOne(id);
         if((String)m.get("name_zh")!=null)ta.setTitle((String) m.get("name_zh"));
         if(m.containsKey("name_en"))ta.setTitle1((String) m.get("name_en"));
         String sss=(String) m.get("name_tw");
         if(m.containsKey("name_tw"))ta.setTitle2(sss);
         if(m.containsKey("content_zh"))ta.setContent((String) m.get("content_zh"));
         if(m.containsKey("content_en"))ta.setContent1((String) m.get("content_en"));
         if(m.containsKey("content_tw"))ta.setContent2((String) m.get("content_tw"));
         if(m.containsKey("isRecommend")){
             boolean b=(boolean)m.get("isRecommend");
             if(b && !ta.isIsRecommend())ta.setRecommendTime(new Date());
             if(!b && ta.isIsRecommend())ta.setRecommendTime(null);
             ta.setIsRecommend(b);
         }
         if(m.containsKey("status")){
             boolean bb=((boolean)m.get("status"));
             if(bb && ta.getStatus()==0)ta.setPublishTime(new Date());
             if(!bb && ta.getStatus()==1)ta.setPublishTime(null);
             ta.setStatus(bb?1:0);
         }
         if(m.containsKey("praiseNumber"))ta.setPraiseNumber(((Integer) m.get("praiseNumber")));
         ta.setModified(new Date());
         List lst=newsRepository.findByTitleAndUser(ta.getTitle(), ta.getUser());
         if((String)m.get("name_zh")!=null){
             if(lst.size()>0){
                 TNews ttt=(TNews) lst.get(0);
                 if((ttt.getTitle().equals(ta.getTitle()) && (ttt.getId()!=ta.getId()))){
                        rm=raiseError(2000);
                        rm.put("success", false);
                 }else{
                        newsRepository.save(ta);
                        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                 }
            }else{
                newsRepository.save(ta);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
            }
         }else{
             newsRepository.save(ta);
             rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         }
        return add(rm);
    }

    /**
     * Add excel-area. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map locale_excel(Map model) {
        Map rm=new HashMap();
        Map map=(Map) model.get("data");
        List list=(List) map.get("data");
        StringBuilder sb=new StringBuilder();
        int ok=0;
        for(int i=0;i<list.size();i++){
                    try{
                    Map m=(Map) list.get(i);
                    String name_zh=(String) m.get("name_zh");
                    String parent=(String) m.get("parent");
                    if(name_zh==null || parent==null){
                        sb.append("区域名称 或 父母 不存在。"+"---------------"+String.valueOf(i+1)+"行");
                        sb.append("\n");
                        ok++;
                        continue;
                    }
                    List<TArea> as=areaRepository.findByName1(name_zh);
                    if(as!=null && as.size()>0){
                        sb.append("相同名称已存在。"+"---------------"+String.valueOf(i+1)+"行");
                        sb.append("\n");
                        ok++;
                        continue;
                    }
                    TArea area=new TArea(UUID.randomUUID().toString());
                    if(parent.equals("--") || parent.equals("--")){
                        area.setLevel(1);
                        area.setParent(null);
                    }
                    else{
                        List<TArea>  plist=areaRepository.findByName1(parent);
                        if(plist==null || plist.isEmpty()){
                            sb.append("区域名称 或 父母 不存在。"+"---------------"+String.valueOf(i+1)+"行");
                            continue;
                        }
                        area.setParent(plist.get(0));
                        if(area.getParent().getParent()==null)area.setLevel(2);
                        else area.setLevel(3);
                    }
                     area.setName1(name_zh);
                    if(m.containsKey("name_en"))area.setName2((String) m.get("name_en"));
                    if(m.containsKey("name_tw"))area.setName3((String) m.get("name_tw"));
                    area.setCreated(new Date());
                    area.setModified(new Date());
                    if(m.containsKey("full_zh"))area.setFname1((String) m.get("full_zh"));
                    if(m.containsKey("full_en"))area.setFname2((String) m.get("full_en"));
                    if(m.containsKey("full_tw"))area.setFname3((String) m.get("full_tw"));

                    //Double b=(Double) m.get("area_code");
                    area.setCode((String) m.get("area_code"));
                    areaRepository.save(area);
                    }catch(Exception e){
                        sb.append("未知错误。"+"-----------"+e.getMessage()+"---------------"+String.valueOf(i+1)+"行");
                        sb.append("\n");
                    }
        }
        file_write("area",sb.toString());
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        return add(rm);
    }
    
    private void file_write(String prefix,String content){
            Calendar cal=Calendar.getInstance();
            int mo=cal.get(Calendar.MONTH)+1;
            int dd=cal.get(Calendar.DATE);
            int mm=cal.get(Calendar.MINUTE);
            int hh=cal.get(Calendar.HOUR);
            int year=cal.get(Calendar.YEAR);
            String date=year+"_"+mo+"_"+dd+"_"+hh+"_"+mm+"";
        String filepath=log_dir+prefix+"_"+date+".txt";
        File file=new File(filepath);
        PrintWriter writer;
        try {
            writer = new PrintWriter(file, "UTF-8");
            writer.println(content);
            writer.close();
        } catch (FileNotFoundException ex) {
            java.util.logging.Logger.getLogger(BaseDataProcess.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            java.util.logging.Logger.getLogger(BaseDataProcess.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    /**
     *  Add excel-airport. 
     * @param model HashMap 
     * @return map HashMap
     */
    @Override
    public Map air_excel(Map model) {
        Map map=(Map) model.get("data");
        List list=(List) map.get("data");
        Map rm=new HashMap();
        StringBuilder sb=new StringBuilder();
        for(int i=0;i<list.size();i++){
            try{
                    Map m=(Map) list.get(i);
                    String name_zh=(String) m.get("name_zh");
                    String area_code=(String) m.get("area_code");
                    if(name_zh==null || area_code==null){
                        sb.append("机场 或 城市名称 不存在。"+"---------------"+String.valueOf(i+1)+"行");
                        sb.append("\n");
                        continue;
                    }
                    List<TArea> areas=areaRepository.findByName1(area_code);
                    if(areas==null || areas.isEmpty()){
                        sb.append(" 城市名称 不存在。"+"---------------"+String.valueOf(i+1)+"行");
                        sb.append("\n");
                        continue;
                    }
                    TArea area=areas.get(0);
                    List<TAirport> as=airportRepository.findByNameZhAndArea(name_zh,area);
                    if(as!=null && as.size()>0){
                        sb.append("相同机场已存在。"+"---------------"+String.valueOf(i+1)+"行");
                        sb.append("\n");
                        continue;
                    }
                    TAirport air=new TAirport();
                    air.setNameZh(name_zh);
                    if(m.containsKey("name_en"))air.setNameEn((String) m.get("name_en"));
                    if(m.containsKey("name_tw"))air.setNameTw((String) m.get("name_tw"));
                    air.setCreated(new Date());
                    air.setModified(new Date());
                    if(m.containsKey("full_zh"))air.setDescriptionZh((String) m.get("full_zh"));
                    if(m.containsKey("full_en"))air.setDescriptionEn((String) m.get("full_en"));
                    if(m.containsKey("full_tw"))air.setDescriptionTw((String) m.get("full_tw"));
                    air.setArea(area);
                    airportRepository.save(air);
            }catch(Exception e){
                sb.append("未知错误。"+"-----------"+e.getMessage()+"---------------"+String.valueOf(i+1)+"行");
                sb.append("\n");
            }
        }
        file_write("airport",sb.toString());
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        return add(rm);
    }

    /**
     * Add excel-travelagency. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map tra_excel(Map model) {
        Map map=(Map) model.get("data");
         List list=(List) map.get("data");
        Map rm=new HashMap();
        StringBuilder sb=new StringBuilder();
        for(int i=0;i<list.size();i++){
            try{
                    Map m=(Map) list.get(i);
                    String name_zh=(String) m.get("name_zh");
                    String area_code=(String) m.get("area_code");
                    if(name_zh==null || area_code==null){
                        sb.append("旅行社名称（全称） 或 城市名称 不存在。"+"---------------"+String.valueOf(i+1)+"行");
                        sb.append("\n");
                        continue;
                    }
                    List<TArea> areas=areaRepository.findByName1(area_code);
                    if(areas==null || areas.isEmpty()){
                         sb.append("城市 不存在。"+"---------------"+String.valueOf(i+1)+"行");
                         sb.append("\n");
                         continue;
                    }
                    TArea area=areas.get(0);
                    List<TTravelAgency> as=travelRepository.findByNameZhAndArea(name_zh,area);
                    if(as!=null && as.size()>0){
                        sb.append("相同旅行社已存在。"+"---------------"+String.valueOf(i+1)+"行");
                        sb.append("\n");
                        continue;
                    }
                    TTravelAgency air=new TTravelAgency();
                    air.setNameZh(name_zh);
                    if(m.containsKey("name_en"))air.setNameEn((String) m.get("name_en"));
                    if(m.containsKey("name_tw"))air.setNameTw((String) m.get("name_tw"));
                    if(m.containsKey("full_zh"))air.setDescriptionZh((String) m.get("full_zh"));
                    if(m.containsKey("full_en"))air.setDescriptionEn((String) m.get("full_en"));
                    if(m.containsKey("full_tw"))air.setDescriptionTw((String) m.get("full_tw"));
                    if(m.containsKey("phone"))air.setPhone((String)m.get("phone"));
                    if(m.containsKey("license_number"))air.setLicense((String)m.get("license_number"));
                    if(m.containsKey("file_url"))air.setImageUrl((String)m.get("file_url"));
                    if(m.containsKey("file_surl"))air.setImageStorage((String)m.get("file_surl"));
                    air.setArea(area);
                    travelRepository.save(air);
            }catch(Exception e){
                sb.append("未知错误。"+"-----------"+e.getMessage()+"---------------"+String.valueOf(i+1)+"行");
                sb.append("\n");
            }
        }
        file_write("travelAgency",sb.toString());
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        return add(rm);
    }
    
    /**
     * Add excel-city introduction. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map city_excel(Map model) {
        Map map=(Map) model.get("data");
        List list=(List) map.get("data");
        Map rm=new HashMap();
        StringBuilder sb=new StringBuilder();
        for(int i=0;i<list.size();i++){
            try{
                    Map m=(Map) list.get(i);
                    String area_code=(String) m.get("area_code");
                    if(area_code==null){
                        sb.append("城市名称不存在。"+"---------------"+String.valueOf(i+1)+"行");
                        sb.append("\n");
                        continue;
                    }
                    List<TArea> areas=areaRepository.findByName1(area_code);
                    if(areas==null || areas.isEmpty()){
                        sb.append("城市不存在。"+"---------------"+String.valueOf(i+1)+"行");
                        sb.append("\n");
                        continue;
                    }
                    TArea area=areas.get(0);
                    List<TCityIntroduction> as=cityRepository.findByArea(area);
                    if(as!=null && as.size()>0){
                        sb.append("相同城市已存在。"+"---------------"+String.valueOf(i+1)+"行");
                        sb.append("\n");
                        continue;
                    }
                    TCityIntroduction air=new TCityIntroduction();
                    if(m.containsKey("full_zh"))air.setIntroduction((String) m.get("full_zh"));
                    if(m.containsKey("full_en"))air.setIntroduction1((String) m.get("full_en"));
                    if(m.containsKey("full_tw"))air.setIntroduction2((String) m.get("full_tw"));
                    air.setArea(area);
                    if(m.containsKey("file_url"))air.setViewUrl((String)m.get("file_url"));
                    if(m.containsKey("file_surl"))air.setStoragePath((String)m.get("file_surl"));
                    cityRepository.save(air);
            }catch(Exception e){
                   sb.append("未知错误。"+"-----------"+e.getMessage()+"---------------"+String.valueOf(i+1)+"行");
                   sb.append("\n");
            }
            
        }
        file_write("city",sb.toString());
       rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        return add(rm);
    }

    /**
     * Add excel-restaurant category. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map rc_excel(Map model) {
        Map map=(Map) model.get("data");
          List list=(List) map.get("data");
        Map rm=new HashMap();
        for(int i=0;i<list.size();i++){
            Map m=(Map) list.get(i);
            String name_zh=(String) m.get("name_zh");
            int sort=((Double) m.get("sort")).intValue();
            String level=(String) m.get("level");
            if(name_zh==null  || level==null)continue;
            long pp=-1;
            List<TCityRestaurantCategory> as=rescRepository.findByNameZhAndParent(name_zh,pp);
            if(as!=null && as.size()>0)continue;
            TCityRestaurantCategory air=new TCityRestaurantCategory();
            air.setNameZh(name_zh);
            air.setNameEn((String) m.get("name_en"));
            air.setNameTw((String) m.get("name_tw"));
            air.setDescription((String) m.get("full_zh"));
            air.setDescription1((String) m.get("full_en"));
            air.setDescription2((String) m.get("full_tw"));
            air.setIconUrl((String) m.get("file_url"));
            air.setIconStorage((String) m.get("file_surl"));
            air.setParent(pp);
            air.setSort(sort);
            air.setLevel(level);
            rescRepository.save(air);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        return add(rm);
    }

    /**
     * Add excel-scenic spot category. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map sc_excel(Map map) {
        List list=(List) map.get("data");
        Map rm=new HashMap();
        for(int i=0;i<list.size();i++){
            Map m=(Map) list.get(i);
            String name_zh=(String) m.get("name_zh");
            int sort=((Double) m.get("sort")).intValue();
            String level=(String) m.get("level");
            if(name_zh==null || level==null)continue;
            long pp=-1;
            List<TCityScenicSpotCategory> as=sscRepository.findByNameZhAndParent(name_zh,pp);
            if(as!=null && as.size()>0)continue;
            TCityScenicSpotCategory air=new TCityScenicSpotCategory();
            air.setNameZh(name_zh);
            air.setNameEn((String) m.get("name_en"));
            air.setNameTw((String) m.get("name_tw"));
            air.setDescriptionZh((String) m.get("full_zh"));
            air.setDescriptionEn((String) m.get("full_en"));
            air.setDescriptionTw((String) m.get("full_tw"));
            air.setSort(sort);
            air.setLevel(level);
            air.setParent(pp);
            air.setIconUrl((String) m.get("file_url"));
            air.setIconStorage((String) m.get("file_surl"));
            sscRepository.save(air);
        }
        return add(rm);
    }
    
    /**
     * Add excel-shop category. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map shc_excel(Map map) {
        List list=(List) map.get("data");
        Map rm=new HashMap();
        for(int i=0;i<list.size();i++){
            Map m=(Map) list.get(i);
            String name_zh=(String) m.get("name_zh");
            int sort=((Double) m.get("sort")).intValue();
            String level=(String) m.get("level");
            if(name_zh==null || sort==0 || level==null)continue;
            long pp=-1;
            List<TCityShopCategory> as=shcRepository.findByNameZhAndParent(name_zh,pp);
            if(as!=null && as.size()>0)continue;
            TCityShopCategory air=new TCityShopCategory();
            air.setNameZh(name_zh);
            air.setNameEn((String) m.get("name_en"));
            air.setNameTw((String) m.get("name_tw"));
            air.setDescriptionZh((String) m.get("full_zh"));
            air.setDescriptionEn((String) m.get("full_en"));
            air.setDescriptionTw((String) m.get("full_tw"));
            air.setIconUrl((String) m.get("file_url"));
            air.setIconStorage((String) m.get("file_surl"));
            air.setParent(pp);
            air.setSort(sort);
            air.setLevel(level);
            shcRepository.save(air);
    }
        return add(rm);
    }

    /**
     * Add excel-restaurant. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map res_excel(Map model) {
        Map map=(Map) model.get("data");
        List list=(List) map.get("data");
        Map rm=new HashMap();
        StringBuilder sb=new StringBuilder();
        for(int i=0;i<list.size();i++){
            Map m=(Map) list.get(i);
            try{
                    String name_zh=(String) m.get("name_zh");
                    String area_code=(String) m.get("area_code");
                    if(name_zh==null || area_code==null){
                        sb.append("饭店名称（全称） 或 城市名称 不存在。" + "---------------").append(String.valueOf(i+1)).append("行");
                        sb.append("\n");
                        continue;
                    }
                    List aas=areaRepository.findByName1(area_code);
                    if(aas==null || aas.size()<1){
                        sb.append("城市 不存在。" + "---------------").append(String.valueOf(i+1)).append("行");
                        sb.append("\n");
                        continue;
                    }
                    TArea area=(TArea) aas.get(0);
                    List<TCityRestaurant> as=resRepository.findByNameZhAndArea(name_zh,area);
                    if(as!=null && as.size()>0){
                        sb.append("相同饭店已存在。"+"---------------"+String.valueOf(i+1)+"行");
                        sb.append("\n");
                        continue;
                    }
                    TCityRestaurant air=new TCityRestaurant();
                    air.setNameZh(name_zh);
                    air.setNameEn((String) m.get("name_en"));
                    air.setNameTw((String) m.get("name_tw"));
                    air.setArea(area);
                    air.setPhone((String)m.get("phone_number"));
                    String pb=(String) m.get("parkbus");
                    if(pb.equals("有"))air.setHasParkBus(1);
                    else if(pb.equals("无"))air.setHasParkBus(2);
                    else air.setHasParkBus(3);
                    String cs=(String) m.get("cstores");
                    if(cs.equals("有"))air.setHasCStores(1);
                    else if(cs.equals("无"))air.setHasCStores(2);
                    else air.setHasCStores(3);
                    String rs=(String) m.get("reservation");
                    if(rs.equals("接受"))air.setHasReservation(1);
                    else if(rs.equals("不接受"))air.setHasReservation(2);
                    else air.setHasReservation(3);
                    String to=(String) m.get("takeout");
                    if(to.equals("有"))air.setHasTakeOut(1);
                    else if(to.equals("无"))air.setHasTakeOut(2);
                    else air.setHasTakeOut(3);
                    air.setCreated(new Date());
                    air.setModified(new Date());
                    String rlevel=(String) m.get("rlevel");
                    int level=4;
                    if(rlevel.equals("高级"))level=3;
                    else if(rlevel.equals("中级"))level=2;
                    else if(rlevel.equals("低级"))level=1;
                    air.setRestaurantLevel(level);
                    
                    String cm=(String) m.get("cmenu");
                    if(cm.equals("有"))air.setHasChineseMenu(1);
                    else if(cm.equals("无"))air.setHasChineseMenu(2);
                    else air.setHasChineseMenu(3);
                    String lang=(String) m.get("language");
                    List<TLang> langs=langRepository.findByNameZh(lang);
                    if(langs!=null && !lang.isEmpty())air.setLanguage(langs.get(0));
                    String up=(String) m.get("union_pay");
                    if(up.equals("支持"))air.setSupportUnionpay(1);
                    else if(up.equals("不支持"))air.setSupportUnionpay(2);
                    else air.setSupportUnionpay(3);
                    String gd=(String) m.get("group_dinner");
                    if(gd.equals("有"))air.setHasGroupDinner(1);
                    else if(gd.equals("无"))air.setHasGroupDinner(2);
                    else air.setHasGroupDinner(3);
                    String sm=(String) m.get("special_menu");
                    if(sm.equals("有"))air.setHasSpecialDinner(1);
                    else if(sm.equals("无"))air.setHasSpecialDinner(2);
                    else air.setHasSpecialDinner(3);
                   if(m.containsKey("cas")){
                        String cas=(String) m.get("cas");
                        List<TCityRestaurantCategory> cass=rescRepository.findByNameZh(cas);
                        if(cass!=null && !cass.isEmpty())air.setCategories(cass.get(0));
                    }
               if(m.containsKey("file_url")){
                    List fudfs=(List) m.get("file_url");
                    List sudfs=(List) m.get("file_surl");
                    if(fudfs!=null && !fudfs.isEmpty()){
                        air.setHeaderUrl((String)fudfs.get(0));
                        air.setHeaderPath((String)sudfs.get(0));
                        if(fudfs.size()>1)air.setViewUrl((String)fudfs.get(1));
                        if(sudfs.size()>1)air.setStoragePath((String)sudfs.get(1));
                        if(fudfs.size()>2)air.setImageUrl((String)fudfs.get(2));
                        if(sudfs.size()>2)air.setImageStorage((String)sudfs.get(2));
                    }
               }
                    resRepository.save(air);
                    TCityRestaurantProperty rep=new TCityRestaurantProperty();
                    rep.setRestaurant(air);
                    rep.setWebsite((String)m.get("web_site"));
                    rep.setContactName((String)m.get("contact_zh"));
                    rep.setContactName1((String)m.get("contact_en"));
                    rep.setContactName2((String)m.get("contact_tw"));
                    rep.setTaste((String)m.get("taste_zh"));
                    rep.setTaste1((String)m.get("taste_en"));
                    rep.setTaste2((String)m.get("taste_tw"));
                    rep.setConsumptionPerPerson((String)m.get("consumptionPerPerson"));
                    rep.setArea((String)m.get("area"));
                    String ptype=(String) m.get("prtype");
                    if(ptype.equals("特色餐"))rep.setType(2);
                    else if(ptype.equals("团餐"))rep.setType(1);
                    else rep.setType(3);
                    int en=4;
                    String env=(String) m.get("environment");
                    if(env.equals("优"))en=3;
                    else if(env.equals("中"))en=2;
                    else if(env.equals("差"))en=1;
                    rep.setEnvironment(en);
                    en=4;
                    String ser=(String) m.get("service_attitude");
                     if(ser.equals("优"))en=3;
                    else if(ser.equals("中"))en=2;
                     else if(ser.equals("差"))en=1;
                    rep.setServiceAttitude(en);
                    String wt=(String)m.get("waiting_time");
                    if(!wt.trim().isEmpty())rep.setWaitingTimeForTable(Double.valueOf(wt).intValue());
                    en=4;
                    String foo=(String) m.get("waiting_food");
                     if(foo.equals("快"))en=3;
                    else if(foo.equals("中"))en=2;
                     else if(foo.equals("慢"))en=1;
                    rep.setWaitingTimeForFood(en);
                    en=4;
                    String dis=(String) m.get("distance");
                     if(dis.equals("近"))en=3;
                    else if(dis.equals("中"))en=2;
                     else if(dis.equals("远"))en=1;
                    rep.setDistanceToAttractions(en);
                    en=4;
                    String prk=(String) m.get("parking_size");
                     if(prk.equals("多"))en=3;
                    else if(prk.equals("中"))en=2;
                     else if(prk.equals("少"))en=1;
                    rep.setParkingSize(en);
                    en=3;
                    String sur=(String) m.get("surroundings");
                     if(sur.equals("治安差"))en=2;
                     else if(sur.equals("治安好"))en=1;
                    rep.setSurroundings(en);
                    rep.setAddressZh((String)m.get("address_zh"));
                    rep.setAddressEn((String)m.get("address_en"));
                    rep.setAddressTw((String)m.get("address_tw"));
                     String hf=(String) m.get("has_wifi");
                    if(hf.equals("有"))rep.setHasWifi(1);
                    else if(hf.equals("无"))rep.setHasWifi(2);
                    else rep.setHasWifi(3);
                    rep.setCircumjacentTrafficZh((String)m.get("circum_zh"));
                    rep.setCircumjacentTrafficEn((String)m.get("circum_en"));
                    rep.setCircumjacentTrafficTw((String)m.get("circum_tw"));
                    en=4;
                    String toi=(String) m.get("toilet_situation");
                     if(toi.equals("干净"))en=3;
                    else if(toi.equals("一般"))en=2;
                     else if(toi.equals("脏"))en=1;
                    rep.setToiletSituation(en);
                    rep.setBriefIntroductionZh((String)m.get("brief_zh"));
                    rep.setBriefIntroductionEn((String)m.get("brief_en"));
                    rep.setBriefIntroductionTw((String)m.get("brief_tw"));
                    rep.setIntroductionZh((String)m.get("intro_zh"));
                    rep.setIntroductionEn((String)m.get("intro_en"));
                    rep.setIntroductionTw((String)m.get("intro_tw"));
                    rerRepository.save(rep);
            }catch(Exception e){
                    sb.append("未知错误。"+"-----------"+e.getMessage()+"---------------"+String.valueOf(i+1)+"行");
                    sb.append("\n");
           }
        }
        file_write("Restaraunt",sb.toString());
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        return add(rm);
    }

    /**
     * Add excel-scenic spot. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map ss_excel(Map model) {
        Map map=(Map) model.get("data");
        List list=(List) map.get("data");
        Map rm=new HashMap();
        StringBuilder sb=new StringBuilder();
        for(int i=0;i<list.size();i++){
            Map m=(Map) list.get(i);
            try{
                    String name_zh=(String) m.get("name_zh");
                    String area_code=(String) m.get("area_code");
                    if(name_zh==null || area_code==null){
                        sb.append("城市景点名称 或 城市名称 不存在。"+"---------------"+String.valueOf(i+1)+"行");
                        sb.append("\n");
                        continue;
                    }
                    List aas=areaRepository.findByName1(area_code);
                    if(aas==null || aas.size()<1){
                        sb.append(" 城市 不存在。"+"---------------"+String.valueOf(i+1)+"行");
                        sb.append("\n");
                        continue;
                    }
                   TArea area=(TArea) aas.get(0);
                   List<TCityScenicSpots> lss=ssRepository.findByNameZhAndArea(name_zh, area);
                   if(lss!=null && lss.size()>0){
                       sb.append("相同城市景点已存在。"+"---------------"+String.valueOf(i+1)+"行");
                       sb.append("\n");
                       continue;
                   }
                   TCityScenicSpots ss=new TCityScenicSpots();
                   ss.setArea(area);
                   ss.setNameZh(name_zh);
                    ss.setNameEn((String) m.get("name_en"));
                    ss.setNameTw((String) m.get("name_tw"));
                    String ht=(String) m.get("hasTicketReservation");
                    if(ht.equals("有"))ss.setHasTicketReservation(1);
                    else if(ht.equals("没有"))ss.setHasTicketReservation(2);
                    else ss.setHasTicketReservation(3);
                    String hd=(String) m.get("hasDocentReservation");
                    if(hd.equals("有"))ss.setHasDocentReservation(1);
                    else if(hd.equals("没有"))ss.setHasDocentReservation(2);
                    else ss.setHasDocentReservation(3);
                    ss.setCreated(new Date());
                    ss.setModified(new Date());
                     if(m.containsKey("file_url")){
                            List fudfs=(List) m.get("file_url");
                            List sudfs=(List) m.get("file_surl");
                            if(fudfs!=null && !fudfs.isEmpty()){
                                ss.setHeaderUrl((String)fudfs.get(0));
                                ss.setHeaderPath((String)sudfs.get(0));
                                if(fudfs.size()>1){
                                    ss.setImageUrl((String)fudfs.get(1));
                                    ss.setImageStorage((String)sudfs.get(1));
                                }
                            }
                       }
                     if(m.containsKey("cas")){
                        String cas=(String) m.get("cas");
                        List<TCityScenicSpotCategory> cass=sscRepository.findByNameZh(cas);
                        if(cass!=null && !cass.isEmpty())ss.setCategories(cass);
                    }
                    ssRepository.save(ss);

                    TCityScenicSpotsProperty sp=new TCityScenicSpotsProperty();
                    sp.setScenic(ss);
                    sp.setWebsite((String)m.get("web_site"));
                    sp.setIntroductionZh((String)m.get("intro_zh"));
                    sp.setIntroductionEn((String)m.get("intro_en"));
                    sp.setIntroductionTw((String)m.get("intro_tw"));
                    String at=(String)m.get("adultTicketPrice");
                    if(!at.trim().isEmpty())sp.setAdultTicketPrice(Double.valueOf(at).floatValue());
                    at=(String)m.get("childrenTicketPrice");
                    if(!at.trim().isEmpty())sp.setChildrenTicketPrice(Double.valueOf(at).floatValue());
                    at=(String)m.get("adultService");
                    if(!at.isEmpty())sp.setAdultService(Double.valueOf(at).floatValue());
                    at=(String)m.get("childService");
                    if(!at.isEmpty())sp.setChildService(Double.valueOf(at).floatValue());
                    sp.setLocationZh((String)m.get("locationZh"));
                    sp.setLocationEn((String)m.get("locationEn"));
                    sp.setLocationTw((String)m.get("locationTw"));
                    sp.setOpenTime((String)m.get("openTime"));
                    sp.setPhone((String)m.get("phone"));
                    ssrRepository.save(sp);
            }catch(Exception e){
                sb.append("未知错误。"+"-----------"+e.getMessage()+"---------------"+String.valueOf(i+1)+"行");
                sb.append("\n");
            }
        }
        file_write("ScenicSpot",sb.toString());
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        return add(rm);
    }

    /**
     * Add excel-shop.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map shop_excel(Map model) {
        Map map=(Map) model.get("data");
        List list=(List) map.get("data");
        Map rm=new HashMap();
        StringBuilder sb=new StringBuilder();
        for(int i=0;i<list.size();i++){
            Map m=(Map) list.get(i);
            try{
                    String name_zh=(String) m.get("name_zh");
                    String area_code=(String) m.get("area_code");
                    if(name_zh==null || area_code==null){
                        sb.append("城市商店名称 或 城市名称 不存在。"+"---------------"+String.valueOf(i+1)+"行");
                        sb.append("\n");
                        continue;
                    }
                    List aas=areaRepository.findByName1(area_code);
                    if(aas==null || aas.size()<1){
                        sb.append("城市 不存在。"+"---------------"+String.valueOf(i+1)+"行");
                        sb.append("\n");
                        continue;
                    }
                   TArea area=(TArea) aas.get(0);
                   List lsh=shopRepository.findByNameZhAndArea(name_zh, area);
                   if(lsh!=null && lsh.size()>0){
                       sb.append("相同城市商店已存在。"+"---------------"+String.valueOf(i+1)+"行");
                       sb.append("\n");
                       continue;
                   }
                   TCityShop shop=new TCityShop();
                   shop.setNameZh(name_zh);
                   shop.setArea(area);
                   shop.setNameEn((String) m.get("name_en"));
                   shop.setNameTw((String) m.get("name_tw"));
                   String hp=(String) m.get("hasParkBus");
                    if(hp.equals("有"))shop.setHasParkBus(1);
                    else if(hp.equals("无"))shop.setHasParkBus(2);
                    else shop.setHasParkBus(3);
                   String su=(String) m.get("supportUnionpay");
                    if(su.equals("支持"))shop.setSupportUnionpay(1);
                    else if(su.equals("不支持"))shop.setSupportUnionpay(2);
                    else shop.setSupportUnionpay(3);
                    shop.setPhone((String)m.get("phone"));
                     if(m.containsKey("file_url")){
                    List fudfs=(List) m.get("file_url");
                    List sudfs=(List) m.get("file_surl");
                    if(fudfs!=null && !fudfs.isEmpty()){
                               shop.setHeaderUrl((String)fudfs.get(0));
                                shop.setHeaderPath((String)sudfs.get(0));
                                if(fudfs.size()>1)shop.setViewUrl((String)fudfs.get(1));
                                if(sudfs.size()>1)shop.setStoragePath((String)sudfs.get(1));
                                if(fudfs.size()>2)shop.setImageUrl((String)fudfs.get(2));
                                if(sudfs.size()>2)shop.setImageStorage((String)sudfs.get(2));
                            }
                       }
                    if(m.containsKey("cas")){
                        String cas=(String) m.get("cas");
                        List<TCityShopCategory> cass=shcRepository.findByNameZh(cas);
                        if(cass!=null && !cass.isEmpty())shop.setCategories(cass);

                    }
                    shop.setOpenTime((String)m.get("openTime"));
                    shop.setCloseTime((String)m.get("closeTime"));
                    shop.setCreated(new Date());
                    shop.setModified(new Date());
                    shopRepository.save(shop);

                    TCityShopProperty pr=new TCityShopProperty();
                    pr.setShop(shop);
                    pr.setWebsite((String)m.get("web_site"));
                    pr.setContactNameZh((String)m.get("contact_zh"));
                    pr.setContactNameEn((String)m.get("contact_en"));
                    pr.setContactNameTw((String)m.get("contact_tw"));
                    int en=4;
                    String prk=(String) m.get("parking_size");
                     if(prk.equals("多"))en=3;
                    else if(prk.equals("中"))en=2;
                     else if(prk.equals("少"))en=1;
                    pr.setParkingSize(en);
                    pr.setCircumjacentTrafficZh((String)m.get("circum_zh"));
                    pr.setCircumjacentTrafficEn((String)m.get("circum_en"));
                    pr.setCircumjacentTrafficTw((String)m.get("circum_tw"));
                    pr.setAddressZh((String)m.get("address_zh"));
                    pr.setAddressEn((String)m.get("address_en"));
                    pr.setAddressTw((String)m.get("address_tw"));
                    pr.setPublicPraiseZh((String)m.get("public_zh"));
                    pr.setPublicPraiseEn((String)m.get("public_en"));
                    pr.setPublicPraiseTw((String)m.get("public_tw"));
                    pr.setIntroductionZh((String)m.get("intro_zh"));
                    pr.setIntroductionEn((String)m.get("intro_en"));
                    pr.setIntroductionTw((String)m.get("intro_tw"));
                    
                    shrRepository.save(pr);
            }catch(Exception e){
                sb.append("未知错误。"+"-----------"+e.getMessage()+"---------------"+String.valueOf(i+1)+"行");
                sb.append("\n");
            }
        }
        file_write("Shop",sb.toString());
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        return add(rm);
    }

    /**
     * Add excel-user. 
     * @param mmodel HashMap
     * @return map HashMap
     */
    @Override
    public Map user_excel(Map mmodel) {
        Map rm=new HashMap();
        Map map=(Map) mmodel.get("data");
        List users=(List) map.get("user");
           StringBuilder sb=new StringBuilder();
            Map m=(Map) users.get(0);
            TUser user=new TUser();
            String area_code=(String) m.get("area_code1");
            List<TArea> as=areaRepository.findByName1(area_code);
            if(as==null || as.isEmpty()){
                rm=createResult(RETURN_ERROR_CODE, "operation.fail", 500, "operation.fail", "false");
                sb.append("手机所在国家 不存在。"+"---------------基本信息");
                sb.append("\n");
                return add(rm);
            }
            
            user.setArea(as.get(0));
            user.setName((String)m.get("name_zh"));
            user.setNameEn((String)m.get("name_en"));
            user.setCreated(new Date());
            String ge=(String) m.get("gender");
            user.setGender(ge.equals("女")?2:1);
            user.setHeadUrl((String)m.get("file_url"));
            user.setHeadStorageUrl((String)m.get("file_surl"));
            List<TLang> tls=new ArrayList<>();
            for(int i=0;i<users.size();i++){
                Map mm=(Map) users.get(i);
                String langs=(String) mm.get("language");
                 List<TLang> lang=langRepository.findByNameZh(langs);
                 if(lang!=null && !lang.isEmpty())tls.add(lang.get(0));
                 else{
                     sb.append("语言不存在。---"+langs);
                     sb.append("\n");
                 }
            }
            String password=(String) m.get("password");
            String salt=WStringUtils.randomString(15);
            user.setSalt(salt);
            user.encryptPassword(password);
            user.setMobile(user.getArea().getCode()+"-"+(String)m.get("mobile"));
            user.setModified(new Date());
            String a_code=(String) m.get("area_code2");
            List<TArea> a_s=areaRepository.findByName1(a_code);
            String phone=(String)m.get("phone");
            if(a_s!=null && phone!=null)user.setPhone(a_s.get(0).getCode()+"-"+phone);
            else{
                sb.append("备用手机所在国家不存在。-----------------------基本信息");
                sb.append("\n");
            }
            String residen=(String) m.get("residence");
            List<TArea> rs=areaRepository.findByName1(residen);
            if(rs==null || rs.isEmpty()){
                sb.append("日常居住地城市不存在---------------基本信息");
                sb.append("\n");
            }
            user.setResidence(rs.get(0));
            String rec=(String) m.get("receivingTaskNotice");
            user.setReceivingTaskNotice(rec.equals("接收")?1:0);
            
            userRepository.save(user);
            
            TUserIdentity ui=new TUserIdentity();
            ui.setCreated(new Date());
            ui.setModified(new Date());
            ui.setAppointmentsNumber(0);
            ui.setIrregularitiesNumber(0);
            ui.setServiceNumber(0);
        
        List uids=(List) map.get("user-identity-docent");
        if(uids!=null && !uids.isEmpty()){
            try{
                    m=(Map) uids.get(0);
                    TUserIdentityDocents uid=new TUserIdentityDocents();
                    uid.setUser(user);
                    if(((String)m.get("nationality")).equals("西方人"))uid.setNationality(2);
                    else uid.setNationality(1);
                    TUserIdentityDocentsExpanded uide=new TUserIdentityDocentsExpanded();
                    if(m.containsKey("work_date")){
                         Long dd=((Long)m.get("work_date"));
                        Date td=new Date();
                        td.setTime(dd);
                        uide.setEmploymentDate(td);
                    }

                    List<TCityScenicSpots> tss=new ArrayList<>();
                    for(int o=0;o<uids.size();o++){
                        Map mm=(Map) uids.get(o);
                        String part=(String) mm.get("part");
                         List<TCityScenicSpots> sss=ssRepository.findByNameZh(part);
                         if(sss==null || sss.isEmpty()){
                             sb.append("讲解范围 不存在。"+"---------------博物馆讲解员------------"+String.valueOf(o+1)+"行");
                             sb.append("\n");
                             continue;
                         }
                         tss.add(sss.get(0));
                    }

                    uid.setSpots(tss);

                    List fudfs=new ArrayList();
                            fudfs=(List) m.get("file_url");
                    List sudfs=new ArrayList();
                            sudfs=(List) m.get("file_surl");

                    for(int k=0;k<fudfs.size();k++){
                        TUserIdentityDocentsFiles tff=new TUserIdentityDocentsFiles(UUID.randomUUID().toString());
                        tff.setUser(user);
                        tff.setFileUrl((String)fudfs.get(k));
                        tff.setStoragePath((String)sudfs.get(k));
                        udfRepository.save(tff);
                    }

                    String ss=(String) m.get("organization");
                        TUserOrganization org=orgRepository.findByName(ss);
                        if(org==null){
                            org=new TUserOrganization();
                            org.setName(ss);
                            org.setCreated(new Date());
                            org.setModified(new Date());
                            orgRepository.save(org);
                        }
                     uide.setOrganization(org);
                     uide.setReferee((String)m.get("refer"));

                    udRepository.save(uid);
                    uide.setIdentity(uid);
                    udeRepository.save(uide);
                    ui.setHasIdentityDocents(true);
            }catch(Exception e){
                sb.append("未知错误。"+"-----------博物馆讲解员---------------"+e.getMessage());
                sb.append("\n");
                ui.setHasIdentityDocents(false);
            }
       }else{
            ui.setHasIdentityDocents(false);
            }
        
       List udds=(List) map.get("user-identity-driver");
       if(udds!=null && !udds.isEmpty()){
           try{
                m=(Map) udds.get(0);
                 TUserIdentityDriver udd=new TUserIdentityDriver();
                 udd.setUser(user);
                 udd.setCreated(new Date());
                 udd.setModified(new Date());
                 udd.setSupportSingleCountry(false);
                 udd.setSupportMultipleCountry(false);
                 udd.setSupportSightseeing(false);
                 for(int b=0;b<udds.size();b++){
                     String work_range=(String) m.get("work_range");
                     if(work_range.equals("单国多日"))udd.setSupportSingleCountry(true);
                     if(work_range.equals("城市观光"))udd.setSupportSightseeing(true);
                     if(work_range.equals("跨国多日"))udd.setSupportMultipleCountry(true);
            }
                 String model=(String) m.get("models");
                 if(model!=null){
                     TCarModels models=modelRepository.findByModel(model);
                     if(models!=null){
                         udd.setModels(models);
                     }else{
                         models=new TCarModels();
                         List mlist=modelRepository.findAll(new Sort(Sort.Direction.DESC, "id"));
                         if(mlist!=null && !mlist.isEmpty()){
                             TCarModels mms=(TCarModels) mlist.get(0);
                             models.setId(mms.getId()+1);
                         }else models.setId(1);
                         models.setModel(model);
                         modelRepository.save(models);
                     }
                 }
                 udd.setIsCarOwner(((String) m.get("isCarOwner")).equals("有"));
                 udd.setHasConditioning(((String) m.get("hasCondition")).equals("有"));
                 String v_con=(String) m.get("vehicle_condition");
                 if(v_con!=null){
                     if(v_con.equals("普通"))udd.setVehicleCondition(1);
                     else if(v_con.equals("舒适"))udd.setVehicleCondition(2);
                     else udd.setVehicleCondition(3);
                 }else{
                     udd.setVehicleCondition(1);
                 }
                 udd.setHasLicenses(((String) m.get("hasLicenses")).equals("有"));
                 String aff=(String) m.get("affiliated");
                 if(aff!=null && aff.equals("个人"))udd.setAffiliated(2);
                 else udd.setAffiliated(1);
                 String urg=(String) m.get("use_range");
                 if(urg.equals("观光"))udd.setSupportService(false);
                 else udd.setSupportService(true);
                String guo=(String) m.get("guo");
                 List<TArea> garea=areaRepository.findByName1(guo);
                 udd.setArea(garea.get(0));
                 udrRepository.save(udd);

                 TUserIdentityDriverExpanded udde=new TUserIdentityDriverExpanded();
                 udde.setIdentity(udd);

                 if(m.containsKey("driving_time")){
                      Long dd=((Long)m.get("driving_time"));
                     Date td=new Date();
                     td.setTime(dd);
                     udde.setDrivingTime(td);
                 }
                 if(m.containsKey("employment_date")){
                      Long dd=((Long)m.get("employment_date"));
                     Date td=new Date();
                     td.setTime(dd);
                     udde.setEmploymentDate(td);
                 }
                 if(m.containsKey("vehicle_purchase_date")){
                      Long dd=((Long)m.get("vehicle_purchase_date"));
                     Date td=new Date();
                     td.setTime(dd);
                     udde.setVehiclePurchaseDate(td);
                 }
                  String ss=(String) m.get("organization");
                     TUserOrganization org=orgRepository.findByName(ss);
                     if(org==null){
                         org=new TUserOrganization();
                         org.setName(ss);
                         org.setCreated(new Date());
                         org.setModified(new Date());
                         orgRepository.save(org);
                    }  
                 udde.setOrganization(org);
                 udde.setReferee((String)m.get("refer"));
                 
                 if(m.containsKey("file_url1")){
                     TUserIdentityDriverFiles files=new TUserIdentityDriverFiles(UUID.randomUUID().toString());
                     files.setFileUrl((String)m.get("file_url1"));
                     files.setStoragePath((String)m.get("file_surl1"));
                     files.setFlag(0);
                     files.setUser(user);
                     uddfRepository.save(files);
                     udde.setHasPicture1(true);
                 }else udde.setHasPicture1(false);
                 if(m.containsKey("file_url2")){
                     TUserIdentityDriverFiles files=new TUserIdentityDriverFiles(UUID.randomUUID().toString());
                     files.setFileUrl((String)m.get("file_url2"));
                     files.setStoragePath((String)m.get("file_surl2"));
                     files.setFlag(0);
                     files.setUser(user);
                     uddfRepository.save(files);
                     udde.setHasPicture2(true);
                 }else udde.setHasPicture2(false);
                 if(m.containsKey("file_url3")){
                     TUserIdentityDriverFiles files=new TUserIdentityDriverFiles(UUID.randomUUID().toString());
                     files.setFileUrl((String)m.get("file_url3"));
                     files.setStoragePath((String)m.get("file_surl3"));
                     files.setFlag(0);
                     files.setUser(user);
                     uddfRepository.save(files);
                     udde.setHasPicture3(true);
                 }else udde.setHasPicture3(false);
                 if(m.containsKey("file_url4")){
                     TUserIdentityDriverFiles files=new TUserIdentityDriverFiles(UUID.randomUUID().toString());
                     files.setFileUrl((String)m.get("file_url4"));
                     files.setStoragePath((String)m.get("file_surl4"));
                     files.setFlag(0);
                     files.setUser(user);
                     uddfRepository.save(files);
                     udde.setHasPicture4(true);
                 }else udde.setHasPicture4(false);
                 udde.setVehicleBrand((String)m.get("brand"));

                 uddeRepository.save(udde);
                 ui.setHasIdentityDriver(true);
           }catch(Exception e){
               ui.setHasIdentityDriver(false);
               sb.append("未知错误。"+"-----------司机兼导游---------------"+e.getMessage());
               sb.append("\n");
           }
       }else{
           ui.setHasIdentityDriver(false);
       }
        List ulgs=(List) map.get("user-identity-Lcguide");
        if(ulgs!=null && !ulgs.isEmpty()){
            try{
                m=(Map) ulgs.get(0);
                TUserIdentityLCGuide ulg=new TUserIdentityLCGuide();
                ulg.setUser(user);
                ulg.setHasGuideCertificate(((String) m.get("hasGuideCertificate")).equals("有")?1:0);
                userLocalRepository.save(ulg);
                
                 List fudfs=(List) m.get("file_url");
                 List sudfs=(List) m.get("file_surl");
                 for(int k=0;k<fudfs.size();k++){
                        TUserIdentityLCGuideFiles tff=new TUserIdentityLCGuideFiles(UUID.randomUUID().toString());
                        tff.setUser(user);
                        tff.setFileUrl((String)fudfs.get(k));
                        tff.setStoragePath((String)sudfs.get(k));
                        ulfRepository.save(tff);
                 }

                TUserIdentityLCGuideExpanded ule=new TUserIdentityLCGuideExpanded();
                  if(m.containsKey("employmentDate")){
                        Long dd=((Long)m.get("employmentDate"));
                       Date td=new Date();
                       td.setTime(dd);
                       ule.setEmploymentDate(td);
                   }
                
                String ss=(String) m.get("organization");
                TUserOrganization org=orgRepository.findByName(ss);
                if(org==null){
                    org=new TUserOrganization();
                    org.setName(ss);
                    org.setCreated(new Date());
                    org.setModified(new Date());
                    orgRepository.save(org);
                }
                ule.setOrganization(org);
                ule.setReferee((String)m.get("refer"));
                ule.setIdentity(ulg);
                uleRepository.save(ule);
                ui.setHasIdentityLocalGuide(true);
            }catch(Exception e){
                sb.append("未知错误。"+"-----------城市地陪---------------"+e.getMessage());
                sb.append("\n");
                ui.setHasIdentityLocalGuide(false);
            }
        }else{
            ui.setHasIdentityLocalGuide(false);
        }
        
        List ugs=(List) map.get("user-identity-Long-Guide");
        if(ugs!=null && !ugs.isEmpty()){
            try{
                    m=(Map) ugs.get(0);
                    TUserIdentityLongGuide ug=new TUserIdentityLongGuide();
                    ug.setUser(user);
                    ug.setCreated(new Date());
                    ug.setModified(new Date());
                    List<TTourismLineCategory> categories=new ArrayList<>();
                    ug.setSupportTourAssemble(0);
                    ug.setSupportTourIndependent(0);
                    ug.setSupportTourVip(0);
                    for(int p=0;p<ugs.size();p++){
                        Map mm=(Map) ugs.get(p);
                        String part=(String) mm.get("part");
                        if(part !=null && part.equals("散拼"))ug.setSupportTourAssemble(1);
                        if(part !=null && part.equals("单团"))ug.setSupportTourIndependent(1);
                        if(part !=null && part.equals("VIP高端"))ug.setSupportTourVip(1);
                        String cas=(String) mm.get("lineCategory");
                        List<TTourismLineCategory> tc=tcRepository.findByNameZh(cas);

                        if(tc==null || tc.isEmpty()){
                            sb.append("线路区域 不存在。" + "---------------境外全陪------------").append(String.valueOf(p+1)).append("行");
                            sb.append("\n");
                            continue;
                        }
                        categories.add(tc.get(0));

                    }
                    ug.setHasGuideCertificate(((String) m.get("hasGuideCertificate")).equals("有")?1:0);
                    ug.setHasLeaderCertificate(((String) m.get("hasLeaderCertificate")).equals("有")?1:0);
                    String tra=(String) m.get("travelAgency");
                    List<TTravelAgency> travel=travelRepository.findByNameZh(tra);
                    ug.setTravelAgency(travel.get(0));
                    userLongRepository.save(ug);

                     TUserIdentityLongGuideExpanded uge=new TUserIdentityLongGuideExpanded();
                          if(m.containsKey("employmentDate")){
                                Long dd=((Long)m.get("employmentDate"));
                               Date td=new Date();
                               td.setTime(dd);
                               uge.setEmploymentDate(td);
                           }
                     String sor=(String) m.get("organization");
                     TUserOrganization org=orgRepository.findByName(sor);
                     if(org==null){
                         org=new TUserOrganization();
                         org.setName(sor);
                         org.setCreated(new Date());
                         org.setModified(new Date());
                         orgRepository.save(org);
                     }
                    uge.setOrganization(org);
                    uge.setReferee((String)m.get("refer"));
                    String guo=(String) m.get("guo");
                    List<TArea> garea=areaRepository.findByName1(guo);
                    uge.setArea(garea.get(0));
                    String resume=(String) m.get("resume");
                    uge.setResume(resume.equals("专业")?1:2);
                    uge.setIdentity(ug);
                    ugeRepository.save(uge);


                     if(m.containsKey("file_url1")){
                          TUserIdentityLongGuideFiles ugf=new TUserIdentityLongGuideFiles(UUID.randomUUID().toString());
                          ugf.setFileUrl((String)m.get("file_url1"));
                          ugf.setStoragePath((String)m.get("file_surl1"));
                          ugf.setFlag(2);
                          ugf.setUser(user);
                          ugfRepository.save(ugf);
                     }
                     if(m.containsKey("file_url2")){
                          TUserIdentityLongGuideFiles ugf=new TUserIdentityLongGuideFiles(UUID.randomUUID().toString());
                          ugf.setFileUrl((String)m.get("file_url2"));
                          ugf.setStoragePath((String)m.get("file_surl2"));
                          ugf.setFlag(1);
                          ugf.setUser(user);
                          ugfRepository.save(ugf);
                     }
                     ui.setHasIdentityLongGuide(true);
            }catch(Exception e){
                ui.setHasIdentityLongGuide(false);
                sb.append("未知错误。"+"-----------境外全陪---------------"+e.getMessage());
                sb.append("\n");
            }
        }else{
            ui.setHasIdentityLongGuide(false);
        }
        
       List uoss=(List) map.get("user-identity-O-Guide");
       if(uoss!=null && !uoss.isEmpty()){
           try{
                m=(Map) uoss.get(0);
                TUserIdentityOGuide uos=new TUserIdentityOGuide();
                 uos.setUser(user);
                 uos.setCreated(new Date());
                 uos.setModified(new Date());
                 List<TTourismLineCategory> categories=new ArrayList<>();
                 uos.setSupportTourAssemble(0);
                 uos.setSupportTourIndependent(0);
                 uos.setSupportTourVip(0);
                 for(int s=0;s<ugs.size();s++){
                     Map mm=(Map) ugs.get(s);
                     String part=(String) mm.get("part");
                     if(part !=null && part.equals("散拼"))uos.setSupportTourAssemble(1);
                     if(part !=null && part.equals("单团"))uos.setSupportTourIndependent(1);
                     if(part !=null && part.equals("VIP高端"))uos.setSupportTourVip(1);

                     String cas=(String) mm.get("lineCategory");
                      List<TTourismLineCategory> tc=tcRepository.findByNameZh(cas);
                     if(tc==null || tc.isEmpty()){
                         sb.append("线路区域 不存在。"+"---------------全兼地------------"+String.valueOf(s+1)+"行");
                         sb.append("\n");
                         continue;
                     }
                     categories.add(tc.get(0));

                 }
                 uos.setLineCategories(categories);
                 uos.setHasEmploymentCertificate(((String) m.get("hasEmploymentCertificate")).equals("有")?1:0);
                 String nation=(String) m.get("nationality");
                 if(nation!=null && nation.equals("台湾"))uos.setNationality(3);
                 else if(nation!=null && nation.equals("外籍"))uos.setNationality(2);
                 else if(nation!=null && nation.equals("港澳"))uos.setNationality(4);
                 else uos.setNationality(1);
                 String rep=(String) m.get("residencePermit");
                 if(rep!=null && rep.equals("短期"))uos.setResidencePermit(2);
                 else if(rep!=null && rep.equals("永居"))uos.setResidencePermit(3);
                 else if(rep!=null && rep.equals("长期"))uos.setResidencePermit(4);
                 else uos.setResidencePermit(1);
                 uos.setSupportDomestic(0);
                 uos.setSupportImmigration(0);
                 String tra=(String) m.get("travelAgency");
                 List<TTravelAgency> travel=travelRepository.findByNameZh(tra);
                 uos.setTravelAgency(travel.get(0));
                 udsRepository.save(uos);

                 TUserIdentityOGuideExpanded uoe=new TUserIdentityOGuideExpanded();
                 uoe.setIdentity(uos);
                      if(m.containsKey("employmentDate")){
                             Long dd=((Long)m.get("employmentDate"));
                            Date td=new Date();
                            td.setTime(dd);
                            uoe.setEmploymentDate(td);
                        }

                 String resume=(String) m.get("resume");
                 uoe.setResume(resume.equals("专业")?1:2);
                 String sor=(String) m.get("organization");
                 TUserOrganization org=orgRepository.findByName(sor);
                 if(org==null){
                      org=new TUserOrganization();
                      org.setName(sor);
                      org.setCreated(new Date());
                      org.setModified(new Date());
                      orgRepository.save(org);
                  }
                 uoe.setOrganization(org);
                 uoe.setReferee((String)m.get("refer"));

                 uoeRepository.save(uoe);

                 List fudfs=(List) m.get("file_url");
                 List sudfs=(List) m.get("file_surl");
                 for(int k=0;k<fudfs.size();k++){
                     TUserIdentityOGuideFiles tff=new TUserIdentityOGuideFiles(UUID.randomUUID().toString());
                     tff.setUser(user);
                     tff.setFileUrl((String)fudfs.get(k));
                     tff.setStoragePath((String)sudfs.get(k));
                     uofRepository.save(tff);
                 }
                 ui.setHasIdentityOverseasLocalGuide(true);
           }catch(Exception e){
               ui.setHasIdentityOverseasLocalGuide(false);
               sb.append("未知错误。"+"-----------全兼地---------------"+e.getMessage());
               sb.append("\n");
           }
       }else ui.setHasIdentityOverseasLocalGuide(false);
      
        List uops=(List) map.get("user-identity-Op");
        if(uops!=null && !uops.isEmpty()){
            try{
                    m=(Map) uops.get(0);
                    TUserIdentityOp uop=new TUserIdentityOp();
                    uop.setUser(user);
                    uop.setCreated(new Date());
                    uop.setModified(new Date());
                    List<TTourismLineCategory> categories=new ArrayList<>();
                    uop.setSupportTourAssemble(false);
                    uop.setSupportTourIndependent(false);
                    uop.setSupportTourMice(false);
                    for(int s=0;s<ugs.size();s++){
                        Map mm=(Map) ugs.get(s);
                        String part=(String) mm.get("part");
                        if(part !=null && part.equals("散拼"))uop.setSupportTourAssemble(true);
                        if(part !=null && part.equals("单团"))uop.setSupportTourIndependent(true);
                        if(part !=null && part.equals("VIP高端"))uop.setSupportTourMice(true);

                        String cas=(String) mm.get("lineCategory");
                         List<TTourismLineCategory> tc=tcRepository.findByNameZh(cas);
                        if(tc==null || tc.isEmpty())continue;
                        categories.add(tc.get(0));

                    }
                    String stra=(String) m.get("travel");
                    String tra=(String) m.get("travelAgency");
                    List<TTravelAgency> travel=travelRepository.findByNameZh(tra);
                    uop.setTravel(travel.get(0));

                    uoRepository.save(uop);

                     List fudfs=(List) m.get("file_url");
                    List sudfs=(List) m.get("file_surl");
                    for(int k=0;k<fudfs.size();k++){
                        TUserIdentityOpExpanded tff=new TUserIdentityOpExpanded(UUID.randomUUID().toString());
                        tff.setUser(user);
                        tff.setBusinessCardUrl((String)fudfs.get(k));
                        tff.setBusinessCardStorageUrl((String)sudfs.get(k));
                        uopeRepository.save(tff);
                    }
                   ui.setHasIdentityOp(true);
            }catch(Exception e){
                ui.setHasIdentityOp(false);
                sb.append("未知错误。"+"-----------Op---------------"+e.getMessage());
            }
        }else ui.setHasIdentityOp(false);
        
         uiRepository.save(ui);
         file_write("User", sb.toString());
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
       return add(rm);
            }

    /**
     *  Read user journey. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map jou_read(Map map) {
        Map rm=new HashMap();
        Map kmap=(Map) map.get("data");
        int cid=(int) kmap.get("cid");
        String key=(String)kmap.get("key");
        int start=(int)kmap.get("page");
          if(start<0)start=1;
          int count=15;
          if(hasText(key)){
            key="%"+key.toLowerCase()+"%";
          }else{
            key="%%";
         }
          Pageable page=new PageRequest(start-1, count);
          int length=0;
          Page<TUserJourney> dt;
          if(cid==1){
              length=jouRepository.count_key_status(key, cid);
              if(length<(start-1)*count){
                    page=new PageRequest(0, count);
              }
              dt=jouRepository.find_key_status(key, cid, page);
          }else{
              length=jouRepository.countByKey(key);
              if(length<(start-1)*count){
                    page=new PageRequest(0, count);
              }
              dt=jouRepository.findByKey(key, page);
          }
         List<TUserJourney> list=dt.getContent();
         
        List lst=new ArrayList();
        for(TUserJourney ta:list){
            Map m=new HashMap();
            m.put("name_zh", ta.getNameZh());
            m.put("leaf", false);
            m.put("name_en", ta.getNameEn());
            m.put("name_tw", ta.getNameTw());
            m.put("dayNumber", ta.getDayNumber());
            m.put("countryNumber", ta.getCountryNumber());
            m.put("user_id", ta.getUser().getId());
            m.put("id", ta.getId());
            m.put("user_name", ta.getUser().getName());
            m.put("travel_name", ta.getTravelAgency().getNameZh());
            m.put("serialNumber", ta.getSerialNumber());
            m.put("totalNumber", ta.getTotalNumber());
            m.put("nature", ta.getNature());
            m.put("status", ta.getStatus());
            m.put("vehicle", ta.getVehicle());
            if(ta.getDepartureExitCity()!=null)m.put("departureExitCity", ta.getDepartureExitCity().getName1());
            if(ta.getReturnExitCity()!=null)m.put("returnExitCity", ta.getReturnExitCity().getName1());
            if(ta.getDepartureEntryCity()!=null)m.put("departureEntryCity", ta.getDepartureEntryCity().getName1());
            if(ta.getBeginTime()!=null)m.put("beginTime", ta.getBeginTime());
            if(ta.getEndTime()!=null)m.put("endTime", ta.getEndTime());
            if(ta.getReturnExitTime()!=null)m.put("returnExitTime", WStringUtils.dateToString(ta.getReturnExitTime()));
            if(ta.getDepartureEntryTime()!=null)m.put("departureEntryTime", WStringUtils.dateToString(ta.getDepartureEntryTime()));
            lst.add(m);
}
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
//        rm.put("children", lst);
        rm.put("total", length);
        rm.put("data", lst);
        return add(rm);
    }
    
    /**
     * Add user journey daily information.  
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map jda_add(Map map) {
        Map rm=new HashMap();
        try{
                Map m=(Map) map.get("data");
                String id=(String) m.get("jid");
                TUserJourney jou=jouRepository.findOne(Long.valueOf(id));
                TUserJourneyDaily jd=new TUserJourneyDaily();
                jd.setCreated(new Date());
                jd.setModified(new Date());
                if(m.containsKey("daily_title"))jd.setTitle((String) m.get("daily_title"));
                if(m.containsKey("daily_hotel"))jd.setHotel((String) m.get("daily_hotel"));
                if(m.containsKey("daily_meal"))jd.setMeal((String) m.get("daily_meal"));
                if(m.containsKey("daily_vehicle"))jd.setVehicle((String) m.get("daily_vehicle"));
                if(m.containsKey("daily_air"))jd.setAirinfo((String) m.get("daily_air"));
                if(m.containsKey("journeyDate")){
                       Long dd=Long.valueOf((String)m.get("journeyDate"))*1000;
                       Date td=new Date();
                       td.setTime(dd);
                       jd.setJourneyDate(td);
                }else  jd.setJourneyDate(new Date());
                TUserJourneyDaily jdd=jdRepository.findByJourneyAndJourneyDate(jou, jd.getJourneyDate());
                if(jdd!=null){
                    rm=raiseError(2000);
                    rm.put("success", false);
                    return add(rm);
                }
                jd.setJourney(jou);
                jd.setDateNumber(1);
                
                jdRepository.save(jd);
                String city_name="";
                List<TUserJourneyDailyCity> cs=new ArrayList<>();
                if(m.containsKey("city_id")){
                    List<String> datas=(List<String>) m.get("city_id");
                    
                    for(int i=0;i<datas.size();i++){
                        String ids=datas.get(i);
                        TArea ar=areaRepository.findOne(ids);
                        city_name=city_name+"-"+ar.getName1();
                        TUserJourneyDailyCity cc=new TUserJourneyDailyCity();
                        cc.setJourney(jd);
                        cc.setCity(ar);
                        cc.setSeq(i+1);
                        dcRepository.save(cc);
                        cs.add(cc);
                    }
                }
//                jd.setCities(cs);
                
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                Map mmm=new HashMap();
                mmm.put("id", jd.getId());
                if(!city_name.isEmpty())mmm.put("city_name", city_name.substring(1));
                mmm.put("journeyString", WStringUtils.dateToString(jd.getJourneyDate()));
                rm.put("data", mmm);
        }catch(Exception e){
               rm=raiseError(3000);
               rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Delete user journey daily information. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map jda_delete(Map map) {
        Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        try{
            TUserJourneyDaily jd=jdRepository.findOne(id);
            jdRepository.delete(jd);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read user journey daily information. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map jda_read(Map model) {
        Map rm=new HashMap();
        Map map=(Map) model.get("data");
        String pid=(String) map.get("jid");
        TUserJourney jou=jouRepository.findOne(Long.valueOf(pid));
        List<TUserJourneyDaily> list=jdRepository.findByJourneyOrderByJourneyDate(jou);
        List lst=new ArrayList();
        for(TUserJourneyDaily air:list){
            Map mm=new HashMap();
            mm.put("id", air.getId().toString());
            mm.put("daily_title", air.getTitle());
            mm.put("daily_hotel", air.getHotel());
            mm.put("daily_meal", air.getMeal());
            mm.put("daily_vehicle", air.getVehicle());
            mm.put("daily_air", air.getAirinfo());
            mm.put("journeyDate", air.getJourneyDate());
            mm.put("journeyString", WStringUtils.dateToString(air.getJourneyDate()));
            List<TUserJourneyDailyCity> datas=dcRepository.findByJourney(air);
            List darea=new ArrayList();
            String city_name="";
            for(TUserJourneyDailyCity cc:datas){
                darea.add(cc.getCity().getId());
                city_name=city_name+"-"+cc.getCity().getName1();
            }
            mm.put("city_id",darea);
            if(!city_name.isEmpty())mm.put("city_name", city_name.substring(1));
            lst.add(mm);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("data", lst);
        return add(rm);
    }

    /**
     * Update user journey daily information. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map jda_update(Map map) {
         Map m=(Map) map.get("data");
         Map rm=new HashMap();
         try{
                Long id=Long.valueOf((String)m.get("id"));
                TUserJourneyDaily ta=jdRepository.findOne(id);
                if((String)m.get("daily_title")!=null)ta.setTitle((String) m.get("daily_title"));
                if(m.containsKey("daily_date"))ta.setDateNumber((int) m.get("daily_date"));
               if(m.containsKey("daily_hotel"))ta.setHotel((String) m.get("daily_hotel"));
               if(m.containsKey("daily_meal"))ta.setMeal((String) m.get("daily_meal"));
               if(m.containsKey("daily_vehicle"))ta.setVehicle((String) m.get("daily_vehicle"));
               if(m.containsKey("daily_air"))ta.setAirinfo((String) m.get("daily_air"));
                if(m.containsKey("journeyDate")){
                       Long dd=Long.valueOf((String)m.get("journeyDate"))*1000;
                       Date td=new Date();
                       td.setTime(dd);
                       ta.setJourneyDate(td);
                       TUserJourneyDaily tdd=jdRepository.findByJourneyAndJourneyDate(ta.getJourney(), ta.getJourneyDate());
                       if(tdd!=null && !tdd.getId().equals(ta.getId())){
                           rm=raiseError(2000);
                           rm.put("success", false);
                           return add(rm);
                       }
                }
                
                if(m.containsKey("city_id")){
                    List<String> datas=(List<String>) m.get("city_id");
                    List<TUserJourneyDailyCity> ccc=ta.getCities();
                    List<TUserJourneyDailyCity> cs=new ArrayList<>();
                    for(TUserJourneyDailyCity kc:ccc){
                        dcRepository.delete(kc);
                    }
                    for(int i=0;i<datas.size();i++){
                        String ids=datas.get(i);
                        TArea ar=areaRepository.findOne(ids);
                        TUserJourneyDailyCity cc=new TUserJourneyDailyCity();
                        cc.setJourney(ta);
                        cc.setCity(ar);
                        cc.setSeq(i+1);
                        dcRepository.save(cc);
                        cs.add(cc);
                    }
                    ta.setCities(cs);
                }
                jdRepository.save(ta);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                
         }catch(Exception e){
             rm=raiseError(3000);
             rm.put("success", false);
         }
         return add(rm);
    }

    /**
     * Add user journey daily detail information. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map jde_add(Map map) {
        Map rm=new HashMap();
        try{
            Map m=(Map) map.get("data");
            String id=(String) m.get("jid");
            TUserJourneyDaily jd=jdRepository.findOne(Long.valueOf(id));
            TUserJourneyDailyDetail jdd=new TUserJourneyDailyDetail();
            jdd.setContents((String)m.get("detail_content"));
            jdd.setCreated(new Date());
            jdd.setModified(new Date());
            jdd.setDaily(jd);
            int dh=(int) m.get("dh");String sdh=dh>9?String.valueOf(dh):"0"+dh;
            int dm=(int) m.get("dm");String sdm=dm>9?String.valueOf(dm):"0"+dm;
            String dt=sdh+":"+sdm+":00";
            TUserJourneyDailyDetail jdt=jddRepository.findByDailyAndJourneyTime(jd, dt);
            if(jdt!=null){
                rm=raiseError(2000);
                rm.put("success", false);
                return add(rm);
            }
            jdd.setJourneyTime(dt);
            jddRepository.save(jdd);
             rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
             Map mmm=new HashMap();
            mmm.put("id", jdd.getId());
            String ds=jdd.getJourneyTime();
            String ss=ds.substring(0, org.apache.commons.lang3.StringUtils.lastIndexOf(ds, ":"));
            mmm.put("detail_time", ss);
            rm.put("data", mmm);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
         return add(rm);
        
    }

    /**
     * Delete user journey daily detail information. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map jde_delete(Map map) {
        Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        try{
            jddRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read user journey daily detail information. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map jde_read(Map model) {
          Map rm=new HashMap();
        Map map=(Map) model.get("data");
        String pid=(String) map.get("jid");
        TUserJourneyDaily jou=jdRepository.findOne(Long.valueOf(pid));
        List<TUserJourneyDailyDetail> list=jddRepository.findByDaily(jou);
        List lst=new ArrayList();
        for(TUserJourneyDailyDetail air:list){
            Map mm=new HashMap();
            mm.put("id", air.getId().toString());
            mm.put("daily_id", air.getDaily().getId());
           mm.put("detail_content", air.getContents());
            String ds=air.getJourneyTime();
                if(ds!=null && ds.contains(":")){
                    String[]dss=ds.split(":");
                    int dhour=Integer.valueOf(dss[0]);
                    int minutes=Integer.valueOf(dss[1]);
                    mm.put("dh", dhour);
                    mm.put("dm", minutes);
                   String ss=ds.substring(0, org.apache.commons.lang3.StringUtils.lastIndexOf(ds, ":"));
                   mm.put("detail_time", ss);
                }
            lst.add(mm);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("data", lst);
        return add(rm);
    }

    /**
     * Update user journey daily detail information. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map jde_update(Map map) {
         Map m=(Map) map.get("data");
         Map rm=new HashMap();
         try{
                Long id=Long.valueOf((String)m.get("id"));
               TUserJourneyDailyDetail jdd=jddRepository.findOne(id);
               if(m.containsKey("detail_content"))jdd.setContents((String) m.get("detail_content"));
                 int dhour=m.containsKey("dh")?(int) m.get("dh"):0;
                int minutes=m.containsKey("dm")?(int) m.get("dm"):0;
                
                String ds=jdd.getJourneyTime();
                if(ds!=null && ds.contains(":")){
                    String[]dss=ds.split(":");
                    if(dhour==0)dhour=Integer.valueOf(dss[0]);
                    if(minutes==0)minutes=Integer.valueOf(dss[1]);
                }
                String sdh=dhour>9?String.valueOf(dhour):"0"+dhour;
                String sdm=minutes>9?String.valueOf(minutes):"0"+minutes;
                String dt=sdh+":"+sdm+":00";
                TUserJourneyDailyDetail jdt=jddRepository.findByDailyAndJourneyTime(jdd.getDaily(), dt);
                if(jdt!=null && jdt.getId().equals(jdd.getId())){
                    rm=raiseError(2000);
                    rm.put("success", false);
                    return add(rm);
                }
             jdd.setJourneyTime(dt);
                jdd.setModified(new Date());
                jddRepository.save(jdd);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         }catch(Exception e){
             rm=raiseError(3000);
             rm.put("success", false);
         }
        return add(rm);
    }

    /**
     * Add user journey hotel information. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map jho_add(Map map) {
        Map rm=new HashMap();
        Map m=(Map) map.get("data");
        try{
                String id=(String) m.get("jou_id");
                TUserJourneyDaily jou=jdRepository.findOne(Long.valueOf(id));
                TUserJourneyHotelInfo jd=jhotelRepository.findByJourneyAndJourneyDate(jou.getJourney(), jou.getJourneyDate());
                if(jd==null){
                    jd=new TUserJourneyHotelInfo();
                    jd.setCreated(new Date());
                    jd.setJourneyDate(jou.getJourneyDate());
                }
                jd.setModified(new Date());
                if(m.containsKey("hotel_hotel"))jd.setHotelName((String) m.get("hotel_hotel"));
                if(m.containsKey("area_name")){
                    String area_name=(String) m.get("area_name");
                    if(area_name!=null && !area_name.isEmpty()){
                        TArea aa=areaRepository.findOne(area_name);
                        jd.setArea(aa);
                    }
                }
                if(m.containsKey("hotel_phone"))jd.setPhone((String) m.get("hotel_phone"));
                if(m.containsKey("hotel_fax"))jd.setFax((String) m.get("hotel_fax"));

                jd.setJourney(jou.getJourney());
                jhotelRepository.save(jd);

                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                Map mmm=new HashMap();
                mmm.put("id", jd.getId());
                rm.put("data", mmm);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Delete user journey hotel information. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map jho_delete(Map map) {
        Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        try{
            jhotelRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read user journey hotel information. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map jho_read(Map model) {
         Map rm=new HashMap();
        Map map=(Map) model.get("data");
        String pid=(String) map.get("jid");
        TUserJourneyDaily jou=jdRepository.findOne(Long.valueOf(pid));
        
        TUserJourneyHotelInfo air=jhotelRepository.findByJourneyAndJourneyDate(jou.getJourney(),jou.getJourneyDate());
        List lst=new ArrayList();
        if(air!=null){
            Map mm=new HashMap();
            mm.put("id", air.getId().toString());
            mm.put("hotel_journeyDate", WStringUtils.dateToString(air.getJourneyDate()));
            mm.put("hotel_hotel", air.getHotelName());
            if(air.getArea()!=null)mm.put("area_name", air.getArea().getName1());
            mm.put("hotel_phone", air.getPhone());
            mm.put("hotel_fax", air.getFax());
            lst.add(mm);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("data", lst);
        return add(rm);
    }

    /**
     * Update user journey hotel information. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map jho_update(Map map) {
        Map m=(Map) map.get("data");
         Map rm=new HashMap();
         try{
                Long id=Long.valueOf((String)m.get("id"));
                TUserJourneyHotelInfo jd=jhotelRepository.findOne(id);
                 if(m.containsKey("hotel_journeyDate")){
                       Long dd=Long.valueOf((String)m.get("hotel_journeyDate"))*1000;
                       Date td=new Date();
                       td.setTime(dd);
                       jd.setJourneyDate(td);
                }  
               if(m.containsKey("hotel_hotel"))jd.setHotelName((String) m.get("hotel_hotel"));
                if(m.containsKey("area_name")){
                    String area_name=(String) m.get("area_name");
                    if(area_name!=null && !area_name.isEmpty()){
                        TArea aa=areaRepository.findOne(area_name);
                        jd.setArea(aa);
                    }
                }
               if(m.containsKey("hotel_phone"))jd.setPhone((String) m.get("hotel_phone"));
               if(m.containsKey("hotel_fax"))jd.setFax((String) m.get("hotel_fax"));

                jhotelRepository.save(jd);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         }catch(Exception e){
             rm=raiseError(3000);
             rm.put("success", false);
         }
         return add(rm);
    }

    /**
     * Add user journey room information. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map jro_add(Map map) {
        Map rm=new HashMap();
        try{
                Map m=(Map) map.get("data");
                String id=(String) m.get("jou_id");
                TUserJourney jou=jouRepository.findOne(Long.valueOf(id));
                TUserJourneyRoomsList jd=new TUserJourneyRoomsList();
                boolean flag=false;
                if(m.containsKey("ro_roomInfo")){
                    String info=(String) m.get("ro_roomInfo");
                    List lst=jroomRepository.findByJneyAndRoomInfo(jou.getId(), info);
                    if(lst !=null && lst.size()>1){
                        rm=raiseError(2000);
                        rm.put("success", false);
                        return add(rm);
                    }
                    if(lst==null || lst.isEmpty()){
                        flag=true;
                    }
                    jd.setRoomInfo(info);
                }
                if(m.containsKey("ro_birthday")){
                        Long dd=Long.valueOf((String)m.get("ro_birthday"))*1000;
                        Date td=new Date();
                        td.setTime(dd);
                        jd.setBirthday(td);
                 }  

                if(m.containsKey("ro_name"))jd.setName((String) m.get("ro_name"));
                if(m.containsKey("ro_relation"))jd.setRelation((String) m.get("ro_relation"));
                if(m.containsKey("ro_lastname"))jd.setLastName((String) m.get("ro_lastname"));
                if(m.containsKey("ro_firstname"))jd.setFirstName((String) m.get("ro_firstname"));
                if(m.containsKey("ro_passportNo"))jd.setPassportNo((String) m.get("ro_passportNo"));
                if(m.containsKey("ro_salesman"))jd.setSalesman((String) m.get("ro_salesman"));
                if(m.containsKey("ro_description"))jd.setDescription((String) m.get("ro_description"));
                
                if(m.containsKey("ro_phone"))jd.setPhone((String) m.get("ro_phone"));
                if(m.containsKey("ro_gender"))jd.setGender((int) m.get("ro_gender"));

                jd.setJourney(jou);
                jroomRepository.save(jd);

                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                Map mmm=new HashMap();
                mmm.put("id", jd.getId());
                mmm.put("flag",flag);
                mmm.put("info", jd.getRoomInfo());
                rm.put("data", mmm);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Delete user journey room information.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map jro_delete(Map map) {
        Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        try{
            jroomRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read user journey room information.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map jro_read(Map model) {
        Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                String pid=(String) map.get("jid");
                TUserJourneyRoomsList r=jroomRepository.findOne(Long.valueOf(pid));
                List<TUserJourneyRoomsList> list=jroomRepository.findByJneyAndRoomInfo(r.getJourney().getId(), r.getRoomInfo());
                List lst=new ArrayList();
                for(TUserJourneyRoomsList air:list){
                    Map mm=new HashMap();
                    mm.put("id", air.getId().toString());
                    mm.put("ro_name", air.getName());
                    mm.put("ro_relation", air.getRelation());
                    mm.put("ro_lastname", air.getLastName());
                    mm.put("ro_firstname", air.getFirstName());
                    mm.put("ro_passportNo", air.getPassportNo());
                    mm.put("ro_salesman", air.getSalesman());
                    mm.put("ro_description", air.getDescription());
                    mm.put("ro_roomInfo", air.getRoomInfo());
                    mm.put("ro_phone", air.getPhone());
                    mm.put("ro_gender", air.getGender());
                    mm.put("ro_birthday", air.getBirthday());
                    lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Update user journey room information.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map jro_update(Map map) {
        Map m=(Map) map.get("data");
         Map rm=new HashMap();
         try{
                Long id=Long.valueOf((String)m.get("id"));
                TUserJourneyRoomsList jd=jroomRepository.findOne(id);
                 if(m.containsKey("ro_birthday")){
                       Long dd=Long.valueOf((String)m.get("ro_birthday"))*1000;
                       Date td=new Date();
                       td.setTime(dd);
                       jd.setBirthday(td);
                }  

               if(m.containsKey("ro_name"))jd.setName((String) m.get("ro_name"));
               if(m.containsKey("ro_relation"))jd.setRelation((String) m.get("ro_relation"));
               if(m.containsKey("ro_lastname"))jd.setLastName((String) m.get("ro_lastname"));
               if(m.containsKey("ro_firstname"))jd.setFirstName((String) m.get("ro_firstname"));
               if(m.containsKey("ro_passportNo"))jd.setPassportNo((String) m.get("ro_passportNo"));
               if(m.containsKey("ro_salesman"))jd.setSalesman((String) m.get("ro_salesman"));
               if(m.containsKey("ro_description"))jd.setDescription((String) m.get("ro_description"));
               if(m.containsKey("ro_roomInfo"))jd.setRoomInfo((String) m.get("ro_roomInfo"));
               if(m.containsKey("ro_phone"))jd.setPhone((String) m.get("ro_phone"));
               if(m.containsKey("ro_gender"))jd.setGender((int) m.get("ro_gender"));

                jroomRepository.save(jd);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         }catch(Exception e){
             rm=raiseError(3000);
             rm.put("success", false);
         }
         return add(rm);
    }

    /**
     * Add user journey car information. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map jca_add(Map map) {
        Map rm=new HashMap();
        try{
                Map m=(Map) map.get("data");
                String id=(String) m.get("jou_id");
                TUserJourney jou=jouRepository.findOne(Long.valueOf(id));
                List<TUserJourneyCarInfo> cars=jcarRepository.findByJourney(jou);
                TUserJourneyCarInfo jd=new TUserJourneyCarInfo();
                if(cars!=null && !cars.isEmpty())jd=cars.get(0);
               if(m.containsKey("car_model"))jd.setCarModel((String) m.get("car_model"));
                if(m.containsKey("driver_name"))jd.setDriverName((String) m.get("driver_name"));
                if(m.containsKey("car_number"))jd.setCarNumber((String) m.get("car_number"));
                if(m.containsKey("car_phone"))jd.setPhone((String) m.get("car_phone"));
                jd.setJourney(jou);
                jd.setCreated(new Date());
                jd.setModified(new Date());
                jcarRepository.save(jd);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                Map mmm=new HashMap();
                mmm.put("id", jd.getId());
                rm.put("data", mmm);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Delete user journey car information. 
     * @param map HashMap
     * @return map HashMap
      */
    @Override
    public Map jca_delete(Map map) {
         Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        try{
            jcarRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read user journey car information. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map jca_read(Map model) {
        Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                String pid=(String) map.get("jid");
                TUserJourney jou=jouRepository.findOne(Long.valueOf(pid));
                List<TUserJourneyCarInfo> list=jcarRepository.findByJourney(jou);
                List lst=new ArrayList();
                for(TUserJourneyCarInfo air:list){
                    Map mm=new HashMap();
                    mm.put("id", air.getId().toString());
                    mm.put("car_model", air.getCarModel());
                    mm.put("driver_name", air.getDriverName());
                    mm.put("car_number", air.getCarNumber());
                    mm.put("car_phone", air.getPhone());
                    lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Update user journey car information. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map jca_update(Map map) {
        Map m=(Map) map.get("data");
         Map rm=new HashMap();
         try{
                Long id=Long.valueOf((String)m.get("id"));
                TUserJourneyCarInfo jd=jcarRepository.findOne(id);
                if(m.containsKey("car_model"))jd.setCarModel((String) m.get("car_model"));
                if(m.containsKey("driver_name"))jd.setDriverName((String) m.get("driver_name"));
                if(m.containsKey("car_number"))jd.setCarNumber((String) m.get("car_number"));
                if(m.containsKey("car_phone"))jd.setPhone((String) m.get("car_phone"));
                jd.setModified(new Date());
                jcarRepository.save(jd);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         }catch(Exception e){
             rm=raiseError(3000);
             rm.put("success", false);
         }
         return add(rm);
    }

    /**
     * Read user journey file. 
     * @param model HashMap
     * @return map HashMap
     */
   @Override
    public Map jfile_read(Map model) {
          Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                String pid=(String) map.get("jid");
                Integer cid=(Integer) map.get("cid");
                TUserJourney jou=jouRepository.findOne(Long.valueOf(pid));
                List<TUserJourneyFiles> list=jfileRepository.findByJourneyAndCategory(jou.getFile(),cid);
                List lst=new ArrayList();
                for(TUserJourneyFiles air:list){
                    Map mm=new HashMap();
                    mm.put("id", air.getId());
                    mm.put("file_url", air.getViewUrl());
                    mm.put("cid", air.getCategory());
                    mm.put("name", "文件");
                    lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Add logo. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map logo_add(Map map) {
        Map rm=new HashMap();
        try{
                Map m=(Map) map.get("data");
                TCategoryLogo jd=new TCategoryLogo();
               if(m.containsKey("icon_url"))jd.setViewUrl((String) m.get("icon_url"));
                if(m.containsKey("idonotknow"))jd.setStoragePath((String) m.get("idonotknow"));
                if(m.containsKey("flag"))jd.setFlag((Boolean) m.get("flag")?1:0);else jd.setFlag(0);
                if(m.containsKey("cid"))jd.setCategory((int) m.get("cid"));
                logoRepository.save(jd);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                Map mmm=new HashMap();
                mmm.put("id", jd.getId());
                rm.put("data", mmm);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Delete logo. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map logo_delete(Map map) {
        Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        try{
            logoRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read logo. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map logo_read(Map model) {
         Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                int pid=(int) map.get("cid");
                
                List<TCategoryLogo> list=logoRepository.findByCategory(pid);
                List lst=new ArrayList();
                for(TCategoryLogo air:list){
                    Map mm=new HashMap();
                    mm.put("id", air.getId().toString());
                    mm.put("flag", air.getFlag()==1);
                    mm.put("icon_url", air.getViewUrl());
                    mm.put("cid", air.getCategory());
                    mm.put("logo_name", "标志");
                    lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Update logo. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map logo_update(Map map) {
         Map m=(Map) map.get("data");
         Map rm=new HashMap();
         try{
                Long id=Long.valueOf((String)m.get("id"));
                TCategoryLogo jd=logoRepository.findOne(id);
                if(m.containsKey("icon_url"))jd.setViewUrl((String) m.get("icon_url"));
                if(m.containsKey("idonotknow")){
                    file_delete_on_fastdfs(jd.getStoragePath());
                     String donot=(String) m.get("idonotknow");
                    jd.setStoragePath(clientApp.get_uid((String)map.get("partner"), donot));
                }
                if(m.containsKey("flag"))jd.setFlag((Boolean) m.get("flag")?1:0);
                logoRepository.save(jd);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         }catch(Exception e){
             rm=raiseError(3000);
             rm.put("success", false);
         }
         return add(rm);
    }

    /**
     * Read museum with docent. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map dmu_read(Map map) {
         Map rm=new HashMap();
         try{
                List<TCityScenicSpots> list=ssRepository.findByHasDocents(1);
                List lst=new ArrayList();
                for(TCityScenicSpots air:list){
                    Map mm=new HashMap();
                    mm.put("id", air.getId().toString());
                    mm.put("name", air.getNameZh());
                    lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read organization read. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map org_read(Map map) {
         Map rm=new HashMap();
         try{
                List<TUserOrganization> list=orgRepository.findAll();
                List lst=new ArrayList();
                for(TUserOrganization air:list){
                    Map mm=new HashMap();
                    mm.put("id", air.getId().toString());
                    mm.put("name", air.getName());
                    mm.put("nameEn", air.getNameEn());
                    mm.put("nameTw", air.getNameTw());
                    lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     *  Read all cities. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map ctall_read(Map map) {
         Map rm=new HashMap();
         try{
             Map model=(Map) map.get("data");
             Integer cid=(Integer) model.get("cid");
             String key=(String) model.get("key");
              if(hasText(key)){
                key="%"+key.toLowerCase()+"%";
              }else{
                key="%%";
             }
                List<TArea> list;
                if(cid==null)list=areaRepository.find_city_all();
                else if(cid==1){
                    list=areaRepository.find_europe_country("中国",key);
                }
                else {
                    list=areaRepository.find_europe_city("中国",key);
                }
                List lst=new ArrayList();
                for(TArea air:list){
                    if(air.getName1().equals("中国"))continue;
                    Map mm=new HashMap();
                    mm.put("id", air.getId());
                    mm.put("name", air.getName1());
                    mm.put("pname", air.getParent()!=null?air.getParent().getName1():"");
                    mm.put("ename", air.getName2());
                    mm.put("flag", air.getFlag()==1);
                    lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }
    
    /**
     * Set city recommend.
     * @param map HashMap
     * @return map HashMap
     */
      @Override
    public Map ctall_post(Map map) {
        Map rm=new HashMap();
        try{
                Map m=(Map) map.get("data");
                String id=(String) m.get("id");
                TArea area=areaRepository.findOne(id);
               if(m.containsKey("flag"))area.setFlag((Boolean)m.get("flag")?1:0);
                areaRepository.save(area);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read car model. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map carm_read(Map map) {
         Map rm=new HashMap();
         try{
                List<TCarModels> list=modelRepository.findAll();
                List lst=new ArrayList();
                for(TCarModels air:list){
                    Map mm=new HashMap();
                    mm.put("id", air.getId());
                    mm.put("name", air.getModel());
                    lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Delete user journey room information. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map jrf_delete(Map map) {
        Map model=(Map) map.get("data");
         String id=(String) model.get("id");
       Map rm=new HashMap();
        try{
            TUserJourneyRoomsList room=jroomRepository.findOne(Long.valueOf(id));
            List<TUserJourneyRoomsList> rlist=jroomRepository.findByJneyAndRoomInfo(room.getJourney().getId(), room.getRoomInfo());
            for(TUserJourneyRoomsList r:rlist){
                jroomRepository.delete(r);
            }
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read user journey room information.
     * @param model HashMap
     * @return model HashMap
     */
    @Override
    public Map jrf_read(Map model) {
        Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                String pid=(String) map.get("jid");
                List<String> list=jroomRepository.findByJney(Long.valueOf(pid));
                List lst=new ArrayList();
                for(String air:list){
                    Map mm=new HashMap();
                    List<TUserJourneyRoomsList> rlist=jroomRepository.findByJneyAndRoomInfo(Long.valueOf(pid), air);
                    mm.put("id", rlist.get(0).getId().toString());
                    mm.put("name", air);
                    lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     *  Do process user journey.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map jset_post(Map map) {
         Map model=(Map) map.get("data");
         String id=(String) model.get("id");
       Map rm=new HashMap();
        try{
            TUserJourney room=jouRepository.findOne(Long.valueOf(id));
            room.setStatus(2);
            jouRepository.save(room);
            noticeServices.journey_state_notice(room);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Add car model. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map carm_add(Map map) {
        Map m=(Map) map.get("data");
        Map rm=new HashMap();
       TCarModels model=new TCarModels();
       String name=(String) m.get("name");
       TCarModels ms=modelRepository.findByModel(name);
       if(ms!=null){
           rm=raiseError(2000);
           rm.put("success", false);
       }else{
           ms=new TCarModels();
           List lst=modelRepository.findAll(new Sort(Sort.Direction.DESC, "id"));
           if(lst!=null && !lst.isEmpty()){
               TCarModels mms=(TCarModels) lst.get(0);
               ms.setId(mms.getId()+1);
           }else{
               ms.setId(1);
           }
           ms.setModel(name);
           modelRepository.save(ms);
           rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
           Map mmm=new HashMap();
           mmm.put("id", ms.getId());
           rm.put("data", mmm);
       }
       return add(rm);
    }

    /**
     * Update car model. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map carm_update(Map map) {
        Map rm=new HashMap();
        Map m=(Map) map.get("data");
       TCarModels model=modelRepository.findOne(Integer.valueOf((String) m.get("id")));
       if((String)m.get("name")!=null){
           String name=(String) m.get("name");
           if(!model.getModel().equals(name)){
               TCarModels mms=modelRepository.findByModel(name);
               if(mms!=null){
                   rm=raiseError(2000);
                   rm.put("success", false);
                   return add(rm);
               }
               model.setModel(name);
           }
       }
       modelRepository.save(model);
       rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
       return add(rm);
    }

    /**
     * Delete car model. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map carm_delete(Map map) {
        Map model=(Map) map.get("data");
        int id=Integer.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        try{
            TCarModels models=modelRepository.findOne(id);
            List as=udrRepository.findByModels(models);
            if(as!=null && !as.isEmpty()){
                    rm=raiseError(2007);
                    rm.put("success", false);
                    return add(rm);
            }
            modelRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Add organization. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map org_add(Map map) {
         Map m=(Map) map.get("data");
        Map rm=new HashMap();
        String name=(String) m.get("name");
        TUserOrganization ms=orgRepository.findByName(name);
       if(ms!=null){
           rm=raiseError(2000);
           rm.put("success", false);
       }else{
           ms=new TUserOrganization();
           ms.setName(name);
           ms.setCreated(new Date());
           ms.setModified(new Date());
           if(m.containsKey("nameTw"))ms.setNameTw((String)m.get("nameTw"));
           if(m.containsKey("nameEn"))ms.setNameEn((String)m.get("nameEn"));
           orgRepository.save(ms);
           rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
           Map mmm=new HashMap();
           mmm.put("id", ms.getId());
           rm.put("data", mmm);
       }
       return add(rm);
    }

    /**
     * Update organization. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map org_update(Map map) {
       Map rm=new HashMap();
       Map m=(Map) map.get("data");
       TUserOrganization model=orgRepository.findOne(Long.valueOf((String) m.get("id")));
       if((String)m.get("name")!=null){
           String name=(String) m.get("name");
           if(!model.getName().equals(name)){
               TUserOrganization mms=orgRepository.findByName(name);
               if(mms!=null){
                   rm=raiseError(2000);
                   rm.put("success", false);
                   return add(rm);
               }
               model.setName(name);
           }
       }
       if(m.containsKey("nameEn"))model.setNameEn((String)m.get("nameEn"));
       if(m.containsKey("nameTw"))model.setNameTw((String)m.get("nameTw"));
       orgRepository.save(model);
       rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
       return add(rm);
    }

    /**
     * Delete organization.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map org_delete(Map map) {
        Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        TUserOrganization org=orgRepository.findOne(id);
        List as=uddeRepository.findByOrganization(org);
        if(as!=null && !as.isEmpty()){
                    rm=raiseError(2007);
                    rm.put("success", false);
                    return add(rm);
         }
        try{
            orgRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Add user journey base information. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map joud_add(Map map) {
        Map rm=new HashMap();
        try{
                Map m=(Map) map.get("data");
                String id=(String) m.get("jou_id");
                TUserJourney jou=jouRepository.findOne(Long.valueOf(id));
                TUserJourneyData jd=joudRepository.findByJourney(jou);
                if(jd==null)jd=new TUserJourneyData();
                if(m.containsKey("cname")){
                    String cname=(String) m.get("cname");
                    TArea area=areaRepository.findOne(cname);
                    jd.setStartCity(area);
                }
                 if(m.containsKey("hotel"))jd.setHotel((String) m.get("hotel"));
                if(m.containsKey("meal"))jd.setMeal((String) m.get("meal"));
                if(m.containsKey("vehicle"))jd.setVehicle((String) m.get("vehicle"));
                if(m.containsKey("rinfo"))jd.setRelationInfo((String) m.get("rinfo"));
                if(m.containsKey("service"))jou.setOther1((String)m.get("service"));
                if(m.containsKey("des"))jou.setOther2((String)m.get("des"));
                if(m.containsKey("direct"))jou.setOther3((String)m.get("direct"));
                jouRepository.save(jou);
                jd.setJourney(jou);
                jd.setCreated(new Date());
                jd.setModified(new Date());
                joudRepository.save(jd);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                Map mmm=new HashMap();
                mmm.put("id", jd.getId());
                rm.put("data", mmm);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read user journey base information. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map joud_read(Map model) {
        Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                String pid=(String) map.get("jid");
                TUserJourney jou=jouRepository.findOne(Long.valueOf(pid));
                TUserJourneyData air=joudRepository.findByJourney(jou);
                List lst=new ArrayList();
                Map mm=new HashMap();
                 if(air!=null){
                        mm.put("id", air.getId().toString());
                        if(air.getStartCity()!=null)mm.put("cname", air.getStartCity().getName1());
                        mm.put("hotel", air.getHotel());
                        mm.put("meal", air.getMeal());
                        mm.put("vehicle", air.getVehicle());
                        mm.put("rinfo", air.getRelationInfo());
                        mm.put("service", air.getJourney().getOther1());
                        mm.put("des", air.getJourney().getOther2());
                        mm.put("direct", air.getJourney().getOther3());
                        lst.add(mm);
                  }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read docent schedule data.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map doi_read(Map model) {
         Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                String key=(String) map.get("key");
                String dd=(String) map.get("dd");
                if(hasText(key)){
                  key="%"+key.toLowerCase()+"%";
                }else{
                  key="%%";
               }
                Calendar scal=Calendar.getInstance();
                List<TDocentsSchedule> data;
                 if(dd!=null && !dd.isEmpty()){
                    dd=dd.split("T")[0];
                    Date d=WStringUtils.stringToDate(dd, "-");
                    scal.setTime(d);
                    data=doiRepository.find_by_key_date(key, scal.getTime());
                }else{
                     data=doiRepository.find_by_key(key);
                 }
                List lst=new ArrayList();
                for(TDocentsSchedule air:data){
                       Map mm=new HashMap();
                        mm.put("id", air.getId().toString());
                        mm.put("sid", air.getScenic().getId().toString());
                        mm.put("sname", air.getScenic().getNameZh());
                        mm.put("sdate", air.getDocentDate());
                        mm.put("adultPrice", air.getAdultPrice());
                        mm.put("childrenPrice", air.getChildrenPrice());
                        lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Add docent schedule.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map doi_add(Map map) {
        Map m=(Map) map.get("data");
        Map rm=new HashMap();
        Long dd=Long.valueOf((String)m.get("sdate"))*1000;
        Date td=new Date();
        td.setTime(dd);
        String sid=m.get("sid").toString();
        TCityScenicSpots sc=ssRepository.findOne(Long.valueOf(sid));
        TDocentsSchedule ms=doiRepository.findByScenicAndDocentDate(sc,td);
       if(ms!=null){
           rm=raiseError(2000);
           rm.put("success", false);
       }else{
           ms=new TDocentsSchedule();
           ms.setScenic(sc);
           ms.setCreated(new Date());
           ms.setDocentDate(td);
           ms.setAdultPrice(Float.valueOf(m.get("adultPrice").toString()));
           if(m.containsKey("childrenPrice"))ms.setChildrenPrice(Float.valueOf(m.get("childrenPrice").toString()));
           doiRepository.save(ms);
           rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
           Map mmm=new HashMap();
           mmm.put("id", ms.getId());
           mmm.put("sname", ms.getScenic().getNameZh());
           rm.put("data", mmm);
       }
       return add(rm);
    }

    /**
     * Update docent schedule.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map doi_update(Map map) {
        Map rm=new HashMap();
       Map m=(Map) map.get("data");
       TDocentsSchedule model=doiRepository.findOne(Long.valueOf( m.get("id").toString()));
        if(m.containsKey("sdate")){
           Long dd=Long.valueOf((String)m.get("sdate"))*1000;
           Date td=new Date();
            td.setTime(dd);
            model.setDocentDate(td);
        }
        if(m.containsKey("sid")){
            String sid=m.get("sid").toString();
           if(!model.getScenic().getId().toString().equals(sid)){
               TCityScenicSpots sc=ssRepository.findOne(Long.valueOf(sid));
               model.setScenic(sc);
           }
        }
        TDocentsSchedule ms=doiRepository.findByScenicAndDocentDate(model.getScenic(),model.getDocentDate());
        if(ms!=null && !ms.getId().equals(model.getId())){
                   rm=raiseError(2000);
                   rm.put("success", false);
                   return add(rm);
        }
         
       if(m.containsKey("adultPrice"))model.setAdultPrice(Float.valueOf(m.get("adultPrice").toString()));
       if(m.containsKey("childrenPrice"))model.setChildrenPrice(Float.valueOf(m.get("childrenPrice").toString()));
       doiRepository.save(model);
       rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
       return add(rm);
    }

    /**
     * Delete docent schedule.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map doi_delete(Map map) {
         Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        TDocentsSchedule org=doiRepository.findOne(id);
        List as=dotRepository.findBySchedule(org);
        if(as!=null && !as.isEmpty()){
                    rm=raiseError(2007);
                    rm.put("success", false);
                    return add(rm);
         }
        try{
            doiRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read user docent.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map dod_read(Map model) {
         Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                String key=(String) map.get("key");
                int flag=(int) map.get("flag");
                List<TUserDocents> data=new ArrayList<>();
                switch(flag){
                    case 1:
                        data=dodRepository.findByDocentsNameContaining(key);
                        break;
                    case 2:
                        if(hasText(key)){
                            key="%"+key.toLowerCase()+"%";
                          }else{
                            key="%%";
                         }
                        data=dodRepository.find_by_key(key);
                        break;
                    case 3:
                        data=dodRepository.findByPhoneContaining(key);
                        break;
                }
                List lst=new ArrayList();
                for(TUserDocents air:data){
                        Map mm=new HashMap();
                        mm.put("id", air.getId().toString());
                        mm.put("sid", air.getScenic().getId().toString());
                        mm.put("sname", air.getScenic().getNameZh());
                        mm.put("dname", air.getDocentsName());
                        mm.put("aname", air.getAccountNum());
                        mm.put("phone", air.getPhone());
                        lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Add user docent.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map dod_add(Map map) {
         Map m=(Map) map.get("data");
        Map rm=new HashMap();
        String phone=(String) m.get("phone");
        String sid=m.get("sid").toString();
        TCityScenicSpots sc=ssRepository.findOne(Long.valueOf(sid));
        TUserDocents ms=dodRepository.findByScenicAndPhone(sc,phone);
       if(ms!=null){
           rm=raiseError(2000);
           rm.put("success", false);
       }else{
           ms=new TUserDocents();
           ms.setScenic(sc);
           ms.setCreated(new Date());
           if(m.containsKey("aname"))ms.setAccountNum((String)m.get("aname"));
           ms.setDocentsName((String)m.get("dname"));
           ms.setPhone(phone);
           dodRepository.save(ms);
           rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
           Map mmm=new HashMap();
           mmm.put("id", ms.getId());
           mmm.put("sname", ms.getScenic().getNameZh());
           rm.put("data", mmm);
       }
       return add(rm);
    }

    /**
     * Update user docent.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map dod_update(Map map) {
         Map rm=new HashMap();
       Map m=(Map) map.get("data");
       TUserDocents model=dodRepository.findOne(Long.valueOf( m.get("id").toString()));
       if(m.containsKey("sid")){
           String sid=m.get("sid").toString();
           if(!model.getScenic().getId().toString().equals(sid)){
               TCityScenicSpots sc=ssRepository.findOne(Long.valueOf(sid));
                model.setScenic(sc);
           }
        }
        if(m.containsKey("phone")){
            String phone=m.get("phone").toString();
            model.setPhone(phone);
        }
        TUserDocents ms=dodRepository.findByScenicAndPhone(model.getScenic(),model.getPhone());
         if(ms!=null && !ms.getId().equals(model.getId())){
                   rm=raiseError(2000);
                   rm.put("success", false);
                   return add(rm);
         }
       if(m.containsKey("aname"))model.setAccountNum((String)m.get("aname"));
       if(m.containsKey("dname"))model.setDocentsName((String)m.get("dname"));
       dodRepository.save(model);
       rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
       return add(rm);
    }

    /**
     * Delete user docent.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map dod_delete(Map map) {
          Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        TUserDocents org=dodRepository.findOne(id);
        List as=dotRepository.findByDocents(org);
        if(as!=null && !as.isEmpty()){
                    rm=raiseError(2007);
                    rm.put("success", false);
                    return add(rm);
         }
        try{
            dodRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read docent schedule assign.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map dot_read(Map model) {
        Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                String id=map.get("did").toString();
                String key=(String) map.get("key");
                int dd=(int) map.get("dd");
                if(hasText(key)){
                  key="%"+key.toLowerCase()+"%";
                }else{
                  key="%%";
               }
                List<TDocentsScheduleAssign> data;
                 if(dd==1){
                     data=dotRepository.find_by_key_schedule(key, Long.valueOf(id));
                 }else{
                     data=dotRepository.find_by_time_schedule(key, Long.valueOf(id));
                 }
                List lst=new ArrayList();
                
                for(TDocentsScheduleAssign air:data){
                        Map mm=new HashMap();
                        mm.put("id", air.getId().toString());
                        mm.put("sid", air.getSchedule().getId().toString());
                        mm.put("did", air.getDocents().getId().toString());
                        mm.put("dname", air.getDocents().getDocentsName());
                        int en=StringUtils.lastIndexOf(air.getDocentsTime(), ":")-1;
                        mm.put("docentsTime", StringUtils.substring(air.getDocentsTime(), 0,en ));
                        mm.put("anum", air.getAssignNumber());
                        lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Add docent schedule assign.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map dot_add(Map map) {
        Map m=(Map) map.get("data");
        Map rm=new HashMap();
        String sid=m.get("sid").toString();
        TDocentsSchedule ss=doiRepository.findOne(Long.valueOf(sid));
        String time=m.get("docentsTime").toString();
        String did=m.get("did").toString();
        TUserDocents ud=dodRepository.findOne(Long.valueOf(did));
        TDocentsScheduleAssign ms=dotRepository.findByDocentsTimeAndScheduleAndDocents(time, ss,ud);
        TDocentsScheduleItems ti=doddRepository.findByScheduleAndDocentTime(ss, time);
        if(ti==null){
            ti=new TDocentsScheduleItems();
        }
       if(ms!=null){
           rm=raiseError(2000);
           rm.put("success", false);
       }else{
           ms=new TDocentsScheduleAssign();
           ms.setDocents(ud);
           ms.setDocentsTime(time);
           ms.setSchedule(ss);
           int anum=(int) m.get("anum");
           ms.setAssignNumber(anum);
           dotRepository.save(ms);
           ti.setDocentTime(time);
           ti.setSchedule(ss);
           if(ti.getTotalNumber()!=null)ti.setTotalNumber(ti.getTotalNumber()+anum);else ti.setTotalNumber(anum);
           if(ti.getRemainNumber()!=null)ti.setRemainNumber(ti.getRemainNumber()+anum);else ti.setRemainNumber(anum);
           doddRepository.save(ti);
           rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
           Map mmm=new HashMap();
           mmm.put("id", ms.getId());
           rm.put("data", mmm);
       }
       return add(rm);
    }

    /**
     * Update docent schedule assign.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map dot_update(Map map) {
       Map rm=new HashMap();
       Map m=(Map) map.get("data");
       TDocentsScheduleAssign model=dotRepository.findOne(Long.valueOf( m.get("id").toString()));
        if(m.containsKey("did")){
            String did=m.get("did").toString();
            TUserDocents ud=dodRepository.findOne(Long.valueOf(did));
            model.setDocents(ud); 
       }
        TDocentsScheduleItems ti=doddRepository.findByScheduleAndDocentTime(model.getSchedule(), model.getDocentsTime());
        if(ti==null){
            ti=new TDocentsScheduleItems();
            ti.setDocentTime(model.getDocentsTime());
            ti.setSchedule(model.getSchedule());
            ti.setTotalNumber(model.getAssignNumber());
            ti.setRemainNumber(model.getAssignNumber());
        }
        if(m.containsKey("docentsTime")){
            String dt=m.get("docentsTime").toString();
            model.setDocentsTime(dt);
            ti.setDocentTime(dt);
        }
        TDocentsScheduleAssign ms=dotRepository.findByDocentsTimeAndScheduleAndDocents(model.getDocentsTime(),model.getSchedule(),model.getDocents());
         if(ms!=null && !ms.getId().equals(model.getId())){
                   rm=raiseError(2000);
                   rm.put("success", false);
                   return add(rm);
         }
       if(m.containsKey("anum")){
           int anum=(int)m.get("anum");
          ti.setTotalNumber(ti.getTotalNumber()-model.getAssignNumber()+anum);
          ti.setRemainNumber(ti.getRemainNumber()-model.getAssignNumber()+anum);
           model.setAssignNumber(anum);
       }
       dotRepository.save(model);
       doddRepository.save(ti);
       rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
       return add(rm);
    }

    /**
     * Delete docent schedule assign.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map dot_delete(Map map) {
        Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        TDocentsScheduleAssign org=dotRepository.findOne(id);
//        List as=uddeRepository.findByOrganization(org);
//        if(as!=null && !as.isEmpty()){
//                    rm=raiseError(2007);
//                    rm.put("success", false);
//                    return add(rm);
//         }
        try{
            dotRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read user docent.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map dodd_read(Map model) {
        Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                String did=(String) map.get("did");
                TDocentsSchedule ss=doiRepository.findOne(Long.valueOf(did));
                List<TUserDocents> data=dodRepository.findByScenic(ss.getScenic());
              List lst=new ArrayList();
                for(TUserDocents air:data){
                        Map mm=new HashMap();
                        mm.put("id", air.getId().toString());
                        mm.put("name", air.getDocentsName());
                        lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }
}
