/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.impl.pingtai;


import com.wwzz.services.api.BookingServices;
import com.wwzz.services.api.pingtai.SysServices;
import com.wwzz.services.domain.admin.TStaff;
import com.wwzz.services.domain.admin.TSystemProperty;
import com.wwzz.services.domain.booking.TUserBookingOrder;
import com.wwzz.services.domain.booking.TUserOrderDocents;
import com.wwzz.services.domain.booking.TUserOrderDriver;
import com.wwzz.services.domain.booking.TUserOrderLocalGuide;
import com.wwzz.services.domain.booking.TUserOrderLongGuide;
import com.wwzz.services.domain.booking.TUserOrderProcessCharge;
import com.wwzz.services.domain.booking.TUserOrderRestaurant;
import com.wwzz.services.domain.booking.TUserOrderShop;
import com.wwzz.services.domain.booking.TUserOrderTicket;
import com.wwzz.services.domain.users.TUserBill;
import com.wwzz.services.domain.users.TUserIdentityLongGuide;
import com.wwzz.services.domain.users.TUserPayAccount;
import com.wwzz.services.impl.ResultDataServices;
import com.wwzz.services.repository.TStaffRepository;
import com.wwzz.services.repository.TSystemPropertyRepository;
import com.wwzz.services.repository.TUserBillRepository;
import com.wwzz.services.repository.TUserBookingOrderRepository;
import com.wwzz.services.repository.TUserBookingTempOrderRepository;
import com.wwzz.services.repository.TUserIdentityLongGuideRepository;
import com.wwzz.services.repository.TUserNoticeRepository;
import com.wwzz.services.repository.TUserOrderDocentsRepository;
import com.wwzz.services.repository.TUserOrderDriverRepository;
import com.wwzz.services.repository.TUserOrderLocalGuideRepository;
import com.wwzz.services.repository.TUserOrderLongGuideRepository;
import com.wwzz.services.repository.TUserOrderOGuideRepository;
import com.wwzz.services.repository.TUserOrderProcessRepository;
import com.wwzz.services.repository.TUserOrderRestaurantRepository;
import com.wwzz.services.repository.TUserOrderShopRepository;
import com.wwzz.services.repository.TUserOrderTicketRepository;
import com.wwzz.services.repository.TUserPayAccountRepository;
import com.wwzz.services.repository.TUserRepository;
import com.wwzz.services.utils.Constants;
import static com.wwzz.services.utils.Constants.RETURN_SUCCESS_CODE;
import com.wwzz.services.utils.CheckRequest;
import com.wwzz.services.utils.Utils;
import com.wwzz.services.utils.WStringUtils;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author tiger
 * This class is a system data management service that do process system data such as password,system property etc.
 */
@Service
@Transactional
public class SysDataProcess extends ResultDataServices implements SysServices, Constants{

    
    @Value("#{serverinfo['apps.key']}")
    private String key;
    
    @Autowired
    private TSystemPropertyRepository rawRepository;
    
    @Autowired
    private TStaffRepository staRepository;
    
    @Value("#{serverinfo['excel.log.dir']}")
    private String log_dir;
    
     @Autowired
    private TUserBookingOrderRepository boRepository;
    
    @Autowired
    private TUserOrderOGuideRepository ogRepository;
    
    @Autowired
    private TUserOrderLongGuideRepository orderLGRepository;
    
    @Autowired
    private TUserBookingTempOrderRepository botRepository;
    
    @Autowired
    private TUserOrderLocalGuideRepository olocalRepository;
    
    @Autowired
    private TUserOrderDriverRepository odriverRepository;
    
    @Autowired
    private TUserOrderDocentsRepository odocentRepository;
    
    @Autowired
    private TUserOrderRestaurantRepository oresRepsoitory;
    
    @Autowired
    private TUserOrderShopRepository oshopRepository;
    
    @Autowired
    private TUserOrderTicketRepository otRepository;
    
    @Autowired
    private TUserRepository userRepository;
    
    @Autowired
    private TUserNoticeRepository noticeRepository;
    
    @Autowired
    private TUserOrderProcessRepository opRepository;
    
    @Autowired
    private TUserIdentityLongGuideRepository userLongRepository;
    
    @Autowired
    private TUserBillRepository billRepository;
    
    @Autowired
    private TUserPayAccountRepository payRepository;
    
    @Autowired
    private BookingServices bookService;
    
    /**
     * Change staff password.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map pass_add(Map map) {
        Map model=(Map) map.get("data");
        Map rm=new HashMap();
        try{
            String name=(String) model.get("name");
            String opass=(String) model.get("opass");
            String npass=(String) model.get("npass");
            TStaff sta=staRepository.findByMobile(name);
            opass=clientApp.get_uid((String)map.get("partner"), opass);
            npass=clientApp.get_uid((String)map.get("partner"), npass);
            if(!sta.getDecryptPassword().equals(opass)){
                rm=raiseError(2009);
                rm.put("success", false);
                return rm;
            }
            sta.setSalt(WStringUtils.randomString(15));
            sta.encryptPassword(npass);
            staRepository.save(sta);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception ex){
             rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read system property.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map raw_read(Map model) {
         Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                String key=(String) map.get("key");
                boolean flag=(boolean) map.get("flag");
                List<TSystemProperty> list;
                int total=0;
                if(flag){
                    Pageable page=new PageRequest(0,10);
                    list=rawRepository.findByKeyContaining("booking", page).getContent();
                }else{
                    int start=(int) map.get("page");
                    if(start<0)return rm;
                    int count=15;
                    if(key==null)key="";
                    Pageable page=new PageRequest(start-1, count);
                    total=rawRepository.countByKeyContaining(key).intValue()-2;
                    if(total<(start-1)*count){
                          page=new PageRequest(0, count);
                    }
                   Page<TSystemProperty> dt=null;
                   dt=rawRepository.findByKeyContaining(key, page);
                   list=dt.getContent();
                }
                List lst=new ArrayList();
                for(TSystemProperty air:list){
                    if(!flag && air.getKey().contains("booking"))continue;
                    Map mm=new HashMap();
                    mm.put("id", air.getId().toString());
                    mm.put("key", air.getKey());
                    mm.put("value", air.getValue());
                    mm.put("description", air.getDescription());
                    mm.put("description_en", air.getDescriptionEn());
                    lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
                rm.put("total", total);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     *  Update system property.
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map raw_update(Map map) {
          Map m=(Map) map.get("data");
         Map rm=new HashMap();
         try{
                Long id=Long.valueOf(m.get("id").toString());
                TSystemProperty sta=rawRepository.findOne(id);
                if(m.containsKey("value")){
                    String name=m.get("value").toString();
                   sta.setValue(name);
                }
              if(m.containsKey("description")){
                  String description=(String) m.get("description");
                 sta.setDescription(description);
              }
               if(m.containsKey("description_en")){
                   String description_en=(String) m.get("description_en");
                   sta.setDescriptionEn(description_en);
               }
                rawRepository.save(sta);
                clientApp.load_system();
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         }catch(Exception e){
             rm=raiseError(3000);
             rm.put("success", false);
         }
         return add(rm);
    }

    /**
     * Delete system property delete.
     * @param map HashMap
     * @return  map HashMap
     */
    @Override
    public Map raw_delete(Map map) {
        Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        try{
            rawRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }
    
    private Map add(Map map){
        map.put("partner", "11111111111111111111");
        Calendar cal=Calendar.getInstance();
        cal.setTime(new Date());
        map.put("timestamp", cal.getTime().getTime());
        CheckRequest cr=new CheckRequest(key, map);
        map.put("sign", cr.hashParams());
        return map;
    }

    /**
     * Read statistics data.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map stc_read(Map model) {
           Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                String key=(String) map.get("key");
                int flag=(int) map.get("flag");
                String stime=(String) map.get("st");
                String etime=(String) map.get("et");
                Calendar scal=Calendar.getInstance();
                Calendar ecal=Calendar.getInstance();
                if(stime!=null && !stime.isEmpty()){
                    stime=stime.split("T")[0];
                    Date dd=WStringUtils.stringToDate(stime, "-");
                    scal.setTime(dd);
                }else{
                    scal.set(Calendar.YEAR, 2015);
                    scal.set(Calendar.MONTH, 7);
                }
//                scal.set(Calendar.HOUR, 0);scal.set(Calendar.MINUTE, 0);scal.set(Calendar.SECOND, 0);
                if(etime!=null && !etime.isEmpty()){
                    etime=etime.split("T")[0];
                    Date dt=WStringUtils.stringToDate(etime, "-");
                    ecal.setTime(dt);
                }else{
                    ecal.setTime(new Date());
                    ecal.set(Calendar.DATE, ecal.get(Calendar.DATE)+1);
                }
//                ecal.set(Calendar.HOUR, 0);ecal.set(Calendar.MINUTE, 0);ecal.set(Calendar.SECOND, 0);
                Integer start=(Integer) map.get("page");
                if(start==null || start<0)start=1;
                int count=15;
               switch(flag){
                   case 1:
                       rm=stc1(scal.getTime(),ecal.getTime(),start,count,false);
                       break;
                   case 2:
                       rm=stc2(scal.getTime(),ecal.getTime(),start,count,false);
                       break;
                   case 3:
                       rm=stc3(scal.getTime(),ecal.getTime(),start,count,false);
                       break;
                   case 4:
                       rm=stc4(scal.getTime(),ecal.getTime(),start,count,false);
                       break;
                   case 5:
                       rm=stc5(scal.getTime(),ecal.getTime(),start,count,false);
                       break;
                   case 6:
                       rm=stc6(scal.getTime(),ecal.getTime(),start,count,false);
                       break;
                   case 7:
                       rm=stc7(scal.getTime(),ecal.getTime(),start,count,false);
                       break;
                   case 8:
                       rm=stc8(scal.getTime(),ecal.getTime(),start,count,false);
                       break;
                   case 9:
                       rm=stc9(scal.getTime(),ecal.getTime(),start,count,false);
                       break;
                   case 10:
                       rm=stc10(scal.getTime(),ecal.getTime(),start,count,false);
                       break;
                   case 11:
                       rm=stc11(scal.getTime(),ecal.getTime(),start,count,false);
                       break;
                   case 12:
                       rm=stc12(scal.getTime(),ecal.getTime(),start,count,false);
                       break;
               }
                
                
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }
    
    private Map stc1(Date s,Date e,int start,int count,boolean flag){
        Map map=new HashMap();
        
        Pageable page=new PageRequest(start-1, count);
        Page<TUserBookingOrder> dt=null;
        String sender="%%";
        int total=boRepository.count_worder(sender, s, e, 7);
        if(total<(start-1)*count){
               page=new PageRequest(0, count);
        }
        if(flag)page=new PageRequest(0, total);
        dt=boRepository.find_worder(sender, s, e, 7, page);
        List<TUserBookingOrder> list=dt.getContent();
        List lst=new ArrayList();
        if(flag){
            Map m=new HashMap();
            m.put("a1", "国家");
            m.put("a2", "城市");
            m.put("a3", "景点名字");
            m.put("a4", "下单来源");
            m.put("a5", "预约时间");
            m.put("a6", "支付时间");
            m.put("a7", "下单ip");
            m.put("a8", "支付金额");
            m.put("a9", "下单量");
            m.put("a10", "当前状态");
            m.put("a11", "出行日期");
            m.put("a12", "提前预定天数");
            m.put("a13", "订单编号");
            lst.add(m);
        }
        for(TUserBookingOrder k:list){
            Map m=new HashMap();
            TUserOrderTicket ot=otRepository.findByBooking(k);
            m.put("a1", k.getArea().getParent().getName1());
            m.put("a2", k.getArea().getName1());
            m.put("a3", ot.getScenic().getNameZh());
            m.put("a4", k.getClient());
            m.put("a5", WStringUtils.dateToString(k.getApplicationTime()));
            if(k.getServiceTime()!=null)m.put("a6", WStringUtils.dateToString(k.getServiceTime()));
            m.put("a7", "");
            m.put("a8", k.getOrderAmount());
            m.put("a9", 1);
            m.put("a10", BOOKING_STATES[k.getStatus()-1]);
            if(k.getTradedTime()!=null)m.put("a11", WStringUtils.dateToString(k.getTradedTime()));
            if(ot.getEnterTime()!=null){
                Long days=(ot.getEnterTime().getTime()-k.getApplicationTime().getTime())/(24*3600*1000);
                m.put("a12", days.intValue());
            }else m.put("a12", "");
            m.put("a13", k.getSerialNumber());
            lst.add(m);
        }
        map=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        map.put("data", lst);
         map.put("total", total);
        return map;
    }
    
    private Map stc2(Date s,Date e,int start,int count,boolean flag){
        Map map=new HashMap();
        Pageable page=new PageRequest(start-1, count);
        Page<TUserBookingOrder> dt=null;
        String sender="%%";
        int total=boRepository.count_worder(sender, s, e, 8);
        if(total<(start-1)*count){
               page=new PageRequest(0, count);
        }
        if(flag)page=new PageRequest(0, total);
        dt=boRepository.find_worder(sender, s, e, 8, page);
        List<TUserBookingOrder> list=dt.getContent();
        List lst=new ArrayList();
         if(flag){
            Map m=new HashMap();
            m.put("a1", "国家");
            m.put("a2", "城市");
            m.put("a3", "餐厅名字");
            m.put("a4", "预约时间");
            m.put("a5", "用餐人数");
            m.put("a6", "下单人所属旅行社");
            m.put("a7", "下单人名称");
            m.put("a8", "下单来院");
            m.put("a9", "清真餐数");
            m.put("a11", "素食数");
            lst.add(m);
        }
        for(TUserBookingOrder k:list){
            Map m=new HashMap();
            TUserOrderRestaurant or=oresRepsoitory.findByBooking(k);
            m.put("a1", k.getArea().getParent().getName1());
            m.put("a2", k.getArea().getName1());
            m.put("a3", or.getRestaurant().getNameZh());
            m.put("a4", WStringUtils.dateToString(k.getApplicationTime()));
            m.put("a5", or.getAdultsNumber());//?
            
            if(k.getApplicant().getIdentity()!=null && k.getApplicant().getIdentity().isHasIdentityLongGuide()){
                TUserIdentityLongGuide lg=userLongRepository.findByUserAndStatus(k.getApplicant(), 1);
                m.put("a6", lg.getTravelAgency().getNameZh());
            }else m.put("a6", "");
            m.put("a7", k.getApplicant().getName());
            m.put("a8", k.getClient());
            m.put("a9", or.getHalalNumber());
            m.put("a11", or.getVegetarianNumber());
            lst.add(m);
        }
        map=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        map.put("data", lst);
         map.put("total", total);
        return map;
    }
    
    private Map stc3(Date s,Date e,int start,int count,boolean flag){
        Map map=new HashMap();
        Pageable page=new PageRequest(start-1, count);
        Page<TUserBookingOrder> dt=null;
        String sender="%%";
        int total=boRepository.count_worder(sender, s, e, 9);
        if(total<(start-1)*count){
               page=new PageRequest(0, count);
        }
        if(flag)page=new PageRequest(0, total);
        dt=boRepository.find_worder(sender, s, e, 9, page);
        List<TUserBookingOrder> list=dt.getContent();
        List lst=new ArrayList();
         if(flag){
            Map m=new HashMap();
            m.put("a1", "国家");
            m.put("a2", "城市");
            m.put("a3", "商店名称");
            m.put("a4", "预约时间");
            m.put("a5", "下单人所属旅行社");
            m.put("a6", "下单人名称");
            m.put("a7", "下单来院");
            m.put("a8", "人数");
            m.put("a9", "进店时间");
            lst.add(m);
        }
        for(TUserBookingOrder k:list){
            Map m=new HashMap();
            TUserOrderShop or=oshopRepository.findByBooking(k);
            m.put("a1", k.getArea().getParent().getName1());
            m.put("a2", k.getArea().getName1());
            m.put("a3", or.getShop().getNameZh());
            m.put("a4", WStringUtils.dateToString(k.getApplicationTime()));
            if(k.getApplicant().getIdentity()!=null && k.getApplicant().getIdentity().isHasIdentityLongGuide()){
                TUserIdentityLongGuide lg=userLongRepository.findByUserAndStatus(k.getApplicant(), 1);
                m.put("a5", lg.getTravelAgency().getNameZh());
            }else   m.put("a5", "");
            m.put("a6", k.getApplicant().getName());
            m.put("a7", k.getClient());
            m.put("a8", or.getBookingNumber());
            m.put("a9", WStringUtils.dateToString(or.getBeginTime()));
            lst.add(m);
        }
        map=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        map.put("data", lst);
         map.put("total", total);
        return map;
    }
    
      private Map stc5(Date s,Date e,int start,int count,boolean flag){
        Map map=new HashMap();
        Pageable page=new PageRequest(start-1, count);
        Page<TUserBookingOrder> dt=null;
        String sender="%%";
        int total=boRepository.count_worder(sender, s, e, 10);
        if(total<(start-1)*count){
               page=new PageRequest(0, count);
        }
        if(flag)page=new PageRequest(0, total);
        dt=boRepository.find_worder(sender, s, e, 10, page);
        List<TUserBookingOrder> list=dt.getContent();
        List lst=new ArrayList();
         if(flag){
            Map m=new HashMap();
            m.put("a1", "国家");
            m.put("a2", "城市");
            m.put("a3", "时间段");
            m.put("a4", "所属当地车公司");
            m.put("a5", "支付时间");
            m.put("a6", "下单人所属旅行社");
            m.put("a7", "下单人名称");
            lst.add(m);
        }
        for(TUserBookingOrder k:list){
            Map m=new HashMap();
            TUserOrderProcessCharge or=opRepository.findByBooking(k);
            m.put("a1", k.getArea().getParent().getName1());
            m.put("a2", k.getArea().getName1());
            if(or.getProcessCharge()!=null)m.put("a3", or.getProcessCharge().getNameZh());
            else m.put("a3", "");
            m.put("a4", or.getCompanyTax());
            if(k.getServiceTime()!=null)m.put("a5", WStringUtils.dateToString(k.getServiceTime()));
            m.put("a7", k.getApplicant().getName());
            if(k.getApplicant().getIdentity()!=null && k.getApplicant().getIdentity().isHasIdentityLongGuide()){
                TUserIdentityLongGuide lg=userLongRepository.findByUserAndStatus(k.getApplicant(), 1);
                m.put("a6", lg.getTravelAgency().getNameZh());
            }else   m.put("a6", "");
            lst.add(m);
        }
        map=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        map.put("data", lst);
         map.put("total", total);
        return map;
    }
      
      private Map stc4(Date s,Date e,int start,int count,boolean flag){
        Map map=new HashMap();
        Pageable page=new PageRequest(start-1, count);
        Page<TUserBookingOrder> dt=null;
        String sender="%%";
        int total=boRepository.count_worder(sender, s, e, 3);
        if(total<(start-1)*count){
               page=new PageRequest(0, count);
        }
        if(flag)page=new PageRequest(0, total);
        dt=boRepository.find_worder(sender, s, e, 3, page);
        List<TUserBookingOrder> list=dt.getContent();
        List lst=new ArrayList();
          if(flag){
            Map m=new HashMap();
            m.put("a1", "国家");
            m.put("a2", "城市");
            m.put("a3", "景点名称");
            m.put("a4", "预约时间");
            m.put("a5", "讲解时间");
            m.put("a6", "团性质");
            lst.add(m);
        }
        for(TUserBookingOrder k:list){
            Map m=new HashMap();
            TUserOrderDocents or=odocentRepository.findByBooking(k);
            m.put("a1", k.getArea().getParent().getName1());
            m.put("a2", k.getArea().getName1());
            m.put("a3", or.getScenic().getNameZh());
            m.put("a4", WStringUtils.dateToString(k.getApplicationTime()));
            if(or.getExplainTime()!=null)m.put("a5", or.getExplainTime());
            m.put("a6", SS_ORDER_TYPE[or.getType()-1]);
            lst.add(m);
        }
        map=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        map.put("data", lst);
         map.put("total", total);
        return map;
    }
      
     private Map stc6(Date s,Date e,int start,int count,boolean flag){
        Map map=new HashMap();
        Pageable page=new PageRequest(start-1, count);
        Page<TUserBookingOrder> dt=null;
        String sender="%%";
        int total=boRepository.count_worder(sender, s, e, 8);
        if(total<(start-1)*count){
               page=new PageRequest(0, count);
        }
        if(flag)page=new PageRequest(0, total);
        dt=boRepository.find_worder(sender, s, e, 8, page);
        List<TUserBookingOrder> list=dt.getContent();
        List lst=new ArrayList();
        if(flag){
            Map m=new HashMap();
            m.put("a1", "国家");
            m.put("a2", "城市");
            m.put("a3", "人数");
            m.put("a4", "小时数");
            m.put("a5", "预约时间");
            m.put("a6", "下单量");
            m.put("a7", "下单时间");
            m.put("a8", "提前约定天数");
            
            m.put("a10", "下单ip");
            lst.add(m);
        }
        for(TUserBookingOrder k:list){
            Map m=new HashMap();
            TUserOrderLocalGuide or=olocalRepository.findByBooking(k);
            m.put("a1", k.getArea().getParent().getName1());
            m.put("a2", k.getArea().getName1());
            m.put("a3", or.getBookingNumber());
            if(or.getEndTime()!=null){
                Long times=(or.getEndTime().getTime()-or.getBeginTime().getTime())/(1000*60);
                int min=times.intValue() % 60;
                int hour=(times.intValue()-min)/60;
                m.put("a4", min+":"+hour);
            }else m.put("a4", "");
            
            m.put("a5", WStringUtils.dateToString(k.getApplicationTime()));
            m.put("a6", 1);
            if(k.getServiceTime()!=null)m.put("a7", WStringUtils.dateToString(k.getServiceTime()));
            Long days=(or.getBeginTime().getTime()-k.getApplicationTime().getTime())/(24*3600*1000);
            m.put("a8", days.intValue());
            
            m.put("a10","" );//?
            lst.add(m);
        }
        map=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        map.put("data", lst);
         map.put("total", total);
        return map;
    }
     
     private Map stc7(Date s,Date e,int start,int count,boolean flag){
        Map map=new HashMap();
        Pageable page=new PageRequest(start-1, count);
        Page<TUserBookingOrder> dt=null;
        String sender="%%";
        int total=boRepository.count_worder(sender, s, e, 5);
        if(total<(start-1)*count){
               page=new PageRequest(0, count);
        }
        if(flag)page=new PageRequest(0, total);
        dt=boRepository.find_worder(sender, s, e, 5, page);
        List<TUserBookingOrder> list=dt.getContent();
        List lst=new ArrayList();
        if(flag){
            Map m=new HashMap();
            m.put("a1", "人数");
            m.put("a2", "小时数");
            m.put("a3", "预约时间");
            m.put("a4", "下单量");
            m.put("a5", "下单时间");
            m.put("a6", "提前约定天数");
            m.put("a7", "用车时间");
            m.put("a8", "车型");
            m.put("a9", "国家数");
            m.put("a10", "出发城市");
            m.put("a11", "返回城市");
            lst.add(m);
        }
        for(TUserBookingOrder k:list){
            Map m=new HashMap();
            TUserOrderDriver or=odriverRepository.findByBooking(k);
            m.put("a1", or.getBookingNumber());
            if(or.getBeginTime()==null || or.getEndTime()==null){
                m.put("a2", "");
            }else{
                Long times=(or.getEndTime().getTime()-or.getBeginTime().getTime())/(1000*60);
                int min=times.intValue() % 60;
                int hour=(times.intValue()-min)/60;
                m.put("a2", min+":"+hour);
            }
            m.put("a3", WStringUtils.dateToString(k.getApplicationTime()));
            m.put("a4", 1);
            if(k.getServiceTime()!=null)m.put("a5", WStringUtils.dateToString(k.getServiceTime()));
            else m.put("a5", "");
            if(or.getBeginTime()==null || k.getApplicationTime()==null){
                m.put("a6","");
            }else{
                Long days=(or.getBeginTime().getTime()-k.getApplicationTime().getTime())/(24*3600*1000);
                m.put("a6", days.intValue());
            }
            m.put("a7", WStringUtils.dateToString(or.getBeginTime()));
            if(or.getCarModel()!=null)m.put("a8",or.getCarModel().getModel());
            else m.put("a8", "");
            m.put("a9",or.getVia());
            if(or.getStartingCity()!=null)m.put("a10",or.getStartingCity().getName1());
            else m.put("a10", "");
            if(or.getEndCity()!=null)m.put("a11",or.getEndCity().getName1());
            else m.put("a11", "");
            lst.add(m);
        }
        map=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        map.put("data", lst);
         map.put("total", total);
        return map;
    }
     
      private Map stc8(Date s,Date e,int start,int count,boolean flag){
        Map map=new HashMap();
        Pageable page=new PageRequest(start-1, count);
        Page<TUserBookingOrder> dt=null;
        String sender="%%";
        int total=boRepository.count_worder(sender, s, e, 1);
        if(total<(start-1)*count){
               page=new PageRequest(0, count);
        }
        if(flag)page=new PageRequest(0, total);
        dt=boRepository.find_worder(sender, s, e, 1, page);
        List<TUserBookingOrder> list=dt.getContent();
        List lst=new ArrayList();
          if(flag){
            Map m=new HashMap();
            m.put("a1", "人数");
            m.put("a2", "天数");
            m.put("a3", "预约时间");
            m.put("a4", "下单量");
            m.put("a5", "下单时间");
            m.put("a6", "提前约定天数");
            m.put("a7", "国家数");
            m.put("a8", "出发城市");
            m.put("a9", "返回城市");
            m.put("a10", "团性质");
            lst.add(m);
        }
        for(TUserBookingOrder k:list){
            Map m=new HashMap();
            TUserOrderLongGuide or=orderLGRepository.findByBooking(k);
            m.put("a1", or.getBookingNumber());
            m.put("a2", or.getDayNumber());
            m.put("a3", WStringUtils.dateToString(k.getApplicationTime()));
            m.put("a4", 1);
            if(k.getServiceTime()!=null)m.put("a5", WStringUtils.dateToString(k.getServiceTime()));
            else m.put("a5", "");
             Long days=(or.getBeginTime().getTime()-k.getApplicationTime().getTime())/(24*3600*1000);
            m.put("a6", days.intValue());
            m.put("a7", or.getCountryNumber());
            if(or.getDepartureEntryCity()!=null)m.put("a8",or.getDepartureEntryCity().getName1());
            else m.put("a8", "");
            if(or.getDepartureExitCity()!=null)m.put("a9",or.getDepartureExitCity().getName1());
            else m.put("a9", "");
            m.put("a10",NATURE[or.getNature()-1]);
            lst.add(m);
        }
        map=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        map.put("data", lst);
         map.put("total", total);
        return map;
    }
      
      private Map stc9(Date s,Date e,int start,int count,boolean flag){
        Map map=new HashMap();
         int total=0;
        List lst=new ArrayList();
         if(flag){
            Map m=new HashMap();
            m.put("a1", "下单数");
            m.put("a2", "下单总额");
            m.put("a3", "订单类型");
//            m.put("a4", "时间");
            lst.add(m);
        }
//         Long bb=(e.getTime()-s.getTime())/(1000*3600*24);
//         int p=bb.intValue();
         
        for(int i=1;i<11;i++){
//            Date ss=s;
//            for(int k=0;k<p+1;k++){
                Map m=new HashMap();
//                Long bk=ss.getTime()+3600*24*1000-1000;
//                Date ee=new Date();
//                ee.setTime(bk);
                List<TUserBookingOrder> pp=boRepository.get_stcs(s, e, i);
                m.put("a1", pp.size());
                float sum=0;
                for(TUserBookingOrder o:pp){
                    if(o.getOrderAmount()!=null)sum=sum+o.getOrderAmount();
                }
                m.put("a2", sum);
                m.put("a3", BOOKING_CATEGORIES[i-1]);
//                m.put("a4", WStringUtils.dateToString(ss));
//                bk=bk+1000;
//                Date st=new Date();
//                st.setTime(bk);
//                ss=st;
//                if(pp.isEmpty())continue;
                lst.add(m);
//            }
        }
        map=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        map.put("data", lst);
         map.put("total", total);
        return map;
    }
      
        private Map stc10(Date s,Date e,int start,int count,boolean flag){
        Map map=new HashMap();
        int total=1;
        Long aa1=userRepository.countByTopicidIsNotNull();
        Long aa2=userRepository.countByDeviceidIsNotNull();
        Long aa3=userRepository.countByOpenidIsNotNull();
        List lst=new ArrayList();
        if(flag){
            Map m=new HashMap();
            m.put("a1", "android用户数");
            m.put("a2", "ios用户数");
            m.put("a3", "微信用户数");
            lst.add(m);
        }
        Map m=new HashMap();
        m.put("a1", aa1.intValue());
        m.put("a2", aa2.intValue());
        m.put("a3", aa3.intValue());
        lst.add(m);
        map=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        map.put("data", lst);
         map.put("total", total);
        return map;
    }
      
     private Map stc11(Date s,Date e,int start,int count,boolean flag){
        Map map=new HashMap();
        
        Pageable page=new PageRequest(start-1, count);
        Page<TUserBookingOrder> dt=null;
        String sender="%%";
        int total=boRepository.count_worder(sender, s, e, 7);
        if(total<(start-1)*count){
               page=new PageRequest(0, count);
        }
        if(flag)page=new PageRequest(0, total);
        dt=boRepository.find_worder(sender, s, e, 7, page);
        List<TUserBookingOrder> list=dt.getContent();
        List lst=new ArrayList();
        if(flag){
            Map m=new HashMap();
            m.put("a1", "国家");
            m.put("a2", "城市");
            m.put("a3", "景点名字");
            m.put("a4", "景点id");
            m.put("a5", "预约时间");
            m.put("a6", "有无讲解器");
            m.put("a7", "有无讲解员");
            lst.add(m);
        }
        for(TUserBookingOrder k:list){
            Map m=new HashMap();
            TUserOrderTicket ot=otRepository.findByBooking(k);
            m.put("a1", k.getArea().getParent().getName1());
            m.put("a2", k.getArea().getName1());
            m.put("a3", ot.getScenic().getNameZh());
            m.put("a4", ot.getScenic().getId().toString());
            m.put("a5", WStringUtils.dateToString(k.getApplicationTime()));
            m.put("a6", "");
            m.put("a7", ot.getScenic().getHasDocents()==1?"有":"无");
            lst.add(m);
        }
        map=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        map.put("data", lst);
         map.put("total", total);
        return map;
    }
     
     private Map stc12(Date s,Date e,int start,int count,boolean flag){
        Map map=new HashMap();
        
        Pageable page=new PageRequest(start-1, count);
        Page<TUserBookingOrder> dt=null;
        String sender="%%";
        int total=boRepository.count_worder(sender, s, e, 7);
        if(total<(start-1)*count){
               page=new PageRequest(0, count);
        }
        if(flag)page=new PageRequest(0, total);
        dt=boRepository.find_worder(sender, s, e, 7, page);
        List<TUserBookingOrder> list=dt.getContent();
        List lst=new ArrayList();
        if(flag){
            Map m=new HashMap();
            m.put("a1", "国家");
            m.put("a2", "城市");
            m.put("a3", "景点名字");
            m.put("a4", "景点id");
            m.put("a5", "下单张数");
            m.put("a6", "流水");
            m.put("a7", "预约时间");
            m.put("a8", "支付时间");
            lst.add(m);
        }
        for(TUserBookingOrder k:list){
            Map m=new HashMap();
            TUserOrderTicket ot=otRepository.findByBooking(k);
            m.put("a1", k.getArea().getParent().getName1());
            m.put("a2", k.getArea().getName1());
            m.put("a3", ot.getScenic().getNameZh());
            m.put("a4", ot.getScenic().getId().toString());
            m.put("a5", 1);
            m.put("a6", "");
            m.put("a7", WStringUtils.dateToString(k.getApplicationTime()));
            if(k.getServiceTime()!=null)m.put("a8", WStringUtils.dateToString(k.getServiceTime()));
            lst.add(m);
        }
        map=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        map.put("data", lst);
         map.put("total", total);
        return map;
    }

     /**
      * Download statistics excel data.
      * @param model HashMap
      * @return map HashMap
      */
    @Override
    public Map ste_post(Map model) {
           Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                String key=(String) map.get("key");
                int flag=(int) map.get("flag");
                String stime=(String) map.get("st");
                String etime=(String) map.get("et");
                Calendar scal=Calendar.getInstance();
                Calendar ecal=Calendar.getInstance();
                if(stime!=null && !stime.isEmpty()){
                    stime=stime.split("T")[0];
                    Date dd=WStringUtils.stringToDate(stime, "-");
                    scal.setTime(dd);
                }else{
                    scal.set(Calendar.YEAR, 2014);
                }
                if(etime!=null && !etime.isEmpty()){
                    etime=etime.split("T")[0];
                    Date dt=WStringUtils.stringToDate(etime, "-");
                    ecal.setTime(dt);
                }else{
                    ecal.setTime(new Date());
                    ecal.set(Calendar.YEAR, ecal.get(Calendar.YEAR)+10);
                }
                int start=1;
                int count=15;
               switch(flag){
                   case 1:
                       rm=stc1(scal.getTime(),ecal.getTime(),start,count,true);
                       break;
                   case 2:
                       rm=stc2(scal.getTime(),ecal.getTime(),start,count,true);
                       break;
                   case 3:
                       rm=stc3(scal.getTime(),ecal.getTime(),start,count,true);
                       break;
                   case 4:
                       rm=stc4(scal.getTime(),ecal.getTime(),start,count,true);
                       break;
                   case 5:
                       rm=stc5(scal.getTime(),ecal.getTime(),start,count,true);
                       break;
                   case 6:
                       rm=stc6(scal.getTime(),ecal.getTime(),start,count,true);
                       break;
                   case 7:
                       rm=stc7(scal.getTime(),ecal.getTime(),start,count,true);
                       break;
                   case 8:
                       rm=stc8(scal.getTime(),ecal.getTime(),start,count,true);
                       break;
                   case 9:
                       rm=stc9(scal.getTime(),ecal.getTime(),start,count,true);
                       break;
                   case 10:
                       rm=stc10(scal.getTime(),ecal.getTime(),start,count,true);
                       break;
                   case 11:
                       rm=stc11(scal.getTime(),ecal.getTime(),start,count,true);
                       break;
                   case 12:
                       rm=stc12(scal.getTime(),ecal.getTime(),start,count,true);
                       break;
                       
               }
               String filepath=log_dir+System.currentTimeMillis()+".xls";
               File file=new File(filepath);
                WritableWorkbook wworkbook = Workbook.createWorkbook(file);
                WritableSheet wsheet = wworkbook.createSheet("First Sheet", 0);
               List<Map> m=(List<Map>) rm.get("data");
               int kk=0;
               for(Map pm:m){
                   int pk=0;
                   for (Iterator iterator = pm.keySet().iterator(); iterator.hasNext();) {
                           String ky=pm.get(iterator.next()).toString();
                            Label label=new Label(pk, kk,ky);
                            wsheet.addCell(label);
                       pk++;
                   }
                   kk++;
               }
               wworkbook.write();
               wworkbook.close();
               String fid=file_upload_on_fastdfs(file);
               rm.put("success", true);
               rm.put("fid", fid);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read user bill data.
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map bill_read(Map model) {
         Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                int flag=(int) map.get("flag");
                int status=(int)map.get("status");
                Integer start=(Integer) map.get("page");
                if(start==null || start<0)start=1;
                int count=15;
                 Pageable page=new PageRequest(start-1, count);
                Page<TUserBill> dt=null;
                Long total=billRepository.countByPaymentMethodAndStatus(flag,status);
                if(total<(start-1)*count){
                     page=new PageRequest(0, count);
                }
               dt=billRepository.findByPaymentMethodAndStatus(flag,status,page);
               List<TUserBill> list=dt.getContent();
               List lst=new ArrayList();
               for(TUserBill bill:list){
                   Map m=new HashMap();
                   m.put("id", bill.getId().toString());
                   m.put("username", bill.getUser().getName());
                   m.put("user_id", bill.getUser().getId().toString());
                   m.put("snum", bill.getSerialNumber());
                   m.put("oname", bill.getOrderName());
                   m.put("booking_id", bill.getBooking().getId().toString());
                   m.put("otime", WStringUtils.dateToString(bill.getOrderTime()));
                   m.put("pay_method", bill.getPaymentMethod());
                   m.put("pay_account", bill.getPaymentAccount());
                   m.put("ptime", WStringUtils.dateToString(bill.getPaymentTime()));
                   m.put("psnum", bill.getPaymentSerialNumber());
                   m.put("status", bill.getStatus());
                   m.put("total", bill.getTotalAmount());
                   m.put("refund", bill.getRefundAmount());
                   m.put("ptype", bill.getPaymentType());
                   m.put("xia", bill.getClient());
                   lst.add(m);
               }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
                rm.put("total", total);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Do process user payment
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map pay_post(Map model) {
        Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                String nid=map.get("nid").toString().substring(1);
                String[] st=nid.split(",");
                Calendar cal=Calendar.getInstance();
                SimpleDateFormat df=new SimpleDateFormat("yyyyMMdd");
//                String ds=String.valueOf(cal.get(Calendar.YEAR))+String.valueOf(cal.get(Calendar.MONTH)+1)+String.valueOf(cal.get(Calendar.DATE));
                Long ff=System.currentTimeMillis()/1000;
                String ds=df.format(new Date())+ff.intValue();
                Map m=new HashMap();
                m.put("batch_no", ds);
                m.put("batch_num", String.valueOf(st.length));
                float sum=0;
                StringBuilder de=new StringBuilder();
                String ids="";
//                "18811085356@163.com"
                for(int i=0;i<st.length;i++){
                    TUserBill bill=billRepository.findOne(Long.valueOf(st[i]));
                    TUserPayAccount pay=payRepository.findByUser(bill.getUser());
                    if(pay==null)continue;
                    ids="-"+bill.getId().toString();
                    if(bill.getTotalAmount()!=null)sum=sum+bill.getTotalAmount();
                    de.append("|").append(bill.getId().toString()).append("^").append(pay.getAlipay()).append("^").
                            append(pay.getAlipayName()).append("^").append(bill.getTotalAmount()).append("^").append(BOOKING_CATEGORIES[bill.getBooking().getCategory()-1]);
                }
                
                String sss=clientApp.set_uid((String)model.get("partner"), ids.substring(1));
                String notify_url=clientApp.getSystemProperty("alipay.notify.url")+(String)model.get("partner")+"/"+sss;
                m.put("batch_fee", Utils.priceToString(sum));
                m.put("detail_data", de.toString().substring(1));
                m.put("notify_url", notify_url);
                rm=bookService.alipay_batch_process(m);
                rm.put("success", true);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }
}
