/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.impl.pingtai;

import com.wwzz.services.api.pingtai.PartDataServices;
import com.wwzz.services.domain.admin.TAuthority;
import com.wwzz.services.domain.admin.TDepartment;
import com.wwzz.services.domain.admin.TRole;
import com.wwzz.services.domain.admin.TStaff;
import com.wwzz.services.impl.ResultDataServices;
import com.wwzz.services.repository.TAuthorityRepository;
import com.wwzz.services.repository.TDepartmentRepository;
import com.wwzz.services.repository.TRoleRepository;
import com.wwzz.services.repository.TStaffRepository;
import com.wwzz.services.utils.Constants;
import static com.wwzz.services.utils.Constants.RETURN_ERROR_CODE;
import static com.wwzz.services.utils.Constants.RETURN_SUCCESS_CODE;
import com.wwzz.services.utils.CheckRequest;
import com.wwzz.services.utils.WStringUtils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author tiger
 * This class is a part management service class that do part management process.
 */
@Service
@Transactional
public class PartDataProcess extends ResultDataServices implements PartDataServices, Constants{
    
     @Value("#{serverinfo['apps.key']}")
    private String key;
    
    @Autowired
    private TAuthorityRepository authRepository;
    
    @Autowired
    private TRoleRepository roleRepository;
    
    @Autowired
    private TStaffRepository staRepository;
    
    @Autowired
    private TDepartmentRepository devRepository;
    
    private Map add(Map map){
        map.put("partner", "11111111111111111111");
        Calendar cal=Calendar.getInstance();
        cal.setTime(new Date());
        map.put("timestamp", cal.getTime().getTime());
        CheckRequest cr=new CheckRequest(key, map);
        map.put("sign", cr.hashParams());
        return map;
    }
   
    /**
     * Add staff authorities. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map auth_add(Map map) {
        Map rm=new HashMap();
        try{
                Map m=(Map) map.get("data");
                String mark=(String) m.get("mark");
                TAuthority auth=new TAuthority();
               if(m.containsKey("name"))auth.setName((String) m.get("name"));
               auth.setMark(mark);
                if(m.containsKey("chain"))auth.setChain((String) m.get("chain"));
                if(m.containsKey("sort"))auth.setSort((int) m.get("sort"));
                if(m.containsKey("level"))auth.setLevel((int) m.get("level"));
                if(m.containsKey("custom"))auth.setCustom((int) m.get("custom"));
                auth.setModified(new Date());
                authRepository.save(auth);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                Map mmm=new HashMap();
                mmm.put("id", auth.getId());
                rm.put("data", mmm);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Delete staff authorities delete. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map auth_delete(Map map) {
         Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        try{
            List as=roleRepository.findByBid(id);
            if(as!=null && !as.isEmpty()){
                rm=raiseError(2007);
                rm.put("success", false);
                return add(rm);
            }
            authRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read staff authorities. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map auth_read(Map model) {
        Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                String pid=(String) map.get("rid");
                List<TAuthority> list;
                if(pid==null || pid.isEmpty()){
                    list=authRepository.findAll();
                }else{
                    TRole role=roleRepository.findOne(Long.valueOf(pid));
                    list=role.getAuthorities();
                }
                List lst=new ArrayList();
                for(TAuthority air:list){
                    Map mm=new HashMap();
                    mm.put("id", air.getId().toString());
                    mm.put("name", air.getName());
                    mm.put("mark", air.getMark());
                    mm.put("chain", air.getChain());
                    mm.put("sort", air.getSort());
                    mm.put("level", air.getLevel());
                    mm.put("custom", air.getCustom());
                    lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Update staff authorities. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map auth_update(Map map) {
        Map m=(Map) map.get("data");
         Map rm=new HashMap();
         try{
                Long id=Long.valueOf((String)m.get("id"));
                TAuthority auth=authRepository.findOne(id);
                if(m.containsKey("name"))auth.setName((String) m.get("name"));
               if(m.containsKey("mark")){
                   String mark=(String) m.get("mark");
                   auth.setMark(mark);
               }
                if(m.containsKey("chain"))auth.setChain((String) m.get("chain"));
                if(m.containsKey("sort"))auth.setSort((int) m.get("sort"));
                if(m.containsKey("level"))auth.setLevel((int) m.get("level"));
                if(m.containsKey("custom"))auth.setCustom((int) m.get("custom"));
                auth.setModified(new Date());
                authRepository.save(auth);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         }catch(Exception e){
             rm=createResult(RETURN_ERROR_CODE, "operation.fail", 500, "operation.fail", "false");
         }
         return add(rm);
    }

    /**
     * Read staff authorities. 
     * @param model
     * @return map
     */
    @Override
    public Map authc_read(Map model) {
        Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                List<TAuthority>  list=authRepository.findAll();
                List lst=new ArrayList();
                for(TAuthority air:list){
                    Map mm=new HashMap();
                    mm.put("id", air.getId().toString());
                    mm.put("name", air.getName());
                    mm.put("mark", air.getMark());
                    mm.put("chain", air.getChain());
                    mm.put("sort", air.getSort());
                    mm.put("level", air.getLevel());
                    mm.put("custom", air.getCustom());
                    lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Add staff roles. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map role_add(Map map) {
        Map rm=new HashMap();
        try{
                Map m=(Map) map.get("data");
                String name=(String) m.get("name");
                String sname=(String) m.get("ssname");
                if(sname==null || sname.isEmpty()){
                    rm=raiseError(2008);
                    rm.put("success", false);
                    return add(rm);
                }
                TStaff sta=staRepository.findByName(sname);
                TRole role=roleRepository.findByName(name);
                if(role!=null){
                    rm=raiseError(2000);
                    rm.put("success", false);
                    return add(rm);
                }
                role=new TRole();
                role.setName(name);
               if(m.containsKey("description"))role.setDescription((String) m.get("description"));
                if(m.containsKey("cas")){
                    List<String> cas=(List) m.get("cas");
                    List<TAuthority> auths=new ArrayList<>();
                        for(int i=0;i<cas.size();i++){
                            Long ii=Long.valueOf(cas.get(i));
                            TAuthority auth=authRepository.findOne(ii);
                            auths.add(auth);
                        }
                    role.setAuthorities(auths);
                }
                role.setCreated(new Date());
                role.setModified(new Date());
                
                role.setCreator(sta);
                role.setModifier(sta);
                roleRepository.save(role);
                clientApp.load_role();
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                Map mmm=new HashMap();
                mmm.put("id", role.getId());
                mmm.put("cname", "ok");
                mmm.put("mname", "tiger");
                mmm.put("modified", WStringUtils.dateToString(role.getModified()));
                rm.put("data", mmm);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }
    
    /**
     * Delete staff roles. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map role_delete(Map map) {
        Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        try{
            List<TDepartment> as=devRepository.findByBid(id);
            if(as!=null && !as.isEmpty()){
                rm=raiseError(2007);
                rm.put("success", false);
                return add(rm);
            }
            roleRepository.delete(id);
            
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read staff roles. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map role_read(Map model) {
        Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                String pid=(String) map.get("pid");
                if(pid==null)pid="";
                String sid=(String) map.get("sid");
                if(sid==null)sid="";
                List<TRole> list;
                if(pid.isEmpty() && sid.isEmpty()){
                    list=roleRepository.findAll();
                }
                else if(pid.isEmpty()){
                    TStaff sta=staRepository.findOne(Long.valueOf(sid));
                    list=sta.getRoles();
                }else{ 
                    TDepartment dev=devRepository.findOne(Long.valueOf(pid));
                    list=dev.getRoles();
                }
                
                List lst=new ArrayList();
                for(TRole air:list){
                    Map mm=new HashMap();
                    mm.put("id", air.getId().toString());
                    mm.put("name", air.getName());
                    mm.put("description", air.getDescription());
                    mm.put("mname", air.getModifier().getName());
                    mm.put("cname", air.getCreator().getName());
                    mm.put("modified", WStringUtils.dateToString(air.getModified()));
                    List<TAuthority> sss=air.getAuthorities();
                    List datas=new ArrayList();
                    for(TAuthority s:sss){
                        datas.add(s.getId().toString());
                    }
                    mm.put("cas", datas);
                    lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Update staff roles. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map role_update(Map map) {
        Map m=(Map) map.get("data");
         Map rm=new HashMap();
         try{
                String sname=(String) m.get("ssname");
                if(sname==null || sname.isEmpty()){
                    rm=raiseError(2008);
                    rm.put("success", false);
                    return add(rm);
                }
                TStaff sta=staRepository.findByName(sname);
                Long id=Long.valueOf((String)m.get("id"));
                TRole role=roleRepository.findOne(id);
                if(m.containsKey("name")){
                    String name=(String) m.get("name");
                    if(!name.equals(role.getName())){
                        TRole ro=roleRepository.findByName(name);
                        if(ro!=null){
                            rm=raiseError(2000);
                            rm.put("success", false);
                            return add(rm);
                        }else{
                            role.setName(name);
                        }
                    }
                }
                if(m.containsKey("description"))role.setDescription((String) m.get("description"));
                if(m.containsKey("cas")){
                    List<String> cas=(List) m.get("cas");
                    List<TAuthority> auths=new ArrayList<>();
                        for(int i=0;i<cas.size();i++){
                            Long ii=Long.valueOf(cas.get(i));
                            TAuthority auth=authRepository.findOne(ii);
                            auths.add(auth);
                        }
                    role.setAuthorities(auths);
                }
                 role.setModified(new Date());
                    
                role.setModifier(sta);
                
                roleRepository.save(role);
                clientApp.load_role();
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         }catch(Exception e){
             rm=raiseError(3000);
             rm.put("success", false);
         }
         return add(rm);
    }

    /**
     * Read staff roles. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map rolec_read(Map model) {
        Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                String pid=(String) map.get("pid");
                String sid=(String)map.get("sid");
                List<TRole> datas=new ArrayList<>();
                if(pid==null || pid.isEmpty()){
                    datas=roleRepository.findAll();
                }else {
                    TDepartment dev=devRepository.findOne(Long.valueOf(pid));
                    datas=dev.getRoles();
                }
                List lst=new ArrayList();
                for(TRole air:datas){
                    Map mm=new HashMap();
                    mm.put("id", air.getId().toString());
                    mm.put("name", air.getName());
                    mm.put("description", air.getDescription());
                    mm.put("mname", air.getModifier().getName());
                    mm.put("cname", air.getCreator().getName());
                    mm.put("modified", WStringUtils.dateToString(air.getModified()));
                    lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Add staff department. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map dev_add(Map map) {
        Map rm;
//        try{
                Map m=(Map) map.get("data");
                String name=(String) m.get("text");
                String sname=(String) m.get("ssname");
                if(sname==null || sname.isEmpty()){
                    rm=raiseError(2008);
                    rm.put("success", false);
                    return add(rm);
                }
                TStaff sta=staRepository.findByName(sname);
                TDepartment dev=devRepository.findByName(name);
                if(dev!=null){
                    rm=raiseError(2000);
                    rm.put("success", false);
                    return add(rm);
                }
                dev=new TDepartment();
                dev.setCreated(new Date());
                dev.setModified(new Date());
                dev.setName(name);
                dev.setCreator(sta);
                dev.setModifier(sta);
                dev.setDescription((String) m.get("description"));

                String pid=(String) m.get("parentId");
                TDepartment area;
                 if(pid.equals("-1")){
                     area=null;
                     dev.setLevel(1);
                 }
                 else {
                     area=devRepository.findOne(Long.valueOf(pid));
                     if(area.getParent()==null)dev.setLevel(2);
                     else dev.setLevel(3);
                 }
                dev.setParent(area);

                   if(m.containsKey("cas")){
                            List<String> cas=(List) m.get("cas");
                            List<TRole> auths=new ArrayList<>();
                                for(int i=0;i<cas.size();i++){
                                    Long ii=Long.valueOf(cas.get(i));
                                    TRole auth=roleRepository.findOne(ii);
                                    auths.add(auth);
                                }
                            dev.setRoles(auths);
                        }

                   devRepository.save(dev);
                    rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                    Map mmm=new HashMap();
                     mmm.put("id", dev.getId());
                     mmm.put("mname", dev.getModifier().getName());
                     mmm.put("modified", WStringUtils.dateToString(dev.getModified()));
                     rm.put("data", mmm);

        return add(rm);
    }

    /**
     * Read staff department. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map dev_read(Map map) {
        Map rm=new HashMap();
        Map kmap=(Map) map.get("data");
        String pid=(String)kmap.get("node");
        String key=(String) kmap.get("key");
        List<TDepartment> list;
       List lst=new ArrayList();
        if(key==null || key.trim().isEmpty()){
            if(pid==null || pid.equals("-1") || pid.isEmpty()){
                list=devRepository.findByParent(null);
            }else{
                TDepartment pa=devRepository.findOne(Long.valueOf(pid));
                list=devRepository.findByParent(pa);
            }
            for(TDepartment ar:list){
                lst.add(devMap(ar));
            }
        }else{
            Sort sort=new Sort(Sort.Direction.ASC, "level");
            list=devRepository.findByNameContaining(key,sort);
            Map<String,Map> vm=new HashMap();
            for(TDepartment a:list){
                switch(a.getLevel()){
                    case 1:
                        Map am=new HashMap();
                        am.put("name", a);
                        vm.put(a.getName(), am);
                        break;
                    case 2:{
                                if(vm.containsKey(a.getParent().getName())){
                                    Map cm=vm.get(a.getParent().getName());
                                    Map aam=new HashMap();
                                    aam.put("name", a);
                                    cm.put(a.getName(), aam);
                                    vm.put(a.getParent().getName(), cm);
                                }else{
                                    Map aam=new HashMap();
                                    aam.put("name", a);
                                    Map cm=new HashMap();
                                    cm.put(a.getName(), aam);
                                    cm.put("name", a.getParent());
                                    vm.put(a.getParent().getName(), cm);
                                }
                            }break;
                    case 3:{
                           if(vm.containsKey(a.getParent().getParent().getName())){
                               Map cm=vm.get(a.getParent().getParent().getName());
                               if(cm.containsKey(a.getParent().getName())){
                                   Map ccm=(Map) cm.get(a.getParent().getName());
                                   Map aam=new HashMap();
                                   aam.put("name", a);
                                   ccm.put(a.getName(), aam);
                                   cm.put(a.getParent().getName(), ccm);
                               }else{
                                   Map aam=new HashMap();
                                   aam.put("name", a);
                                   Map ccm=new HashMap();
                                   ccm.put("name", a.getParent());
                                   ccm.put(a.getName(),aam);
                                   cm.put(a.getParent().getName(),ccm);
                               }
                               vm.put(a.getParent().getParent().getName(), cm);
                           }else{
                               Map aam=new HashMap();
                               aam.put("name", a);
                               Map ccm=new HashMap();
                               ccm.put("name", a.getParent());
                               ccm.put(a.getName(), aam);
                               Map cm=new HashMap();
                               cm.put("name", a.getParent().getParent());
                               cm.put(a.getParent().getName(), ccm);
                               vm.put(a.getParent().getParent().getName(), cm);
                           }
                    }break;
                }
            }
            for(String s:vm.keySet()){
                Map sm=vm.get(s);
                TDepartment nm=(TDepartment) sm.get("name");
                Map m1=devMap(nm);
                m1.put("expanded", true);
                if(sm.keySet().size()>1){
                    List c2=new ArrayList();
                    for(Object sc:sm.keySet()){
                          if(sc.toString().equals("name"))continue;
                          Map scm=(Map) sm.get(sc);
                          TDepartment ns=(TDepartment) scm.get("name");
                          Map m2=devMap(ns);
                          if((boolean)m2.get("leaf")){
                              c2.add(m2);
                              continue;
                          }
                          m2.put("expanded", true);
                          if(scm.keySet().size()>1){
                              List c3=new ArrayList();
                              for(Object oo:scm.keySet()){
                                  if(oo.toString().equals("name"))continue;
                                  Map ssm=(Map) scm.get(oo);
                                  TDepartment np=(TDepartment) ssm.get("name");
                                  Map m3=devMap(np);
                                  c3.add(m3);
                              }
                              m2.put("children", c3);
                          }else{
                              if(!(boolean)m2.get("leaf"))m2.put("loaded", true);
                              m2.put("expanded", false);
                          }
                          c2.add(m2);
                    }
                    m1.put("children", c2);
                }else{
                    m1.put("loaded", true);
                }
                lst.add(m1);
            }
        }
        rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        rm.put("children", lst);
        rm.put("data", lst);
        return add(rm);
    }

    /**
     * Delete staff department. 
     * @param id String 
     * @return map HashMap
     */
    @Override
    public Map dev_delete(String id) {
         Map rm=new HashMap();
         try{
             TDepartment dev=devRepository.findOne(Long.valueOf(id));
             List<TStaff> as=staRepository.findByDepartment(dev);
             if(as!=null && !as.isEmpty()){
                rm=raiseError(2007);
                rm.put("success", false);
                return add(rm);
            }
            devRepository.delete(dev);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         }catch(Exception e){
             rm=raiseError(3000);
             rm.put("success", false);
         }
        return add(rm);
    }

    /**
     * Update staff department. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map dev_update(Map map) {
        Map m=(Map) map.get("data");
         Map rm=new HashMap();
         try{
                String sname=(String) m.get("ssname");
                if(sname==null || sname.isEmpty()){
                    rm=raiseError(2008);
                    rm.put("success", false);
                    return add(rm);
                }
                TStaff sta=staRepository.findByName(sname);
                String id=(String) m.get("id");
                TDepartment ta=devRepository.findOne(Long.valueOf(id));
                if((String)m.get("text")!=null){
                    String name=(String) m.get("text");
                    if(!name.equals(ta.getName())){
                        TDepartment ddd=devRepository.findByName(name);
                        if(ddd!=null){
                            rm=raiseError(2000);
                            rm.put("success", false);
                            return add(rm);
                        }
                    }
                    ta.setName((String) m.get("text"));
                }
                if(m.containsKey("description"))ta.setDescription((String) m.get("description"));
                if(m.containsKey("cas")){
                         List<String> cas=(List) m.get("cas");
                         List<TRole> auths=new ArrayList<>();
                         for(int i=0;i<cas.size();i++){
                                  Long ii=Long.valueOf(cas.get(i));
                                  TRole auth=roleRepository.findOne(ii);
                                   auths.add(auth);
                         }
                         ta.setRoles(auths);
                }
                ta.setModifier(sta);
                ta.setModified(new Date());

                devRepository.save(ta);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         }catch(Exception e){
             rm=raiseError(3000);
             rm.put("success", false);
         }
        return add(rm);
    }
    
    private Map devMap(TDepartment ta){
        Map m=new HashMap();
            boolean bb=false;
            m.put("text", ta.getName());
            m.put("description", ta.getDescription());
            m.put("id", ta.getId());
            if(ta.getParent()!=null)m.put("parentId", ta.getParent().getId());
            else m.put("parentId","-1");
            if(ta.getCreated()!=null)m.put("created", WStringUtils.dateToString(ta.getCreated()));
            if(ta.getModified()!=null)m.put("modified", WStringUtils.dateToString(ta.getModified()));
            if(ta.getCreator()!=null)m.put("cname", ta.getCreator().getName());
            if(ta.getModifier()!=null)m.put("mname", ta.getModifier().getName());
            List<TRole> sss=ta.getRoles();
                    List datas=new ArrayList();
                    for(TRole s:sss){
                        datas.add(s.getId().toString());
                    }
           m.put("cas", datas);
            m.put("leaf",false);
        return m;
    }

    /**
     * Add staff. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map sta_add(Map map) {
        Map rm=new HashMap();
        try{
                Map m=(Map) map.get("data");
                String pid=(String) m.get("pid");
                String sname=(String) m.get("ssname");
                if(sname==null || sname.isEmpty()){
                    rm=raiseError(2008);
                    rm.put("success", false);
                    return add(rm);
                }
                TStaff st=staRepository.findByName(sname);
                TDepartment dev=devRepository.findOne(Long.valueOf(pid));
                if(dev==null){
                    rm=raiseError(2006);
                    rm.put("success", false);
                    return add(rm);
                }
                String name=(String) m.get("name");
                String mobile=(String) m.get("mobile");
                TStaff sta=staRepository.findByMobile(mobile);
                if(sta!=null){
                    rm=raiseError(2000);
                    rm.put("success", false);
                    return add(rm);
                }
                sta=new TStaff();
                sta.setName(name);
                sta.setDepartment(dev);
               sta.setMobile(mobile);
               if(m.containsKey("password")){
                   String pass=(String) m.get("password");
                   String salt=WStringUtils.randomString(15);
                   
                   if(pass!=null && !pass.trim().isEmpty()){
                       sta.setSalt(salt);
                       sta.encryptPassword((String) m.get("password"));
                   }
               }
               if(m.containsKey("loginName"))sta.setLoginName((String) m.get("loginName"));
               if(m.containsKey("enable"))sta.setEnabled((Boolean) m.get("enable"));
               if(m.containsKey("gender"))sta.setGender((int)m.get("gender"));
               if(m.containsKey("cas")){
                    List<String> cas=(List) m.get("cas");
                    List<TRole> auths=new ArrayList<>();
                        for(int i=0;i<cas.size();i++){
                            Long ii=Long.valueOf(cas.get(i));
                            TRole auth=roleRepository.findOne(ii);
                            auths.add(auth);
                        }
                    sta.setRoles(auths);
                }
                sta.setCreated(new Date());
                sta.setModified(new Date());
                
                sta.setCreator(st);
                sta.setModifier(st);
                staRepository.save(sta);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                Map mmm=new HashMap();
                mmm.put("id", sta.getId());
                mmm.put("cname", sta.getCreator().getName());
                mmm.put("mname", sta.getModifier().getName());
                mmm.put("modified", WStringUtils.dateToString(sta.getModified()));
                rm.put("data", mmm);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Delete staff department. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map sta_delete(Map map) {
        Map model=(Map) map.get("data");
        Long id=Long.valueOf((String) model.get("id"));
        Map rm=new HashMap();
        try{
            TStaff sta=staRepository.findOne(id);
            List as=devRepository.findByModifierOrCreator(sta, sta);
            if(as!=null && !as.isEmpty()){
                rm=raiseError(2007);
                rm.put("success", false);
                return add(rm);
            }
            staRepository.delete(id);
            rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Read staff department. 
     * @param model HashMap
     * @return map HashMap
     */
    @Override
    public Map sta_read(Map model) {
        Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                String pid=(String) map.get("sid");
                String key=(String) map.get("key");
                List<TStaff> list;
                if(pid==null)pid="";
                 int start=(int) map.get("page");
                if(start<0)return rm;
                int count=15;
                if(hasText(key)){
                  key="%"+key.toLowerCase()+"%";
                }else{
                  key="%%";
               }
               int total=0;
                Pageable page=new PageRequest(start-1, count);
               Page<TStaff> dt=null;
                if(pid.isEmpty()){
                    total=staRepository.countByMobileContaining(key).intValue();
                      if(total<(start-1)*count){
                          page=new PageRequest(0, count);
                     }
                    dt=staRepository.findByMobileContaining(key, page);
                    
                }else{
                    TDepartment dev=devRepository.findOne(Long.valueOf(pid));
                    List<TDepartment> devs=new ArrayList<>();
                    if(dev.getLevel()==1) devs=devRepository.findByCid(Long.valueOf(pid));
                    devs.add(dev);
                    total=staRepository.countByDepart(Long.valueOf(pid));
                      if(total<(start-1)*count){
                          page=new PageRequest(0, count);
                      }
                    dt=staRepository.findByDepart(Long.valueOf(pid), page);
                    
                }
               list=dt.getContent();
                List lst=new ArrayList();
                for(TStaff air:list){
                    Map mm=new HashMap();
                    mm.put("id", air.getId().toString());
                    mm.put("name", air.getName());
                    mm.put("mobile", air.getMobile());
                    mm.put("enable", air.isEnabled());
                    mm.put("mname", air.getModifier().getName());
                    mm.put("cname", air.getCreator().getName());
                    mm.put("modified", WStringUtils.dateToString(air.getModified()));
                    mm.put("password", air.getPassword());
                    mm.put("gender", air.getGender());
                    mm.put("last", WStringUtils.dateToString(air.getLastAccess()));
                    List<TRole> sss=air.getRoles();
                    List datas=new ArrayList();
                    for(TRole s:sss){
                        datas.add(s.getId().toString());
                    }
                    mm.put("cas", datas);
                    lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
                rm.put("total", total);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Update staff department. 
     * @param map HashMap
     * @return map HashMap
     */
    @Override
    public Map sta_update(Map map) {
        Map m=(Map) map.get("data");
         Map rm=new HashMap();
         try{
                String sname=(String) m.get("ssname");
                if(sname==null || sname.isEmpty()){
                    rm=raiseError(2008);
                    rm.put("success", false);
                    return add(rm);
                }
                TStaff st=staRepository.findByName(sname);
                Long id=Long.valueOf((String)m.get("id"));
                TStaff sta=staRepository.findOne(id);
                if(m.containsKey("name")){
                    String name=(String) m.get("name");
                   sta.setName(name);
                }
              if(m.containsKey("mobile")){
                  String mobile=(String) m.get("mobile");
                   if(!mobile.equals(sta.getMobile())){
                        TStaff ro=staRepository.findByMobile(mobile);
                        if(ro!=null){
                            rm=createResult(RETURN_ERROR_CODE, "already.exist", 500, "already.exist", "false");
                            return add(rm);
                        }else{
                            sta.setMobile(mobile);
                        }
                    }
              }
               if(m.containsKey("password")){
                   String pass=(String) m.get("password");
                   String salt=WStringUtils.randomString(15);
                   if(pass!=null && !pass.trim().isEmpty()){
                       sta.setSalt(salt);
                       sta.encryptPassword((String) m.get("password"));
                   }
               }
               if(m.containsKey("loginName"))sta.setLoginName((String) m.get("loginName"));
               if(m.containsKey("enable"))sta.setEnabled((Boolean) m.get("enable"));
               if(m.containsKey("gender"))sta.setGender((int)m.get("gender"));
               if(m.containsKey("cas")){
                    List<String> cas=(List) m.get("cas");
                    List<TRole> auths=new ArrayList<>();
                        for(int i=0;i<cas.size();i++){
                            Long ii=Long.valueOf(cas.get(i));
                            TRole auth=roleRepository.findOne(ii);
                            auths.add(auth);
                        }
                    sta.setRoles(auths);
                }
               sta.setModified(new Date());
               sta.setModifier(st);
                staRepository.save(sta);
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
         }catch(Exception e){
             rm=raiseError(3000);
             rm.put("success", false);
         }
         return add(rm);
    }
}
