/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.impl.pingtai;

import com.wwzz.services.api.pingtai.BookServices;
import com.wwzz.services.domain.base.TTourismLineCategory;
import com.wwzz.services.domain.booking.TUserBookingOrder;
import com.wwzz.services.domain.booking.TUserBookingTempOrder;
import com.wwzz.services.domain.booking.TUserOrderDocents;
import com.wwzz.services.domain.booking.TUserOrderDriver;
import com.wwzz.services.domain.booking.TUserOrderLocalGuide;
import com.wwzz.services.domain.booking.TUserOrderLongGuide;
import com.wwzz.services.domain.booking.TUserOrderOGuide;
import com.wwzz.services.domain.booking.TUserOrderProcessCharge;
import com.wwzz.services.domain.booking.TUserOrderRestaurant;
import com.wwzz.services.domain.booking.TUserOrderShop;
import com.wwzz.services.domain.booking.TUserOrderTicket;
import com.wwzz.services.domain.news.TUserNotice;
import com.wwzz.services.domain.users.TUser;
import com.wwzz.services.impl.ResultDataServices;
import com.wwzz.services.repository.TUserBookingOrderRepository;
import com.wwzz.services.repository.TUserBookingTempOrderRepository;
import com.wwzz.services.repository.TUserNoticeRepository;
import com.wwzz.services.repository.TUserOrderDocentsRepository;
import com.wwzz.services.repository.TUserOrderDriverRepository;
import com.wwzz.services.repository.TUserOrderLocalGuideRepository;
import com.wwzz.services.repository.TUserOrderLongGuideRepository;
import com.wwzz.services.repository.TUserOrderOGuideRepository;
import com.wwzz.services.repository.TUserOrderProcessRepository;
import com.wwzz.services.repository.TUserOrderRestaurantRepository;
import com.wwzz.services.repository.TUserOrderShopRepository;
import com.wwzz.services.repository.TUserOrderTicketRepository;
import com.wwzz.services.repository.TUserRepository;
import com.wwzz.services.utils.Constants;
import static com.wwzz.services.utils.Constants.RETURN_ERROR_CODE;
import static com.wwzz.services.utils.Constants.RETURN_SUCCESS_CODE;
import com.wwzz.services.utils.Utils;
import com.wwzz.services.utils.WStringUtils;
import com.wwzz.services.utils.CheckRequest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 *@author tiger
 * This class is a booking data management service class that
 *  do process booking data.
 */
@Service
@Transactional
public class BookDataProcess extends ResultDataServices implements BookServices, Constants{
    
    @Value("#{serverinfo['apps.key']}")
    private String key;
    
    @Autowired
    private TUserBookingOrderRepository boRepository;
    
    @Autowired
    private TUserOrderOGuideRepository ogRepository;
    
    @Autowired
    private TUserOrderLongGuideRepository orderLGRepository;
    
    @Autowired
    private TUserBookingTempOrderRepository botRepository;
    
    @Autowired
    private TUserOrderLocalGuideRepository olocalRepository;
    
    @Autowired
    private TUserOrderDriverRepository odriverRepository;
    
    @Autowired
    private TUserOrderDocentsRepository odocentRepository;
    
    @Autowired
    private TUserOrderRestaurantRepository oresRepsoitory;
    
    @Autowired
    private TUserOrderShopRepository oshopRepository;
    
    @Autowired
    private TUserOrderTicketRepository otRepository;
    
    @Autowired
    private TUserRepository userRepository;
    
    @Autowired
    private TUserNoticeRepository noticeRepository;
    
    @Autowired
    private TUserOrderProcessRepository opRepository;

    /**
     * Booking data read.
     * @param model HashMap including search condition.
     * @return map HashMap
     */
    @Override
    public Map book_read(Map model) {
        Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                String sender=(String) map.get("sender");
                String snum=(String) map.get("snum");
                if(snum==null)snum="";
                String stime=(String) map.get("stime");
                int shour=(int) map.get("shour");
                int smin=(int) map.get("smin");
                String etime=(String) map.get("etime");
                int ehour=(int) map.get("ehour");
                int emin=(int) map.get("emin");
                boolean flag=(boolean) map.get("flag");
                int cat=(int) map.get("cat");
                Calendar scal=Calendar.getInstance();
                Calendar ecal=Calendar.getInstance();
                if(stime!=null && !stime.isEmpty()){
                    stime=stime.split("T")[0];
                    Date dd=WStringUtils.stringToDate(stime, "-");
                    scal.setTime(dd);
                    scal.set(Calendar.HOUR, shour);
                    scal.set(Calendar.MINUTE, smin);
                }else{
                    scal.set(Calendar.YEAR, 2014);
                }
                if(etime!=null && !etime.isEmpty()){
                    etime=etime.split("T")[0];
                    Date dt=WStringUtils.stringToDate(etime, "-");
                    ecal.setTime(dt);
                    ecal.set(Calendar.HOUR, ehour);
                    ecal.set(Calendar.MINUTE, emin);
                }else{
                    ecal.setTime(new Date());
                    ecal.set(Calendar.YEAR, ecal.get(Calendar.YEAR)+10);
                }
               
                List<TUserBookingOrder> list;
              
                 int start=(int) map.get("page");
                if(start<0)start=1;
                int count=15;
                if(hasText(sender)){
                  sender="%"+sender.toLowerCase()+"%";
                }else{
                  sender="%%";
               }
              int total=0;
               Pageable page=new PageRequest(start-1, count);
               Page<TUserBookingOrder> dt=null;
               if(cat==4 || cat==5){
                   Integer type=(Integer) map.get("type");
                   if(!flag){
                       total=odriverRepository.count_forder(cat, type);
                       if(total<(start-1)*count){
                          page=new PageRequest(0, count);
                      }
                       dt=odriverRepository.find_forder(cat, type, page);
                   }else{
                       if(snum.isEmpty()){
                           total=odriverRepository.count_worder(sender, scal.getTime(), ecal.getTime(), cat,type);
                           if(total<(start-1)*count){
                                page=new PageRequest(0, count);
                            }
                            dt=odriverRepository.find_worder(sender, scal.getTime(), ecal.getTime(), cat, type, page);
                         }else{
                           total=odriverRepository.count_order(sender, snum, scal.getTime(), ecal.getTime(), cat,type);
                           if(total<(start-1)*count){
                                page=new PageRequest(0, count);
                            }
                             dt=odriverRepository.find_order(sender, snum, scal.getTime(), ecal.getTime(), cat,type, page);
                         }
                   }
               }else{
                    if(!flag){
                        total=boRepository.countByCategoryOrderByApplicationTimeDesc(cat).intValue();
                        if(total<(start-1)*count){
                                page=new PageRequest(0, count);
                            }
                        dt=boRepository.findByCategoryOrderByApplicationTimeDesc(cat,page);
                    }else{
                       if(snum.isEmpty()){
                           total=boRepository.count_worder(sender, scal.getTime(), ecal.getTime(), cat);
                           if(total<(start-1)*count){
                                page=new PageRequest(0, count);
                            }
                          dt=boRepository.find_worder(sender, scal.getTime(), ecal.getTime(), cat, page);
                       }else{
                           total=boRepository.count_order(sender, snum, scal.getTime(), ecal.getTime(), cat);
                           if(total<(start-1)*count){
                                page=new PageRequest(0, count);
                            }
                           dt=boRepository.find_order(sender, snum, scal.getTime(), ecal.getTime(), cat, page);
                       }
                    }
               }
               list=dt.getContent();
                List lst=new ArrayList();
                for(TUserBookingOrder air:list){
                    Map mm=new HashMap();
                    mm.put("id", air.getId().toString());
                    mm.put("num", air.getSerialNumber());
                    mm.put("stime", Utils.dateToString(air.getApplicationTime()));
                    mm.put("sname", air.getApplicant().getName());
                    mm.put("sid", air.getApplicant().getId().toString());
                    mm.put("status", BOOKING_STATES[air.getStatus()-1]);
                    if(air.getExecutor()!=null){
                        mm.put("ename", air.getExecutor().getName());
                        mm.put("etime", Utils.dateToString(air.getTradedTime()));
                        mm.put("emoney", air.getOrderAmount());
                        mm.put("flag", true);
                    }else{
                        mm.put("ename", "抢单");
                        mm.put("etime", "");
                        mm.put("emoney", "");
                        mm.put("flag", false);
                    }
                    lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
                rm.put("total", total);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }
    
    private Map add(Map map){
        map.put("partner", "11111111111111111111");
        Calendar cal=Calendar.getInstance();
        cal.setTime(new Date());
        map.put("timestamp", cal.getTime().getTime());
        CheckRequest cr=new CheckRequest(key, map);
        map.put("sign", cr.hashParams());
        return map;
    }

    /**
     *  Booking data detail information read.
     * @param model HashMap including user id.
     * @return map HashMap
     */
    @Override
    public Map dbook_read(Map model) {
        Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                String bid=(String) map.get("bid");
                TUserBookingOrder bo=boRepository.findOne(Long.valueOf(bid));
                List lst=new ArrayList();
                switch(bo.getCategory()){
                    case BOOKING_CATEGORY_LG:
                        TUserOrderLongGuide og=orderLGRepository.findByBooking(bo);
                        if(og==null){
                            rm=createResult(RETURN_ERROR_CODE, "operation.fail", 500, "operation.fail", "false");
                            return add(rm);
                        }
                       lst=getLGs(og);
                       break;
                    case BOOKING_CATEGORY_OG:
                        TUserOrderOGuide oog=ogRepository.findByBooking(bo);
                        if(oog==null){
                            rm=createResult(RETURN_ERROR_CODE, "operation.fail", 500, "operation.fail", "false");
                            return add(rm);
                        }
                       lst=getOversel(oog);
                        break;
                    case BOOKING_CATEGORY_LC:
                        TUserOrderLocalGuide lg=olocalRepository.findByBooking(bo);
                        if(lg==null){
                            rm=createResult(RETURN_ERROR_CODE, "operation.fail", 500, "operation.fail", "false");
                            return add(rm);
                        }
                       lst=getLocal(lg);
                        break;
                    case BOOKING_CATEGORY_DRV_1:
                    case BOOKING_CATEGORY_DRV_2:
                        TUserOrderDriver od=odriverRepository.findByBooking(bo);
                        if(od==null){
                            rm=createResult(RETURN_ERROR_CODE, "operation.fail", 500, "operation.fail", "false");
                            return add(rm);
                        }
                       lst=getDriver(od);
                       break;
                    case BOOKING_CATEGORY_SS_PIAO:
                        TUserOrderTicket ot=otRepository.findByBooking(bo);
                        if(ot==null){
                            rm=createResult(RETURN_ERROR_CODE, "operation.fail", 500, "operation.fail", "false");
                            return add(rm);
                        }
                       lst=getTicket(ot);
                       break;
                    case BOOKING_CATEGORY_RESTURANT:
                        TUserOrderRestaurant or=oresRepsoitory.findByBooking(bo);
                        if(or==null){
                            rm=createResult(RETURN_ERROR_CODE, "operation.fail", 500, "operation.fail", "false");
                            return add(rm);
                        }
                        lst=getOr(or);
                        break;
                    case BOOKING_CATEGORY_SHOP:
                        TUserOrderShop oh=oshopRepository.findByBooking(bo);
                        if(oh==null){
                            rm=createResult(RETURN_ERROR_CODE, "operation.fail", 500, "operation.fail", "false");
                            return add(rm);
                        }
                        lst=getOh(oh);
                        break;
                    case BOOKING_CATEGORY_DOCENTS:
                        TUserOrderDocents odd=odocentRepository.findByBooking(bo);
                        if(odd==null){
                            rm=createResult(RETURN_ERROR_CODE, "operation.fail", 500, "operation.fail", "false");
                            return add(rm);
                        }
                        lst=getOd(odd);
                        break;
                    case BOOKING_CATEGORY_PP:
                        TUserOrderProcessCharge op=opRepository.findByBooking(bo);
                        if(op==null){
                            rm=createResult(RETURN_ERROR_CODE, "operation.fail", 500, "operation.fail", "false");
                            return add(rm);
                        }
                        lst=getOp(op);
                        break;
                }
                    
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }
    
    /**
     * 
     * @param og TUserOrderLongGuide
     * @return list- long guide detail information .
     */
    private List getLGs(TUserOrderLongGuide og){
        List list=new ArrayList();
        Map map=new HashMap();
        
        map.put("name", "所带团性质");
        map.put("des", NATURE[og.getNature()-1]);
        list.add(map);
        
        map=new HashMap();
        map.put("name", "线路区域");
        List<TTourismLineCategory> datas=og.getLineCategories();
        StringBuilder sb=new StringBuilder();
        for(TTourismLineCategory c:datas){
            sb.append("-");
            sb.append(c.getNameZh());
        }
        String s=sb.toString().substring(1);
        map.put("des", s);
        list.add(map);
        map=new HashMap();
        map.put("name", "几天");
        map.put("des", og.getDayNumber());
        list.add(map);
        map=new HashMap();
        map.put("name", "几国");
        map.put("des", og.getCountryNumber());
        list.add(map);
        map=new HashMap();
        map.put("name", "国内出境城市");
        map.put("des", og.getDepartureExitCity()!=null?og.getDepartureExitCity().getName1():"");
        list.add(map);
        map=new HashMap();
        map.put("name", "欧洲入境城市");
        map.put("des", og.getDepartureEntryCity()!=null?og.getDepartureEntryCity().getName1():"");
        list.add(map);
         map=new HashMap();
        map.put("name", "出发时间");
        map.put("des", og.getBeginTime()!=null?WStringUtils.dateToString(og.getBeginTime()):"");
        list.add(map);
         map=new HashMap();
        map.put("name", "结束时间");
        map.put("des", og.getEndTime()!=null?WStringUtils.dateToString(og.getEndTime()):"");
        list.add(map);
         map=new HashMap();
        map.put("name", "人数");
        map.put("des", og.getBookingNumber());
        list.add(map);
         map=new HashMap();
         String st="有";
         if(og.getNeedLeaderCertificate()==2)st="不限";
        map.put("name", "领队证");
        map.put("des", st);
        list.add(map);
        map=new HashMap();
        st="有";
         if(og.getNeedEmploymentCertificate()==2)st="无";
        map.put("name", "导游证");
        map.put("des", st);
        list.add(map);
        return list;
    }
    
    /**
     * 
     * @param os TUserOrderOGuide
     * @return list- overseal guide detail information .
     */
    private List getOversel(TUserOrderOGuide os){
        List list=new ArrayList();
        Map map=new HashMap();
        map.put("name", "所带团性质");
        map.put("des", NATURE[os.getNature()-1]);
        list.add(map);
        map=new HashMap();
        map.put("name", "线路区域");
        List<TTourismLineCategory> datas=os.getLineCategories();
        StringBuilder sb=new StringBuilder();
        for(TTourismLineCategory c:datas){
            sb.append("-");
            sb.append(c.getNameZh());
        }
        String s=sb.toString().substring(1);
        map.put("des", s);
        list.add(map);
        map=new HashMap();
        map.put("name", "几天");
        map.put("des", os.getDayNumber());
        list.add(map);
        map=new HashMap();
        map.put("name", "几国");
        map.put("des", os.getCountryNumber());
        list.add(map);
        map=new HashMap();
        map.put("name", "国内出境城市");
        map.put("des", os.getDepartureExitCity()!=null?os.getDepartureExitCity().getName1():"");
        list.add(map);
        map=new HashMap();
        map.put("name", "欧洲离境城市");
        map.put("des", os.getReturnExitCity()!=null?os.getReturnExitCity().getName1():"");
        list.add(map);
        map=new HashMap();
        map.put("name", "欧洲离境时间");
        map.put("des", os.getReturnExitTime()!=null?WStringUtils.dateToString(os.getReturnExitTime()):"");
        list.add(map);
        map=new HashMap();
        map.put("name", "欧洲入境城市");
        map.put("des", os.getDepartureEntryCity()!=null?os.getDepartureEntryCity().getName1():"");
        list.add(map);
        map=new HashMap();
        map.put("name", "欧洲入境时间");
        map.put("des", os.getDepartureEntryTime()!=null?WStringUtils.dateToString(os.getDepartureEntryTime()):"");
        list.add(map);
         map=new HashMap();
        map.put("name", "出发时间");
        map.put("des", os.getBeginTime()!=null?WStringUtils.dateToString(os.getBeginTime()):"");
        list.add(map);
         map=new HashMap();
        map.put("name", "结束时间");
        map.put("des", os.getEndTime()!=null?WStringUtils.dateToString(os.getEndTime()):"");
        list.add(map);
         map=new HashMap();
        map.put("name", "人数");
        map.put("des", os.getBookingNumber());
        list.add(map);
         map=new HashMap();
         String st="有";
         if(os.getNeedEmploymentCertificate()==2)st="不限";
        map.put("name", "导游 资格证");
        map.put("des", st);
        list.add(map);
        map=new HashMap();
        map.put("name", "特殊要求");
        map.put("des", os.getRequirement());
        list.add(map);

        return list;
    }
    
    /**
     * 
     * @param lg TUserOrderLocalGuide
     * @return list- local guide detail information .
     */
    private List getLocal(TUserOrderLocalGuide lg){
        List list=new ArrayList();
        Map map=new HashMap();
        map.put("name", "地区");
        map.put("des", lg.getArea().getName1());
        list.add(map);
        map=new HashMap();
        map.put("name", "开始时间");
        map.put("des", lg.getBeginTime()!=null?Utils.dateToString(lg.getBeginTime()):"");
        list.add(map);
        map=new HashMap();
        map.put("name", "结束时间");
        map.put("des", lg.getEndTime()!=null?Utils.dateToString(lg.getEndTime()):"");
        list.add(map);
        map=new HashMap();
        map.put("name", "人数");
        map.put("des", lg.getBookingNumber());
        list.add(map);
        map=new HashMap();
         String st="需要";
         if(lg.getNeedGuideCertificate()==2)st="不限";
        map.put("name", "领队证");
        map.put("des", st);
        list.add(map);
        map=new HashMap();
        map.put("name", "特殊要求");
        map.put("des", lg.getRequirement());
        list.add(map);
        return list;
    }
    
    /**
     * 
     * @param od TUserOrderDriver
     * @return list- driver detail information .
     */
    private List getDriver(TUserOrderDriver od){
        List list=new ArrayList();
        String st="司机";
        if(od.getCategory()==2)st="专车";
        Map map=new HashMap();
        map.put("name", "订单分类");
        map.put("des", st);
        list.add(map);
        
        map=new HashMap();
        map.put("name", "订单类型");
        map.put("des", DRV_ORDER_TYPE[od.getType()-1]);
        list.add(map);
        map=new HashMap();
        map.put("name", "所在地区");
        map.put("des", od.getArea()!=null?od.getArea().getName1():"");
        list.add(map);
        map=new HashMap();
        map.put("name", "机场");
        map.put("des", od.getAirport()!=null?od.getAirport().getNameZh():"");
        list.add(map);
        map=new HashMap();
        map.put("name", "航班");
        map.put("des", od.getFlight());
        list.add(map);
        map=new HashMap();
        map.put("name", "人数");
        map.put("des", od.getBookingNumber());
        list.add(map);
          map=new HashMap();
        map.put("name", "特殊要求");
        map.put("des", od.getRequirement());
        list.add(map);
        map=new HashMap();
        map.put("name", "用车开始时间");
        map.put("des", od.getBeginTime()!=null?Utils.dateToString(od.getBeginTime()):"");
        list.add(map);
        map=new HashMap();
        map.put("name", "用车结束时间");
        map.put("des", od.getEndTime()!=null?Utils.dateToString(od.getEndTime()):"");
        list.add(map);
        map=new HashMap();
        map.put("name", "最小车型");
        map.put("des", od.getCarModel()!=null?od.getCarModel().getModel():"");
        list.add(map);
        map=new HashMap();
        st="需要";
         if(od.getNeedLicenses()==2)st="不限";
        map.put("name", "运营许可");
        map.put("des", st);
        list.add(map);
        map=new HashMap();
        st="需要";
         if(od.getNeedGuide()==2)st="不限";
        map.put("name", "兼职导游");
        map.put("des", st);
        list.add(map);
        map=new HashMap();
        map.put("name", "途经国家");
        map.put("des", od.getVia());
        list.add(map);
        map=new HashMap();
        map.put("name", "上车城市");
        map.put("des", od.getStartingCity()!=null?od.getStartingCity().getName1():"");
        list.add(map);
        map=new HashMap();
        map.put("name", "上车地点");
        map.put("des", od.getStartingAddress());
        list.add(map);
        map=new HashMap();
        map.put("name", "下车城市");
        map.put("des", od.getEndCity()!=null?od.getEndCity().getName1():"");
        list.add(map);
        map=new HashMap();
        map.put("name", "下车地点");
        map.put("des", od.getEndAddress());
        list.add(map);
        return list;
    }
    
    /**
     * 
     * @param ot TUserOrderTicket
     * @return list- ticket detail information .
     */
    private List getTicket(TUserOrderTicket ot){
        List list=new ArrayList();
        Map map=new HashMap();
        map.put("name", "景区");
        map.put("des", ot.getScenic().getNameZh());
        list.add(map);
        map=new HashMap();
        map.put("name", "入园时间");
        map.put("des", ot.getEnterTime()!=null?Utils.dateToString(ot.getEnterTime()):"");
        list.add(map);
        map=new HashMap();
        map.put("name", "取票时间");
        map.put("des", ot.getGetTime()!=null?Utils.dateToString(ot.getGetTime()):"");
        list.add(map);
        map=new HashMap();
        map.put("name", "成人数");
        map.put("des", ot.getAdultsNumber());
        list.add(map);
        map=new HashMap();
        map.put("name", "儿童数");
        map.put("des", ot.getChildrenNumber());
        list.add(map);
        map=new HashMap();
        map.put("name", "特殊要求");
        map.put("des", ot.getRequirement());
        list.add(map);
        return list;
    }
    
    /**
     * 
     * @param or TUserOrderRestaurant
     * @return list- restaurant booking detail information .
     */
    private List getOr(TUserOrderRestaurant or){
        List list=new ArrayList();
        Map map=new HashMap();
        map.put("name", "餐厅");
        map.put("des", or.getRestaurant().getNameZh());
        list.add(map);
        map=new HashMap();
        map.put("name", "餐厅套餐");
        map.put("des", or.getPackSet().getNameZh());
        list.add(map);
        String style=or.getStyle()==1?"午餐":"晚餐";
        map=new HashMap();
        map.put("name", "类型");
        map.put("des", style);
        list.add(map);
        map=new HashMap();
        map.put("name", "成人数");
        map.put("des", or.getAdultsNumber());
        list.add(map);
        map=new HashMap();
        map.put("name", "回民餐人数");
        map.put("des", or.getHalalNumber());
        list.add(map);
        map=new HashMap();
        map.put("name", "素餐人数");
        map.put("des", or.getVegetarianNumber());
        list.add(map);
        map=new HashMap();
        map.put("name", "特殊要求");
        map.put("des", or.getRequirement());
        list.add(map);
        map=new HashMap();
        map.put("name", "到店时间");
        map.put("des", or.getBookingTime()!=null?Utils.dateToString(or.getBookingTime()):"");
        list.add(map);
        return list;
    }
    
    /**
     * 
     * @param oh TUserOrderShop
     * @return list- shop booking detail information .
     */
    private List getOh(TUserOrderShop oh){
         List list=new ArrayList();
        Map map=new HashMap();
        map.put("name", "商店");
        map.put("des", oh.getShop().getNameZh());
        list.add(map);
        map=new HashMap();
        map.put("name", "到店时间");
        map.put("des", oh.getBeginTime()!=null?Utils.dateToString(oh.getBeginTime()):"");
        list.add(map);
        map=new HashMap();
        map.put("name", "会员卡");
        map.put("des", oh.getCard()!=null?oh.getCard().getCardNumber():"");
        list.add(map);
        map=new HashMap();
        map.put("name", "人数");
        map.put("des", oh.getBookingNumber());
        list.add(map);
        map=new HashMap();
        String st="需要";
        if(oh.getNeedBeginNotice()==0)st="不需要";
        map.put("name", "到店通知");
        map.put("des", st);
        list.add(map);
        map=new HashMap();
        map.put("name", "特殊要求");
        map.put("des", oh.getRequirements());
        list.add(map);
        return list;
    }
    
    /**
     * 
     * @param od TUserOrderDocents
     * @return list- docent booking detail information .
     */
    private List getOd(TUserOrderDocents od){
        List list=new ArrayList();
        Map map=new HashMap();
        map.put("name", "景点");
        map.put("des", od.getScenic().getNameZh());
        list.add(map);
        map=new HashMap();
        map.put("name", "参观日期");
        map.put("des", od.getVisitDate()!=null?Utils.dateToString(od.getVisitDate()):"");
        list.add(map);
        map=new HashMap();
        map.put("name", "讲解形式");
        map.put("des", SS_ORDER_TYPE[od.getType()-1]);
        list.add(map);
        map=new HashMap();
        map.put("name", "门票");
        String st="需要";
        if(od.isNeedTicket()==0)st="不需要";
        map.put("des", st);
        list.add(map);
        if(od.getGender()!=0){
            map=new HashMap();
            map.put("name", "性别");
            map.put("des", GENDER_NAME[od.getGender()-1]);
            list.add(map);
        }
        if(od.getNationality()!=0){
            map=new HashMap();
            map.put("name", "偏好");
            map.put("des", NATIONALITY[od.getNationality()-1]);
            list.add(map);
        }
        map=new HashMap();
        map.put("name", "成人数");
        map.put("des", od.getAdultsNumber());
        list.add(map);
        map=new HashMap();
        map.put("name", "儿童数");
        map.put("des", od.getChildrenNumber());
        list.add(map);
        map=new HashMap();
        map.put("name", "讲解时间");
        map.put("des", od.getExplainTime()!=null?od.getExplainTime():"");
        list.add(map);
        map=new HashMap();
        map.put("name", "场次");
        map.put("des", od.getExplainTimes());
        list.add(map);
        /*map=new HashMap();
        map.put("name", "讲解服务");
        map.put("des", od.getSchedule()!=null?od.getSchedule().getDocentItems():"");
        list.add(map);*/
        map=new HashMap();
        map.put("name", "特殊要求");
        map.put("des", od.getRequirements());
        list.add(map);
        
        return list;
    }
    
    /**
     * 
     * @param op TUserOrderProcessCharge
     * @return list- process charge booking detail information .
     */
    private List getOp(TUserOrderProcessCharge op){
        List list=new ArrayList();
        Map map=new HashMap();
        map.put("name", "国家");
        map.put("des", op.getArea().getParent().getName1());
        list.add(map);
        map=new HashMap();
        map.put("name", "城市");
        map.put("des", op.getArea().getName1());
        list.add(map);
        map=new HashMap();
        map.put("name", "预约日期");
        map.put("des", WStringUtils.dateToString(op.getBookingDate()));
        list.add(map);
        map=new HashMap();
        map.put("name", "车公司名");
        map.put("des", op.getVehicleCompanyName());
        list.add(map);
        map=new HashMap();
        map.put("name", "车牌号码");
        map.put("des", op.getVehicleLicence());
        list.add(map);
        map=new HashMap();
        map.put("name", "公司地址");
        map.put("des",op.getCompanyAddress());
        list.add(map);
        map=new HashMap();
        map.put("name", "公司税号");
        map.put("des", op.getCompanyTax());
        list.add(map);
        map=new HashMap();
        map.put("name", "公司电号");
        map.put("des", op.getCompanyPhone());
        list.add(map);
        map=new HashMap();
        map.put("name", "邮箱地址");
        map.put("des", op.getReceiveEmail());
        list.add(map);
        map=new HashMap();
        map.put("name", "接收传真号");
        map.put("des", op.getReceiveFax());
        list.add(map);
        map=new HashMap();
        map.put("name", "欧元金额");
        map.put("des", op.getProcessCharge().getPrice());
        list.add(map);
        return list;
    }

    /**
     *  Booking temp data read.
     * @param model HashMap including booking id
     * @return map HashMap
     */
    @Override
    public Map booktmp_read(Map model) {
         Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                String bid=(String) map.get("bid");
                TUserBookingOrder bo=boRepository.findOne(Long.valueOf(bid));
                List<TUserBookingTempOrder> datas=botRepository.findByBooking(bo);
                List lst=new ArrayList();
                for(TUserBookingTempOrder to:datas){
                    Map mm=new HashMap();
                    mm.put("name", to.getBidder().getName());
                    mm.put("btime", Utils.dateToString(to.getSubmissionTime()));
                    mm.put("bmoney", String.valueOf(to.getBidAmount()));
                    mm.put("id", to.getId().toString());
                    lst.add(mm);
                }
                rm=createResult(RETURN_SUCCESS_CODE, "operation.success", 0, "", "true");
                rm.put("data", lst);
         }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }

    /**
     * Booking message read.
     * @param model HashMap
     * @return  map HashMap
     */
    @Override
    public Map ant_read(Map model) {
         Map rm=new HashMap();
        try{
                Map map=(Map) model.get("data");
                 int start=(int) map.get("page");
                if(start<0)start=1;
                int count=15;
                int total=0;
                TUser user=userRepository.find_admin();
               Pageable page=new PageRequest(start-1, count);
               total=noticeRepository.count_notice(user);
               if(total<(start-1)*count){
                         page=new PageRequest(0, count);
                }
               Page<TUserNotice> dt=null;
               dt=noticeRepository.find_notice(user, page);
               List<TUserNotice> list=dt.getContent();
               List lst=new ArrayList();
               for(TUserNotice uc:list){
                   Map m=new HashMap();
                   m.put("title", uc.getTitle());
                   m.put("id", uc.getId().toString());
                   m.put("sname", uc.getSender().getName());
                   m.put("stime", Utils.dateToString(uc.getSendTime()));
                   m.put("ssname", uc.getBooking().getApplicant().getName());
                   m.put("sstime", Utils.dateToString(uc.getBooking().getApplicationTime()));
                   m.put("status", BOOKING_STATES[uc.getBooking().getStatus()-1]);
                   m.put("bid", uc.getBooking().getId().toString());
                  lst.add(m);
               }
               rm.put("data", lst);
               rm.put("total", total);
        }catch(Exception e){
            rm=raiseError(3000);
            rm.put("success", false);
        }
        return add(rm);
    }
    
    
    
}
