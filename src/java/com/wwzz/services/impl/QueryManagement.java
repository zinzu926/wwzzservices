/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.impl;

import com.wwzz.services.api.QueryServices;
import com.wwzz.services.domain.base.TDocentsSchedule;
import com.wwzz.services.domain.base.TDocentsScheduleAssign;
import com.wwzz.services.domain.base.TDocentsScheduleItems;
import com.wwzz.services.repository.TDocentsScheduleAssignRepository;
import com.wwzz.services.repository.TDocentsScheduleItemsRepository;
import com.wwzz.services.repository.TDocentsScheduleRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author JBOSS
 */
@Service
@Transactional
public class QueryManagement implements QueryServices{
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private TDocentsScheduleItemsRepository sditRepo;
    @Autowired
    private TDocentsScheduleRepository dcsdlRepo;
    @Autowired
    private TDocentsScheduleAssignRepository dcsassg;
    
    @Override
    public Long getQueryResultCount(String sql) {
        Long count;
        try{
            count=(Long) em.createQuery(sql).getSingleResult();
        }catch(NoResultException ex){
            count=0L;
        }
        return count;
    }

    @Override
    public Map find_resturant_by_country(String cid, int start, int count) {
        String sql="select p from TCityRestaurant p where p.area in (select n from TArea n where n.parent.id=:id) order by p.id asc";
        String sql1="select count(p) from TCityRestaurant p where p.area in (select n from TArea n where n.parent.id='"+cid+"')";
        long cnt=getQueryResultCount(sql1);
        List lst=em.createQuery(sql).setParameter("id", cid).setFirstResult(start*count).setMaxResults(count).getResultList();
        Map m=new HashMap();
        m.put("count", cnt);
        m.put("data", lst);
        return m;
    }

    @Override
    public Map find_resturant_by_city(String cid, int start, int count) {
        String sql="select p from TCityRestaurant p where p.area.id=:id order by p.id asc";
        String sql1="select count(p) from TCityRestaurant p where p.area.id='"+cid+"'";
        long cnt=getQueryResultCount(sql1);
        List lst=em.createQuery(sql).setParameter("id", cid).setFirstResult(start*count).setMaxResults(count).getResultList();
        Map m=new HashMap();
        m.put("count", cnt);
        m.put("data", lst);
        return m;
    }

    @Override
    public List assign_docents(Long schedule, String dctime, Long sdlitem) {
        TDocentsSchedule sdl=dcsdlRepo.findOne(schedule);
        TDocentsScheduleItems dcsitem=sditRepo.findOne(sdlitem);
        List<TDocentsScheduleAssign> lst=dcsassg.findByScheduleAndDocentsTimeOrderByDocents_IdAsc(sdl, dctime);
        return null;
    }
    
}
