/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.impl;

import com.wwzz.services.api.SMServices;
import com.wwzz.services.domain.base.TPhoneTest;
import com.wwzz.services.repository.TPhoneTestRepository;
import com.wwzz.services.utils.Constants;
import static com.wwzz.services.utils.Constants.RETURN_ERROR_CODE;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author JBoss
 */
@Service
public class SMServicesImpl extends ResultDataServices implements SMServices,Constants{
    @Autowired
    private TPhoneTestRepository phoneRepo;
    @Autowired
    private MessageSource resources;
    //private int count;
    public SMServicesImpl() {
       
    }
    
    @Override
    public Map send_check_code(String code,String phone){
        //count=Integer.parseInt(props.getProperty(SMS_COUNT_PER_PHONE, "5"));
        TPhoneTest ph=phoneRepo.findByPhoneAndDateAndArea(phone, new Date(),code);
        if(ph==null)ph=new TPhoneTest();
        String cn=generateAuthCode();
        String message=getResource("send.check.code.success", cn);
        String sms=getSystemProperty(SMS_SERVICE_KEY);
        RestTemplate rt=new RestTemplate();
        List conv=new ArrayList<>();
        conv.add(new MappingJackson2HttpMessageConverter());
        conv.add(new StringHttpMessageConverter());
        conv.add(new FormHttpMessageConverter());
        rt.setMessageConverters(conv);
        Map mm=new HashMap();
        String pn="+"+code+phone;
        mm.put("number", pn);
        mm.put("message", message);
        try{
            Map result=rt.postForObject(sms, mm, Map.class);
            String success=(String) result.get(SUCCESS);
            if(success!=null){
                ph.setCode(cn);
                ph.setCount(ph.getCount()==null?1:ph.getCount()+1);
                ph.setDate(new Date());
                ph.setPhone(phone);
                ph.setArea(code);
                phoneRepo.save(ph);
                Map m=createResult(RETURN_SUCCESS_CODE, "send.check.code.ok", ERROR_CODE_OK, "",pn);
                m.put(RETURN_MESSAGE, resources.getMessage("send.check.code.ok", new String[]{pn}, new Locale("zh")));
                return m;
            }else{
                return createResult(RETURN_ERROR_CODE, "send.check.code.error2", 100, "send.check.code.error2");
            }
        }catch(RestClientException | NoSuchMessageException ex){
            return createResult(RETURN_ERROR_CODE, "send.check.code.error2", 100, "send.check.code.error2");
        }
    }
    
    private String generateAuthCode(){
        int uniqueId = (int) (System.currentTimeMillis() & 0xfffff);
        return Integer.toString(uniqueId);
    }

    @Override
    public Map send_sms_to(String phone, String message) {
        String sms=getSystemProperty(SMS_SERVICE_KEY);
        RestTemplate rt=new RestTemplate();
        List conv=new ArrayList<>();
        conv.add(new MappingJackson2HttpMessageConverter());
        conv.add(new StringHttpMessageConverter());
        conv.add(new FormHttpMessageConverter());
        rt.setMessageConverters(conv);
        Map mm=new HashMap();
        mm.put("number", phone);
        mm.put("message", message);
        try{
            Map result=rt.postForObject(sms, mm, Map.class);
            String res=(String) result.get("success");
            if(res==null){
                return createResult(RETURN_ERROR_CODE, GENERAL_ERROR, 100, GENERAL_ERROR);
            }else{
                return createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
            }
            
        }catch(Exception ex){
            return createResult(RETURN_ERROR_CODE, GENERAL_ERROR, 100, GENERAL_ERROR);
        }
    }
      
}
