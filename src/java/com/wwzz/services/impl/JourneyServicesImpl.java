/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.impl;

import com.wwzz.services.api.JourneyServices;
import com.wwzz.services.api.UserNoticeServices;
import com.wwzz.services.domain.base.TArea;
import com.wwzz.services.domain.base.TTravelAgency;
import com.wwzz.services.domain.booking.TUserBookingOrder;
import com.wwzz.services.domain.booking.TUserOrderDocents;
import com.wwzz.services.domain.booking.TUserOrderRestaurant;
import com.wwzz.services.domain.booking.TUserOrderShop;
import com.wwzz.services.domain.booking.TUserOrderTicket;
import com.wwzz.services.domain.journey.TUserJourney;
import com.wwzz.services.domain.journey.TUserJourneyCarInfo;
import com.wwzz.services.domain.journey.TUserJourneyDaily;
import com.wwzz.services.domain.journey.TUserJourneyDailyCity;
import com.wwzz.services.domain.journey.TUserJourneyDailyDetail;
import com.wwzz.services.domain.journey.TUserJourneyData;
import com.wwzz.services.domain.journey.TUserJourneyFiles;
import com.wwzz.services.domain.journey.TUserJourneyHotelInfo;
import com.wwzz.services.domain.journey.TUserJourneyRoomsList;
import com.wwzz.services.domain.news.TUserJournal;
import com.wwzz.services.domain.news.TUserNotice;
import com.wwzz.services.domain.users.TUser;
import com.wwzz.services.repository.TAreaRepository;
import com.wwzz.services.repository.TCityRestaurantRepository;
import com.wwzz.services.repository.TCityScenicSpotsRepository;
import com.wwzz.services.repository.TCityShopRepository;
import com.wwzz.services.repository.TTravelAgencyRepository;
import com.wwzz.services.repository.TUserBookingOrderRepository;
import com.wwzz.services.repository.TUserJournalRepository;
import com.wwzz.services.repository.TUserJourneyCarInfoRepository;
import com.wwzz.services.repository.TUserJourneyDailyDetailRepository;
import com.wwzz.services.repository.TUserJourneyDailyRepository;
import com.wwzz.services.repository.TUserJourneyDataRepository;
import com.wwzz.services.repository.TUserJourneyFilesRepository;
import com.wwzz.services.repository.TUserJourneyHotelInfoRepository;
import com.wwzz.services.repository.TUserJourneyRepository;
import com.wwzz.services.repository.TUserJourneyRoomsListRepository;
import com.wwzz.services.repository.TUserNoticeRepository;
import com.wwzz.services.repository.TUserOrderDocentsRepository;
import com.wwzz.services.repository.TUserOrderRestaurantRepository;
import com.wwzz.services.repository.TUserOrderShopRepository;
import com.wwzz.services.repository.TUserOrderTicketRepository;
import com.wwzz.services.repository.TUserRepository;
import static com.wwzz.services.utils.Constants.ERROR_CODE_OK;
import static com.wwzz.services.utils.Constants.RETURN_ERROR_CODE;
import static com.wwzz.services.utils.Constants.RETURN_SUCCESS_CODE;
import com.wwzz.services.utils.Utils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.velocity.VelocityEngineUtils;

/**
 *
 * @author JBOSS
 */
@Service
@Transactional
public class JourneyServicesImpl extends ResultDataServices implements JourneyServices{
    @Autowired
    private TUserRepository userRepo;
    @Autowired
    private TUserJourneyFilesRepository userJfRepo;
    @Autowired
    private TTravelAgencyRepository travelRepo;
    @Autowired
    private TAreaRepository areaRepo;
    @Autowired
    private TUserJourneyRepository userJRepo;
    @Autowired
    private TUserJourneyCarInfoRepository userJCar;
    @Autowired
    private TUserJourneyDailyDetailRepository userJDD;
    @Autowired
    private TUserJourneyDailyRepository userJD;
    @Autowired
    private TUserJourneyHotelInfoRepository userJH;
    @Autowired
    private TUserJourneyRoomsListRepository userJR;
    @Autowired
    private VelocityEngine velocityEngine;
    @Autowired
    private UserNoticeServices noticeServices;
    @Autowired
    private TUserBookingOrderRepository orderRepo;
    @Autowired
    private TUserOrderShopRepository orderShRepo;
    @Autowired
    private TUserOrderRestaurantRepository orderRtRepo;
    @Autowired
    private TUserOrderTicketRepository orderTkRepo;
    @Autowired
    private TUserOrderDocentsRepository orderDcRepo;
    @Autowired
    private TUserJourneyHotelInfoRepository journeyHRepo;
    @Autowired
    private TUserJournalRepository journalRepo;
    @Autowired
    private TUserJourneyRoomsListRepository journeyRoRepo;
    @Autowired
    private TUserJourneyDataRepository journeyDRepo;
    @Autowired
    private TUserNoticeRepository noticeRepo;
    
    @Override
    public Map register_my_journey(Map params) {
        Map m=auth_apps(params, 1);
        if(!isAppAuth(m))return m;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        String jid=(String) params.get("jjid");
        String jname=(String) params.get("jname");
        String travel=(String) params.get("travel");
        String sn=(String) params.get("sn");
        Integer cn=(Integer) params.get("cn");
        Integer day=(Integer) params.get("dayn");
        Integer arean=(Integer) params.get("arean");
        String outArea=(String) params.get("oa");
        String inArea=(String) params.get("ia");
        String outArea1=(String) params.get("oa1");
        String gdate=(String) params.get("gdate");
        String idate=(String) params.get("idate");
        String odate=(String) params.get("odate");
        Integer vp=(Integer) params.get("vp");
        if(!hasText(jid)){
           return raiseError(1004);
        }
        if(!hasText(jname)){
            return raiseError(1090);
        }
        if(!hasText(travel)){
            return raiseError(1091);
        }
        TTravelAgency ta=travelRepo.findOne(Long.parseLong(travel));
        if(ta==null){
            return raiseError(1091);
        }
        if(!hasText(sn)){
            return raiseError(1092);
        }
        if(cn==null || cn<1){
            return raiseError(1093);
        }
        if(day==null || day<1){
            return raiseError(1094);
        }
        if(arean==null || arean<1){
            return raiseError(1095);
        }
        if(!hasText(outArea)){
            return raiseError(1096);
        }
        TArea ar=areaRepo.findOne(outArea);
        if(ar==null){
            return raiseError(1096);
        }
        if(!hasText(inArea)){
            return raiseError(1097);
        }
        TArea ar1=areaRepo.findOne(inArea);
        if(ar1==null){
            return raiseError(1097);
        }
        if(!hasText(outArea1)){
            return raiseError(1098);
        }
        TArea ar2=areaRepo.findOne(outArea1);
        if(ar2==null){
            return raiseError(1098);
        }
        long t1,t2,t3;
        if(!hasText(gdate)){
            return raiseError(1099);
        }
        try{t1=Long.parseLong(gdate);
        }catch(NumberFormatException ex){return raiseError(1099);}
        
        if(!hasText(idate)){
            return raiseError(1100);
        }
        try{t2=Long.parseLong(idate);
        }catch(NumberFormatException ex){return raiseError(1100);}
        
        if(!hasText(odate)){
            return raiseError(1101);
        }
        try{t3=Long.parseLong(odate);
        }catch(NumberFormatException ex){return raiseError(1101);}
        if(vp==null){
            return raiseError(1102);
        }else if(vp<1 || vp>4)return raiseError(1102);
        TUserJourney ju=new TUserJourney();
        ju.setNameZh(jname);ju.setTravelAgency(ta);
        ju.setSerialNumber(sn);ju.setTotalNumber(cn);
        ju.setDayNumber(day);ju.setCountryNumber(arean);
        ju.setDepartureExitCity(ar);
        ju.setDepartureEntryCity(ar1);
        ju.setReturnExitCity(ar2);
        ju.setBeginTime(new Date(t1));
        ju.setDepartureEntryTime(new Date(t2));
        ju.setEndTime(new Date(t3));
        ju.setReturnExitTime(ju.getEndTime());
        if(userJfRepo.countByJourneyAndCategory(jid, 1)==0){
            return raiseError(1103);
        }
        if(userJfRepo.countByJourneyAndCategory(jid, 2)==0){
            return raiseError(1104);
        }
        if(userJfRepo.countByJourneyAndCategory(jid, 3)==0){
            return raiseError(1105);
        }
        if(userJfRepo.countByJourneyAndCategory(jid, 4)==0){
            return raiseError(1106);
        }
        ju.setFile(jid);ju.setStatus(JOURNEY_STATE_1);
        ju.setCreated(new Date());ju.setModified(new Date());
        ju.setUser(user);
        userJRepo.save(ju);
        noticeServices.journey_state_notice(ju);
        m=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return m;
    }

    @Override
    public Map add_my_journey_files(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String cid=(String) param.get("cid");
        Integer cat=(Integer) param.get("cat");
        String store=(String) param.get("store");
        String view=(String) param.get("view");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(cid)){
            return raiseError(1004);
        }
        if(cat==null){
            return raiseError(1004);
        }else if(cat<1 || cat>4){
            return raiseError(1004);
        }
        if(!hasText(view)){
            return raiseError(1004);
        }
        if(!hasText(store)){
            return raiseError(1004);
        }
        TUserJourneyFiles ff=new TUserJourneyFiles();
        ff.setId(UUID.randomUUID().toString());
        ff.setJourney(cid);
        ff.setCategory(cat);
        ff.setStoragePath(store);
        ff.setViewUrl(view);
        userJfRepo.save(ff);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("view", ff.getViewUrl());
        rm.put("fid", ff.getId());
        return rm;
    }

    @Override
    public Map remove_my_journey_files(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String cid=(String) param.get("cid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(cid)){
            return raiseError(1032);
        }
        TUserJourneyFiles sf=userJfRepo.findOne(cid);
        if(sf!=null){
            file_delete_on_fastdfs(sf.getStoragePath());
            userJfRepo.delete(sf);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map add_journey_hotel_info(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String hid=(String) param.get("hid");
        String date=(String) param.get("date");
        String ar=(String) param.get("area");
        String hname=(String) param.get("hname");
        String phone=(String) param.get("phone");
        String fax=(String) param.get("fax");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(hid)){
            return raiseError(1032);
        }
        if(!NumberUtils.isNumber(hid)){
            return raiseError(1032);
        }
        TUserJourneyHotelInfo hotel=userJH.findOne(Long.parseLong(hid));
        if(hotel==null){
            return raiseError(1032);
        }
        if(!hasText(date) || !NumberUtils.isDigits(date)){
            return raiseError(1111);
        }
        TArea area = null;
        if(hasText(ar) && !ar.equals("--")){
            area=areaRepo.findOne(ar);
            if(area==null)return raiseError(1032);
        }
        if(!hasText(hname)){
            return raiseError(1112);
        }
        if(!hasText(phone)){
            return raiseError(1113);
        }
        hotel.setArea(area);
        hotel.setFax(fax);hotel.setHotelName(hname);
        hotel.setJourneyDate(new Date(Long.parseLong(date)));
        hotel.setPhone(phone);hotel.setModified(new Date());
        userJH.save(hotel);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map add_journey_room_info(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String room=(String) param.get("room");
        String name=(String) param.get("name");
        Integer sex=(Integer) param.get("sex");
        String rp=(String) param.get("rp");
        String py=(String) param.get("py");
        String pym=(String) param.get("pym");
        String birth=(String) param.get("birth");
        String pass=(String) param.get("pass");
        String phone=(String) param.get("phone");
        String name1=(String) param.get("name1");
        Integer sex1=(Integer) param.get("sex1");
        String rp1=(String) param.get("rp1");
        String py1=(String) param.get("py1");
        String pym1=(String) param.get("pym1");
        String birth1=(String) param.get("birth1");
        String pass1=(String) param.get("pass1");
        String phone1=(String) param.get("phone1");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        if(!hasText(room))return raiseError(1032);
        TUserJourney ju=userJRepo.findOne(Long.parseLong(jid));
        if(ju==null){
            return raiseError(1114);
        }
        List<TUserJourneyRoomsList> temp=new ArrayList();
        if(!hasText(name))return raiseError(1117);
        if(!hasText(py))return raiseError(1118);
        if(!hasText(pym))return raiseError(1119);
        if(!hasText(pass))return raiseError(1120);
        if(!hasText(phone))return raiseError(1121);
        if(hasText(name1)){
            if(!hasText(py1))return raiseError(1118);
            if(!hasText(pym1))return raiseError(1119);
            if(!hasText(pass1))return raiseError(1120);
            if(!hasText(phone1))return raiseError(1121);
        }
        TUserJourneyRoomsList rr=new TUserJourneyRoomsList();
        rr.setRoomInfo(room);rr.setJourney(ju);
        rr.setName(name);rr.setGender(sex==null?1:sex);
        if(hasText(birth)){
            rr.setBirthday(new Date(Long.parseLong(birth)));
        }
        rr.setPassportNo(pass);rr.setPhone(phone);
        rr.setFirstName(py);rr.setLastName(pym);
        rr.setRelation(rp);
        temp.add(rr);
        if(hasText(name1)){
            rr=new TUserJourneyRoomsList();
            rr.setRoomInfo(room);rr.setJourney(ju);
            rr.setName(name1);rr.setGender(sex1==null?1:sex1);
            if(hasText(birth1)){
                rr.setBirthday(new Date(Long.parseLong(birth1)));
            }
            rr.setPassportNo(pass1);rr.setPhone(phone1);
            rr.setFirstName(py1);rr.setLastName(pym1);
            rr.setRelation(rp1);
            temp.add(rr);
        }
        List<TUserJourneyRoomsList>list= userJR.findByJourneyAndRoomInfo(ju, room);
        if(!list.isEmpty()){
            for (TUserJourneyRoomsList r : list) {
                userJR.delete(r);
            }
        }
        userJR.save(temp);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map add_journey_car_info(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String cid=(String) param.get("cid");
        String cname=(String) param.get("cname");
        String drvname=(String) param.get("drvname");
        String carno=(String) param.get("cn");
        String phone=(String) param.get("phone");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(cid) || !NumberUtils.isDigits(cid)){
            return raiseError(1032);
        }
        TUserJourneyCarInfo cinfo=userJCar.findOne(Long.parseLong(cid));
        if(cinfo==null){
            return raiseError(1032);
        }
        if(!hasText(drvname)){
            return raiseError(1109);
        }
        if(!hasText(carno)){
            return raiseError(1110);
        }
        if(!hasText(phone)){
            return raiseError(1113);
        }
        cinfo.setCarNumber(carno);
        cinfo.setPhone(phone);
        cinfo.setDriverName(drvname);
        cinfo.setCarModel(cname);
        cinfo.setModified(new Date());
        userJCar.save(cinfo);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm; 
    }

    @Override
    public Map add_journey_daily_info(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        List times=(List) param.get("times");
        List items=(List) param.get("items");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        if(times==null || times.isEmpty()){
            return raiseError(1032);
        }
        if(items==null || items.isEmpty()){
            return raiseError(1032);
        }
        if(times.size()!=items.size()){
            return raiseError(1032);
        }
        TUserJourneyDaily ju=userJD.findOne(Long.parseLong(jid));
        if(ju==null){
            return raiseError(1114);
        }
        List<TUserJourneyDailyDetail> dds=userJDD.findByDaily(ju);
        if(!dds.isEmpty()){
            for (TUserJourneyDailyDetail dd : dds) {
                userJDD.delete(dd);
            }
        }
        dds=new ArrayList<>();
        TUserJourneyDailyDetail dd;
        for (int i=0;i< times.size();i++) {
            String time=(String) times.get(i);
            String item=(String) items.get(i);
            if(!hasText(time)){
                return raiseError(1032);
            }
            if(!hasText(item)){
                return raiseError(1032);
            }
            dd=new TUserJourneyDailyDetail();
            dd.setContents(item);
            dd.setJourneyTime(time);
            dd.setDaily(ju);dd.setCreated(new Date());
            dd.setModified(new Date());
            dds.add(dd);
        }
        userJDD.save(dds);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm; 
    }
    /**
     * View My Journey Edit
     * @param param
     * @return 
     */
    @Override
    public Map view_journey_info_1(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        TUserJourney ju=userJRepo.findOne(Long.parseLong(jid));
        if(ju==null){
            return raiseError(1114);
        }
        Map result=new HashMap();
        List<TUserJourneyCarInfo> carlst=userJCar.findByJourney(ju);
        if(carlst!=null && !carlst.isEmpty()){
            TUserJourneyCarInfo cinfo=carlst.get(0);
            Map pm=new HashMap();
            pm.put("cid", cinfo.getId().toString());
            pm.put("cm", Utils.valueOf(cinfo.getCarModel()));
            pm.put("name", Utils.valueOf(cinfo.getDriverName()));
            pm.put("cn", Utils.valueOf(cinfo.getCarNumber()));
            pm.put("phone", Utils.valueOf(cinfo.getPhone()));
            result.put("carinfo", getDetailInfo("car", pm));
            result.put("cid", cinfo.getId().toString());
        }else{
            result.put("carinfo", "--");
            result.put("cid", "--");
        }
        result.put("other1", Utils.valueOf(ju.getOther1()));
        result.put("other2", Utils.valueOf(ju.getOther2()));
        result.put("other3", Utils.valueOf(ju.getOther3()));
        List<TUserJourneyDaily> dlist=userJD.findByJourneyOrderByJourneyDateAsc(ju);
        List data=new ArrayList();
        if(dlist!=null && !dlist.isEmpty()){
            for (TUserJourneyDaily dt : dlist) {
                Map mm=new HashMap();
                mm.put("ddid",dt.getId().toString());
                mm.put("date",StringUtils.substringAfter((Utils.dateToString(dt.getJourneyDate(),"-")),"-"));
                mm.put("title",dt.getTitle());
                mm.put("count",userJDD.countByDaily(dt));
                data.add(mm);
            }
        }
        result.put("jr", data);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, result);
        return rm; 
    }

    @Override
    public Map view_journey_info_2(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        TUserJourney ju=userJRepo.findOne(Long.parseLong(jid));
        if(ju==null){
            return raiseError(1114);
        }
        List<TUserJourneyHotelInfo> hotels=userJH.findByJourney(ju);
        List data=new ArrayList();
        if(hotels!=null && !hotels.isEmpty()){
            for (TUserJourneyHotelInfo dt : hotels) {
                Map mm=new HashMap();
                mm.put("hid",dt.getId().toString());
                mm.put("date",StringUtils.substringAfter((Utils.dateToString(dt.getJourneyDate(),"-")),"-"));
                mm.put("title",Utils.valueOf(dt.getHotelName()));
                mm.put("hinfo",Utils.valueOf(dt.getPhone())+"|"+Utils.valueOf(dt.getFax()));
                data.add(mm);
            }
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        return rm; 
    }

    @Override
    public Map view_journey_info_3(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        TUserJourney ju=userJRepo.findOne(Long.parseLong(jid));
        if(ju==null){
            return raiseError(1114);
        }
        List<TUserJourneyRoomsList> rooms=userJR.findByJourneyOrderByRoomInfoAsc(ju);
        List data=new ArrayList();
        if(rooms!=null && !rooms.isEmpty()){
            Map<String,List> ddlist=new LinkedHashMap();
            for (TUserJourneyRoomsList dt : rooms) {
                String rid=dt.getRoomInfo();
                if(ddlist.containsKey(rid)){
                    List lst=ddlist.get(rid);
                    lst.add(dt);
                }else{
                    List lst1=new ArrayList();
                    lst1.add(dt);
                    ddlist.put(rid, lst1);
                }
            }
            for (Map.Entry<String, List> et : ddlist.entrySet()) {
                Map mm=new HashMap();
                List value = et.getValue();
                String ss;Map v=new HashMap();
                TUserJourneyRoomsList rr;
                if(!value.isEmpty() && value.size()==2){
                    rr=(TUserJourneyRoomsList) value.get(0);
                    ss=rr.getName();
                    v.put("name", Utils.valueOf(rr.getName()));
                    v.put("sex", rr.getGender()==1?"男":"女");
                    v.put("rp", Utils.valueOf(rr.getRelation()));
                    v.put("py", Utils.valueOf(rr.getFirstName()));
                    v.put("pym", Utils.valueOf(rr.getLastName()));
                    v.put("b", rr.getBirthday()==null?"  ":Utils.dateToString(rr.getBirthday(), "-"));
                    v.put("pass", Utils.valueOf(rr.getPassportNo()));
                    v.put("phone", Utils.valueOf(rr.getPhone()));
                    rr=(TUserJourneyRoomsList) value.get(1);
                    ss=ss+"        "+rr.getName();
                    v.put("name1", Utils.valueOf(rr.getName()));
                    v.put("sex1", rr.getGender()==1?"男":"女");
                    v.put("rp1", Utils.valueOf(rr.getRelation()));
                    v.put("py1", Utils.valueOf(rr.getFirstName()));
                    v.put("pym1", Utils.valueOf(rr.getLastName()));
                    v.put("b1", rr.getBirthday()==null?"  ":Utils.dateToString(rr.getBirthday(), "-"));
                    v.put("pass1", Utils.valueOf(rr.getPassportNo()));
                    v.put("phone1", Utils.valueOf(rr.getPhone()));
                }else{
                    rr=(TUserJourneyRoomsList) value.get(0);
                    ss=rr.getName();
                    v.put("name", Utils.valueOf(rr.getName()));
                    v.put("sex", rr.getGender()==1?"男":"女");
                    v.put("rp", Utils.valueOf(rr.getRelation()));
                    v.put("py", Utils.valueOf(rr.getFirstName()));
                    v.put("pym", Utils.valueOf(rr.getLastName()));
                    v.put("b", rr.getBirthday()==null?"  ":Utils.dateToString(rr.getBirthday(), "-"));
                    v.put("pass", Utils.valueOf(rr.getPassportNo()));
                    v.put("phone", Utils.valueOf(rr.getPhone()));
                    //
                    v.put("name1", "  ");v.put("sex1", "--");v.put("rp1", "--");
                    v.put("py1", "--");v.put("pym1", "--");v.put("b1", "--");
                    v.put("pass1", "--");v.put("phone1", "--");
                }
                mm.put("room", et.getKey());
                mm.put("title", ss);
                mm.put("mans", getDetailInfo("roominfo", v));
                data.add(mm);
            }
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        return rm; 
    }

    private String getDetailInfo(String vm, Map data){
        String ss="resources/"+vm+".vm";
        String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, ss,"UTF-8", data);
        return text;
    }

    @Override
    public Map view_journey_daily_info(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        TUserJourneyDaily ju=userJD.findOne(Long.parseLong(jid));
        if(ju==null){
            return raiseError(1114);
        }
        List<TUserJourneyDailyDetail> details=userJDD.findByDaily(ju);
        List data=new ArrayList();
        for (TUserJourneyDailyDetail dd : details) {
            Map m=new HashMap();
            m.put("time", StringUtils.substringBeforeLast(dd.getJourneyTime(),":"));
            if(dd.getContents()!=null){
                if(dd.getContents().length()>50)m.put("cnt", dd.getContents().substring(0, 50)+"...");
                else m.put("cnt", dd.getContents());
            }else m.put("cnt", "--");
            data.add(m);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        return rm; 
    }

    @Override
    public Map load_journey_daily_info(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        TUserJourneyDaily ju=userJD.findOne(Long.parseLong(jid));
        if(ju==null){
            return raiseError(1114);
        }
        List<TUserJourneyDailyDetail> details=userJDD.findByDaily(ju);
        List data=new ArrayList();
        for (TUserJourneyDailyDetail dd : details) {
            Map m=new HashMap();
            m.put("time", StringUtils.substringBeforeLast(dd.getJourneyTime(),":"));
            if(dd.getContents()!=null){
                m.put("cnt", dd.getContents());
            }else m.put("cnt", "--");
            data.add(m);
        }
        String dt=getResource("journey.date.title",Utils.dateToString(ju.getJourneyDate(),"-"),Integer.toString(ju.getDateNumber()));
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        rm.put("date", dt);rm.put("title", ju.getTitle());
        return rm; 
    }

    @Override
    public Map load_journey_hotel_info(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        TUserJourneyHotelInfo hotel=userJH.findOne(Long.parseLong(jid));
        if(hotel==null){
            return raiseError(1116);
        }
        Map mm=new HashMap();
        mm.put("hid",hotel.getId().toString());
        mm.put("date",hotel.getJourneyDate()==null?"--":Long.toString(hotel.getJourneyDate().getTime()));
        mm.put("area",hotel.getArea()==null?"--":hotel.getArea().getId());
        mm.put("areaN",hotel.getArea()==null?"--":hotel.getArea().getName1());
        mm.put("name",Utils.valueOf(hotel.getHotelName()));
        mm.put("phone",Utils.valueOf(hotel.getPhone()));
        mm.put("fax",Utils.valueOf(hotel.getFax()));
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, mm);
        return rm; 
    }

    @Override
    public Map load_journey_car_info(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        TUserJourney ju=userJRepo.findOne(Long.parseLong(jid));
        if(ju==null){
            return raiseError(1114);
        }
        List<TUserJourneyCarInfo> carlst=userJCar.findByJourney(ju);
        Map pm=new HashMap();
        if(carlst!=null && !carlst.isEmpty()){
            TUserJourneyCarInfo cinfo=carlst.get(0);
            pm.put("id", cinfo.getId().toString());
            pm.put("cm", Utils.valueOf(cinfo.getCarModel()));
            pm.put("name", Utils.valueOf(cinfo.getDriverName()));
            pm.put("cn", Utils.valueOf(cinfo.getCarNumber()));
            pm.put("phone", Utils.valueOf(cinfo.getPhone()));
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("info", pm);
        return rm; 
    }

    @Override
    public Map load_journey_rooms_info(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String room=(String) param.get("room");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        if(!hasText(room)){
            return raiseError(1115);
        }
        TUserJourney ju=userJRepo.findOne(Long.parseLong(jid));
        if(ju==null){
            return raiseError(1114);
        }
        List<TUserJourneyRoomsList> rooms=userJR.findByJourneyAndRoomInfo(ju,room);
        Map data=new HashMap();
        if(rooms!=null && !rooms.isEmpty()){
            TUserJourneyRoomsList rr;
            Map v=view_room_info(rooms);
            data.put("room", room);
            data.put("info", v);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        return rm; 
    }
    private Map view_room_info(List<TUserJourneyRoomsList> rooms){
        TUserJourneyRoomsList rr;
        Map v=new HashMap();
        if (!rooms.isEmpty() && rooms.size() == 2) {
            rr = (TUserJourneyRoomsList) rooms.get(0);
            v.put("name", Utils.valueOf(rr.getName()));
            v.put("sex", rr.getGender() == 1 ? "男" : "女");
            v.put("rp", Utils.valueOf(rr.getRelation()));
            v.put("py", Utils.valueOf(rr.getFirstName()));
            v.put("pym", Utils.valueOf(rr.getLastName()));
            v.put("b", rr.getBirthday() == null ? "--" : Long.toString(rr.getBirthday().getTime()));
            v.put("c", rr.getBirthday() == null ? "  " : Utils.dateToString(rr.getBirthday(),"-"));
            v.put("pass", Utils.valueOf(rr.getPassportNo()));
            v.put("phone", Utils.valueOf(rr.getPhone()));
            rr = (TUserJourneyRoomsList) rooms.get(1);
            v.put("name1", Utils.valueOf(rr.getName()));
            v.put("sex1", rr.getGender() == 1 ? "男" : "女");
            v.put("rp1", Utils.valueOf(rr.getRelation()));
            v.put("py1", Utils.valueOf(rr.getFirstName()));
            v.put("pym1", Utils.valueOf(rr.getLastName()));
            v.put("b1", rr.getBirthday() == null ? "--" : Long.toString(rr.getBirthday().getTime()));
            v.put("c1", rr.getBirthday() == null ? "  " : Utils.dateToString(rr.getBirthday(),"-"));
            v.put("pass1", Utils.valueOf(rr.getPassportNo()));
            v.put("phone1", Utils.valueOf(rr.getPhone()));
        } else {
            rr = (TUserJourneyRoomsList) rooms.get(0);
            v.put("name", Utils.valueOf(rr.getName()));
            v.put("sex", rr.getGender() == 1 ? "男" : "女");
            v.put("rp", Utils.valueOf(rr.getRelation()));
            v.put("py", Utils.valueOf(rr.getFirstName()));
            v.put("pym", Utils.valueOf(rr.getLastName()));
            v.put("b", rr.getBirthday() == null ? "--" : Long.toString(rr.getBirthday().getTime()));
            v.put("c", rr.getBirthday() == null ? "  " : Utils.dateToString(rr.getBirthday(),"-"));
            v.put("pass", Utils.valueOf(rr.getPassportNo()));
            v.put("phone", Utils.valueOf(rr.getPhone()));
            //
            v.put("name1", "--");
            v.put("sex1", "--");
            v.put("rp1", "--");
            v.put("py1", "--");
            v.put("pym1", "--");
            v.put("b1", "--");
            v.put("c1", "  ");
            v.put("pass1", "--");
            v.put("phone1", "--");
        }
        return v;
    }
    @Override
    public Map add_journey_other_info(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String value=(String) param.get("value");
        Integer cat=(Integer) param.get("cat");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        TUserJourney ju=userJRepo.findOne(Long.parseLong(jid));
        if(ju==null){
            return raiseError(1114);
        }
        if(cat==null)return raiseError(1032);
        switch(cat){
            case 1:ju.setOther1(value);break;
            case 2:ju.setOther2(value);break;
            case 3:ju.setOther3(value);break;
        }
        userJRepo.save(ju);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm; 
    }

    @Override
    public Map update_my_journey_state(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        TUserJourney ju=userJRepo.findOne(Long.parseLong(jid));
        if(ju==null){
            return raiseError(1114);
        }
        if(ju.getStatus()!=JOURNEY_STATE_2){
            return raiseError(1123);
        }else{
            ju.setStatus(JOURNEY_STATE_3);
            ju.setModified(new Date());
        }
        userJRepo.save(ju);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm; 
    }

    @Override
    public Map view_my_journey(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        TUserJourney ju=userJRepo.findOne(Long.parseLong(jid));
        if(ju==null){
            return raiseError(1114);
        }
        List<TUserJourneyDaily> dailies=userJD.findByJourneyOrderByJourneyDateAsc(ju);
        List data=new ArrayList();
        StringBuilder sb=new StringBuilder();
        sb.append(ju.getCountryNumber()).append("国").append(ju.getDayNumber()).append("日");
        String prefix=clientApp.getUrl("file.type.prefix");
        if(!dailies.isEmpty()){
            for (int i=0;i<dailies.size();i++) {
               TUserJourneyDaily da=dailies.get(i);
               Map m=new HashMap();
               String dt=getResource("journey.daily.item", Integer.toString(i+1));
               dt=dt+"\n"+Utils.dateToString(da.getJourneyDate(), "/");
               m.put("date", dt);
               m.put("jdate", Long.toString(da.getJourneyDate().getTime()));
               if(hasText(da.getAirinfo())){
                   m.put("air", getResource("journey.air"));
                   m.put("aicon", prefix+"air.png");
                   m.put("airid", Long.toString(da.getId()));
               }else{
                   m.put("air", "--");
               }
               TUserJourneyHotelInfo hinfo=journeyHRepo.findByJourneyAndJourneyDate(ju, da.getJourneyDate());
               if(hinfo!=null){
                   m.put("hotel", "酒店");m.put("hicon", prefix+"hotel.png");
                   m.put("hid", hinfo.getId().toString());
               }else {
                   m.put("hotel", "酒店");m.put("hid","--");m.put("hicon", prefix+"hotel.png");
               }
               List<TUserJourneyDailyCity> cities=da.getCities();
               List lst=new ArrayList();
               if(!cities.isEmpty()){
                   for (TUserJourneyDailyCity ct : cities) {
                       Map city=new HashMap();
                       city.put("city", Utils.valueOf(ct.getCity().getName1()));
                       city.put("cityId", ct.getCity().getId());
                       Date[] dts=Utils.today(ct.getJourney().getJourneyDate());
                       List<TUserBookingOrder> booking=orderRepo.find_by_journey_area(ju,ct.getCity().getId(),dts[0],dts[1]);
                       List bks=new ArrayList();
                       if(!booking.isEmpty()){
                           for (TUserBookingOrder bk : booking) {
                               Map mm=new HashMap();
                               mm.put("bbid", bk.getId().toString());
                               String tit=BOOKING_JOURNEY_CATEGORIES[bk.getCategory()-1];
                               String tit1=Utils.timeToString(bk.getServiceTime());
                               mm.put("title", getResource("journey.booking.title", tit,tit1));
                               mm.put("curl", prefix+"bk"+Integer.toString(bk.getCategory())+".png");
                               switch(bk.getCategory()){
                                   case BOOKING_CATEGORY_SHOP:
                                       TUserOrderShop ss=orderShRepo.findByBooking(bk);
                                       mm.put("bcat", 100);
                                       if(ss!=null)mm.put("hurl", ss.getShop().getImageUrl());break;
                                   case BOOKING_CATEGORY_RESTURANT:
                                       TUserOrderRestaurant rt=orderRtRepo.findByBooking(bk);
                                       mm.put("bcat", 100);
                                       if(rt!=null)mm.put("hurl", rt.getRestaurant().getImageUrl());break;
                                   case BOOKING_CATEGORY_DOCENTS:
                                       TUserOrderDocents dc=orderDcRepo.findByBooking(bk);
                                       if(dc!=null){
                                           mm.put("hurl", dc.getScenic().getImageUrl());
                                           if(dc.getType()==1){
                                                if(bk.getStatus()==BOOKING_STATE_1)mm.put("bcat", 200);
                                                else mm.put("bcat", 400);
                                            }else{
                                                mm.put("bcat", 300);
                                            } 
                                       }break;
                                   case BOOKING_CATEGORY_SS_PIAO:
                                       TUserOrderTicket tk=orderTkRepo.findByBooking(bk);
                                       mm.put("bcat", 100);
                                       if(tk!=null)mm.put("hurl", tk.getScenic().getImageUrl());break;
                                   case BOOKING_CATEGORY_DRV_1:    
                                   case BOOKING_CATEGORY_DRV_2:
                                   case BOOKING_CATEGORY_LG:
                                   case BOOKING_CATEGORY_LC:
                                   case BOOKING_CATEGORY_OG:
                                        if(bk.getStatus()==BOOKING_STATE_1)mm.put("bcat", 200);
                                        else mm.put("bcat", 400);mm.put("hurl", "--");
                                        break;    
                                   default:
                                       mm.put("bcat", 300);
                                       mm.put("hurl", "--");break;
                               }
                               bks.add(mm);
                           }
                       }
                       city.put("booking", bks);
                       lst.add(city);
                   }
               }
               m.put("cities", lst);
               data.add(m);
            }
        }
        Calendar cal=Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        Date dt=cal.getTime();
        TUserNotice p=noticeRepo.find_journey_order_notice(ju.getId(), dt);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        rm.put("count", p!=null?"1":"0");
        rm.put("title", sb.toString());
        rm.put("title1", getResource("journey.booking.title1"));
        
        return rm; 
    }
    @PersistenceContext
    private EntityManager em;
    
    @Override
    public Map view_my_journal_list(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1032);
        }
        List<TUserJournal>lst=journalRepo.findByUser(user);
        List data=new ArrayList();
        for (TUserJournal jj : lst) {
            Map m=new HashMap();
            m.put("id", jj.getId().toString());
            m.put("title", Utils.valueOf(jj.getTitle()));
            m.put("date", Utils.dateToString(jj.getModified(),"-"));
            data.add(m);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public Map add_my_journal(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String cnt=(String) param.get("cnt");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1032);
        }
        if(!hasText(cnt)){
            return raiseError(1032);
        }
        TUserJournal jr=new TUserJournal();
        jr.setContent(cnt);
        if(cnt.length()<=20){
            jr.setTitle(cnt);
        }else jr.setTitle(StringUtils.substring(cnt,0, 20));
        jr.setCreated(new Date());
        jr.setModified(new Date());
        jr.setUser(user);
        journalRepo.save(jr);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map update_my_journal(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String cnt=(String) param.get("cnt");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1032);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        TUserJournal jr=journalRepo.findOne(Long.parseLong(jid));
        if(jr==null)return raiseError(1032);
        if(!hasText(cnt)){
            return raiseError(1032);
        }
        jr.setContent(cnt);
        if(cnt.length()<=20){
            jr.setTitle(cnt);
        }else jr.setTitle(StringUtils.substring(cnt,0, 20));
        jr.setModified(new Date());
        journalRepo.save(jr);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map remove_my_journal(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1032);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        TUserJournal jr=journalRepo.findOne(Long.parseLong(jid));
        if(jr!=null)journalRepo.delete(jr);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        return rm;
    }

    @Override
    public Map view_my_journey_detail(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1032);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        TUserJourney jj=userJRepo.findOne(Long.parseLong(jid));
        if(jj==null)return raiseError(1032);
        
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        //rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public Map view_my_journey_rooms(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1032);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        TUserJourney jj=userJRepo.findOne(Long.parseLong(jid));
        if(jj==null)return raiseError(1032);
        List<TUserJourneyRoomsList> rooms=userJR.findByJourneyOrderByRoomInfoAsc(jj);
        List data=new ArrayList();
        if(!rooms.isEmpty()){
            Map dm=new HashMap();
            for(TUserJourneyRoomsList r:rooms){
                String rn=r.getRoomInfo();
                if(dm.containsKey(rn)){
                    List mm=(List) dm.get(rn);
                    mm.add(createMap(r,0));
                }else{
                    List mm=new ArrayList();
                    mm.add(createMap(r,0));
                    dm.put(rn, mm);
                }
            }
            Set<String> keys = dm.keySet();
            for(String ky: keys){
                Map m=new HashMap();
                m.put("room", ky);
                m.put("data", dm.get(ky));
                data.add(m);
            }
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, data);
        return rm;
    }
    private Map createMap(TUserJourneyRoomsList t,int flag){
        Map m=new HashMap();
        m.put("name", Utils.valueOf(t.getName()));
        m.put("py", Utils.valueOf(t.getFirstName()));
        m.put("pm", Utils.valueOf(t.getLastName()));
        m.put("pass", Utils.valueOf(t.getPassportNo()));
        if(flag==1){
            String gn;
            if(t.getGender()==null)gn="--";
            else{
                gn=t.getGender()==1?"男":"女";
            }
            m.put("sex", gn);
            m.put("rn", Utils.valueOf(t.getRelation()));
            m.put("birth", Utils.dateToString(t.getBirthday(),"-"));
            m.put("phone", Utils.valueOf(t.getPhone()));
        }
        return m;
    }

    @Override
    public Map view_my_journal(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1032);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        TUserJournal jr=journalRepo.findOne(Long.parseLong(jid));
        if(jr==null)return raiseError(1032);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, Utils.valueOf(jr.getContent()));
        rm.put("jid", jid);
        return rm;
    }

    @Override
    public Map view_my_journey_rooms_detail(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String room=(String) param.get("room");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1032);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        TUserJourney jj=userJRepo.findOne(Long.parseLong(jid));
        if(jj==null)return raiseError(1032);
        if(!hasText(room))return raiseError(1032);
        List<TUserJourneyRoomsList> rooms=userJR.findByJourneyAndRoomInfo(jj,room);
        Map m=view_room_info(rooms);
        //String rr=getDetailInfo("room",m);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("info", m);
        return rm;
    }

    @Override
    public Map view_my_journey_detail9(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1032);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        TUserJourney jj=userJRepo.findOne(Long.parseLong(jid));
        if(jj==null)return raiseError(1032);
        TUserJourneyData jd=journeyDRepo.findByJourney(jj);
        Map m=new HashMap();
        StringBuilder sb=new StringBuilder();
        sb.append(jj.getCountryNumber()).append("国");
        sb.append(jj.getDayNumber()).append("游");
        m.put("name", sb.toString());
        if(jj.getTravelAgency()!=null)
        m.put("tv", jj.getTravelAgency().getNameZh());
        else m.put("tv", "--");
        m.put("number",Utils.valueOf(jj.getSerialNumber()));
        m.put("count",jj.getTotalNumber());
        if(jd!=null){
            m.put("city", jd.getStartCity()!=null?jd.getStartCity().getName1():" ");
            m.put("rst", jd.getHotel()==null?" ":jd.getHotel());
            m.put("meal", jd.getMeal()==null?" ":jd.getMeal());
            m.put("vh", jd.getVehicle()==null?" ":jd.getVehicle());
            m.put("addr", jd.getRelationInfo()==null?" ":jd.getRelationInfo());
        }else{
            m.put("city", " ");
            m.put("rst", " ");
            m.put("meal", " ");
            m.put("vh", " ");
            m.put("addr", " ");
        }
        String rr=getDetailInfo("jr",m);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("info", rr);
        return rm;
    }

    @Override
    public Map view_my_hotel_info(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String hid=(String) param.get("hid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1032);
        }
        if(!hasText(hid) || !NumberUtils.isNumber(hid)){
            return raiseError(1032);
        }
        TUserJourneyHotelInfo jj=journeyHRepo.findOne(Long.parseLong(hid));
        if(jj==null)return raiseError(1032);
        Map m=new HashMap();
        m.put("name", jj.getHotelName());
        m.put("phone", Utils.valueOf(jj.getPhone()));
        m.put("fax", Utils.valueOf(jj.getFax()));
        if(jj.getArea()!=null){
            m.put("addr", jj.getArea().getName1());
        }else m.put("addr", "--");
        m.put("date", Utils.dateToString(jj.getJourneyDate(), "-"));
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, m);
        return rm;
    }

    @Override
    public Map view_journey_daily_air(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String aid=(String) param.get("aid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1032);
        }
        if(!hasText(aid) || !NumberUtils.isNumber(aid)){
            return raiseError(1032);
        }
        TUserJourneyDaily jd=userJD.findOne(Long.parseLong(aid));
        if(jd==null){
            return raiseError(1147);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("air", Utils.valueOf(jd.getAirinfo()));
        return rm;
    }

    @Override
    public Map view_journey_today_air(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1032);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        TUserJourney ju=userJRepo.findOne(Long.parseLong(jid));
        if(ju==null){
            return raiseError(1032);
        }
        TUserJourneyDaily jd=userJD.findByJourneyAndJourneyDate(ju, new Date());
        if(jd==null){
            return raiseError(1148);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("air", Utils.valueOf(jd.getAirinfo()));
        return rm;
    }

    @Override
    public Map view_journey_state(Map param) {
        Map rm=auth_apps(param,1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        String jid=(String) param.get("jid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return raiseError(1000);
        }
        if(!hasText(jid) || !NumberUtils.isNumber(jid)){
            return raiseError(1032);
        }
        TUserJourney ju=userJRepo.findOne(Long.parseLong(jid));
        if(ju==null){
            return raiseError(1114);
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        if(ju.getStatus()==JOURNEY_STATE_1)rm.put("cat", 100);
        else if(ju.getStatus()==JOURNEY_STATE_2)rm.put("cat", 200);
        else rm.put("cat", 300);
        return rm;
    }
}
