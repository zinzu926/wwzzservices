/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.impl;

import com.wwzz.fastdfs.ClientGlobal;
import com.wwzz.fastdfs.StorageClient1;
import com.wwzz.fastdfs.StorageServer;
import com.wwzz.fastdfs.TrackerClient;
import com.wwzz.fastdfs.TrackerServer;
import com.wwzz.fastdfs.commons.MyException;
import com.wwzz.fastdfs.commons.NameValuePair;
import com.wwzz.services.client.ApplicationClient;
import com.wwzz.services.utils.Constants;
import static com.wwzz.services.utils.Constants.ERROR_CODE;
import static com.wwzz.services.utils.Constants.ERROR_CODE_DESC;
import static com.wwzz.services.utils.Constants.ERROR_CODE_OK;
import static com.wwzz.services.utils.Constants.RETURN_CODE;
import static com.wwzz.services.utils.Constants.RETURN_MESSAGE;
import static com.wwzz.services.utils.Constants.RETURN_NO_ERROR;
import static com.wwzz.services.utils.Constants.RETURN_SUCCESS_CODE;
import static com.wwzz.services.utils.Constants.RETURN_SUCCESS_OK;
import static com.wwzz.services.utils.Constants.SUCCESS;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.util.StringUtils;

/**
 *
 * @author JBoss
 */
public class ResultDataServices implements Constants{
    @Autowired
    private MessageSource resources;
    @Autowired
    protected ApplicationClient clientApp;
    
    public ResultDataServices() {
    }
    
    public String getResource(String key) {
        try{
            return resources.getMessage(key, null, new Locale("zh"));
        }catch(NoSuchMessageException ex){
            return null;
        }
    }

    public String getResource(String key, String... values) {
        try{
            return resources.getMessage(key, values, new Locale("zh"));
        }catch(NoSuchMessageException ex){
            return null;
        }
    }

    public Map createResult(String rc, String rm, int error, String errdesc,String... success) {
        Map result=new HashMap();
        result.put(RETURN_CODE, rc);
        result.put(RETURN_MESSAGE, rc.equals(RETURN_SUCCESS_CODE)?RETURN_SUCCESS_OK:getResource(rm));
        result.put(ERROR_CODE, error);
        result.put(ERROR_CODE_DESC, error==ERROR_CODE_OK?RETURN_NO_ERROR:getResource(errdesc));
        if(success.length>0){
            result.put(SUCCESS, Boolean.valueOf(success[0]));
        }
        return result;
    }

    public Map createMessage(String rc, String prefix, int error, String... success) {
        Map result=new HashMap();
        result.put(RETURN_CODE, rc);
        String msg;
        if(success.length>0){
            msg=getResource(prefix+Integer.toString(error));
        }else{
            msg=getResource(prefix+Integer.toString(error),success);
        }
        result.put(RETURN_MESSAGE, rc.equals(RETURN_SUCCESS_CODE)?RETURN_SUCCESS_OK:msg);
        result.put(ERROR_CODE, error);
        result.put(ERROR_CODE_DESC, error==ERROR_CODE_OK?RETURN_NO_ERROR:msg);
        return result;
    }
    
    public boolean hasText(String value){
        return StringUtils.hasText(value);
    }
    
    public Map auth_apps(Map param,int mode){
        String uid=(String) param.get("uid");
        String pt=(String) param.get("partner");
        if(!hasText(pt)){
            return createResult(RETURN_ERROR_CODE, "general.error", 200, "general.error");
        }
        String key=clientApp.getAppKey(pt);
        if(key==null){
            return createResult(RETURN_ERROR_CODE, "general.error", 200, "general.error");
        }
        if(mode==1){
            if(!hasText(uid)){
                return createResult(RETURN_ERROR_CODE, "general.error", 200, "general.error");
            }
        }        
        return createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
    }
    
    public boolean isAppAuth(Map result){
        if(result==null)return false;
        String ss=(String) result.get(RETURN_CODE);
        return ss.equals(RETURN_SUCCESS_CODE);
    }
    
    public String getErrorMessage(int error){
        return getResource("error."+Integer.toString(error));
    }
    
    public Map raiseError(int error,String... success) {
        Map result=new HashMap();
        String msg=getResource("error."+Integer.toString(error),success);
        result.put(RETURN_CODE,RETURN_ERROR_CODE );
        result.put(RETURN_MESSAGE, msg);
        result.put(ERROR_CODE, error);
        result.put(ERROR_CODE_DESC, msg);
        return result;
    }
    
    public void file_delete_on_fastdfs(String id){
        try {
            ClientGlobal.init(clientApp.getFileServerConf());
            TrackerClient tracker = new TrackerClient();
            TrackerServer trackerServer = tracker.getConnection();
            StorageServer storageServer = null;
            StorageClient1 client = new StorageClient1(trackerServer, storageServer);
            client.delete_file1(id);
            trackerServer.close();
        } catch (IOException | IllegalStateException | MyException ex) {

        }
    }
    
    public String getSystemProperty(String key){
        return clientApp.getSystemProperty(key);
    }
    
    public String file_copy_on_fastdfs(String id){
        try {
            ClientGlobal.init(clientApp.getFileServerConf());
            TrackerClient tracker = new TrackerClient();
            TrackerServer trackerServer = tracker.getConnection();
            StorageServer storageServer = null;
            StorageClient1 client = new StorageClient1(trackerServer, storageServer);
            String fn=org.apache.commons.lang3.StringUtils.substringAfterLast(id, "/");
            int m=client.download_file1(id, "/tmp/"+fn);
            if(m==0){
                NameValuePair[] metaList = new NameValuePair[1];
                metaList[0] = new NameValuePair("file-copy", "");
                String fileId = client.upload_file1("/tmp/"+fn, null, metaList);
                File f=new File("/tmp/"+fn);
                if(f.exists())f.delete();
                return fileId;
            }
            trackerServer.close();
        } catch (IOException | IllegalStateException | MyException ex) {
            
        }
        return  null;
    }
    
    public String file_upload_on_fastdfs(File file){
        try {
            ClientGlobal.init(clientApp.getFileServerConf());
            TrackerClient tracker = new TrackerClient();
            TrackerServer trackerServer = tracker.getConnection();
            StorageServer storageServer = null;
            StorageClient1 client = new StorageClient1(trackerServer, storageServer);
             NameValuePair[] metaList = new NameValuePair[1];
            metaList[0] = new NameValuePair("fileName", file.getAbsolutePath());
            String fileId = client.upload_file1(file.getAbsolutePath(), null, metaList);
            String fid=org.apache.commons.lang.StringUtils.substringAfter(fileId, "/");
            String http=clientApp.getUrlByFile("http.prefix")+fid;
            return http;
        } catch (IOException | MyException ex) {
            Logger.getLogger(ResultDataServices.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
