/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.impl;

import com.wwzz.services.api.TaskServices;
import com.wwzz.services.domain.booking.TUserBookingOrder;
import com.wwzz.services.domain.booking.TUserBookingTempOrder;
import com.wwzz.services.domain.users.TUser;
import com.wwzz.services.repository.TUserBookingOrderRepository;
import com.wwzz.services.repository.TUserBookingTempOrderRepository;
import com.wwzz.services.repository.TUserRepository;
import static com.wwzz.services.utils.Constants.RETURN_ERROR_CODE;
import com.wwzz.services.utils.Utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author JBOSS
 */
@Service
@Transactional
public class MyTaskServices extends ResultDataServices implements TaskServices{
    @Autowired
    private TUserRepository userRepo;
    @Autowired
    private TUserBookingOrderRepository bookingRepo;
    @Autowired
    private TUserBookingTempOrderRepository orderTpRepo;
    
    @Override
    public Map view_my_task(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        Integer flag=(Integer) params.get("flag");
        Integer start=(Integer) params.get("start");
        Integer count=(Integer) params.get("count");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(flag==null)return raiseError(1032);
        if(start==null)start=0;
        if(count==null)count=20;
        count=count>20?20:count;
        Pageable page=new PageRequest(start, count);
        Page<TUserBookingOrder> orders;
        if(flag==100){
            orders=bookingRepo.find_my_task_confirm(user, page);
        }else{
            orders=orderTpRepo.find_my_task(user, page);
        }
        List data=new ArrayList();
        List<TUserBookingOrder> lst=orders.getContent();
        if(!lst.isEmpty()){
            for (TUserBookingOrder b : lst) {
                Map m=new HashMap();
                m.put("bid", b.getId().toString());
                m.put("title", b.getTitle());
                m.put("time", Utils.dateToString1(b.getApplicationTime()));
                m.put("cancel", b.getStatus()<=BOOKING_STATE_2?1:0);
                m.put("curl", getResource("booking.img.prefix", Integer.toString(b.getCategory())));
                m.put("chatid", Utils.valueOf(b.getApplicant().getChatid()));
                m.put("status", BOOKING_STATES[b.getStatus()-1]);
                data.add(m);
            }
        }
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put("total", orders.getTotalElements());
        rm.put(DATA_KEY, data);
        return rm;
    }

    @Override
    public Map view_my_task_detail(Map params) {
        Map rm=auth_apps(params, 1);
        if(!isAppAuth(rm))return rm;
        String uid=(String) params.get("uid");
        String pt=(String) params.get("partner");
        String bbid=(String) params.get("bbid");
        String es=clientApp.get_uid(pt, uid);
        TUser user=userRepo.findByMobile(es);
        if(user==null){
            return createResult(RETURN_ERROR_CODE, "user.no.exist", 210, "user.no.exist");
        }
        if(!hasText(bbid) || !NumberUtils.isNumber(bbid)){
            return raiseError(1032);
        }
        TUserBookingOrder b=bookingRepo.findOne(Long.parseLong(bbid));
        if(b==null){
            return raiseError(1032);
        }
        Map m=new HashMap();
        m.put("bid", b.getId().toString());
        m.put("phone", Utils.valueOf(b.getApplicant().getMobile()));
        m.put("chatid", Utils.valueOf(b.getApplicant().getChatid()));
        m.put("uname", Utils.valueOf(b.getApplicant().getName()));
        if(b.getExecutor()==null){
            TUserBookingTempOrder top=orderTpRepo.findByBidderAndBooking(user, b);
            if(top==null)return raiseError(1032);
            m.put("price", Utils.priceToString(top.getBidAmount()));
        }
        else {
            if(b.getExecutor().getMobile().equals(user.getMobile()))
            m.put("price", Utils.priceToString(b.getOrderAmount()));
            else return raiseError(1032);
        }
        m.put("status", BOOKING_STATES[b.getStatus()-1]);
        m.put("jr", b.getJourney()!=null?1:0);
        if(b.getJourney()!=null){
            m.put("jid", b.getJourney().getId().toString());
        }
        m.put("cat",BOOKING_CATEGORIES[b.getCategory()-1]);
        m.put("cimage",Utils.valueOf(b.getApplicant().getHeadUrl()));
        m.put("flag",b.getCategory()>2?1:0);
        rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        rm.put(DATA_KEY, m);
        return rm;
    }
    
}
