/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.controller;

import com.wwzz.services.api.JourneyServices;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author JBOSS
 */
@RestController
@RequestMapping(value = "/journey",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
public class JourneyController {
    @Autowired
    private JourneyServices services;
    
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public Map add_my_journey(@RequestBody Map params){
        return services.register_my_journey(params);
    }
    
    @RequestMapping(value = "/files/add",method = RequestMethod.POST)
    public Map add_my_journey_files(@RequestBody Map params){
        return services.add_my_journey_files(params);
    }

    @RequestMapping(value = "/files/remove",method = RequestMethod.POST)
    public Map remove_my_journey_files(@RequestBody Map params){
        return services.remove_my_journey_files(params);
    }
    
    @RequestMapping(value = "/view/1",method = RequestMethod.POST)
    public Map view_my_journey_data_1(@RequestBody Map params){
        return services.view_journey_info_1(params);
    }
    
    @RequestMapping(value = "/view/22",method = RequestMethod.POST)
    public Map view_my_journey_data_2(@RequestBody Map params){
        return services.view_journey_info_2(params);
    }
    
    @RequestMapping(value = "/view/333",method = RequestMethod.POST)
    public Map view_my_journey_data_3(@RequestBody Map params){
        return services.view_journey_info_3(params);
    }
    
    @RequestMapping(value = "/view/daily",method = RequestMethod.POST)
    public Map view_my_journey_daily(@RequestBody Map params){
        return services.view_journey_daily_info(params);
    }
    // load data to edit
    @RequestMapping(value = "/load/daily",method = RequestMethod.POST)
    public Map load_my_journey_daily(@RequestBody Map params){
        return services.load_journey_daily_info(params);
    }
    
    @RequestMapping(value = "/load/hotel",method = RequestMethod.POST)
    public Map load_my_journey_hotel(@RequestBody Map params){
        return services.load_journey_hotel_info(params);
    }
    
    @RequestMapping(value = "/load/rooms",method = RequestMethod.POST)
    public Map load_my_journey_rooms(@RequestBody Map params){
        return services.load_journey_rooms_info(params);
    }
    
    @RequestMapping(value = "/load/car",method = RequestMethod.POST)
    public Map load_my_journey_car(@RequestBody Map params){
        return services.load_journey_car_info(params);
    }
    // Edit
    @RequestMapping(value = "/add/daily",method = RequestMethod.POST)
    public Map add_my_journey_daily(@RequestBody Map params){
        return services.add_journey_daily_info(params);
    }
    
    @RequestMapping(value = "/add/hotel",method = RequestMethod.POST)
    public Map add_my_journey_hotel(@RequestBody Map params){
        return services.add_journey_hotel_info(params);
    }
    
    @RequestMapping(value = "/add/rooms",method = RequestMethod.POST)
    public Map add_my_journey_rooms(@RequestBody Map params){
        return services.add_journey_room_info(params);
    }
    
    @RequestMapping(value = "/add/car",method = RequestMethod.POST)
    public Map add_my_journey_car(@RequestBody Map params){
        return services.add_journey_car_info(params);
    }
    
    @RequestMapping(value = "/add/other",method = RequestMethod.POST)
    public Map add_my_journey_other(@RequestBody Map params){
        return services.add_journey_other_info(params);
    }
    
    @RequestMapping(value = "/update/state",method = RequestMethod.POST)
    public Map update_my_journey_state(@RequestBody Map params){
        return services.update_my_journey_state(params);
    }
    
    @RequestMapping(value = "/view/result",method = RequestMethod.POST)
    public Map view_my_journey_result(@RequestBody Map params){
        return services.view_my_journey(params);
    }
    
    @RequestMapping(value = "/add/journal",method = RequestMethod.POST)
    public Map add_my_journal(@RequestBody Map params){
        return services.add_my_journal(params);
    }
    
    @RequestMapping(value = "/update/journal",method = RequestMethod.POST)
    public Map update_my_journal(@RequestBody Map params){
        return services.update_my_journal(params);
    }
    
    @RequestMapping(value = "/remove/journal",method = RequestMethod.POST)
    public Map remove_my_journal(@RequestBody Map params){
        return services.remove_my_journal(params);
    }
    
    @RequestMapping(value = "/view/jlist",method = RequestMethod.POST)
    public Map view_my_journal_list(@RequestBody Map params){
        return services.view_my_journal_list(params);
    }
    
    @RequestMapping(value = "/view/journal",method = RequestMethod.POST)
    public Map view_my_journal(@RequestBody Map params){
        return services.view_my_journal(params);
    }
    
    @RequestMapping(value = "/view/rooms",method = RequestMethod.POST)
    public Map view_my_journey_rooms(@RequestBody Map params){
        return services.view_my_journey_rooms(params);
    }
    
    @RequestMapping(value = "/view/rooms/detail",method = RequestMethod.POST)
    public Map view_my_journey_rooms_detail(@RequestBody Map params){
        return services.view_my_journey_rooms_detail(params);
    }
    
    @RequestMapping(value = "/view/jr/detail",method = RequestMethod.POST)
    public Map view_my_journey_detail(@RequestBody Map params){
        return services.view_my_journey_detail9(params);
    }
    
    @RequestMapping(value = "/view/jr/daily/air",method = RequestMethod.POST)
    public Map view_journey_daily_air(@RequestBody Map params){
        return services.view_journey_daily_air(params);
    }
    
    @RequestMapping(value = "/view/jr/today/air",method = RequestMethod.POST)
    public Map view_journey_today_air(@RequestBody Map params){
        return services.view_journey_today_air(params);
    }
    
    @RequestMapping(value = "/view/state",method = RequestMethod.POST)
    public Map view_journey_state(@RequestBody Map params){
        return services.view_journey_state(params);
    }
    
}   

