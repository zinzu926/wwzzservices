/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.controller;

import com.wwzz.services.api.UserNoticeServices;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author JBOSS
 */
@RestController
@RequestMapping(value = "/notice",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
public class UserNoticeController {
    @Autowired
    private UserNoticeServices services;
    
    @RequestMapping(value = "/view/bids",method = RequestMethod.POST)
    public Map view_my_bid_notice(@RequestBody Map params){
        return services.view_my_bid_notice(params);
    }
    
    @RequestMapping(value = "/journey/order",method = RequestMethod.POST)
    public Map notice_my_journey_order(@RequestBody Map params){
        return services.journey_order_notice();
    }
    
    @RequestMapping(value = "/notify/jrn/order",method = RequestMethod.POST)
    public Map notify_my_journey_order(@RequestBody Map params){
        return services.recieve_notice_by_journey_order(params);
    }
    
    @RequestMapping(value = "/view/order/city",method = RequestMethod.POST)
    public Map view_journey_order_city(@RequestBody Map params){
        return services.view_city_by_journey_order(params);
    }
    
    @RequestMapping(value = "/view/city/target",method = RequestMethod.POST)
    public Map view_city_target(@RequestBody Map params){
        return services.view_city_data_jr_order(params);
    }
    
    @RequestMapping(value = "/view/journey",method = RequestMethod.POST)
    public Map view_my_journey_notice(@RequestBody Map params){
        return services.view_my_journey_notice(params);
    }
    
    @RequestMapping(value = "/view/system",method = RequestMethod.POST)
    public Map view_my_system_notice(@RequestBody Map params){
        return services.view_my_journey_system(params);
    }
    
    @RequestMapping(value = "/confirm/status",method = RequestMethod.POST)
    public Map confirm_status(@RequestBody Map params){
        return services.confirm_notice_status(params);
    }
    
    @RequestMapping(value = "/view/jds",method = RequestMethod.POST)
    public Map view_journey_daily_notice(@RequestBody Map params){
        return services.view_journey_daily_notice(params);
    }
    
    @RequestMapping(value = "/view/jds/item",method = RequestMethod.POST)
    public Map view_journey_daily_notice_item(@RequestBody Map params){
        return services.view_daily_notice(params);
    }
    
    @RequestMapping(value = "/view/airinfo",method = RequestMethod.POST)
    public Map view_notice_airinfo(@RequestBody Map params){
        return services.view_notice_airinfo(params);
    }
    
    @RequestMapping(value = "/view/title",method = RequestMethod.POST)
    public Map view_notice_title(@RequestBody Map params){
        return services.view_notice_title(params);
    }
}
