/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.controller;

import com.wwzz.services.api.pingtai.SysServices;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author tiger
 * This class is a system data management controller that do process system data such as password change,system property etc.
 */
@RestController
@RequestMapping(value = "/apps/base/sys",produces = "application/json;charset=UTF-8")
public class SysDataController {
     @Autowired
    private SysServices services;
    
     /**
      *  Change staff password. 
      * @param map HashMap
      * @return map HashMap
      */
    @RequestMapping(value = "/mpass/add",method = RequestMethod.POST)
    public Map pass_add(@RequestBody Map map){
        return services.pass_add(map);
    }
    
    /**
     * Read system property.
     * @param map HashMap
     * @return map HashMap
     */
       @RequestMapping(value = "/mraw/read",method = RequestMethod.POST)
    public Map mraw_read(@RequestBody Map map){
        return services.raw_read(map);
    }
    
    /**
     * Delete system property.
     * @param map HashMap
     * @return map HashMap
     */
      @RequestMapping(value = "/mraw/delete",method = RequestMethod.POST)
    public Map mraw_delete(@RequestBody Map map){
        return services.raw_delete(map);
    }
    
    /**
     * Update system property.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mraw/update",method = RequestMethod.POST)
    public Map mraw_update(@RequestBody Map map){
        return services.raw_update(map);
    }
    
    /**
     * Read statistical data.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mstc/read",method = RequestMethod.POST)
    public Map mstc_read(@RequestBody Map map){
        return services.stc_read(map);
    }
    
    /**
     * Down statistical excel data.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mste/post",method = RequestMethod.POST)
    public Map mste_post(@RequestBody Map map){
        return services.ste_post(map);
    }
    
    /**
     * Read user  bill data.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mbill/read",method = RequestMethod.POST)
    public Map mbill_read(@RequestBody Map map){
        return services.bill_read(map);
    }
    
    /**
     * Do payment process.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mpay/post",method = RequestMethod.POST)
    public Map mpay_post(@RequestBody Map map){
        return services.pay_post(map);
    }
}
