/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.controller;

import com.wwzz.services.api.MasterDataServices;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author JBoss
 */
@RestController
@RequestMapping(value = "/base",produces = "application/json;charset=utf-8")
public class BaseServiceController {
    @Autowired
    private MasterDataServices services;
    
    @RequestMapping(value = "/country/codes",method = RequestMethod.POST)
    public Map view_codes(@RequestBody Map params){
        String level=(String) params.get("level");
        return services.view_country_codes(level);
    }
    
    @RequestMapping(value = "/lang",method = RequestMethod.POST)
    public Map view_lang(@RequestBody Map params){
        return services.view_lanaguage();
    }
    
    @RequestMapping(value = "/lines/cat",method = RequestMethod.POST)
    public Map view_lines_cat(@RequestBody Map params){
        return services.view_line_category();
    }
    
    @RequestMapping(value = "/travel/list",method = RequestMethod.POST)
    public Map view_travel_list(@RequestBody Map params){
        return services.view_travel();
    }
    
    @RequestMapping(value = "/orgs/list",method = RequestMethod.POST)
    public Map view_user_orgs(@RequestBody Map params){
        return services.view_user_orgs();
    }
    
    @RequestMapping(value = "/cars/list",method = RequestMethod.POST)
    public Map view_user_cars(@RequestBody Map params){
        return services.view_car_models();
    }
    
    @RequestMapping(value = "/sss/list",method = RequestMethod.POST)
    public Map view_user_scenic_spots(@RequestBody Map params){
        return services.view_scenic_spots();
    }
    
    @RequestMapping(value = "/search/country",method = RequestMethod.POST)
    public Map search_country(@RequestBody Map params){
        String country=(String) params.get("cid");
        String key=(String) params.get("key");
        return services.search_city_by_name(country, key);
    }
    
    @RequestMapping(value = "/views/airports",method = RequestMethod.POST)
    public Map view_airports(@RequestBody Map params){
        String country=(String) params.get("cid");
        return services.view_airports_by_city(country);
    }
    
    @RequestMapping(value = "/views/europe/city",method = RequestMethod.POST)
    public Map view_europe_city(@RequestBody Map params){
        return services.view_europe_city_all();
    }
    
    @RequestMapping(value = "/views/exchange",method = RequestMethod.POST)
    public Map view_exchange(@RequestBody Map params){
        return services.view_exchange_rate();
    }
    
    @RequestMapping(value = "/views/logo/city",method = RequestMethod.POST)
    public Map view_logo_city(@RequestBody Map params){
        return services.view_logo_city();
    }
    
    @RequestMapping(value = "/china/province",method = RequestMethod.POST)
    public Map view_china_province(@RequestBody Map params){
        return services.view_provice_of_china();
    }
    
    @RequestMapping(value = "/china/travel",method = RequestMethod.POST)
    public Map view_china_travel(@RequestBody Map params){
        return services.view_travel_list(params);
    }
    
    @RequestMapping(value = "/china/city",method = RequestMethod.POST)
    public Map view_china_city(@RequestBody Map params){
        return services.view_china_city(params);
    }
    
    @RequestMapping(value = "/home/city",method = RequestMethod.POST)
    public Map view_home_city(@RequestBody Map params){
        return services.view_home_city(params);
    }
    
    @RequestMapping(value = "/europ/country",method = RequestMethod.POST)
    public Map view_europ_country(@RequestBody Map params){
        return services.view_europ_country(params);
    }
    
    @RequestMapping(value = "/city/target",method = RequestMethod.POST)
    public Map view_city_target(@RequestBody Map params){
        return services.view_city_object(params);
    }
    
    @RequestMapping(value = "/pcs/country",method = RequestMethod.POST)
    public Map view_pcs_country(@RequestBody Map params){
        return services.view_jinsong_country();
    }
    
    @RequestMapping(value = "/pcs/city",method = RequestMethod.POST)
    public Map view_pcs_city(@RequestBody Map params){
        return services.view_jinsong_city(params);
    }
    
    @RequestMapping(value = "/pcs/items",method = RequestMethod.POST)
    public Map view_pcs_items(@RequestBody Map params){
        return services.view_jinsong_items(params);
    }
    
    @RequestMapping(value = "/views/exchange1",method = RequestMethod.POST)
    public Map view_exchange1(@RequestBody Map params){
        return services.view_current_exchange(params);
    }
}
