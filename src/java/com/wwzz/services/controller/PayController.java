/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.controller;

import com.wwzz.pay.alipay.util.AlipayNotify;
import com.wwzz.services.api.BookingServices;
import com.wwzz.services.repository.TUserBookingOrderRepository;
import com.wwzz.services.utils.Constants;
import com.wwzz.weixin.newpay.res.ResponseHandler;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author JBOSS
 */
@Controller
@RequestMapping("/wwzzpay")
public class PayController {
    @Autowired
    private TUserBookingOrderRepository orderRepo;
    @Autowired
    private BookingServices bookingServices;
    
    @RequestMapping(value="/alipay_wap_notify/{uid}/{partner}/{bid}",method = RequestMethod.POST)
    public String aliPayWapReturnUrl(@PathVariable("uid") String uid,@PathVariable("partner")String pt,
            @PathVariable("bid")String bid, HttpServletResponse response,HttpServletRequest request,ModelMap modelMap) throws Exception    { 
	response.setCharacterEncoding("UTF-8");
        
        //获取支付宝POST过来反馈信息
        Map<String, String> params = new HashMap<>();
        Map requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            params.put(name, valueStr);
        }
        System.out.println("Pay Params:"+params);
        String trade_status = params.get("trade_status");
        String buyer_email= params.get("buyer_email");
        String trade_no= params.get("trade_no");
        String price= params.get("price");
        String pay_time= params.get("gmt_payment");
        
        PrintWriter out;
        try {
            out = response.getWriter();
            if (trade_status.equals("TRADE_SUCCESS")) {
                bookingServices.alipay_post_process(uid, pt, bid);
                bookingServices.add_user_bill(uid, pt, bid,buyer_email,trade_no,price,pay_time,
                        Constants.PAYMENT_SYSTEM_ALIPAY,null);
                out.println("success");	
            } else if(trade_status.equals("TRADE_FINISHED")){
                
            }else{
                out.println("fail");
            }
            out.close();
        } catch (IOException e) {

        }
        return null;
    }
    
    @RequestMapping(value = "/alipay_wap_return", method = RequestMethod.GET)
    public String aliPayReturnUrl(HttpServletResponse response, HttpServletRequest request, ModelMap modelMap) throws Exception {
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = null;
        try {
            out = response.getWriter();
        } catch (IOException e) {
        }

        Map<String, String> params = new HashMap<>();
        Map requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                        : valueStr + values[i] + ",";
            }
            valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }

        String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"), "UTF-8");

        String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"), "UTF-8");

        String result = new String(request.getParameter("result").getBytes("ISO-8859-1"), "UTF-8");

        boolean verify_result = AlipayNotify.verifyReturn(params);

        if (verify_result) {//验证成功
            int user_id = 0;
            if (request.getSession().getAttribute("user_id") != null) {
                user_id = Integer.parseInt(request.getSession().getAttribute("user_id").toString());
                return "redirect:/memberMain/main/" + user_id;
            } else {
                modelMap.put("errormsg", "参数获取失败<user_id>!");
                return "wap_tip";
            }
        } else {
            modelMap.put("errormsg", "同步通知验证失败!");
            return "wap_tip";
        }

    }
    
    @RequestMapping(value="/alipay_batch_notify/{partner}/{bid}",method = RequestMethod.POST)
    public String aliPay_batch_trabs_notify(@PathVariable("partner")String pt,
            @PathVariable("bid")String bills, HttpServletResponse response,HttpServletRequest request,ModelMap modelMap) throws Exception    { 
	response.setCharacterEncoding("UTF-8");
        
        //获取支付宝POST过来反馈信息
        Map<String,String> params = new HashMap<>();
	Map requestParams = request.getParameterMap();
	for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                    valueStr = (i == values.length - 1) ? valueStr + values[i]
                                    : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
            params.put(name, valueStr);
	}
        System.out.println("Pay Params:"+params);
        PrintWriter out;
        out = response.getWriter();
        String success_details = params.get("success_details");
        String fail_details=params.get("fail_details");
        if(success_details!=null){
            bookingServices.alipay_batch_postprocess(pt, bills);
            out.println("success");
        }else{
            out.println("fail");
        }
        out.close();
        return null;
    }
    
    @RequestMapping(value="/wxpay_notify/{uid}/{partner}/{bid}")
    public String weixin_jsPay_notifyUrl(@PathVariable("uid") String uid,@PathVariable("partner")String pt,
            @PathVariable("bid")String bid, HttpServletResponse response,HttpServletRequest request,ModelMap modelMap){ 
        ResponseHandler resHandler = new ResponseHandler(request,response);
        System.out.println("resHandler:"+resHandler.getSmap());
        if (resHandler.getSmap().get("return_code").equals("SUCCESS") && 
            resHandler.getSmap().get("result_code").equals("SUCCESS")){
            String openid=resHandler.getSmap().get("openid").toString();
            String tid=resHandler.getSmap().get("transaction_id").toString();
            String tno=resHandler.getSmap().get("out_trade_no").toString();
            String price=resHandler.getSmap().get("total_fee").toString();
            String time_end=resHandler.getSmap().get("time_end").toString();
            SimpleDateFormat ss1=new SimpleDateFormat("yyyyMMddHHmmss");
            Date dt;
            try {
                dt=ss1.parse(time_end);
            } catch (ParseException ex) {
                dt=new Date();
            }
            ss1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            time_end=ss1.format(dt);
            bookingServices.alipay_post_process(uid, pt, bid);
            bookingServices.add_user_bill(uid, pt, bid,openid,tno,price,time_end,
                        Constants.PAYMENT_SYSTEM_WEIXIN,tid);
        }
        return null;
    }
   
}
