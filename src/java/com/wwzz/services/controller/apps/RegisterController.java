/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.controller.apps;

import com.wwzz.services.api.ChatManager;
import com.wwzz.services.api.SMServices;
import com.wwzz.services.api.apps.AppServices;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author JBoss
 */
@RestController
@RequestMapping(value = "/apps/register",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
public class RegisterController {
    @Autowired
    private AppServices services;
    @Autowired
    private SMServices smservices;
    @Autowired
    private ChatManager chatService;
    private Map resultMap;
    
    @RequestMapping(value = "/auth/code",method = RequestMethod.POST)
    public Map verify_phone_code(@RequestBody Map params){
        String code=(String) params.get("code");
        String phone=(String) params.get("phone");
        String area=(String) params.get("area");
        resultMap=services.verify_phone_code(code, phone, area);
        return resultMap;
    }
    
    @RequestMapping(value = "/send/check/code",method = RequestMethod.POST)
    public Map send_code(@RequestBody Map params){
        String code=(String) params.get("code");
        String phone=(String) params.get("phone");
        resultMap=smservices.send_check_code(code, phone);
        return resultMap;
    }
    
    @RequestMapping(value = "/auth/regcode",method = RequestMethod.POST)
    public Map verify_reg_code(@RequestBody Map params){
        String code=(String) params.get("code");
        String phone=(String) params.get("phone");
        String area=(String) params.get("area");
        resultMap=services.verify_reg_code(code, phone, area);
        return resultMap;
    }
    
    @RequestMapping(value = "/user/add",method = RequestMethod.POST)
    public Map add_user_base_info(@RequestBody Map params){
        return services.user_register(params);
    }
    
    @RequestMapping(value = "/user/info/view",method = RequestMethod.POST)
    public Map view_user_info(@RequestBody Map params){
        return services.view_user_info(params);
    }
    
    @RequestMapping(value = "/user/add/detail",method = RequestMethod.POST)
    public Map add_user_main_info(@RequestBody Map params){
        return services.user_register_info(params);
    }
    
    @RequestMapping(value = "/user/add/character",method = RequestMethod.POST)
    public Map user_reg_character(@RequestBody Map params){
        return services.user_character_reg(params);
    }
    
    @RequestMapping(value = "/user/op/add",method = RequestMethod.POST)
    public Map add_user_op(@RequestBody Map params){
        return services.add_user_identity_op(params);
    }
    
    @RequestMapping(value = "/user/op/view",method = RequestMethod.POST)
    public Map view_user_op(@RequestBody Map params){
        return services.view_user_identity_op(params);
    }
    
    @RequestMapping(value = "/user/op/rms",method = RequestMethod.POST)
    public Map remove_user_op(@RequestBody Map params){
        return services.remove_user_identity_op(params);
    }
    
    @RequestMapping(value = "/user/mingpian/add",method = RequestMethod.POST)
    public Map add_user_mingpian(@RequestBody Map params){
        return services.add_op_mingpian_file(params);
    }
    
    @RequestMapping(value = "/user/mingpian/del",method = RequestMethod.POST)
    public Map remove_user_mingpian(@RequestBody Map params){
        return services.remove_op_mingpian_file(params);
    }
    
    @RequestMapping(value = "/user/ids/view",method = RequestMethod.POST)
    public Map view_identity_list(@RequestBody Map params){
        return services.view_user_identity(params);
    }
    
    @RequestMapping(value = "/user/mingpian/view",method = RequestMethod.POST)
    public Map view_mingpian_list(@RequestBody Map params){
        return services.view_mingpian_file(params);
    }
    
    @RequestMapping(value = "/user/lg/view",method = RequestMethod.POST)
    public Map view_user_lg(@RequestBody Map params){
        return services.view_lg_info(params);
    }
    
    @RequestMapping(value = "/user/lg/add",method = RequestMethod.POST)
    public Map lg_add(@RequestBody Map params){
        return services.add_user_lg(params);
    }
    
    @RequestMapping(value = "/user/lg/rm",method = RequestMethod.POST)
    public Map remove_user_lg(@RequestBody Map params){
        return services.remove_user_lg(params);
    }
    
    @RequestMapping(value = "/user/lg/files/add",method = RequestMethod.POST)
    public Map lg_add_files(@RequestBody Map params){
        return services.add_user_lg_files(params);
    }
    
    @RequestMapping(value = "/user/lg/files/rm",method = RequestMethod.POST)
    public Map remove_lg_files(@RequestBody Map params){
        return services.remove_user_lg_files(params);
    }
    
    //-- OO
    @RequestMapping(value = "/user/oog/view",method = RequestMethod.POST)
    public Map view_user_og(@RequestBody Map params){
        return services.view_og_info(params);
    }
    
    @RequestMapping(value = "/user/oog/add",method = RequestMethod.POST)
    public Map og_user_add(@RequestBody Map params){
        return services.add_user_og(params);
    }
    
    @RequestMapping(value = "/user/oog/rm",method = RequestMethod.POST)
    public Map remove_user_og(@RequestBody Map params){
        return services.remove_user_og(params);
    }
    
    @RequestMapping(value = "/user/oog/files/add",method = RequestMethod.POST)
    public Map og_add_files(@RequestBody Map params){
        return services.add_user_og_files(params);
    }
    
    @RequestMapping(value = "/user/oog/files/rm",method = RequestMethod.POST)
    public Map remove_og_files(@RequestBody Map params){
        return services.remove_user_og_files(params);
    }
    
    //-- LocalGuide
    @RequestMapping(value = "/user/lcg/view",method = RequestMethod.POST)
    public Map view_user_lcg(@RequestBody Map params){
        return services.view_lcg_info(params);
    }
    
    @RequestMapping(value = "/user/lcg/add",method = RequestMethod.POST)
    public Map lcg_user_add(@RequestBody Map params){
        return services.add_user_lcg(params);
    }
    
    @RequestMapping(value = "/user/lcg/rm",method = RequestMethod.POST)
    public Map remove_user_lcg(@RequestBody Map params){
        return services.remove_user_lcg(params);
    }
    
    @RequestMapping(value = "/user/lcg/files/add",method = RequestMethod.POST)
    public Map lcg_add_files(@RequestBody Map params){
        return services.add_user_lcg_files(params);
    }
    
    @RequestMapping(value = "/user/lcg/files/rm",method = RequestMethod.POST)
    public Map remove_lcg_files(@RequestBody Map params){
        return services.remove_user_lcg_files(params);
    }
    
    //-- Driver
    @RequestMapping(value = "/user/drv/view",method = RequestMethod.POST)
    public Map view_user_drv(@RequestBody Map params){
        return services.view_driver_info(params);
    }
    
    @RequestMapping(value = "/user/drv/add",method = RequestMethod.POST)
    public Map drv_user_add(@RequestBody Map params){
        return services.add_user_driver(params);
    }
    
    @RequestMapping(value = "/user/drv/rm",method = RequestMethod.POST)
    public Map remove_user_drv(@RequestBody Map params){
        return services.remove_user_driver(params);
    }
    
    @RequestMapping(value = "/user/drv/files/add",method = RequestMethod.POST)
    public Map drv_add_files(@RequestBody Map params){
        return services.add_user_driver_files(params);
    }
    
    @RequestMapping(value = "/user/drv/files/rm",method = RequestMethod.POST)
    public Map remove_drv_files(@RequestBody Map params){
        return services.remove_user_driver_files(params);
    }
    
    //-- Driver
    @RequestMapping(value = "/user/dc/view",method = RequestMethod.POST)
    public Map view_user_dc(@RequestBody Map params){
        return services.view_docent_info(params);
    }
    
    @RequestMapping(value = "/user/dc/add",method = RequestMethod.POST)
    public Map dc_user_add(@RequestBody Map params){
        return services.add_user_docent(params);
    }
    
    @RequestMapping(value = "/user/dc/rm",method = RequestMethod.POST)
    public Map remove_user_dc(@RequestBody Map params){
        return services.remove_user_docent(params);
    }
    
    @RequestMapping(value = "/user/dc/files/add",method = RequestMethod.POST)
    public Map dc_add_files(@RequestBody Map params){
        return services.add_user_docent_files(params);
    }
    
    @RequestMapping(value = "/user/dc/files/rm",method = RequestMethod.POST)
    public Map remove_dc_files(@RequestBody Map params){
        return services.remove_user_docent_files(params);
    }
    
    @RequestMapping(value = "/user/intro/add",method = RequestMethod.POST)
    public Map add_user_intro(@RequestBody Map params){
        return services.add_user_intoduce(params);
    }
    
    @RequestMapping(value = "/user/photo/add",method = RequestMethod.POST)
    public Map add_user_photos(@RequestBody Map params){
        return services.add_user_photo(params);
    }
    
    @RequestMapping(value = "/user/photo/list",method = RequestMethod.POST)
    public Map view_user_photos(@RequestBody Map params){
        return services.view_user_photo_list(params);
    }
    
    @RequestMapping(value = "/user/photo/rms",method = RequestMethod.POST)
    public Map remove_user_photos(@RequestBody Map params){
        return services.remove_user_photo(params);
    }
    
    @RequestMapping(value = "/up/bima",method = RequestMethod.POST)
    public Map change_user_password(@RequestBody Map params){
        return services.change_password(params);
    }
    
    @RequestMapping(value = "/add/roster",method = RequestMethod.POST)
    public Map add_chat_roster(@RequestBody Map params){
        return chatService.add_user_roster(params);
    }
    
    @RequestMapping(value = "/view/roster",method = RequestMethod.POST)
    public Map view_chat_roster(@RequestBody Map params){
        return chatService.get_user_rost(params);
    }
    
    @RequestMapping(value = "/apple/device",method = RequestMethod.POST)
    public Map register_apple_device(@RequestBody Map params){
        return services.register_device_apple(params);
    }
    
    @RequestMapping(value = "/prefile",method = RequestMethod.POST)
    public void preprocess_file(@RequestBody Map params){
        services.preprocess_drv_file(params);
    }
    
    @RequestMapping(value = "/add/wtimes",method = RequestMethod.POST)
    public Map add_user_work_time(@RequestBody Map params){
        return services.add_user_work_time(params);
    }
    
    @RequestMapping(value = "/view/wtimes",method = RequestMethod.POST)
    public Map view_user_work_time(@RequestBody Map params){
        return services.view_user_work_time(params);
    }
    
    @RequestMapping(value = "/alipay/register",method = RequestMethod.POST)
    public Map register_user_alipay(@RequestBody Map params){
        return services.register_alipay_account(params);
    }
    
    @RequestMapping(value = "/alipay/view",method = RequestMethod.POST)
    public Map view_user_alipay(@RequestBody Map params){
        return services.view_alipay_account(params);
    }
    
    @RequestMapping(value = "/CIF9B419KJNPEXXFGMXJ",method = RequestMethod.POST)
    public Map view_file_server_conf(HttpServletRequest request){
        return services.view_file_server_conf();
    }
}
