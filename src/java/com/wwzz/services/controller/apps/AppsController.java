/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.controller.apps;

import com.wwzz.services.api.apps.LoginServices;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author JBoss
 */
@RestController
@RequestMapping(value = "/apps",produces = "application/json;charset=UTF-8")
public class AppsController {
    @Autowired
    private LoginServices services;
    
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public Map client_login(@RequestBody Map param){
        return services.app_login(param);
    }
    
    @RequestMapping(value = "/zxzxzx",method = RequestMethod.POST)
    public Map silent_login(@RequestBody Map param){
        return services.silent_login(param);
    }
    
    @RequestMapping(value = "/view/wuser",method = RequestMethod.POST)
    public Map view_user_info_by_openid(@RequestBody Map param){
        return services.get_user_info_by_openid(param);
    }
    
    @RequestMapping(value = "/logout/iphone",method = RequestMethod.POST)
    public Map logout_iphone(@RequestBody Map param){
        return services.iphone_logout(param);
    }
    
}
