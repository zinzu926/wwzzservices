/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 *
 * @author JBOSS
 */
public class LoggingIntercepter extends HandlerInterceptorAdapter{
    private static final Logger logger = LoggerFactory.getLogger("com.wwzz.services.controller");

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        long currentTime = System.currentTimeMillis();
        request.setAttribute("bTime", currentTime);
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        long currentTime = System.currentTimeMillis();
        long beginTime = (long)request.getAttribute("bTime");
        long processedTime = currentTime - beginTime;
        logger.info("URL:" +request.getRequestURI()+"-Consumed Time:"+processedTime+"ms");
        super.afterCompletion(request, response, handler, ex);
    }
    
    
}
