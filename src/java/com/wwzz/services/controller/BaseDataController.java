/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.controller;

import com.wwzz.services.api.pingtai.BaseDataServices;
import java.util.HashMap;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Tiger
 * This class is a base data controller that do base process.
 */
@RestController
@RequestMapping(value = "/apps/base",produces = "application/json;charset=UTF-8")
public class BaseDataController {
    @Autowired
    private BaseDataServices bservices;
    private Map result;
    
    @RequestMapping(value = "/area/view/state/all",method = RequestMethod.POST)
    public Map findAreaByLevel(@RequestBody Map params){
        result=new HashMap();
        result.put("datas", bservices.findAreaByLevel("--"));
        return result;
    }
    
    /**
     * Add area.
     * @param map HashMap including area informations
     * @return  map HashMap
     */
    @RequestMapping(value = "/mlocale/post",method = RequestMethod.POST)
    public Map mlocale_add(@RequestBody Map map){
        Map model=(Map) map.get("data");
        String id=(String) model.get("id");
          if(id==null || id.length()<9){
            return bservices.locale_add(map);
        }else{
            if(model.keySet().size()==1){
                return bservices.locale_delete(id);
            }else{
                return bservices.locale_update(map);
            }
        }
    }
    
    /**
     * Read area.
     * @param map HashMap including keyword, parentId ,etc
     * @return map HashMap
     */
    @RequestMapping(value = "/mlocale/read",method = RequestMethod.POST)
    public Map mlocale_read(@RequestBody Map map){
        return bservices.locale_read(map);
    }

    /**
     * Add language.
     * @param map HashMap including language informations
     * @return map HashMap 
     */
    @RequestMapping(value = "/mlang/add",method = RequestMethod.POST)
    public Map mlang_add(@RequestBody Map map){
        return bservices.lang_add(map);
    }
    
    /**
     * Read language.
     * @param map HashMap including keyword etc
     * @return map HashMap
     */
    @RequestMapping(value = "/mlang/read",method = RequestMethod.POST)
    public Map mlang_read(@RequestBody Map map){
        return bservices.lang_read(map);
    }
    
    /**
     *  Delete language.
     * @param map HashMap including language id
     * @return map HashMap
     */
    @RequestMapping(value = "/mlang/delete",method = RequestMethod.POST)
    public Map mlang_delete(@RequestBody Map map){
        return bservices.lang_delete(map);
    }
    
    /**
     *  Update language.
     * @param map HashMap including language information
     * @return map HashMap
     */
    @RequestMapping(value = "/mlang/update",method = RequestMethod.POST)
    public Map mlang_update(@RequestBody Map map){
        return bservices.lang_update(map);
    }
    
    /**
     * Add airport.
     * @param map HashMap including airport information
     * @return map HashMap
     */
    @RequestMapping(value = "/mair/add",method = RequestMethod.POST)
    public Map mair_add(@RequestBody Map map){
        return bservices.air_add(map);
    }
    
    /**
     * Read airport.
     * @param map HashMap including keyword, area id etc
     * @return map HashMap 
     */
    @RequestMapping(value = "/mair/read",method = RequestMethod.POST)
    public Map mair_read(@RequestBody Map map){
        return bservices.air_read(map);
    }
    
    /**
     * Delete airport.
     * @param map HashMap including airport id
     * @return map HashMap
     */
    @RequestMapping(value = "/mair/delete",method = RequestMethod.POST)
    public Map mair_delete(@RequestBody Map map){
        return bservices.air_delete(map);
    }
    
    /**
     * Update airport.
     * @param map HashMap including airport informations
     * @return map HashMap 
     */
    @RequestMapping(value = "/mair/update",method = RequestMethod.POST)
    public Map mair_update(@RequestBody Map map){
        return bservices.air_update(map);
    }
    
    /**
     * Add travelagency.
     * @param map HashMap including travel agency informations
     * @return map HashMap
     */
    @RequestMapping(value = "/mtra/add",method = RequestMethod.POST)
    public Map mtra_add(@RequestBody Map map){
        return bservices.tra_add(map);
    }
    
    /**
     * Read travelagency.
     * @param map HashMap including keyword,area id
     * @return map HashMap
     */
    @RequestMapping(value = "/mtra/read",method = RequestMethod.POST)
    public Map mtra_read(@RequestBody Map map){
        return bservices.tra_read(map);
    }
    
    /**
     * Delete travelagency.
     * @param map HashMap including travel agency id
     * @return map HashMap
     */
    @RequestMapping(value = "/mtra/delete",method = RequestMethod.POST)
    public Map mtra_delete(@RequestBody Map map){
        return bservices.tra_delete(map);
    }
    
    /**
     * Update travelagency.
     * @param map HashMap including travel agency information
     * @return map HashMap
     */
    @RequestMapping(value = "/mtra/update",method = RequestMethod.POST)
    public Map mtra_update(@RequestBody Map map){
        return bservices.tra_update(map);
    }
    
    /**
     * Add tourism line category. 
     * @param map HashMap including tourism line category information
     * @return map HashMap
     */
      @RequestMapping(value = "/mtor/add",method = RequestMethod.POST)
    public Map mtor_add(@RequestBody Map map){
        return bservices.tor_add(map);
    }
    
    /**
     * Read tourism line category.
     * @param map HashMap including keyword
     * @return map HashMap
     */
    @RequestMapping(value = "/mtor/read",method = RequestMethod.POST)
    public Map mtor_read(@RequestBody Map map){
        return bservices.tor_read(map);
    }
    
    /**
     * Delete tourism line category.
     * @param map HashMap including tourism line category id
     * @return map HashMap
     */
    @RequestMapping(value = "/mtor/delete",method = RequestMethod.POST)
    public Map mtor_delete(@RequestBody Map map){
        return bservices.tor_delete(map);
    }
    
    /**
     *  Update tourism line category.
     * @param map HashMap including tourism line category informations
     * @return map HashMap
     */
    @RequestMapping(value = "/mtor/update",method = RequestMethod.POST)
    public Map mtor_update(@RequestBody Map map){
        return bservices.tor_update(map);
    }
    
    /**
     * Add tourism line. 
     * @param map HashMap including tourism line information
     * @return map HashMap
     */
      @RequestMapping(value = "/mline/add",method = RequestMethod.POST)
    public Map mline_add(@RequestBody Map map){
        return bservices.line_add(map);
    }
    
    /**
     * Read tourism line.
     * @param map HashMap including keyword, tourism line category id
     * @return map HashMap
     */
    @RequestMapping(value = "/mline/read",method = RequestMethod.POST)
    public Map mline_read(@RequestBody Map map){
        return bservices.line_read(map);
    }
    
    /**
     * Delete tourism line.
     * @param map HashMap including tourism line id
     * @return map HashMap
     */
    @RequestMapping(value = "/mline/delete",method = RequestMethod.POST)
    public Map mline_delete(@RequestBody Map map){
        return bservices.line_delete(map);
    }
    
    /**
     * Update tourism line.
     * @param map HashMap including tourism line information
     * @return map HashMap
     */
    @RequestMapping(value = "/mline/update",method = RequestMethod.POST)
    public Map mline_update(@RequestBody Map map){
        return bservices.line_update(map);
    }
    
    /**
     * Add process charge category.
     * @param map HashMap including process charge category information
     * @return map HashMap
     */
    @RequestMapping(value = "/mcm/add",method = RequestMethod.POST)
    public Map mcm_add(@RequestBody Map map){
        Map model=(Map) map.get("data");
        String id=(String) model.get("id");
           if(id==null || id.contains("-")){
            return bservices.cm_add(map);
        }else{
            if(model.keySet().size()==1){
                return bservices.cm_delete(id);
            }else{
                return bservices.cm_update(map);
            }
        }
    }
    
    /**
     * Read process charge category.
     * @param map HashMap including parent id
     * @return map HashMap
     */
    @RequestMapping(value = "/mcm/read",method = RequestMethod.POST)
    public Map mcm_read(@RequestBody Map map){
        return bservices.cm_read(map);
    }
    
    /**
     * Add city introduction.
     * @param map HashMap including city introduction informations
     * @return map HashMap
     */
    @RequestMapping(value = "/mcity/add",method = RequestMethod.POST)
    public Map mcity_add(@RequestBody Map map){
        return bservices.city_add(map);
    }
    
    /**
     * Read city introduction.
     * @param map HashMap including area id
     * @return map HashMap
     */
    @RequestMapping(value = "/mcity/read",method = RequestMethod.POST)
    public Map mcity_read(@RequestBody Map map){
        return bservices.city_read(map);
    }
    
    /**
     * Add restaurant category. 
     * @param map HashMap incluing restaurant category information
     * @return map HashMap
     */
    @RequestMapping(value = "/mrc/add",method = RequestMethod.POST)
    public Map mrc_add(@RequestBody Map map){
        Map model=(Map) map.get("data");
        String id=(String) model.get("id");
          if(id==null || id.contains("-")){
            return bservices.resc_add(map);
        }else{
            if(model.keySet().size()==1){
                return bservices.resc_delete(id);
            }else{
                return bservices.resc_update(map);
            }
        }
    }
    
    /**
     * Read restaurant category.
     * @param map HashMap including parent id
     * @return map HashMap
     */
    @RequestMapping(value = "/mrc/read",method = RequestMethod.POST)
    public Map mrc_read(@RequestBody Map map){
        return bservices.resc_read(map);
    }
    
    /**
     * Read restaurant category with checking state. 
     * @param map HashMap including parent id
     * @return map HashMap
     */
   @RequestMapping(value = "/mrcc/read",method = RequestMethod.POST)
    public Map mrcc_read(@RequestBody Map map){
        return bservices.rcc_read(map);
    }

    /**
     * Add restaurant.
     * @param map HashMap including restaurant information
     * @return map HashMap
     */
      @RequestMapping(value = "/mres/add",method = RequestMethod.POST)
    public Map mres_add(@RequestBody Map map){
        return bservices.res_add(map);
    }
    
    /**
     * Read restaurant. 
     * @param map HashMap including keyword,parent id
     * @return map HashMap
     */
    @RequestMapping(value = "/mres/read",method = RequestMethod.POST)
    public Map mres_read(@RequestBody Map map){
        return bservices.res_read(map);
    }
    
    /**
     * Delete restaurant.
     * @param map HashMap including restaurant id
     * @return map HashMap
     */
    @RequestMapping(value = "/mres/delete",method = RequestMethod.POST)
    public Map mres_delete(@RequestBody Map map){
        return bservices.res_delete(map);
    }
    
    /**
     * Update restaurant.
     * @param map HashMap including restaurant information
     * @return map HashMap
     */
    @RequestMapping(value = "/mres/update",method = RequestMethod.POST)
    public Map mres_update(@RequestBody Map map){
        return bservices.res_update(map);
    }
    
    /**
     * Add restaurant package.
     * @param map HashMap restaurant package information
     * @return map HashMap
     */
   @RequestMapping(value = "/mrep/add",method = RequestMethod.POST)
    public Map mrep_add(@RequestBody Map map){
        return bservices.rep_add(map);
    }
    
    /**
     * Read restaurant package.
     * @param map HashMap including restaurant id
     * @return map HashMap
     */
    @RequestMapping(value = "/mrep/read",method = RequestMethod.POST)
    public Map mrep_read(@RequestBody Map map){
        return bservices.rep_read(map);
    }
    
    /**
     * Delete restaurant package.
     * @param map HashMap including restaurant package id
     * @return map HashMap
     */
    @RequestMapping(value = "/mrep/delete",method = RequestMethod.POST)
    public Map mrep_delete(@RequestBody Map map){
        return bservices.rep_delete(map);
    }
    
    /**
     * Update restaurant package.
     * @param map HashMap including restaurant package information
     * @return map HashMap
     */
    @RequestMapping(value = "/mrep/update",method = RequestMethod.POST)
    public Map mrep_update(@RequestBody Map map){
        return bservices.rep_update(map);
    }
    
    /**
     * Add restaurant property. 
     * @param map HashMap including restaurant property information
     * @return map HashMap
     */
     @RequestMapping(value = "/mrer/add",method = RequestMethod.POST)
    public Map mrer_add(@RequestBody Map map){
        return bservices.rer_add(map);
    }
    
    /**
     * Read restaurant property.
     * @param map HashMap including restaurant id
     * @return map HashMap
     */
    @RequestMapping(value = "/mrepr/read",method = RequestMethod.POST)
    public Map mrepr_read(@RequestBody Map map){
        return bservices.repr_read(map);
    }
    
    /**
     * Add Scenic spot category.
     * @param map HashMap including scenic spot category information
     * @return map HashMap
     */
    @RequestMapping(value = "/mssc/add",method = RequestMethod.POST)
    public Map mssc_add(@RequestBody Map map){
        Map model=(Map) map.get("data");
        String id=(String) model.get("id");
          if(id==null || id.contains("-")){
            return bservices.ssc_add(map);
        }else{
            if(model.keySet().size()==1){
                return bservices.ssc_delete(id);
            }else{
                return bservices.ssc_update(map);
            }
        }
    }
    
    /**
     *  Read Scenic spot category.
     * @param map HashMap including parent id
     * @return map HashMap
     */
    @RequestMapping(value = "/mssc/read",method = RequestMethod.POST)
    public Map mssc_read(@RequestBody Map map){
        return bservices.ssc_read(map);
    }
    
    /**
     * Read Scenic spot category with checking state.
     * @param map HashMap including parent id
     * @return map HashMap
     */
    @RequestMapping(value = "/mscc/read",method = RequestMethod.POST)
    public Map mscc_read(@RequestBody Map map){
        return bservices.scc_read(map);
    }
    
    /**
     * Add Scenic spot.
     * @param map HashMap including scenic spot information
     * @return map HashMap
     */
     @RequestMapping(value = "/mss/add",method = RequestMethod.POST)
    public Map mss_add(@RequestBody Map map){
        return bservices.ss_add(map);
    }
    
    /**
     * Read Scenic spot.
     * @param map HashMap including keyword,area id
     * @return map HashMap
     */
    @RequestMapping(value = "/mss/read",method = RequestMethod.POST)
    public Map mss_read(@RequestBody Map map){
        return bservices.ss_read(map);
    }
    
    /**
     * Delete Scenic spot.
     * @param map HashMap including scenic spot id
     * @return map HashMap
     */
    @RequestMapping(value = "/mss/delete",method = RequestMethod.POST)
    public Map mss_delete(@RequestBody Map map){
        return bservices.ss_delete(map);
    }
    
    /**
     * Update Scenic spot.
     * @param map HashMap including scenic spot information
     * @return map HashMap
     */
    @RequestMapping(value = "/mss/update",method = RequestMethod.POST)
    public Map mss_update(@RequestBody Map map){
        return bservices.ss_update(map);
    }
    
    /**
     * Add scenic spot property.
     * @param map HashMap including scenic spot property information
     * @return map HashMap
     */
    @RequestMapping(value = "/mssr/add",method = RequestMethod.POST)
    public Map mssr_add(@RequestBody Map map){
        return bservices.ssr_add(map);
    }
    
    /**
     * Read scenic spot property.
     * @param map HashMap including scenic spot id
     * @return map HashMap
     */
    @RequestMapping(value = "/mssr/read",method = RequestMethod.POST)
    public Map mssr_read(@RequestBody Map map){
        return bservices.ssr_read(map);
    }
    
    /**
     * Add shop category.
     * @param map HashMap including shop category information
     * @return map HashMap
     */
    @RequestMapping(value = "/mshc/add",method = RequestMethod.POST)
    public Map mshc_add(@RequestBody Map map){
         Map model=(Map) map.get("data");
        String id=(String) model.get("id");
          if(id==null || id.contains("-")){
            return bservices.shc_add(map);
        }else{
            if(model.keySet().size()==1){
                return bservices.shc_delete(id);
            }else{
                return bservices.shc_update(map);
            }
        }
    }
    
    /**
     * Read shop category.
     * @param map HashMap including parent id
     * @return map HashMap
     */
    @RequestMapping(value = "/mshc/read",method = RequestMethod.POST)
    public Map mshc_read(@RequestBody Map map){
        return bservices.shc_read(map);
    }
    
    /**
     * Read shop category.
     * @param map HashMap including parent id 
     * @return map HashMap
     */
    @RequestMapping(value = "/mshcc/read",method = RequestMethod.POST)
    public Map mshcc_read(@RequestBody Map map){
        return bservices.shcc_read(map);
    }
    
    /**
     * Add shop.
     * @param map HashMap including shop information
     * @return map HashMap
     */
    @RequestMapping(value = "/msh/add",method = RequestMethod.POST)
    public Map msh_add(@RequestBody Map map){
        return bservices.sh_add(map);
    }
    
    /**
     * Read shop.
     * @param map HashMap including shop informations
     * @return map HashMap
     */
    @RequestMapping(value = "/msh/read",method = RequestMethod.POST)
    public Map msh_read(@RequestBody Map map){
        return bservices.sh_read(map);
    }
    
    /**
     * Delete shop.
     * @param map HashMap including shop id
     * @return map HashMap
     */
    @RequestMapping(value = "/msh/delete",method = RequestMethod.POST)
    public Map msh_delete(@RequestBody Map map){
        return bservices.sh_delete(map);
    }
    
    /**
     * Update shop.
     * @param map HashMap including shop information
     * @return map HashMap
     */
    @RequestMapping(value = "/msh/update",method = RequestMethod.POST)
    public Map msh_update(@RequestBody Map map){
        return bservices.sh_update(map);
    }
    
    /**
     * Add shop property.
     * @param map HashMap including shop property information
     * @return map HashMap
     */
    @RequestMapping(value = "/mshr/add",method = RequestMethod.POST)
    public Map mshr_add(@RequestBody Map map){
        return bservices.shr_add(map);
    }
    
    /**
     * Read shop property.
     * @param map HashMap including shop id
     * @return map HashMap
     */
    @RequestMapping(value = "/mshr/read",method = RequestMethod.POST)
    public Map mshr_read(@RequestBody Map map){
        return bservices.shr_read(map);
    }

    /**
     * Add scenic spot docent.
     * @param map HashMap scenic spot docent information
     * @return map HashMap
     */
     @RequestMapping(value = "/mssd/add",method = RequestMethod.POST)
    public Map mssd_add(@RequestBody Map map){
        return bservices.ssd_add(map);
    }
    
    /**
     * Read scenic spot docent.
     * @param map HashMap including scenic spot id
     * @return map HashMap
     */
    @RequestMapping(value = "/mssd/read",method = RequestMethod.POST)
    public Map mssd_read(@RequestBody Map map){
        return bservices.ssd_read(map);
    }
    
    /**
     * Delete scenic spot docent.
     * @param map HashMap including scenic spot docent id
     * @return map HashMap
     */
    @RequestMapping(value = "/mssd/delete",method = RequestMethod.POST)
    public Map mssd_delete(@RequestBody Map map){
        return bservices.ssd_delete(map);
    }
    
    /**
     * Update scenic spot docent.
     * @param map HashMap scenic spot docent information
     * @return map HashMap
     */
    @RequestMapping(value = "/mssd/update",method = RequestMethod.POST)
    public Map mssd_update(@RequestBody Map map){
        return bservices.ssd_update(map);
    }
    
    /**
     * Add news. 
     * @param map HashMap including news information
     * @return map HashMap
     */
  @RequestMapping(value = "/mnews/add",method = RequestMethod.POST)
    public Map mnews_add(@RequestBody Map map){
        return bservices.news_add(map);
    }
    
    /**
     * Read news. 
     * @param map HashMap including area id
     * @return map HashMap
     */
    @RequestMapping(value = "/mnews/read",method = RequestMethod.POST)
    public Map mnews_read(@RequestBody Map map){
        return bservices.news_read(map);
    }
    
    /**
     * Delete news.
     * @param map HashMap including news id
     * @return map HashMap
     */
    @RequestMapping(value = "/mnews/delete",method = RequestMethod.POST)
    public Map mnews_delete(@RequestBody Map map){
        return bservices.news_delete(map);
    }
    
    /**
     * Update news.
     * @param map HashMap including news information
     * @return map HashMap
     */
    @RequestMapping(value = "/mnews/update",method = RequestMethod.POST)
    public Map mnews_update(@RequestBody Map map){
        return bservices.news_update(map);
    }

    /**
     * Add excel-area.
     * @param map HashMap including area informations
     * @return map HashMap
     */
    @RequestMapping(value = "/mlocale/excel",method = RequestMethod.POST)
    public Map mlocale_excel(@RequestBody Map map){
        return bservices.locale_excel(map);
    }
    
    /**
     * Add excel-airport.
     * @param map HashMap including airport informations
     * @return map HashMap
     */
    @RequestMapping(value = "/mair/excel",method = RequestMethod.POST)
    public Map mair_excel(@RequestBody Map map){
        return bservices.air_excel(map);
    }
    
    /**
     * Add excel-travelAgency.
     * @param map HashMap including travelAgency informations
     * @return map HashMap
     */
    @RequestMapping(value = "/mtra/excel",method = RequestMethod.POST)
    public Map mtra_excel(@RequestBody Map map){
        return bservices.tra_excel(map);
    }
    
    /**
     * Add excel-city introduction .
     * @param map HashMap including city introduction informations
     * @return map HashMap
     */
    @RequestMapping(value = "/mcity/excel",method = RequestMethod.POST)
    public Map mcity_excel(@RequestBody Map map){
        return bservices.city_excel(map);
    }
    
    /**
     * Add excel-restaurant.
     * @param map HashMap including restaurant informations
     * @return map HashMap
     */
    @RequestMapping(value = "/mres/excel",method = RequestMethod.POST)
    public Map mres_excel(@RequestBody Map map){
        return bservices.res_excel(map);
    }
    
    /**
     * Add excel-scenic spot.
     * @param map HashMap including scenic spot informations
     * @return map HashMap
     */
    @RequestMapping(value = "/mss/excel",method = RequestMethod.POST)
    public Map mss_excel(@RequestBody Map map){
        return bservices.ss_excel(map);
    }
    
    /**
     * Add excel-shop. 
     * @param map HashMap including shop informations
     * @return map HashMap
     */
    @RequestMapping(value = "/mshop/excel",method = RequestMethod.POST)
    public Map mshop_excel(@RequestBody Map map){
        return bservices.shop_excel(map);
    }
    
    /**
     * Add excel-user.
     * @param map HashMap including user informations
     * @return map HashMap
     */
    @RequestMapping(value = "/muser/excel",method = RequestMethod.POST)
    public Map muser_excel(@RequestBody Map map){
        return bservices.user_excel(map);
    }
    
    /**
     * Read user journey.
     * @param map HashMap including keyword,category information
     * @return map HashMap
     */
     @RequestMapping(value = "/mjou/read",method = RequestMethod.POST)
    public Map mjou_read(@RequestBody Map map){
        return bservices.jou_read(map);
    }
    
    /**
     * Add user journey daily information.
     * @param map HashMap including user journey daily information
     * @return map HashMap
     */
    @RequestMapping(value = "/mjda/add",method = RequestMethod.POST)
    public Map mjda_add(@RequestBody Map map){
        return bservices.jda_add(map);
    }
    
    /**
     * Read user journey daily information. 
     * @param map HashMap including user journey id
     * @return map HashMap
     */
    @RequestMapping(value = "/mjda/read",method = RequestMethod.POST)
    public Map mjda_read(@RequestBody Map map){
        return bservices.jda_read(map);
    }
    
    /**
     * Delete user journey daily information.
     * @param map HashMap including user journey daily id
     * @return map HashMap
     */
    @RequestMapping(value = "/mjda/delete",method = RequestMethod.POST)
    public Map mjda_delete(@RequestBody Map map){
        return bservices.jda_delete(map);
    }
    
    /**
     * Update user journey daily information.
     * @param map HashMap including  user journey daily information
     * @return map HashMap
     */
    @RequestMapping(value = "/mjda/update",method = RequestMethod.POST)
    public Map mjda_update(@RequestBody Map map){
        return bservices.jda_update(map);
    }
    
    /**
     * Add user journey daily detail information.
     * @param map HashMap including user journey daily detail information
     * @return map HashMap
     */
    @RequestMapping(value = "/mjde/add",method = RequestMethod.POST)
    public Map mjde_add(@RequestBody Map map){
        return bservices.jde_add(map);
    }
    
    /**
     * Read user journey daily detail information. 
     * @param map HashMap including user journey daily id
     * @return map HashMap
     */
    @RequestMapping(value = "/mjde/read",method = RequestMethod.POST)
    public Map mjde_read(@RequestBody Map map){
        return bservices.jde_read(map);
    }
    
    /**
     * Delete user journey daily detail information.
     * @param map HashMap including user journey daily detail id
     * @return map HashMap
     */
      @RequestMapping(value = "/mjde/delete",method = RequestMethod.POST)
    public Map mjde_delete(@RequestBody Map map){
        return bservices.jde_delete(map);
    }
    
    /**
     * Update user journey daily detail information. 
     * @param map HashMap including user journey daily detail information
     * @return map HashMap
     */
    @RequestMapping(value = "/mjde/update",method = RequestMethod.POST)
    public Map mjde_update(@RequestBody Map map){
        return bservices.jde_update(map);
    }
    
    /**
     * Add user journey hotel information.
     * @param map HashMap including user journey hotel information
     * @return map HashMap
     */
     @RequestMapping(value = "/mjho/add",method = RequestMethod.POST)
    public Map mjho_add(@RequestBody Map map){
        return bservices.jho_add(map);
    }
    
    /**
     * Read user journey hotel information. 
     * @param map HashMap including user journey id
     * @return map HashMap
     */
    @RequestMapping(value = "/mjho/read",method = RequestMethod.POST)
    public Map mjho_read(@RequestBody Map map){
        return bservices.jho_read(map);
    }
    
    /**
     * Delete user journey hotel information.
     * @param map HashMap user journey hotel id
     * @return map HashMap
     */
      @RequestMapping(value = "/mjho/delete",method = RequestMethod.POST)
    public Map mjho_delete(@RequestBody Map map){
        return bservices.jho_delete(map);
    }
    
    /**
     * Update user journey hotel information.
     * @param map HashMap including user journey hotel information
     * @return map HashMap
     */
    @RequestMapping(value = "/mjho/update",method = RequestMethod.POST)
    public Map mjho_update(@RequestBody Map map){
        return bservices.jho_update(map);
    }
    
    /**
     * Add user journey room information. 
     * @param map HashMap including user journey room information
     * @return map HashMap
     */
      @RequestMapping(value = "/mjro/add",method = RequestMethod.POST)
    public Map mjro_add(@RequestBody Map map){
        return bservices.jro_add(map);
    }
    
    /**
     * Read user journey room information. 
     * @param map HashMap including user journey id
     * @return map HashMap
     */
    @RequestMapping(value = "/mjro/read",method = RequestMethod.POST)
    public Map mjro_read(@RequestBody Map map){
        return bservices.jro_read(map);
    }
    
    /**
     * Delete user journey room information. 
     * @param map HashMap including user journey room id
     * @return map HashMap
     */
      @RequestMapping(value = "/mjro/delete",method = RequestMethod.POST)
    public Map mjro_delete(@RequestBody Map map){
        return bservices.jro_delete(map);
    }
    
    /**
     * Update user journey room information.
     * @param map HashMap including user journey room information
     * @return map HashMap
     */
    @RequestMapping(value = "/mjro/update",method = RequestMethod.POST)
    public Map mjro_update(@RequestBody Map map){
        return bservices.jro_update(map);
    }
    
    /**
     * Add user journey car information. 
     * @param map HashMap including user journey car information
     * @return map HashMap
     */
    @RequestMapping(value = "/mjca/add",method = RequestMethod.POST)
    public Map mjca_add(@RequestBody Map map){
        return bservices.jca_add(map);
    }
    
    /**
     * Read user journey car information.
     * @param map HashMap including user journey id
     * @return map HashMap
     */
    @RequestMapping(value = "/mjca/read",method = RequestMethod.POST)
    public Map mjca_read(@RequestBody Map map){
        return bservices.jca_read(map);
    }
    
    /**
     * Delete user journey car information.
     * @param map HashMap including user journey car id
     * @return map HashMap
     */
      @RequestMapping(value = "/mjca/delete",method = RequestMethod.POST)
    public Map mjca_delete(@RequestBody Map map){
        return bservices.jca_delete(map);
    }
    
    /**
     * Update user journey car information.
     * @param map HashMap including user journey car information
     * @return map HashMap
     */
    @RequestMapping(value = "/mjca/update",method = RequestMethod.POST)
    public Map mjca_update(@RequestBody Map map){
        return bservices.jca_update(map);
    }
    
    /**
     *  Add user journey base information. 
     * @param map HashMap including user journey base information
     * @return  HashMap
     */
    @RequestMapping(value = "/mjoud/add",method = RequestMethod.POST)
    public Map mjoud_add(@RequestBody Map map){
        return bservices.joud_add(map);
    }
    
    /**
     * Read user journey base information. 
     * @param map HashMap including user journey id
     * @return map HashMap
     */
    @RequestMapping(value = "/mjoud/read",method = RequestMethod.POST)
    public Map mjoud_read(@RequestBody Map map){
        return bservices.joud_read(map);
    }
    
    /**
     * Read user journey file.
     * @param map HashMap including user journey id
     * @return map HashMap
     */
    @RequestMapping(value = "/mjfile/read",method = RequestMethod.POST)
    public Map mjfile_read(@RequestBody Map map){
        return bservices.jfile_read(map);
    }

    /**
     * Add logo.
     * @param map HashMap including logo information
     * @return map HashMap
     */
     @RequestMapping(value = "/mlogo/add",method = RequestMethod.POST)
    public Map mlogo_add(@RequestBody Map map){
        return bservices.logo_add(map);
    }
    
    /**
     *  Read logo.
     * @param map HashMap
     * @return  HashMap
     */
    @RequestMapping(value = "/mlogo/read",method = RequestMethod.POST)
    public Map mlogo_read(@RequestBody Map map){
        return bservices.logo_read(map);
    }
    
    /**
     * Delete logo. 
     * @param map HashMap including logo id
     * @return map HashMap 
     */
      @RequestMapping(value = "/mlogo/delete",method = RequestMethod.POST)
    public Map mlogo_delete(@RequestBody Map map){
        return bservices.logo_delete(map);
    }
    
    /**
     * Update logo.
     * @param map HashMap including logo information
     * @return map HashMap
     */
    @RequestMapping(value = "/mlogo/update",method = RequestMethod.POST)
    public Map mlogo_update(@RequestBody Map map){
        return bservices.logo_update(map);
    }
    
    /**
     * Read museum with docent.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mdmu/read",method = RequestMethod.POST)
    public Map mdmu_read(@RequestBody Map map){
        return bservices.dmu_read(map);
    }
    
    /**
     * Read organization.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/morg/read",method = RequestMethod.POST)
    public Map morg_read(@RequestBody Map map){
        return bservices.org_read(map);
    }
    
    /**
     * Add organization.
     * @param map HashMap organization information
     * @return map HashMap
     */
    @RequestMapping(value = "/morg/add",method = RequestMethod.POST)
    public Map morg_add(@RequestBody Map map){
        return bservices.org_add(map);
    }
    
    /**
     * Update organization. 
     * @param map HashMap including organization 
     * @return map
     */
    @RequestMapping(value = "/morg/update",method = RequestMethod.POST)
    public Map morg_update(@RequestBody Map map){
        return bservices.org_update(map);
    }
    
    /**
     * Delete organization.
     * @param map HashMap including organization id
     * @return map HashMap
     */
    @RequestMapping(value = "/morg/delete",method = RequestMethod.POST)
    public Map morg_delete(@RequestBody Map map){
        return bservices.org_delete(map);
    }
    
    /**
     * Read all cities.
     * @param map HashMap 
     * @return map HashMap
     */
    @RequestMapping(value = "/mctall/read",method = RequestMethod.POST)
    public Map mctall_read(@RequestBody Map map){
        return bservices.ctall_read(map);
    }
    
    /**
     * Set city recommend flag.
     * @param map HashMap including city information
     * @return map HashMap
     */
    @RequestMapping(value = "/mctall/post",method = RequestMethod.POST)
    public Map mctall_post(@RequestBody Map map){
        return bservices.ctall_post(map);
    }
    
    /**
     * Read car model. 
     * @param map HashMap 
     * @return map HashMap
     */
    @RequestMapping(value = "/mcarm/read",method = RequestMethod.POST)
    public Map mcarm_read(@RequestBody Map map){
        return bservices.carm_read(map);
    }
    
    /**
     * Add car model. 
     * @param map HashMap including car information
     * @return map HashMap
     */
     @RequestMapping(value = "/mcarm/add",method = RequestMethod.POST)
    public Map mcarm_add(@RequestBody Map map){
        return bservices.carm_add(map);
    }
    
    /**
     * Update car model.
     * @param map HashMap including car information
     * @return map HashMap
     */
     @RequestMapping(value = "/mcarm/update",method = RequestMethod.POST)
    public Map mcarm_update(@RequestBody Map map){
        return bservices.carm_update(map);
    }
    
    /**
     * Delete car model. 
     * @param map HashMap including car id
     * @return map HashMap
     */
     @RequestMapping(value = "/mcarm/delete",method = RequestMethod.POST)
    public Map mcarm_delete(@RequestBody Map map){
        return bservices.carm_delete(map);
    }
    
    /**
     *  Read user journey room infomation.
     * @param map HashMap
     * @return map HashMap
     */
   @RequestMapping(value = "/mjrf/read",method = RequestMethod.POST)
    public Map mjrf_read(@RequestBody Map map){
        return bservices.jrf_read(map);
    }
    
    /**
     * Delete user journey room infomation.
     * @param map HashMap including user journey room id
     * @return map HashMap
     */
      @RequestMapping(value = "/mjrf/delete",method = RequestMethod.POST)
    public Map mjrf_delete(@RequestBody Map map){
        return bservices.jrf_delete(map);
    }
    
    /**
     * Do user journey process.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mjset/post",method = RequestMethod.POST)
    public Map mjset_post(@RequestBody Map map){
        return bservices.jset_post(map);
    }
    
    /**
     * Read docent schedule.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mdoi/read",method = RequestMethod.POST)
    public Map mdoi_read(@RequestBody Map map){
        return bservices.doi_read(map);
    }
    
    /**
     * Add docent schedule.
     * @param map HashMap including docent schedule information
     * @return map HashMap
     */
    @RequestMapping(value = "/mdoi/add",method = RequestMethod.POST)
    public Map mdoi_add(@RequestBody Map map){
        return bservices.doi_add(map);
    }
    
    /**
     * Update docent schedule.
     * @param map HashMap including docent schedule information
     * @return map HashMap
     */
    @RequestMapping(value = "/mdoi/update",method = RequestMethod.POST)
    public Map mdoi_update(@RequestBody Map map){
        return bservices.doi_update(map);
    }
    
    /**
     * Delete docent schedule.
     * @param map HashMap including docent schedule id
     * @return map HashMap
     */
    @RequestMapping(value = "/mdoi/delete",method = RequestMethod.POST)
    public Map mdoi_delete(@RequestBody Map map){
        return bservices.doi_delete(map);
    }
    
    /**
     * Read user docent.
     * @param map HashMap
     * @return map HashMap
     */
     @RequestMapping(value = "/mdod/read",method = RequestMethod.POST)
    public Map mdod_read(@RequestBody Map map){
        return bservices.dod_read(map);
    }
    
    /**
     * Add user docent.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mdod/add",method = RequestMethod.POST)
    public Map mdod_add(@RequestBody Map map){
        return bservices.dod_add(map);
    }
    
    /**
     * Update user docent.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mdod/update",method = RequestMethod.POST)
    public Map mdod_update(@RequestBody Map map){
        return bservices.dod_update(map);
    }
    
    /**
     * Delete user docent.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mdod/delete",method = RequestMethod.POST)
    public Map mdod_delete(@RequestBody Map map){
        return bservices.dod_delete(map);
    }
    
    /**
     * Read docent schedule assign.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mdot/read",method = RequestMethod.POST)
    public Map mdot_read(@RequestBody Map map){
        return bservices.dot_read(map);
    }
    
    /**
     * Add docent schedule assign.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mdot/add",method = RequestMethod.POST)
    public Map mdot_add(@RequestBody Map map){
        return bservices.dot_add(map);
    }
    
    /**
     * Update docent schedule assign.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mdot/update",method = RequestMethod.POST)
    public Map mdot_update(@RequestBody Map map){
        return bservices.dot_update(map);
    }
    
    /**
     * Delete docent schedule assign.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mdot/delete",method = RequestMethod.POST)
    public Map mdot_delete(@RequestBody Map map){
        return bservices.dot_delete(map);
    }
    
    /**
     * Read user docent.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mdodd/read",method = RequestMethod.POST)
    public Map mdodd_read(@RequestBody Map map){
        return bservices.dodd_read(map);
    }
}

