/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.controller;

import com.wwzz.services.api.pingtai.UserServices;
import com.wwzz.services.client.ApplicationClient;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author tiger
 * This class is a user manager controller.
 * This class is concerned with user management ,for example , user add ,read, update and user characteristic etc.
 * 
 */
@RestController
@RequestMapping(value = "/apps/base/user",produces = "application/json;charset=UTF-8")
public class UserDataController {
     @Autowired
    private UserServices bservices;      /**user management operation service. */
    @Autowired
    private ApplicationClient clientApp; /**App initial loading process service. */
    
    /**
     * Add user group.
     * @param map HashMap
     * @return map HashMap
     */
       @RequestMapping(value = "/mugroup/add",method = RequestMethod.POST)
    public Map mug_add(@RequestBody Map map){
        return bservices.ugroup_add(map);
    }
    
    /**
     * Read user group.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mugroup/read",method = RequestMethod.POST)
    public Map mug_read(@RequestBody Map map){
        return bservices.ugroup_read(map);
    }
    
    /**
     * Delete user group.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mugroup/delete",method = RequestMethod.POST)
    public Map mug_delete(@RequestBody Map map){
        return bservices.ugroup_delete(map);
    }
    
    /**
     * Update user group.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mugroup/update",method = RequestMethod.POST)
    public Map mug_update(@RequestBody Map map){
        return bservices.ugroup_update(map);
    }
    
    /**
     * Add user.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/muset/add",method = RequestMethod.POST)
    public Map muset_add(@RequestBody Map map){
        return bservices.uset_add(map);
    }
    
    /**
     * Read user.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/muset/read",method = RequestMethod.POST)
    public Map muset_read(@RequestBody Map map){
        return bservices.uset_read(map);
    }
    
    /**
     * Delete user.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/muset/delete",method = RequestMethod.POST)
    public Map muset_delete(@RequestBody Map map){
        return bservices.uset_delete(map);
    }
  
    /**
     * Update user.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/muset/update",method = RequestMethod.POST)
    public Map muset_update(@RequestBody Map map){
        return bservices.uset_update(map);
    }
    
    /**
     * Add user property.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mupr/add",method = RequestMethod.POST)
    public Map mupr_add(@RequestBody Map map){
        return bservices.upr_add(map);
    }
    
    /**
     * Read user property.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mupr/read",method = RequestMethod.POST)
    public Map mupr_read(@RequestBody Map map){
        return bservices.upr_read(map);
    }

    /**
     * Add user identity.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mudn/add",method = RequestMethod.POST)
    public Map mudn_add(@RequestBody Map map){
        return bservices.udn_add(map);
    }
    
    /**
     * Read user identity.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mudn/read",method = RequestMethod.POST)
    public Map mudn_read(@RequestBody Map map){
        return bservices.udn_read(map);
    }
    
    /**
     * Add user identity op.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mudo/add",method = RequestMethod.POST)
    public Map mudo_add(@RequestBody Map map){
        return bservices.udo_add(map);
    }
    
    /**
     * Read user identity op.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mudo/read",method = RequestMethod.POST)
    public Map mudo_read(@RequestBody Map map){
        return bservices.udo_read(map);
    }
    
    /**
     * Add user identity docent.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mudd/add",method = RequestMethod.POST)
    public Map mudd_add(@RequestBody Map map){
        return bservices.udd_add(map);
    }
    
    /**
     * Read user identity docent.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mudd/read",method = RequestMethod.POST)
    public Map mudd_read(@RequestBody Map map){
        return bservices.udd_read(map);
    }
    
    /**
     * Add user identity local guide.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mudl/add",method = RequestMethod.POST)
    public Map mudl_add(@RequestBody Map map){
        return bservices.udl_add(map);
    }
    
    /**
     * Read user identity local guide.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mudl/read",method = RequestMethod.POST)
    public Map mudl_read(@RequestBody Map map){
        return bservices.udl_read(map);
    }
    
    /**
     * Add user identity long guide.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mudg/add",method = RequestMethod.POST)
    public Map mudg_add(@RequestBody Map map){
        return bservices.udg_add(map);
    }
    
    /**
     * Read user identity long guide.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mudg/read",method = RequestMethod.POST)
    public Map mudg_read(@RequestBody Map map){
        return bservices.udg_read(map);
    }
    
    /**
     * Add user identity overseal guide.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/muds/add",method = RequestMethod.POST)
    public Map muds_add(@RequestBody Map map){
        return bservices.uds_add(map);
    }
    
    /**
     * Read user identity overseal guide.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/muds/read",method = RequestMethod.POST)
    public Map muds_read(@RequestBody Map map){
        return bservices.uds_read(map);
    }
    
    /**
     * Add user identity op file.
     * @param map HashMap
     * @return map HashMap
     */
     @RequestMapping(value = "/mop/add",method = RequestMethod.POST)
    public Map mop_add(@RequestBody Map map){
        return bservices.op_add(map);
    }
    
    /**
     * Read user identity op file.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mop/read",method = RequestMethod.POST)
    public Map mop_read(@RequestBody Map map){
        return bservices.op_read(map);
    }
    
    /**
     * Delete user identity op file.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mop/delete",method = RequestMethod.POST)
    public Map mop_delete(@RequestBody Map map){
        return bservices.op_delete(map);
    }
    
    /**
     * Add user identity docent file.
     * @param map HashMap
     * @return map HashMap
     */
     @RequestMapping(value = "/muddup/add",method = RequestMethod.POST)
    public Map muddup_add(@RequestBody Map map){
        return bservices.uddup_add(map);
    }
    
    /**
     * Read user identity docent file.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/muddup/read",method = RequestMethod.POST)
    public Map muddup_read(@RequestBody Map map){
        return bservices.uddup_read(map);
    }
    
    /**
     * Delete user identity docent file.
     * @param map HashMap
     * @return map HashMap
     */
      @RequestMapping(value = "/muddup/delete",method = RequestMethod.POST)
    public Map muddup_delete(@RequestBody Map map){
        return bservices.uddup_delete(map);
    }
    
    /**
     * Add user identity local guide file.
     * @param map HashMap
     * @return map HashMap
     */
     @RequestMapping(value = "/mudlup/add",method = RequestMethod.POST)
    public Map mudlup_add(@RequestBody Map map){
        return bservices.udlup_add(map);
    }
    
    /**
     * Read user identity local guide file.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mudlup/read",method = RequestMethod.POST)
    public Map mudlup_read(@RequestBody Map map){
        return bservices.udlup_read(map);
    }
    
    /**
     * Delete user identity local guide file.
     * @param map HashMap
     * @return map HashMap
     */
      @RequestMapping(value = "/mudlup/delete",method = RequestMethod.POST)
    public Map mudlup_delete(@RequestBody Map map){
        return bservices.udlup_delete(map);
    }
    
    /**
     * Add user identity long guide 领队证照片 file.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mudgc/add",method = RequestMethod.POST)
    public Map mudgc_add(@RequestBody Map map){
        return bservices.udgc_add(map);
    }
    
    /**
     * Read user identity long guide 领队证照片 file.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mudgc/read",method = RequestMethod.POST)
    public Map mudgc_read(@RequestBody Map map){
        return bservices.udgc_read(map);
    }
    
    /**
     * Delete user identity long guide 领队证照片 file.
     * @param map HashMap
     * @return map HashMap
     */
      @RequestMapping(value = "/mudgc/delete",method = RequestMethod.POST)
    public Map mudgc_delete(@RequestBody Map map){
        return bservices.udgc_delete(map);
    }
    
    /**
     * Add user identity long guide 导游证照片 file.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mudgg/add",method = RequestMethod.POST)
    public Map mudgg_add(@RequestBody Map map){
        return bservices.udgg_add(map);
    }
    
    /**
     * Read user identity long guide 导游证照片 file.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mudgg/read",method = RequestMethod.POST)
    public Map mudgg_read(@RequestBody Map map){
        return bservices.udgg_read(map);
    }
    
    /**
     * Delete user identtiy long guide 导游证照片 file.
     * @param map HashMap
     * @return map HashMap
     */
      @RequestMapping(value = "/mudgg/delete",method = RequestMethod.POST)
    public Map mudgg_delete(@RequestBody Map map){
        return bservices.udgg_delete(map);
    }
    
    /**
     * Add user identity overseal guide file.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mudsup/add",method = RequestMethod.POST)
    public Map mudsup_add(@RequestBody Map map){
        return bservices.udsup_add(map);
    }
    
    /**
     * Read user identity overseal guide file.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mudsup/read",method = RequestMethod.POST)
    public Map mudsup_read(@RequestBody Map map){
        return bservices.udsup_read(map);
    }
    
    /**
     * Delete user identity overseal guide file.
     * @param map HashMap
     * @return map HashMap
     */
      @RequestMapping(value = "/mudsup/delete",method = RequestMethod.POST)
    public Map mudsup_delete(@RequestBody Map map){
        return bservices.udsup_delete(map);
    }
    
    /**
     * Add user identity driver.
     * @param map HashMap
     * @return map HashMap
     */
     @RequestMapping(value = "/mudr/read",method = RequestMethod.POST)
    public Map mudr_read(@RequestBody Map map){
        return bservices.udr_read(map);
    }
    
    /**
     * Read user identity driver.
     * @param map HashMap
     * @return map HashMap
     */
   @RequestMapping(value = "/mudr/add",method = RequestMethod.POST)
    public Map mudr_add(@RequestBody Map map){
        return bservices.udr_add(map);
    }
    
    /**
     * Add user identity driver file.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mudrup/add",method = RequestMethod.POST)
    public Map mudrup_add(@RequestBody Map map){
        return bservices.udrup_add(map);
    }
    
    /**
     * Read user identity driver file.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mudrup/read",method = RequestMethod.POST)
    public Map mudrp_read(@RequestBody Map map){
        return bservices.udrup_read(map);
    }
    
    /**
     * Delete user identity driver file.
     * @param map HashMap
     * @return map HashMap
     */
      @RequestMapping(value = "/mudrup/delete",method = RequestMethod.POST)
    public Map mudrup_delete(@RequestBody Map map){
        return bservices.udrup_delete(map);
    }
    
    /**
     * Read user information(password,salt) with mobile.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/userInfo/read",method = RequestMethod.POST)
    public Map userInfo_read(@RequestBody Map map){
        return bservices.userinfo_read(map);
    }
    
    /**
     * Read user role with mobile.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/roles/read",method = RequestMethod.POST)
    public Map roles_read(@RequestBody Map map){
        Map m=new HashMap();
        m.put("data", clientApp.getRoles());
        return m;
    }
    
    /**
     * Read urls for user mobile.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/url/read",method = RequestMethod.POST)
    public Map url_read(@RequestBody Map map){
        return bservices.url_read(map);
    }
    
    /**
     * Read user skill state. 
     * @param map HashMap
     * @return map HashMap
     */
     @RequestMapping(value = "/must/read",method = RequestMethod.POST)
    public Map must_read(@RequestBody Map map){
        return bservices.ust_read(map);
    }
}
