/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.controller;

import com.wwzz.services.api.pingtai.PartDataServices;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author tiger
 * This class is part management controller that do part process.
 */
@RestController
@RequestMapping(value = "/apps/base/part",produces = "application/json;charset=UTF-8")
public class PartDataController {
    
    @Autowired
    private PartDataServices bservices;
    
    /**
     *  Add staff authorities. 
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mauth/add",method = RequestMethod.POST)
    public Map mauth_add(@RequestBody Map map){
        return bservices.auth_add(map);
    }
    
    /**
     * Read staff authorities. 
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mauth/read",method = RequestMethod.POST)
    public Map mauth_read(@RequestBody Map map){
        return bservices.auth_read(map);
    }
    
    /**
     * Delete staff authorities. 
     * @param map HashMap
     * @return map HashMap
     */
      @RequestMapping(value = "/mauth/delete",method = RequestMethod.POST)
    public Map mauth_delete(@RequestBody Map map){
        return bservices.auth_delete(map);
    }
    
    /**
     * Update staff authorities. 
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mauth/update",method = RequestMethod.POST)
    public Map mauth_update(@RequestBody Map map){
        return bservices.auth_update(map);
    }
    
    /**
     * Read staff authorities.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mauthc/read",method = RequestMethod.POST)
    public Map mauthc_read(@RequestBody Map map){
        return bservices.authc_read(map);
    }
    
    /**
     * Add staff roles. 
     * @param map HashMap
     * @return map HashMap
     */
     @RequestMapping(value = "/mrole/add",method = RequestMethod.POST)
    public Map mrole_add(@RequestBody Map map){
        return bservices.role_add(map);
    }
    
    /**
     * Read staff roles.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mrole/read",method = RequestMethod.POST)
    public Map mrole_read(@RequestBody Map map){
        return bservices.role_read(map);
    }
    
    
    /**
     * Delete staff roles.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mrole/delete",method = RequestMethod.POST)
    public Map mrole_delete(@RequestBody Map map){
        return bservices.role_delete(map);
    }
    
    /**
     * Update staff roles. 
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mrole/update",method = RequestMethod.POST)
    public Map mrole_update(@RequestBody Map map){
        return bservices.role_update(map);
    }
    
    /**
     * Read staff roles. 
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mrolec/read",method = RequestMethod.POST)
    public Map mrolec_read(@RequestBody Map map){
        return bservices.rolec_read(map);
    }
    
    /**
     * Add staff department. 
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mdev/add",method = RequestMethod.POST)
    public Map mdev_add(@RequestBody Map map){
         Map model=(Map) map.get("data");
        String id=(String) model.get("id");
          if(id==null || id.contains("-")){
            return bservices.dev_add(map);
        }else{
            if(model.keySet().size()==2){
                return bservices.dev_delete(id);
            }else{
                return bservices.dev_update(map);
            }
        }
    }

    /**
     * Read staff department.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/mdev/read",method = RequestMethod.POST)
    public Map mdev_read(@RequestBody Map map){
        return bservices.dev_read(map);
    }
    
    /**
     * Add staff. 
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/msta/add",method = RequestMethod.POST)
    public Map msta_add(@RequestBody Map map){
        return bservices.sta_add(map);
    }
    
    /**
     * Read staff. 
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/msta/read",method = RequestMethod.POST)
    public Map msta_read(@RequestBody Map map){
        return bservices.sta_read(map);
    }
    
    /**
     * Delete staff. 
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/msta/delete",method = RequestMethod.POST)
    public Map msta_delete(@RequestBody Map map){
        return bservices.sta_delete(map);
    }
    
    /**
     * Update staff. 
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/msta/update",method = RequestMethod.POST)
    public Map msta_update(@RequestBody Map map){
        return bservices.sta_update(map);
    }
}
