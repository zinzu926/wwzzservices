/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.controller;

import com.wwzz.services.domain.booking.TUserBookingOrder;
import com.wwzz.services.domain.users.TUser;
import com.wwzz.services.impl.ResultDataServices;
import com.wwzz.services.repository.TUserBookingOrderRepository;
import com.wwzz.weixin.newpay.util.CommonUtil;
import com.wwzz.weixin.newpay.util.Sha1Util;
import com.wwzz.weixin.newpay.util.XMLUtil;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author JBOSS
 */
@RestController
@RequestMapping(value = "/weixin")
public class WeixinController extends ResultDataServices{

    @Autowired
    private TUserBookingOrderRepository bookRepo;
    
    @RequestMapping("/wxpay_info")
    public Map wxPayMainPage(@RequestBody Map params, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String oo=(String) params.get("bbid");
        if(!hasText(oo) || !NumberUtils.isNumber(oo))return raiseError(1032);
        TUserBookingOrder booking=bookRepo.findOne(Long.parseLong(oo));
        if(booking==null)return raiseError(1032);
        TUser user=booking.getExecutor();
        if(user==null)return raiseError(1032);
        String openId = user.getOpenid();//"o6LJis3NaUuOa-eKZmKWFLQ-SXnQ";//
        Map rm=createResult(RETURN_SUCCESS_CODE, "", ERROR_CODE_OK, "");
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + path + "/";
        if (null != openId && !openId.equals("")) {
            String PartnerId , PartnerKey , AppID, AppSecret , merchantPublicCert;
                //判断此支付方式是否存在  
            //查询证书信息是否存在 
            PartnerId = "1247425201";//财付通商户身份标识
            PartnerKey = "l4lv52c369cp2kl1a4pseojzw5cae1y9";//财付通商户权限密钥 Key
            AppID = "wx41d260ce24717605";//公众号身份标识
            AppSecret = "48ecd8d847234dc150646d0a6221d037";//公众平台 API的权限获取所需密钥 Key
            //merchantPublicCert = systemPayment.getMerchantPublicCert();//公众平台p12证书文件
            if (!PartnerId.equals("") && !PartnerKey.equals("") && !AppID.equals("") && !AppSecret.equals("")) {
                System.out.println("**********************************************************************************************");
                //获取请求者，调试信息。
                String ip = request.getHeader("x-forwarded-for");
                if (ip == null || ip.length() == 0) {
                    ip = request.getRemoteAddr();
                }
                System.out.println("请求IP：" + ip);
                System.out.println("请求时间：" + new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(Calendar.getInstance().getTime()));

                System.out.println("获取用户openID:" + openId);
                System.out.println("财付通商户身份标识：" + PartnerId);
                System.out.println("财付通商户权限密钥 Key：" + PartnerKey);
                System.out.println("公众号身份标识：" + AppID);
                System.out.println("公众平台 API的权限获取所需密钥：" + AppSecret);
                System.out.println("**********************************************************************************************");
                 //jsapi接口
                //初始化
                //调试模式
                boolean DEBUG_ = true;
                String NOTIFY_URL = basePath + "marketNew_wxpay/payNotifyUrl/";// + paymentId;  //支付完成后的回调处理页面,*替换成notify_url.asp所在路径
                //初始化 
                String noncestr = Sha1Util.getNonceStr();
                String timestamp = Sha1Util.getTimeStamp();
                // 设置预支付参数
                SortedMap<String, String> signParams = new TreeMap<>();
                signParams.put("appid", AppID);
                signParams.put("body", "Test Pay"); // 描述
                signParams.put("notify_url", NOTIFY_URL); // 通知地址
                signParams.put("out_trade_no", "LG-"+Long.toString(System.currentTimeMillis())); // 商户订单号
                signParams.put("mch_id", PartnerId); // 设置商户号
                signParams.put("total_fee", "10"); // 商品总金额,以分为单位
                signParams.put("spbill_create_ip", request.getRemoteAddr()); // 订单生成机器IP，指用户浏览器端IP
                signParams.put("trade_type", "JSAPI");
                signParams.put("nonce_str", noncestr);
                signParams.put("openid", openId);
                String sign = "";
                try {
                    sign = Sha1Util.createSign(signParams, PartnerKey);//这里需要注意的是：生成签名的sign并不需要参与签名，但是请求参数里面是必填的参数。
                } catch (Exception e) {
                }
                // 增加非参与签名的额外参数
                System.out.println("sign:"+sign);
                signParams.put("sign", sign);
                System.out.println("signMap:"+signParams);
                String UNIFIED_ORDER_URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
                String requestXML = XMLUtil.getRequestXml(signParams);
                System.out.println("requestXML:"+requestXML);
                //最后我们要将这些参数以POST方式调用微信统一支付接口：代码如下
                String result = CommonUtil.getPrePayId(UNIFIED_ORDER_URL, requestXML);
                System.out.println("request:"+result);
                Map<Object, Object> map = new HashMap<>();
                map = XMLUtil.doXMLParse(result);
                if (map.get("return_code").toString().equals("SUCCESS")) {
                    String prePayId = map.get("prepay_id").toString();// 预支付订单号
                    System.out.println("prePayId:" + prePayId);
                    // 设置package参数  // 
                    SortedMap<String, String> paySignpackageParams = new TreeMap<>();
                    paySignpackageParams.put("appId", AppID);
                    paySignpackageParams.put("timeStamp", timestamp);
                    paySignpackageParams.put("nonceStr", noncestr);
                    paySignpackageParams.put("package", "prepay_id=" + prePayId);
                    paySignpackageParams.put("signType", "MD5");
                    String paySign = "";
                    try {
                        paySign = Sha1Util.createSign(paySignpackageParams, PartnerKey);
                    } catch (Exception e) {
                    }
                    rm.put("APP_ID", AppID);
                    rm.put("timestamp", timestamp);
                    rm.put("noncestr", noncestr);
                    rm.put("packageValue", "prepay_id=" + prePayId + "");
                    rm.put("paySign", paySign);
                    rm.put("showwxpaytitle", 1);//支付安全提示文字
                    return rm;
                } else {
                    return raiseError(1032);
                }
            } 
        } else {
            return raiseError(1032);
        }
        return raiseError(1032);
    }
    
    @RequestMapping("/wxpay_notify/{uid}/{partner}/{bid}}")
    public Map weixin_pay_notify(@PathVariable("uid") String uid,@PathVariable("partner")String pt,
            @PathVariable("bid")String bid,HttpServletRequest request, HttpServletResponse response) throws Exception {
        return null;
    }
}
