/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.controller;

import com.wwzz.services.api.ChatManager;
import com.wwzz.services.api.DataViewServices;
import com.wwzz.services.impl.MyTaskServices;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author JBOSS
 */
@RestController
@RequestMapping(value = "/mydata",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
public class DataViewController {
    @Autowired
    private DataViewServices services;
    @Autowired
    private MyTaskServices taskServices;
    @Autowired
    private ChatManager chatServices;
    
    @RequestMapping(value = "/booking/list",method = RequestMethod.POST)
    public Map view_my_booking_list(@RequestBody Map params){
        return services.view_my_booking_list(params);
    }
    
    @RequestMapping(value = "/bids/list",method = RequestMethod.POST)
    public Map view_my_bids_list(@RequestBody Map params){
        return services.view_my_bids_list(params);
    }
    
    @RequestMapping(value = "/booking/detail",method = RequestMethod.POST)
    public Map view_my_booking_detail(@RequestBody Map params){
        return services.view_my_booking_data_cat1(params);
    }
    
    @RequestMapping(value = "/bids/info",method = RequestMethod.POST)
    public Map view_my_bid_detail(@RequestBody Map params){
        return services.view_my_booking_target(params);
    }
    
    @RequestMapping(value = "/bidded/info",method = RequestMethod.POST)
    public Map view_my_bidded_detail(@RequestBody Map params){
        return services.view_my_bid_info(params);
    }
    
    @RequestMapping(value = "/jrn/mylist",method = RequestMethod.POST)
    public Map view_my_journey_list(@RequestBody Map params){
        return services.view_my_journey_list(params);
    }
    
    @RequestMapping(value = "/booking/data/detail",method = RequestMethod.POST)
    public Map view_my_booking_data(@RequestBody Map params){
        return services.view_booking_detail(params);
    }
    
    @RequestMapping(value = "/booking/search/items",method = RequestMethod.POST)
    public Map view_my_booking_search_items(@RequestBody Map params){
        return services.view_booking_items(params);
    }
    
    @RequestMapping(value = "/bider/user",method = RequestMethod.POST)
    public Map view_my_bider_info(@RequestBody Map params){
        return services.view_bider_skill(params);
    }
    
    @RequestMapping(value = "/my/task",method = RequestMethod.POST)
    public Map view_my_task(@RequestBody Map params){
        return taskServices.view_my_task(params);
    }
    @RequestMapping(value = "/my/task/detail",method = RequestMethod.POST)
    public Map view_my_task_detail(@RequestBody Map params){
        return taskServices.view_my_task_detail(params);
    }   
    
    @RequestMapping(value = "/weather",method = RequestMethod.POST)
    public Map view_weather(@RequestBody Map params){
        return services.view_weather(params);
    }   
    
    @RequestMapping(value = "/weixin/bid/state",method = RequestMethod.POST)
    public Map view_weixin_bid_state(@RequestBody Map params){
        return services.view_weixin_bid_state(params);
    }
    
    @RequestMapping(value = "/jrn/hinfo",method = RequestMethod.POST)
    public Map view_journey_hotel_info(@RequestBody Map params){
        return services.view_journey_hotel(params);
    }
    
    @RequestMapping(value = "/prebids",method = RequestMethod.POST)
    public Map view_bids_will(@RequestBody Map params){
        return services.view_bid_will(params);
    }
    
    @RequestMapping(value = "/add/chat",method = RequestMethod.POST)
    public Map add_chat_message(@RequestBody Map params){
        return chatServices.add_chat_message(params);
    }
    
    @RequestMapping(value = "/view/chat",method = RequestMethod.POST)
    public Map view_chat_history(@RequestBody Map params){
        return chatServices.view_chat_history(params);
    }
    
    @RequestMapping(value = "/alipay/info",method = RequestMethod.POST)
    public Map view_alipay_info(@RequestBody Map params){
        return services.view_alipay_info(params);
    }
    
    @RequestMapping(value = "/update/chat",method = RequestMethod.POST)
    public void update_chat_state(@RequestBody Map params){
        chatServices.update_chat_state(params);
    }
    
    @RequestMapping(value = "/view/bills/list",method = RequestMethod.POST)
    public Map view_bills_list(@RequestBody Map params){
        return services.view_user_bill_list(params);
    }
    
    @RequestMapping(value = "/view/bills/info",method = RequestMethod.POST)
    public Map view_bills_info(@RequestBody Map params){
        return services.view_user_bill_info(params);
    }
    
}
