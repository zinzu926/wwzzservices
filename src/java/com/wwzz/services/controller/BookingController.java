/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.controller;

import com.wwzz.services.api.BookingServices;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author JBOSS
 */
@RestController
@RequestMapping(value = "/booking",produces = "application/json;charset=UTF-8",method = RequestMethod.POST)
public class BookingController {
    @Autowired
    private BookingServices services;
    
    @RequestMapping(value = "/view/cities",method = RequestMethod.POST)
    public Map view_all_city(@RequestBody Map params){
        return services.view_all_city(params);
    }
    
    @RequestMapping(value = "/view/recom/cities",method = RequestMethod.POST)
    public Map view_recommended_city(@RequestBody Map params){
        return services.view_city_recommended(params);
    }
    
    @RequestMapping(value = "/view/recom/objs",method = RequestMethod.POST)
    public Map view_recommended_object(@RequestBody Map params){
        return services.view_recommended_object_list(params);
    }
    
    @RequestMapping(value = "/view/countries",method = RequestMethod.POST)
    public Map view_country_booking(@RequestBody Map params){
        return services.view_country_booking();
    }
    
    @RequestMapping(value = "/view/cards",method = RequestMethod.POST)
    public Map view_shop_card(@RequestBody Map params){
        return services.view_shop_cards(params);
    }
    
    @RequestMapping(value = "/find/objs",method = RequestMethod.POST)
    public Map search_by_city(@RequestBody Map params){
        return services.search_object_by_city(params);
    }
    
    @RequestMapping(value = "/shop/add",method = RequestMethod.POST)
    public Map add_shop_order(@RequestBody Map params){
        return services.booking_order_shop(params);
    }
    
    @RequestMapping(value = "/card/add",method = RequestMethod.POST)
    public Map add_user_card(@RequestBody Map params){
        return services.add_shop_card(params);
    }
    
    @RequestMapping(value = "/card/rms",method = RequestMethod.POST)
    public Map remove_user_card(@RequestBody Map params){
        return services.remove_shop_card(params);
    }
    
    @RequestMapping(value = "/view/recom/countries",method = RequestMethod.POST)
    public Map view_recommended_country(@RequestBody Map params){
        return services.view_country_recommended(params);
    }
    
    @RequestMapping(value = "/restaurant/add",method = RequestMethod.POST)
    public Map add_restaurant_booking(@RequestBody Map params){
        return services.booking_order_restaurant(params);
    }
    
    @RequestMapping(value = "/view/rstpkg",method = RequestMethod.POST)
    public Map view_restaurant_pkg(@RequestBody Map params){
        return services.view_restaurant_set(params);
    }
    
    @RequestMapping(value = "/view/rstrecord",method = RequestMethod.POST)
    public Map view_restaurant_record(@RequestBody Map params){
        return services.view_restaurant_record(params);
    }
    
    @RequestMapping(value = "/ssticket/add",method = RequestMethod.POST)
    public Map add_ticket_booking(@RequestBody Map params){
        return services.booking_order_ticket(params);
    }
    
    @RequestMapping(value = "/view/mycards",method = RequestMethod.POST)
    public Map view_shop_my_card(@RequestBody Map params){
        return services.view_user_cards(params);
    }
    
    @RequestMapping(value = "/view/shops",method = RequestMethod.POST)
    public Map view_shop_by_city(@RequestBody Map params){
        return services.view_shop_by_city(params);
    }
    
    @RequestMapping(value = "/shop/order/files/add",method = RequestMethod.POST)
    public Map add_shop_order_file(@RequestBody Map params){
        return services.add_shop_order_file(params);
    }
    
    @RequestMapping(value = "/shop/order/files/rms",method = RequestMethod.POST)
    public Map remove_shop_order_file(@RequestBody Map params){
        return services.remove_shop_order_file(params);
    }
    
    @RequestMapping(value = "/conversation",method = RequestMethod.POST)
    public Map generate_conversation(@RequestBody Map params){
        return services.generate_conversion_id(params);
    }
    
    @RequestMapping(value = "/items/detail",method = RequestMethod.POST)
    public Map view_items_detail(@RequestBody Map params){
        return services.view_search_items(params);
    }
    
    @RequestMapping(value = "/vws/ss/schedule",method = RequestMethod.POST)
    public Map view_ss_docents_schedule(@RequestBody Map params){
        return services.view_ss_docents_schedule(params);
    }
    
    @RequestMapping(value = "/ss/docents/add",method = RequestMethod.POST)
    public Map booking_ss_docents(@RequestBody Map params){
        return services.booking_order_docents(params);
    }
    
    @RequestMapping(value = "/vws/card/detail",method = RequestMethod.POST)
    public Map view_card_details(@RequestBody Map params){
        return services.view_shop_card_detail(params);
    }
    
    @RequestMapping(value = "/guide/lg/add",method = RequestMethod.POST)
    public Map booking_add_lg(@RequestBody Map params){
        return services.booking_lg_order(params);
    }
    
    @RequestMapping(value = "/lg/order/files/add",method = RequestMethod.POST)
    public Map add_lg_order_file(@RequestBody Map params){
        return services.add_lg_order_file(params);
    }
    
    @RequestMapping(value = "/lg/order/files/rms",method = RequestMethod.POST)
    public Map remove_lg_order_file(@RequestBody Map params){
        return services.remove_lg_order_file(params);
    }
    
    @RequestMapping(value = "/guide/og/add",method = RequestMethod.POST)
    public Map booking_add_og(@RequestBody Map params){
        return services.booking_og_order(params);
    }
    
    @RequestMapping(value = "/og/order/files/add",method = RequestMethod.POST)
    public Map add_og_order_file(@RequestBody Map params){
        return services.add_og_order_file(params);
    }
    
    @RequestMapping(value = "/og/order/files/rms",method = RequestMethod.POST)
    public Map remove_og_order_file(@RequestBody Map params){
        return services.remove_og_order_file(params);
    }
    
    @RequestMapping(value = "/lc/order/add",method = RequestMethod.POST)
    public Map booking_add_lc(@RequestBody Map params){
        return services.booking_lc_order(params);
    }
    
    @RequestMapping(value = "/drv/order/files/add",method = RequestMethod.POST)
    public Map add_drv_order_file(@RequestBody Map params){
        return services.add_driver_order_file(params);
    }
    
    @RequestMapping(value = "/drv/order/files/rms",method = RequestMethod.POST)
    public Map remove_drv_order_file(@RequestBody Map params){
        return services.remove_driver_order_file(params);
    }
    
    @RequestMapping(value = "/drv/order/add",method = RequestMethod.POST)
    public Map booking_add_drv(@RequestBody Map params){
        return services.booking_driver_order(params);
    }
    
    @RequestMapping(value = "/view/data",method = RequestMethod.POST)
    public Map view_booking_notice(@RequestBody Map params){
        return services.view_my_booking_notice(params);
    }
    
    @RequestMapping(value = "/add/bid",method = RequestMethod.POST)
    public Map bid_booking(@RequestBody Map params){
        return services.add_bidding(params);
    }
    
    @RequestMapping(value = "/cancel",method = RequestMethod.POST)
    public Map cancel_booking(@RequestBody Map params){
        return services.cancel_my_booking(params);
    }
    
    @RequestMapping(value = "/confirm/guide/bider",method = RequestMethod.POST)
    public Map confirm_guide_bidder(@RequestBody Map params){
        return services.confirm_guide_bidder(params);
    }
    
    @RequestMapping(value = "/join/journey",method = RequestMethod.POST)
    public Map join_with_journey(@RequestBody Map params){
        return services.combine_with_journey(params);
    }
    
    @RequestMapping(value = "/pcs",method = RequestMethod.POST)
    public Map booking_pcs(@RequestBody Map params){
        return services.booking_process_charge(params);
    }
    
    @RequestMapping(value = "/view/wxpay",method = RequestMethod.POST)
    public Map view_wxpay_info(@RequestBody Map params){
        return services.get_weixin_payinfo_booking(params);
    }
    
    @RequestMapping(value = "/view/wxpay/apps",method = RequestMethod.POST)
    public Map view_wxpay_info_for_app(@RequestBody Map params){
        return services.get_weixin_payinfo_for_app(params);
    }
}
