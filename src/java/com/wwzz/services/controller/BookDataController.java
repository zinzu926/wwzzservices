/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wwzz.services.controller;

import com.wwzz.services.api.pingtai.BookServices;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author tiger
 * This class is a booking data management controller that
 * do process booking data 
 */
@RestController
@RequestMapping(value = "/apps/base/book",produces = "application/json;charset=UTF-8")
public class BookDataController {
    
    @Autowired
    private BookServices bkservices;
    
    /**
     * Read booking data.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/book/read",method = RequestMethod.POST)
    public Map book_read(@RequestBody Map map){
        return bkservices.book_read(map);
    }
    
    /**
     * Read booking data detail information.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/dbook/read",method = RequestMethod.POST)
    public Map dbook_read(@RequestBody Map map){
        return bkservices.dbook_read(map);
    }
    
    /**
     * Read booking temp data.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/booktmp/read",method = RequestMethod.POST)
    public Map booktmp_read(@RequestBody Map map){
        return bkservices.booktmp_read(map);
    }
    
    /**
     * Read booking message.
     * @param map HashMap
     * @return map HashMap
     */
    @RequestMapping(value = "/ant/read",method = RequestMethod.POST)
    public Map ant_read(@RequestBody Map map){
        return bkservices.ant_read(map);
    }
}
